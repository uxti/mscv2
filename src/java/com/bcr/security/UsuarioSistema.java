/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.security;

import com.bcr.model.Usuario;
import java.util.Collection;
import java.util.logging.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 *
 * @author Renato
 */
public class UsuarioSistema extends User {
    private static final long serialVersionUID = 1L;
    private Usuario usuario;

    public UsuarioSistema(Usuario username,  Collection<? extends GrantedAuthority> authorities) {
        super(username.getLogin(), username.getSenha(), authorities);
        this.usuario = username;
    }

    public Usuario getUsuario() {
        return usuario;
    }

 
   
    
    
}
