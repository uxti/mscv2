/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.CfopEJB;
import com.bcr.model.Cfop;
import com.bcr.util.MensageFactory;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Charles
 */
@ManagedBean
@ViewScoped
public class CfopMB {
    
    @EJB
    CfopEJB cfopEJB;
    private Cfop cfop;

    public CfopMB() {
        cfop = new Cfop();
    }
    
    public void salvar() {
        if (cfop.getIdCfop() == null) {
            try {
                cfopEJB.Salvar(cfop);
                cfop = new Cfop();
                MensageFactory.addMensagemPadraoSucesso("salvar");
            } catch (Exception e) {
                MensageFactory.addMensagemPadraoErro("salvar");
                System.out.println("Erro ocorrido: " + e);
            }
        } else {
            try {
                cfopEJB.Atualizar(cfop);
                cfop = new Cfop();
                MensageFactory.addMensagemPadraoSucesso("editar");
            } catch (Exception e) {
                MensageFactory.addMensagemPadraoErro("editar");
                System.out.println("Erro ocorrido: " + e);
            }
        }
    }
    
    public List<Cfop> listarCfops() {
        return cfopEJB.ListarTodos();
    }

    public void excluir() {
        try {
            cfopEJB.Excluir(cfop, cfop.getIdCfop());
                MensageFactory.addMensagemPadraoSucesso("excluir");
        } catch (Exception e) {
            MensageFactory.addMensagemPadraoErro("excluir");
            System.out.println("Erro ocorrido é: " + e);
        }
    }
}
