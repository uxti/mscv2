/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.BancoEJB;
import com.bcr.controller.CartaoEJB;
import com.bcr.controller.EmpresaEJB;
import com.bcr.model.Banco;
import com.bcr.model.Cartao;
import com.bcr.model.Empresa;
import com.bcr.util.MensageFactory;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author JOAOPAULO
 */
@ManagedBean
@ViewScoped
public class CartaoMB implements Serializable{

    /**
     * Creates a new instance of CartaoMB
     */
    @EJB
    CartaoEJB cEJB;
    private Cartao cartao;
    @EJB
    EmpresaEJB eEJB;
    private Empresa empresa;
    @EJB
    BancoEJB bEJB;
    private Banco banco;

    public CartaoMB() {
        novo();
    }

    public void novo() {
        cartao = new Cartao();
        banco = new Banco();
        empresa = new Empresa();
    }

    public Cartao getCartao() {
        try {
            if(cartao.getIdCartao() != null){
                banco = cartao.getIdBanco();
            }
        } catch (Exception e) {
        }
        return cartao;
    }

    public void setCartao(Cartao cartao) {
        this.cartao = cartao;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }
    
    public void salvar() {
        HttpServletRequest request = (HttpServletRequest) FacesContext
                .getCurrentInstance().getExternalContext().getRequest();
        empresa = eEJB.SelecionarPorID(Integer.parseInt(request.getParameter("id_empresa")));
        cartao.setIdEmpresa(empresa);
        if(banco != null){
            cartao.setIdBanco(banco);
        }
        if (cartao.getIdCartao() == null) {
            try {
                cEJB.Salvar(cartao);
                novo();
                MensageFactory.info("Salvo com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar salvar!", null);
                System.out.println("Erro identificado: " + e);
            }
        } else {
            try {
                cEJB.Atualizar(cartao);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar atualizar!", null);
                System.out.println("Erro identificado: " + e);
            }
        }
    }
    
    public void excluir(){
        try {
            cEJB.Excluir(cartao, cartao.getIdCartao());
            MensageFactory.info("Cliente excluído com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.warn("Não foi possível excluir o cartão!", null);
            System.out.println("O erro ocorrido é: " + e);
        }
    }
    public List<Cartao> listarCartoes(){
        return cEJB.ListarTodos();
    }
}
