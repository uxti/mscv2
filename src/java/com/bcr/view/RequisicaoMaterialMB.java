/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.ProdutoEJB;
import com.bcr.controller.RequisicaoMaterialEJB;
import com.bcr.controller.RequisicaoMaterialItemEJB;
import com.bcr.controller.UsuarioEJB;
import com.bcr.model.Cliente;
import com.bcr.model.Cotacao;
import com.bcr.model.CotacaoItem;
import com.bcr.model.Empresa;
import com.bcr.model.LocalTrabalho;
import com.bcr.model.Produto;
import com.bcr.util.MensageFactory;
import com.bcr.model.RequisicaoMaterial;
import com.bcr.model.RequisicaoMaterialItem;
import com.bcr.model.Usuario;
import com.bcr.util.Bcrutils;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Charles
 */
@ManagedBean
@ViewScoped
public class RequisicaoMaterialMB implements Serializable {

    /* EJB's */
    @EJB
    RequisicaoMaterialEJB requisicaoMaterialEJB;
    @EJB
    UsuarioEJB usuarioEJB;
    @EJB
    RequisicaoMaterialItemEJB requisicaoMaterialItemEJB;
    @EJB
    ProdutoEJB pEJB;
    /* Objetos */
    private RequisicaoMaterial requisicaoMaterial;
    private RequisicaoMaterial requisicaoMaterialReferente;
    private RequisicaoMaterialItem requisicaoMaterialItem;
    private Usuario usuario;
    private LocalTrabalho localTrabalho;
    private Empresa empresa;
    private Cliente cliente;
    /* Listas */
    private List<RequisicaoMaterial> listaRequisicaoMaterial;
    private List<RequisicaoMaterial> listaRequisicaoMaterialSelecionadas;
    private List<LocalTrabalho> listaLocalTrabalho;
    private List<RequisicaoMaterialItem> listaRequisicaoMaterialItems;
    private List<Cliente> listaClientes;
    private List<Produto> listaProdutos;
    /* Variáveis temporárias */
    private String lote;
    private BigDecimal quantidade, valorItem, valorTotalItem, valorTotalItensRequisicao;
    private Integer quantidadeTotalItensRequisicao;
    List<RequisicaoMaterial> lista = new ArrayList<RequisicaoMaterial>();
    private LazyDataModel<RequisicaoMaterial> model;
    private Cotacao cotacao;

    /* Início metodos padrões */
    public RequisicaoMaterialMB() {
        novo();
    }

    public void novo() {
        try {
            lista = new ArrayList<RequisicaoMaterial>();
            requisicaoMaterial = new RequisicaoMaterial();
            requisicaoMaterialReferente = new RequisicaoMaterial();
            requisicaoMaterialItem = new RequisicaoMaterialItem();
            usuario = new Usuario();
            localTrabalho = new LocalTrabalho();
            empresa = new Empresa();
            cliente = new Cliente();
            listaRequisicaoMaterial = new ArrayList<RequisicaoMaterial>();
            listaRequisicaoMaterialSelecionadas = new ArrayList<RequisicaoMaterial>();
            listaProdutos = new ArrayList<Produto>();
            listaLocalTrabalho = new ArrayList<LocalTrabalho>();
            listaRequisicaoMaterialItems = new ArrayList<RequisicaoMaterialItem>();
            listaClientes = new ArrayList<Cliente>();
            quantidade = new BigDecimal(BigInteger.ONE);
            valorItem = new BigDecimal(BigInteger.ZERO);
            valorTotalItem = new BigDecimal(BigInteger.ZERO);
            quantidadeTotalItensRequisicao = 0;
            requisicaoMaterial.setDtCadastro(new Date());
            cotacao = new Cotacao();
            carregarRequisicoes();
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void carregarRequisicoes() {
        model = new LazyDataModel<RequisicaoMaterial>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<RequisicaoMaterial> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and p." + filterProperty + " like'%" + filterValue + "%'");
                        } else {
                            sf.append(" where p." + filterProperty + " like'%" + filterValue + "%'");
                        }
                    } else {
                        sf.append(" and p." + filterProperty + " like'%" + filterValue + "%'");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " desc");
                }
                Clausula = sf.toString();

                if (Clausula.contains("_")) {
                    Clausula = Clausula.replace("_", "");
                }
                if (Clausula.contains("() -")) {
                    Clausula = Clausula.replace("() -", "");
                }
                if (Clausula.contains("..")) {
                    Clausula = Clausula.replace("..", "");
                }

                System.out.println(Clausula);
                setRowCount(Integer.parseInt(requisicaoMaterialEJB.contarRegistro(Clausula).toString()));
                lista = requisicaoMaterialEJB.listarRequisicaoMaterialLazyModeWhere(first, pageSize, Clausula);
                return lista;
            }
        };
    }

    public void novaRequisicao() {
        novo();
        requisicaoMaterial.setDtCadastro(new Date());
        requisicaoMaterial.setStatus("Emitido");
        requisicaoMaterial.setTipo("E");
        requisicaoMaterial.setValor(BigDecimal.ZERO);
    }

    public void novaItemRequisicao() {
        requisicaoMaterialItem = new RequisicaoMaterialItem();
        valorTotalItem = new BigDecimal(BigInteger.ZERO);
    }

    public void Salvar() {
        usuario = usuarioEJB.pegaUsuarioLogadoNaSessao();
        requisicaoMaterial.setIdUsuario(usuario);
        requisicaoMaterial.setIdEmpresa(usuario.getIdEmpresa());
        if (requisicaoMaterial.getIdRequisicaoMaterial() == null) {
            try {
                requisicaoMaterial.setStatus("Emitido");
                requisicaoMaterial.setTipo("S");
                requisicaoMaterialEJB.SalvarRequisicao(requisicaoMaterial, listaRequisicaoMaterialItems);
                MensageFactory.addMensagemPadraoSucesso("salvar");
            } catch (Exception e) {
                MensageFactory.addMensagemPadraoErro("salvar");
                System.out.println(e);
            }
        } else {
            try {
                requisicaoMaterialEJB.SalvarRequisicao(requisicaoMaterial, listaRequisicaoMaterialItems);
                MensageFactory.addMensagemPadraoSucesso("editar");
            } catch (Exception e) {
                MensageFactory.addMensagemPadraoErro("editar");
                System.out.println(e);
            }
        }
        carregarRequisicoes();
    }

    public List<RequisicaoMaterial> listarRequisicoes() {
        return requisicaoMaterialEJB.listarRequisaoMateriais();
    }

    public void Excluir() {
        if (requisicaoMaterial.getIdRequisicaoMaterial() != null) {
            try {
                requisicaoMaterialEJB.Excluir(requisicaoMaterial, requisicaoMaterial.getIdRequisicaoMaterial());
                MensageFactory.addMensagemPadraoSucesso("excluir");
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar excluir. Verifique o log!", null);
                Bcrutils.escreveLogErro(e);
            }
        }
    }

    public void excluirItem() {
        try {
            int indice = 0;
            List<RequisicaoMaterialItem> listaTemporaria = new ArrayList<RequisicaoMaterialItem>();
            listaTemporaria = listaRequisicaoMaterialItems;
            for (RequisicaoMaterialItem rmi : listaTemporaria) {
                if (rmi == requisicaoMaterialItem) {
                    listaRequisicaoMaterialItems.remove(indice);
                    break;
                } else {
                    indice++;
                }
            }
            calcularValoresListaRequisicao();
            MensageFactory.info("Item Removido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.info("Não foi possível excluir. Tente novamente.", null);
            System.out.println(e);
        }
    }

    public void adicionarItem() {
        try {
            requisicaoMaterialItem.setDescricao(requisicaoMaterialItem.getIdProduto().getDescricao());
            requisicaoMaterialItem.setStatus("Aguardando");
            listaRequisicaoMaterialItems.add(requisicaoMaterialItem);
            requisicaoMaterialItem = new RequisicaoMaterialItem();
            requisicaoMaterial.setRequisicaoMaterialItemList(listaRequisicaoMaterialItems);
            calcularValoresListaRequisicao();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void removerItemRequisicaoMaterial(int index) {
        try {
            requisicaoMaterial.getRequisicaoMaterialItemList().remove(index);
            MensageFactory.info("Item removido!", "Item removido!");
        } catch (Exception e) {
            MensageFactory.info("Não foi possível remover!", "Não foi possível remover!");
            Bcrutils.escreveLogErro(e);
        }
    }

    public void teste() {
        System.out.println("oiiiiiii");
//        requisicaoMaterialItem.setDescricao(requisicaoMaterialItem.getIdProduto().getDescricao());
//        System.out.println(requisicaoMaterialItem.getDescricao());
    }

    public String calcularValoresListaRequisicao() {
        valorTotalItensRequisicao = new BigDecimal(BigInteger.ZERO);
        quantidadeTotalItensRequisicao = 0;
        for (RequisicaoMaterialItem rmi : listaRequisicaoMaterialItems) {
            BigDecimal valorTotalRequisicao = new BigDecimal(BigInteger.ZERO);
            valorTotalRequisicao = rmi.getVlrUnitario().multiply(BigDecimal.valueOf(rmi.getQuantidade().longValue()));
            valorTotalItensRequisicao = valorTotalItensRequisicao.add(valorTotalRequisicao);
            quantidadeTotalItensRequisicao++;
        }
        requisicaoMaterial.setValor(valorTotalItensRequisicao);
        return Bcrutils.formatarMoeda(valorTotalItensRequisicao);
    }


    /* Início metodos de preenchimento */
    public void carregarDados() {
        try {
            usuario = usuarioEJB.pegaUsuarioLogadoNaSessao();
            empresa = usuario.getIdEmpresa();
        } catch (Exception e) {
            MensageFactory.info("Erro ao carregar os dados do usuário. Verifique seu cadastro.", null);
            System.out.println(e);
        }

    }

    public void selecionarItem(RequisicaoMaterialItem rmi) {
        requisicaoMaterialItem = rmi;
    }

    public void setarValores() {
        if (requisicaoMaterialItem.getIdProduto() != null) {
            requisicaoMaterialItem.setVlrUnitario(requisicaoMaterialItem.getIdProduto().getPrecoCusto());
            requisicaoMaterialItem.setQuantidade(1);
        }
    }

    public boolean verificaSePodeEditar() {
        if (requisicaoMaterial.getIdRequisicaoMaterial() != null) {
            if (!requisicaoMaterial.getStatus().equals("Emitido")) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public boolean verificaSePodeRecusar() {
        if (requisicaoMaterial.getIdRequisicaoMaterial() != null) {
            if (!requisicaoMaterial.getStatus().equals("Finalizado") || requisicaoMaterial.getStatus().equals("Recusado")) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public void setarCliente() {
        if (requisicaoMaterial.getIdLocalTrabalho() != null) {
            requisicaoMaterial.setIdCliente(requisicaoMaterial.getIdLocalTrabalho().getIdCentroCusto().getIdCliente());
        }
    }

    public void aprovar_item(RequisicaoMaterialItem item) {
        item.setQuantidadeAprovada(item.getQuantidade());
    }

    public void recusar_item(RequisicaoMaterialItem item) {
        item.setQuantidadeAprovada(0);
    }

    public void recusar() {
        try {
            requisicaoMaterial.setStatus("Recusado");
            requisicaoMaterialEJB.Atualizar(requisicaoMaterial);
            requisicaoMaterial = new RequisicaoMaterial();
            listaRequisicaoMaterial = new ArrayList<RequisicaoMaterial>();
            MensageFactory.info("Requisição recusada!", "Requisição recusada!");
        } catch (Exception e) {
            MensageFactory.error("Não foi possível recusar!", "Não foi possível recusar!");
            Bcrutils.escreveLogErro(e);
        }


    }

    public boolean semSelecionar() {
        if (listaRequisicaoMaterialSelecionadas.isEmpty()) {
            if (listaRequisicaoMaterialSelecionadas.size() == 1) {
                requisicaoMaterial = listaRequisicaoMaterialSelecionadas.get(0);
            }
            return true;
        } else {
            if (listaRequisicaoMaterialSelecionadas.size() == 1) {
                requisicaoMaterial = listaRequisicaoMaterialSelecionadas.get(0);
            }
            return false;
        }
    }

    public boolean verificaSePodeAprovarRecusar() {
        if (listaRequisicaoMaterialSelecionadas.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public void aprovarRequisicoes() {
        if (!listaRequisicaoMaterialSelecionadas.isEmpty()) {
            try {
                for (RequisicaoMaterial ri : listaRequisicaoMaterialSelecionadas) {
                    ri.setStatus("Aprovado");
                    for (RequisicaoMaterialItem rmi : ri.getRequisicaoMaterialItemList()) {
                        rmi.setQuantidadeAprovada(rmi.getQuantidade());
                        requisicaoMaterialEJB.atualizarItem(rmi);
                    }
                    requisicaoMaterialEJB.Atualizar(ri);
                }
                MensageFactory.info("Requisições aprovadas!", null);
            } catch (Exception e) {
                MensageFactory.error("Não foi possível aprovar as requisições.", null);
            }

        }
        carregarRequisicoes();
    }

    public void aprovarRequisicao() {
        try {
            requisicaoMaterial.setStatus("Aprovado");
            requisicaoMaterialEJB.Atualizar(requisicaoMaterial);
            for (RequisicaoMaterialItem ri : requisicaoMaterial.getRequisicaoMaterialItemList()) {
                requisicaoMaterialEJB.atualizarItem(ri);
            }
            MensageFactory.info("Requisições aprovada!", null);
        } catch (Exception e) {
            MensageFactory.error("Não foi possível aprovar a requisição.", null);
        }
    }

    public void recusarRequisicoes() {
        if (!listaRequisicaoMaterialSelecionadas.isEmpty()) {
            try {
                for (RequisicaoMaterial ri : listaRequisicaoMaterialSelecionadas) {
                    ri.setStatus("Recusado");
                    requisicaoMaterialEJB.Atualizar(ri);
                }
                MensageFactory.info("Requisições recusadas!", null);
            } catch (Exception e) {
                MensageFactory.error("Não foi possível recusar as requisições.", null);
            }
        }
        carregarRequisicoes();
    }

    public void recusarRequisicao() {
        try {
            requisicaoMaterial.setStatus("Recusado");
            requisicaoMaterialEJB.Atualizar(requisicaoMaterial);
            MensageFactory.info("Requisições recusada!", null);
        } catch (Exception e) {
            MensageFactory.error("Não foi possível recusar a requisição.", null);
        }
    }

    /* Fim metodos de preenchimento */
//    Inicio dos metodos para análise
    public List<RequisicaoMaterial> listaRequisicoesParaAnalise() {
        return requisicaoMaterialEJB.listarRequisicoesParaAnalise();
    }

    public List<RequisicaoMaterial> listaRequisicoesParaAlmoxarifado() {
        return requisicaoMaterialEJB.listarRequisicoesParaAlmoxarifado();
    }

    public boolean verificaParaAnalise() {
        if (listaRequisicaoMaterialSelecionadas.isEmpty() || listaRequisicaoMaterialSelecionadas.size() > 1) {
            return true;
        } else {
            requisicaoMaterial = listaRequisicaoMaterialSelecionadas.get(0);
            if (requisicaoMaterial.getIdRequisicaoMaterial() == null && requisicaoMaterial.getStatus() != "Emitido") {
                return true;
            } else {
                return false;
            }
        }

    }

    public void processarValores() {
        for (RequisicaoMaterialItem ri : requisicaoMaterial.getRequisicaoMaterialItemList()) {
            ri.setQuantidadeAprovada(ri.getQuantidade());
            System.out.println(ri.getQuantidadeAprovada());
        }
    }

    //Verificações para validações de almoxarifadas e para ordem de compras
    public boolean verificarSeTemEstoqueNaRequisicao(RequisicaoMaterial requisicaoMaterial) {
        boolean retorno = false;
        if (!requisicaoMaterial.getRequisicaoMaterialItemList().isEmpty()) {
            for (RequisicaoMaterialItem ri : requisicaoMaterial.getRequisicaoMaterialItemList()) {
                System.out.println("Produto Qnt" + ri.getIdProduto().getQuantidade());
                System.out.println("Requisicao Qnt " + ri.getQuantidadeAprovada());
                if (ri.getIdProduto().getQuantidade() < ri.getQuantidadeAprovada()) {
                    retorno = true;
                }
            }
        }
        return retorno;
    }

    public String verificarQuantidadeItemEstoque(RequisicaoMaterialItem requisicaoMaterialItem) {
        if (requisicaoMaterialItem.getQuantidadeAprovada() > requisicaoMaterialItem.getIdProduto().getQuantidade()) {
            return "icons/Alert.png";
        } else {
            return "icons/confirmar.png";
        }
    }

    public boolean verificarQuantidadeItemEstoqueBol(RequisicaoMaterialItem requisicaoMaterialItem) {
        if (requisicaoMaterialItem.getQuantidade() > requisicaoMaterialItem.getIdProduto().getQuantidade()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean verificarSePodeSair(List<RequisicaoMaterialItem> itens) {
        int situacao = 0;
        for (RequisicaoMaterialItem ri : itens) {
            if (ri.getStatus() != null) {
                if (!ri.getStatus().equals("Aceito")) {
                    situacao = 1;
                }
            }
        }
        if (situacao == 0) {
            return false;
        } else {
            return true;
        }
    }

    public void finalizarSaidaRequisicaoMaterial(RequisicaoMaterial requisicaoMaterial) {
        requisicaoMaterial.setStatus("Finalizado");
        requisicaoMaterialEJB.Atualizar(requisicaoMaterial);
        MensageFactory.info("Requisição finalizada com sucesso!", null);
    }

    public void confirmarAnaliseDeItem(RequisicaoMaterialItem ri, List<RequisicaoMaterialItem> list) {
        ri.setStatus("Aceito");
        requisicaoMaterialItemEJB.Atualizar(ri);
        ri.getIdProduto().setQuantidadeReservada(ri.getIdProduto().getQuantidadeReservada() + ri.getQuantidadeAprovada());
        pEJB.Atualizar(ri.getIdProduto());

        int i = 0;
        for (RequisicaoMaterialItem riii : list) {
            if (riii.getStatus().equals("Cotação")) {
                i = 1;
            }
        }
        if (i == 1) {
            ri.getIdRequisicaoMaterial().setStatus("Aguardando Cotação");
            requisicaoMaterialEJB.Atualizar(ri.getIdRequisicaoMaterial());
        }
    }

    public void cotarItem(RequisicaoMaterialItem item) {
        item.setStatus("Cotação");
        requisicaoMaterialItemEJB.Atualizar(item);
        item.getIdProduto().setQuantidadeReservada(item.getIdProduto().getQuantidadeReservada() + item.getQuantidadeAprovada());
        pEJB.Atualizar(item.getIdProduto());
        item.getIdRequisicaoMaterial().setStatus("Aguardando Cotaçao");
        requisicaoMaterialEJB.Atualizar(item.getIdRequisicaoMaterial());
        MensageFactory.info("Item marcado para cotar!", null);
    }

    public String statusRequisicao(String s) {
        try {
            if (s.equals("Aprovado")) {
                return "label label-success";
            } else if (s.equals("Recusado")) {
                return "label label-danger";
            } else if (s.equals("Finalizado")) {
                return "label label-info";
            } else if (s.equals("Emitido")) {
                return "label label-warning";
            } else {
                return "";
            }
        } catch (Exception e) {
            return "";
        }

    }


    /* Início GET's e SET's */
    public RequisicaoMaterial getRequisicaoMaterial() {
        return requisicaoMaterial;
    }

    public void setRequisicaoMaterial(RequisicaoMaterial requisicaoMaterial) {
        this.requisicaoMaterial = requisicaoMaterial;
    }

    public RequisicaoMaterialItem getRequisicaoMaterialItem() {
        return requisicaoMaterialItem;
    }

    public void setRequisicaoMaterialItem(RequisicaoMaterialItem requisicaoMaterialItem) {
        this.requisicaoMaterialItem = requisicaoMaterialItem;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public LocalTrabalho getLocalTrabalho() {
        return localTrabalho;
    }

    public void setLocalTrabalho(LocalTrabalho localTrabalho) {
        this.localTrabalho = localTrabalho;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<RequisicaoMaterial> getListaRequisicaoMaterial() {
        return listaRequisicaoMaterial;
    }

    public void setListaRequisicaoMaterial(List<RequisicaoMaterial> listaRequisicaoMaterial) {
        this.listaRequisicaoMaterial = listaRequisicaoMaterial;
    }

    public List<LocalTrabalho> getListaLocalTrabalho() {
        return listaLocalTrabalho;
    }

    public void setListaLocalTrabalho(List<LocalTrabalho> listaLocalTrabalho) {
        this.listaLocalTrabalho = listaLocalTrabalho;
    }

    public List<RequisicaoMaterialItem> getListaRequisicaoMaterialItems() {
        return listaRequisicaoMaterialItems;
    }

    public void setListaRequisicaoMaterialItems(List<RequisicaoMaterialItem> listaRequisicaoMaterialItems) {
        this.listaRequisicaoMaterialItems = listaRequisicaoMaterialItems;
    }

    public List<Cliente> getListaClientes() {
        return listaClientes;
    }

    public void setListaClientes(List<Cliente> listaClientes) {
        this.listaClientes = listaClientes;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public RequisicaoMaterial getRequisicaoMaterialReferente() {
        return requisicaoMaterialReferente;
    }

    public void setRequisicaoMaterialReferente(RequisicaoMaterial requisicaoMaterialReferente) {
        this.requisicaoMaterialReferente = requisicaoMaterialReferente;
    }

    public List<RequisicaoMaterial> getListaRequisicaoMaterialSelecionadas() {
        return listaRequisicaoMaterialSelecionadas;
    }

    public void setListaRequisicaoMaterialSelecionadas(List<RequisicaoMaterial> listaRequisicaoMaterialSelecionadas) {
        this.listaRequisicaoMaterialSelecionadas = listaRequisicaoMaterialSelecionadas;
    }

    public List<Produto> getListaProdutos() {
        return listaProdutos;
    }

    public void setListaProdutos(List<Produto> listaProdutos) {
        this.listaProdutos = listaProdutos;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public BigDecimal getValorItem() {
        return valorItem;
    }

    public void setValorItem(BigDecimal valorItem) {
        this.valorItem = valorItem;
    }

    public BigDecimal getValorTotalItem() {
        return valorTotalItem;
    }

    public void setValorTotalItem(BigDecimal valorTotalItem) {
        this.valorTotalItem = valorTotalItem;
    }

    public BigDecimal getValorTotalItensRequisicao() {
        return valorTotalItensRequisicao;
    }

    public void setValorTotalItensRequisicao(BigDecimal valorTotalItensRequisicao) {
        this.valorTotalItensRequisicao = valorTotalItensRequisicao;
    }

    public Integer getQuantidadeTotalItensRequisicao() {
        return quantidadeTotalItensRequisicao;
    }

    public void setQuantidadeTotalItensRequisicao(Integer quantidadeTotalItensRequisicao) {
        this.quantidadeTotalItensRequisicao = quantidadeTotalItensRequisicao;
    }

    public List<RequisicaoMaterial> getLista() {
        return lista;
    }

    public void setLista(List<RequisicaoMaterial> lista) {
        this.lista = lista;
    }

    public LazyDataModel<RequisicaoMaterial> getModel() {
        return model;
    }

    public void setModel(LazyDataModel<RequisicaoMaterial> model) {
        this.model = model;
    }

    public Cotacao getCotacao() {
        return cotacao;
    }

    public void setCotacao(Cotacao cotacao) {
        this.cotacao = cotacao;
    }
}
