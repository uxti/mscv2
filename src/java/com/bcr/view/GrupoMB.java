/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.GrupoEJB;
import com.bcr.model.Grupo;
import com.bcr.util.MensageFactory;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class GrupoMB implements Serializable {

    @EJB
    GrupoEJB gEJB;
    private Grupo grupo;

    /**
     * Creates a new instance of GrupoMB
     */
    public GrupoMB() {
        novo();
    }

    public void novo() {
        grupo = new Grupo();
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public void salvar() {
        if (grupo.getIdGrupo() == null) {
            try {
                gEJB.Salvar(grupo);
                novo();
                MensageFactory.info("Salvo efetuado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar salvar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        } else {
            try {
                gEJB.Atualizar(grupo);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar atualizar!", null);
                System.out.println("Erro ocorrido: " + e);
            }

        }
    }

    public List<Grupo> listarGrupos() {
        return gEJB.ListarTodos();
    }

    public void excluir() {
        try {
            gEJB.Excluir(grupo, grupo.getIdGrupo());
            MensageFactory.info("Excluido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            System.out.println("Erro ocorrido é: " + e);
        }
    }
}
