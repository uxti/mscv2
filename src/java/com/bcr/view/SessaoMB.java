/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.UsuarioEJB;
import com.bcr.model.Usuario;
import java.util.Date;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.HttpSession;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Renato
 */
@ManagedBean
@SessionScoped
public class SessaoMB {

    private Usuario usuario;
    @EJB
    UsuarioEJB uEJB;

    public SessaoMB() {
    }

    public String retornaUsuario() {
        String usuarioLog = "";
        Object usuarioLogado = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (usuarioLogado instanceof UserDetails) {
            return usuarioLog = ((UserDetails) usuarioLogado).getUsername();
        } else {
            return usuarioLog = usuarioLogado.toString().toUpperCase();
        }
    }

    public Usuario getUsuario() {
        try {
            if (usuario.getIdUsuario() == null) {
                usuario.setDtUltimoAcesso(new Date());
                uEJB.Atualizar(usuario);
                usuario = (Usuario) uEJB.pegaUsuarioLogado();
                return usuario;
            } else {
                return usuario;
            }
        } catch (Exception e) {
//            usuario.setDtUltimoAcesso(new Date());
//            uEJB.Atualizar(usuario);
            return usuario = (Usuario) uEJB.pegaUsuarioLogado();
        }
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @RequestMapping("logout")
    public String logout(HttpSession session) {
        session.invalidate();
        return "redirect:login";
    }
}
