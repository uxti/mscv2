/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.NCMEJB;
import com.bcr.model.Ncm;
import com.bcr.util.MensageFactory;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author JOAOPAULO
 */
@ManagedBean
@ViewScoped
public class NCMMB implements Serializable {

    @EJB
    NCMEJB nEJB;
    private Ncm ncm;

    public NCMMB() {
        novo();
    }

    public void novo() {
        ncm = new Ncm();
    }

    public Ncm getNcm() {
        return ncm;
    }

    public void setNcm(Ncm ncm) {
        this.ncm = ncm;
    }

    public void salvar() {
        if (ncm.getIdNcm() == null) {
            try {
                nEJB.Salvar(ncm);
                novo();
                MensageFactory.info("Salvo efetuado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar salvar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        } else {
            try {
                nEJB.Atualizar(ncm);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar atualizar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        }
    }

    public List<Ncm> listarNcms() {
        return nEJB.ListarTodos();
    }

    public void excluir() {
        try {
            nEJB.Excluir(ncm, ncm.getIdNcm());
            MensageFactory.info("Excluido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            System.out.println("Erro ocorrido é: " + e);
        }
    }
}
