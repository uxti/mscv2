/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.UnidadeEJB;
import com.bcr.model.Unidade;
import com.bcr.util.Conexao;
import com.bcr.util.MensageFactory;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author JOAOPAULO
 */
@ManagedBean
@ViewScoped
public class UnidadeMB implements Serializable {

    @EJB
    UnidadeEJB uEJB;
    private Unidade unidade;

    public UnidadeMB() {
        novo();
    }

    public void novo() {
        unidade = new Unidade();
    }

    public Unidade getUnidade() {
        return unidade;
    }

    public void setUnidade(Unidade unidade) {
        this.unidade = unidade;
    }

    public void salvar() {
        if (unidade.getIdUnidade() == null) {
            try {
                uEJB.Salvar(unidade);
                novo();
                MensageFactory.info("Salvo efetuado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar salvar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        } else {
            try {
                uEJB.Atualizar(unidade);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar atualizar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        }
    }

    public List<Unidade> listarUnidades() {
        return uEJB.ListarTodos();
    }

    public void excluir() {
        try {
            uEJB.Excluir(unidade, unidade.getIdUnidade());
            MensageFactory.info("Excluido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            System.out.println("Erro ocorrido é: " + e);
        }
    }

 
}