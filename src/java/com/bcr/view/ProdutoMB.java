/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.EmpresaEJB;
import com.bcr.controller.FornecedorEJB;
import com.bcr.controller.GrupoEJB;
import com.bcr.controller.LocalizacaoEJB;
import com.bcr.controller.NCMEJB;
import com.bcr.controller.ProdutoEJB;
import com.bcr.controller.ProdutoFornecedorEJB;
import com.bcr.controller.SubGrupoEJB;
import com.bcr.controller.UnidadeEJB;
import com.bcr.model.Empresa;
import com.bcr.model.Fornecedor;
import com.bcr.model.Grupo;
import com.bcr.model.Localizacao;
import com.bcr.model.Ncm;
import com.bcr.model.Produto;
import com.bcr.model.ProdutoFornecedor;
import com.bcr.model.SubGrupo;
import com.bcr.model.Unidade;
import com.bcr.util.MensageFactory;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author JOAOPAULO
 */
@ManagedBean
@ViewScoped
public class ProdutoMB implements Serializable {

    @EJB
    ProdutoEJB pEJB;
    private Produto produto;
    private LazyDataModel<Produto> produtoLazy;
    private List<Produto> produtos;
    @EJB
    EmpresaEJB eEJB;
    private Empresa empresa;
    @EJB
    GrupoEJB gEJB;
    private Grupo grupo;
    @EJB
    SubGrupoEJB sgEJB;
    private SubGrupo subGrupo;
    @EJB
    UnidadeEJB uEJB;
    private Unidade unidadeEntrada;
    private Unidade unidadeSaida;
    @EJB
    ProdutoFornecedorEJB pfEJB;
    private List<ProdutoFornecedor> produtoFornecedores;
    @EJB
    NCMEJB nEJB;
    private Ncm ncm;
    @EJB
    FornecedorEJB fEJB;
    private Fornecedor fornecedor;
    @EJB
    LocalizacaoEJB lEJB;
    private Localizacao localizacao;

    public ProdutoMB() {
        novo();
    }

    public void novo() {
        produto = new Produto();
        produtoFornecedores = new ArrayList<ProdutoFornecedor>();
        unidadeEntrada = new Unidade();
        unidadeSaida = new Unidade();
        grupo = new Grupo();
        subGrupo = new SubGrupo();
        fornecedor = new Fornecedor();
        produto.setDtCadastro(new Date());
        produto.setInativo("0");
        produto.setFracionado("N");
        empresa = new Empresa();
        grupo = new Grupo();
        subGrupo = new SubGrupo();
        unidadeEntrada = new Unidade();
        unidadeSaida = new Unidade();
        ncm = new Ncm();
        localizacao = new Localizacao();
    }

    public void salvar() {
        HttpServletRequest request = (HttpServletRequest) FacesContext
                .getCurrentInstance().getExternalContext().getRequest();
        empresa = eEJB.SelecionarPorID(Integer.parseInt(request.getParameter("id_empresa")));
        produto.setIdEmpresa(empresa);
        if (grupo != null) {
            produto.setIdGrupo(grupo);
        }
        if (subGrupo != null) {
            produto.setIdSubgrupo(subGrupo);
        }

        if (unidadeEntrada != null) {
            produto.setIdUnidadeEntrada(unidadeEntrada);
        }
        if (unidadeSaida.getIdUnidade() != null) {
            System.out.println("Entrou no if da unidade de saida");
            produto.setIdUnidadeSaida(unidadeSaida);
        }
        if (ncm != null) {
            produto.setIdNCM(ncm);
        }
        if (localizacao != null) {
            produto.setIdLocalizacao(localizacao);
        }
        if (produto.getIdProduto() == null) {
            try {
                System.out.println("Produto Fornecedores: " + produtoFornecedores.size());
                pEJB.salvar(produto, produtoFornecedores);
                MensageFactory.info("Produto salvo com sucesso!", null);
                novo();
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar salvar!", null);
                System.out.println("Erro ocorrido é: ");
                System.out.println(e);
            }
        } else {
            try {
                produto.setDtUltimaAtualizacao(new Date());
                pEJB.salvar(produto, produtoFornecedores);
                MensageFactory.info("Produto atualizado com sucesso!", null);
                novo();
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar atualizar!", null);
                System.out.println("Erro ocorrido é: ");
                System.out.println(e);
            }
        }

    }

    public void carregarDatatableProduto() {
        produtoLazy = new LazyDataModel<Produto>() {
            private static final long serialVersionUID = 1L;

            public List<Produto> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and c." + filterProperty + " like'%" + filterValue + "%'");
                        } else {
                            sf.append(" where c." + filterProperty + " like'%" + filterValue + "%'");
                        }
                    } else {
                        sf.append(" and c." + filterProperty + " like'%" + filterValue + "%'");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by c." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by c." + sortField + " desc");
                }
                Clausula = sf.toString();

                if (Clausula.contains("_")) {
                    Clausula = Clausula.replace("_", "");
                }
                if (Clausula.contains("() -")) {
                    Clausula = Clausula.replace("() -", "");
                }
                if (Clausula.contains("..")) {
                    Clausula = Clausula.replace("..", "");
                }
                setRowCount(Integer.parseInt(pEJB.contarRegistros(Clausula).toString()));
                produtos = pEJB.listarProdutosLazy(first, pageSize, Clausula);
                return produtos;

            }
        };
    }

    public List<Grupo> listaGrupos() {
        return gEJB.ListarTodos();
    }

    public List<Ncm> listaNCM() {
        return nEJB.ListarTodos();
    }

    public List<Localizacao> listaLocalizacoes() {
        return lEJB.ListarTodos();
    }

    public void setarST() {
        if ("S".equals(produto.getTipoTributacao())) {
            produto.setCst("060");
        } else {
            produto.setCst("000");
        }
    }

    public List<Produto> listarProdutos() {
        return pEJB.ListarTodos();
    }

    public void excluir() {
        try {
            pEJB.Excluir(produto, produto.getIdProduto());
            MensageFactory.info("Produto excluído com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir o produto!", null);
            System.out.println("O erro ocorrido é: ");
            System.out.println(e);
        }
    }

    public Produto getProduto() {
        if (produto != null) {
            if (produto.getIdGrupo() != null) {
                grupo = produto.getIdGrupo();
            }
            if (produto.getIdSubgrupo() != null) {
                subGrupo = produto.getIdSubgrupo();
            }
            if (produto.getIdUnidadeEntrada() != null) {
                unidadeEntrada = produto.getIdUnidadeEntrada();
            }
            if (produto.getIdUnidadeSaida() != null) {
                unidadeSaida = produto.getIdUnidadeSaida();
            }
            if (produto.getProdutoFornecedorList() != null) {
                produtoFornecedores = produto.getProdutoFornecedorList();
            }
            if (produto.getIdLocalizacao() != null) {
                localizacao = new Localizacao();
            }
            try {
                return produto;

            } catch (Exception e) {
                return new Produto();
            }
        } else {
            return new Produto();
        }
    }

    public List<SubGrupo> listadeSubGrupoPorGrupo() {
        try {
            return pEJB.carregaListadeSubgrupoPorGrupo(grupo.getIdGrupo());
        } catch (Exception e) {
            return new ArrayList<SubGrupo>();

        }
    }

    public void adicionarFornecedor() {
        System.out.println(fornecedor.getNome());
        ProdutoFornecedor pf = new ProdutoFornecedor();
        pf.setIdFornecedor(fornecedor);
        produtoFornecedores.add(pf);
    }

    public void excluirFornecedor(ProdutoFornecedor pf, int index) {

        try {
            if (pf.getIdProdutoFornecedor() != null) {
                System.out.println(pf.getIdProdutoFornecedor());
                pfEJB.Excluir(pf, pf.getIdProdutoFornecedor());
            }
            produtoFornecedores.remove(index);
            MensageFactory.info("Fornecedor removido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.fatal("Erro ao tentar excluir o Fornecedor!", null);
            System.out.println("Erro ocorrido no metodo excluirFornecedor: " + e);
        }
    }

    public void verificaSeFracionado() {
        if (produto.getFracionado().equals("N")) {
            if (produto.getIdUnidadeEntrada() != null) {
                produto.setIdUnidadeSaida(produto.getIdUnidadeEntrada());
                produto.setBaseConversao("D");
                produto.setFatorConversaoEntrada(BigDecimal.ONE);
                produto.setFatorConversaoSaida(BigDecimal.ONE);
            }
        }
    }

    public void onRowSelect(SelectEvent event) {
        MensageFactory.info("Selecionou o produto " + ((Produto) event.getObject()).getDescricao(), null);
    }
    
     public List<Produto> completeMethodProduto(String var) {
        return pEJB.autoCompleteProduto(var);
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public SubGrupo getSubGrupo() {
        return subGrupo;
    }

    public void setSubGrupo(SubGrupo subGrupo) {
        this.subGrupo = subGrupo;
    }

    public Unidade getUnidadeEntrada() {
        return unidadeEntrada;
    }

    public void setUnidadeEntrada(Unidade unidadeEntrada) {
        this.unidadeEntrada = unidadeEntrada;
    }

    public Unidade getUnidadeSaida() {
        return unidadeSaida;
    }

    public void setUnidadeSaida(Unidade unidadeSaida) {
        this.unidadeSaida = unidadeSaida;
    }

    public Ncm getNcm() {
        return ncm;
    }

    public void setNcm(Ncm ncm) {
        this.ncm = ncm;
    }

    public List<ProdutoFornecedor> getProdutoFornecedores() {
        return produtoFornecedores;
    }

    public void setProdutoFornecedores(List<ProdutoFornecedor> produtoFornecedores) {
        this.produtoFornecedores = produtoFornecedores;
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public Localizacao getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(Localizacao localizacao) {
        this.localizacao = localizacao;
    }

    public LazyDataModel<Produto> getProdutoLazy() {
        return produtoLazy;
    }

    public void setProdutoLazy(LazyDataModel<Produto> produtoLazy) {
        this.produtoLazy = produtoLazy;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }
}
