/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.CidadeEJB;
import com.bcr.model.Cidade;
import com.bcr.model.Cliente;
import com.bcr.model.Estado;
import com.bcr.util.MensageFactory;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class CidadeMB implements Serializable {

    @EJB
    CidadeEJB cEJB;
    private Cidade cidade;
    private Estado estado;
    private List<Cidade> cidades;
    private LazyDataModel<Cidade> cidadesLazy;

    public CidadeMB() {
        novo();
    }

    public void novo() {
        cidade = new Cidade();
        estado = new Estado();
        cidades = new ArrayList<Cidade>();
        carregarDatatableCidade();
        RequestContext rc = RequestContext.getCurrentInstance();
        rc.execute("document.getElementById('frmCadastro:nome').value=''");
    }

    public void salvar() {

        if (cidade.getIdCidade() == null) {
            if (cEJB.verificaIBGECadastrado(cidade.getCodigoIBGE()) > 0L) {
                MensageFactory.error("Atenção esta cidade com este código de ibge já se encontra cadastrada!", null);
            } else {
                try {
                    if (estado.getIdEstado() != null) {
                        cidade.setIdEstado(estado);
                    }
                    cEJB.Salvar(cidade);
                    novo();

                    MensageFactory.info("Salvo com sucesso!", null);
                } catch (Exception e) {
                    if (e.getCause().toString().contains("MySQLIntegrityConstraintViolationException")) {
                        MensageFactory.error("Atenção esta cidade com este código de ibge já se encontra cadastrada!", null);
                        System.out.println("Erro ocorrido: " + e);
                    } else {
                        MensageFactory.error("Erro ao tentar salvar!", null);
                        System.out.println("Erro ocorrido: " + e);
                    }
                }
            }
        } else {
            try {
                if (estado.getIdEstado() != null) {
                    cidade.setIdEstado(estado);
                }
                cEJB.Atualizar(cidade);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                if (e.getCause().toString().contains("MySQLIntegrityConstraintViolationException")) {
                    MensageFactory.error("Atenção esta cidade com este código de ibge já se encontra cadastrada!", null);
                    System.out.println("Erro ocorrido: " + e);
                } else {
                    MensageFactory.error("Erro ao tentar atualizar!", null);
                    System.out.println("Erro ocorrido: " + e);
                }
            }
        }
    }

    public void excluir() {
        try {
            cEJB.Excluir(cidade, cidade.getIdCidade());
            MensageFactory.info("Excluido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            System.out.println("Erro ocorrido é: " + e);
        }
    }

    public void carregarDatatableCidade() {
        cidadesLazy = new LazyDataModel<Cidade>() {
            private static final long serialVersionUID = 1L;

            public List<Cidade> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and c." + filterProperty + " like'%" + filterValue + "%'");
                        } else {
                            sf.append(" where c." + filterProperty + " like'%" + filterValue + "%'");
                        }
                    } else {
                        sf.append(" and c." + filterProperty + " like'%" + filterValue + "%'");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by c." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by c." + sortField + " desc");
                }
                Clausula = sf.toString();

                if (Clausula.contains("_")) {
                    Clausula = Clausula.replace("_", "");
                }
                if (Clausula.contains("() -")) {
                    Clausula = Clausula.replace("() -", "");
                }
                if (Clausula.contains("..")) {
                    Clausula = Clausula.replace("..", "");
                }
                setRowCount(Integer.parseInt(cEJB.contarRegistros(Clausula).toString()));
                cidades = cEJB.listarClientesLazy(first, pageSize, Clausula);
                return cidades;

            }
        };
    }

    public Cidade getCidade() {
        try {
            if (cidade.getIdEstado() != null) {
                estado = cidade.getIdEstado();
            }
        } catch (Exception e) {
        }

        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public List<Cidade> getCidades() {
        return cidades;
    }

    public void setCidades(List<Cidade> cidades) {
        this.cidades = cidades;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public LazyDataModel<Cidade> getCidadesLazy() {
        return cidadesLazy;
    }

    public void setCidadesLazy(LazyDataModel<Cidade> cidadesLazy) {
        this.cidadesLazy = cidadesLazy;
    }

    public List<Cidade> listarCidades() {
        return cEJB.ListarTodos();
    }
    
    public List<Cidade> completarCidades(String nome){
        try {
            MensageFactory.info("Cidade encontrada!", null);
            return  cEJB.completarCidadePorNome(nome);
        } catch (Exception e) {
            MensageFactory.warn("Não encontrou nenhuma cidade!", null);
            return new ArrayList<Cidade>();
        }
       
    }
}
