/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.AgrupamentoEJB;
import com.bcr.model.Agrupamento;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class AgrupamentoMB implements Serializable{

    @EJB
    AgrupamentoEJB aEJB;
    private Agrupamento agrupamento;
   

    /**
     * Creates a new instance of AgrupamentoMB
     */
    public AgrupamentoMB() {
        novo();
    }

    public void novo() {
        agrupamento = new Agrupamento();
       
    }

    public void salvar() {
        if (agrupamento.getIdAgrupamento() == null) {
            try {
                aEJB.Salvar(agrupamento);
                novo();
                MensageFactory.info("Salvo com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao salvar!", null);
                Bcrutils.escreveLogErro(e);
            }
        } else {
            try {
                aEJB.Atualizar(agrupamento);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao atualizar!", null);
                Bcrutils.escreveLogErro(e);
            }
        }
    }

    public List<Agrupamento> listaAgrupamentos() {
        return aEJB.ListarTodos();
    }

    public void excluir() {
        try {
            aEJB.Excluir(agrupamento, agrupamento.getIdAgrupamento());
            MensageFactory.info("Excluído com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            Bcrutils.escreveLogErro(e);
        }
    }

    
    public Agrupamento getAgrupamento() {
        return agrupamento;
    }

    public void setAgrupamento(Agrupamento agrupamento) {
        this.agrupamento = agrupamento;
    }

 
}
