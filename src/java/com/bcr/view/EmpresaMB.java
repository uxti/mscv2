/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.CaixaEJB;
import com.bcr.controller.CidadeEJB;
import com.bcr.controller.EmpresaEJB;
import com.bcr.controller.EstadoEJB;
import com.bcr.model.Caixa;
import com.bcr.model.CaixaItem;
import com.bcr.model.Cidade;
import com.bcr.model.Empresa;
import com.bcr.model.Estado;
import com.bcr.model.PlanoContas;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import java.io.Serializable;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class EmpresaMB implements Serializable {

    @EJB
    EmpresaEJB eEJB;
    private Empresa empresa;
    @EJB
    CidadeEJB cEJB;
    private Cidade cidade;
    private List<Cidade> cidades;
    private LazyDataModel<Cidade> cidadesLazy;
    @EJB
    EstadoEJB ufEJB;
    @EJB
    CaixaEJB caEJB;
    private Date dtPrimeiroCaixa;
    private PlanoContas planoContas;

    /**
     * Creates a new instance of EmpresaMB
     */
    public EmpresaMB() {
        novo();
    }

    public void novo() {
        empresa = new Empresa();
        cidade = new Cidade();
        cidades = new ArrayList<Cidade>();
        planoContas = new PlanoContas();
    }

    public void carregarDatatableCidade() {
        cidadesLazy = new LazyDataModel<Cidade>() {
            private static final long serialVersionUID = 1L;

            public List<Cidade> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and c." + filterProperty + " like'%" + filterValue + "%'");
                        } else {
                            sf.append(" where c." + filterProperty + " like'%" + filterValue + "%'");
                        }
                    } else {
                        sf.append(" and c." + filterProperty + " like'%" + filterValue + "%'");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by c." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by c." + sortField + " desc");
                }
                Clausula = sf.toString();

                if (Clausula.contains("_")) {
                    Clausula = Clausula.replace("_", "");
                }
                if (Clausula.contains("() -")) {
                    Clausula = Clausula.replace("() -", "");
                }
                if (Clausula.contains("..")) {
                    Clausula = Clausula.replace("..", "");
                }
                setRowCount(Integer.parseInt(cEJB.contarRegistros(Clausula).toString()));
                cidades = cEJB.listarClientesLazy(first, pageSize, Clausula);
                return cidades;

            }
        };
    }

    public void pesquisarCEP() throws MalformedURLException, DocumentException {
        String cep = empresa.getCep();
        if (cep != null) {
            String cep2 = cep.replace("-", "");
            URL url = new URL("http://viacep.com.br/ws/" + cep2 + "/xml/");
            Document document = getDocumento(url);
            Element root = document.getRootElement();
            cidade = new Cidade();
            String ibge = "";
            String uf = "";
            String nomeCidade = "";
            for (Iterator i = root.elementIterator(); i.hasNext();) {
                Element element = (Element) i.next();
                if (element.getQualifiedName().equals("logradouro")) {
                    empresa.setLogradouro(element.getText());
                    System.out.println(element.getText());
                }
                if (element.getQualifiedName().equals("bairro")) {
                    empresa.setBairro(element.getText());
                }
                if (element.getQualifiedName().equals("ibge")) {
                    ibge = element.getText();
                }
                if (element.getQualifiedName().equals("uf")) {
                    uf = element.getText();
                }
                if (element.getQualifiedName().equals("localidade")) {
                    nomeCidade = element.getText();
                    cidade = cEJB.pesquisarCidadePorNome(nomeCidade);
                }
            }
            if (cidade.getIdCidade() == null) {
                Cidade c = new Cidade();
                c.setNome(nomeCidade);
                c.setCep(cep);
                c.setCodigoIBGE(ibge);
                Estado ee = new Estado();
                ee = ufEJB.pesquisarPorSigla(uf);
                c.setIdEstado(ee);
                cEJB.Salvar(c);
                cidade = c;
            }
        }
    }

    public static Document getDocumento(URL url) throws DocumentException {
        SAXReader reader = new SAXReader();
        Document document = reader.read(url);
        return document;
    }

    public List<Empresa> listaEmpresas() {
        return eEJB.ListarTodos();
    }

    public void salvar() {
        System.out.println("chamou o salvar");
        if (cidade.getIdCidade() != null) {
            empresa.setIdCidade(cidade);
            empresa.setIdEstado(cidade.getIdEstado());
               System.out.println("setou a cidade");
        }
        if (empresa.getIdEmpresa() == null) {
               System.out.println("ta no if");
            try {
//                empresa.getIdParametrizacao().setCorTopo("#" + empresa.getIdParametrizacao().getCorTopo());
//                empresa.getIdParametrizacao().setCorLateral("#" + empresa.getIdParametrizacao().getCorLateral());
//                empresa.getIdParametrizacao().setCorBarras("#" + empresa.getIdParametrizacao().getCorBarras());
                eEJB.Salvar(empresa);
                MensageFactory.info("Salvo com sucesso!", null);
//                Empresa e = eEJB.retornaUltimaEmpresaInserida();
//                System.out.println(e.getIdEmpresa());
//                lancarMovimentoCaixa(empresa.getCaixaInicialValor(), e);
                novo();
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar salvar!", null);
                System.out.println("Erro identificado: " + e);
            }
        } else {
               System.out.println("ta no else");
            try {
                   System.out.println("ta no try");
//                empresa.getIdParametrizacao().setCorTopo("#" + empresa.getIdParametrizacao().getCorTopo());
//                empresa.getIdParametrizacao().setCorLateral("#" + empresa.getIdParametrizacao().getCorLateral());
//                empresa.getIdParametrizacao().setCorBarras("#" + empresa.getIdParametrizacao().getCorBarras());
                eEJB.Atualizar(empresa);
                MensageFactory.info("Atualizado com sucesso!", null);
                novo();
            } catch (Exception e) {
                   System.out.println("ta no catch");
                MensageFactory.error("Erro ao tentar atualizar!", null);
                System.out.println("Erro identificado: " + e);
                Bcrutils.escreveLogErro(e);
            }
        }
    }

    public void excluir() {
        try {
            eEJB.Excluir(empresa, empresa.getIdEmpresa());
            MensageFactory.info("Cidade excluída com sucesso!", null);
            novo();
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            System.out.println("Erro identificado: " + e);
        }
    }

    public void lancarMovimentoCaixa(BigDecimal valorRepassado, Empresa empresa) {
        Caixa c = new Caixa();
        if (caEJB.verificarExisteMovimentoCaixa(dtPrimeiroCaixa, empresa) == 0) {
            caEJB.criaNovoCaixa(dtPrimeiroCaixa, empresa);
        }
        c = caEJB.retornaCaixaPorData(dtPrimeiroCaixa, empresa);
        CaixaItem ci = new CaixaItem();
        ci.setHistorico("Primeiro Lançamento no Caixa de Abertura da Empresa.");
        ci.setIdCaixa(c);
//        ci.setIdContasReceber(contasReceber);
        ci.setTipo("C");
        ci.setIdPlanoContas(planoContas);
        ci.setValor(valorRepassado);
        caEJB.inserirMovimentacao(ci);
        ci = new CaixaItem();
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public List<Empresa> listarEmpresas() {
        return eEJB.ListarTodos();
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public List<Cidade> getCidades() {
        return cidades;
    }

    public void setCidades(List<Cidade> cidades) {
        this.cidades = cidades;
    }

    public LazyDataModel<Cidade> getCidadesLazy() {
        return cidadesLazy;
    }

    public void setCidadesLazy(LazyDataModel<Cidade> cidadesLazy) {
        this.cidadesLazy = cidadesLazy;
    }

    public Date getDtPrimeiroCaixa() {
        return dtPrimeiroCaixa;
    }

    public void setDtPrimeiroCaixa(Date dtPrimeiroCaixa) {
        this.dtPrimeiroCaixa = dtPrimeiroCaixa;
    }

    public PlanoContas getPlanoContas() {
        return planoContas;
    }

    public void setPlanoContas(PlanoContas planoContas) {
        this.planoContas = planoContas;
    }
}
