/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.CaixaEJB;
import com.bcr.controller.EmpresaEJB;
import com.bcr.controller.MovimentoBancarioEJB;
import com.bcr.model.Banco;
import com.bcr.model.Caixa;
import com.bcr.model.CaixaItem;
import com.bcr.model.ContaCorrente;
import com.bcr.model.Empresa;
import com.bcr.model.MovimentacaoBancaria;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class MovimentacaoBancariaMB implements Serializable {

    @EJB
    MovimentoBancarioEJB mbEJB;
    @EJB
    CaixaEJB cEJB;
    private MovimentacaoBancaria movimentacaoBancaria;
    private List<MovimentacaoBancaria> listaMovimentacoes;
    private Banco banco;
    private ContaCorrente contaCorrente;
    private Banco bancoDestino;
    private ContaCorrente contaCorrenteDestino;
    private String tipoMov;
    private Date dtMovimentoInicial;
    private Date dtMovimentoFinal;
    private Part arquivo;
    private BigDecimal credito, debito, saldo = new BigDecimal(BigInteger.ZERO);
    @EJB
    EmpresaEJB empEJB;

    /**
     * Creates a new instance of MovimentacaoBancariaMB
     */
    public MovimentacaoBancariaMB() {
        novo();
    }

    public void novo() {
        movimentacaoBancaria = new MovimentacaoBancaria();
        listaMovimentacoes = new ArrayList<MovimentacaoBancaria>();
        banco = new Banco();
        contaCorrente = new ContaCorrente();
        bancoDestino = new Banco();
        contaCorrenteDestino = new ContaCorrente();
    }

    public void salvar() {
        if (movimentacaoBancaria.getIdmovimentacaobancariaItem() == null) {
            try {
                mbEJB.Salvar(movimentacaoBancaria);
                novo();
                MensageFactory.info("Movimentação bancária lançada com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao lançar a movimentação bancária!", null);
                System.out.println("O erro ocorrido é: " + e);
                System.out.println(e);
            }
        } else {
            try {
                mbEJB.Salvar(movimentacaoBancaria);
                novo();
                MensageFactory.info("Movimentação bancária atualizada com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao atualizar a movimentação bancária!", null);
                System.out.println("O erro ocorrido é: " + e);
                System.out.println(e);
            }
        }
    }

    public boolean verificaSePodeSacarDepositarTransferir() {
        if (banco.getIdBanco() == null) {
            return true;
        } else {
            return false;
        }
    }

    public MovimentacaoBancaria getMovimentacaoBancaria() {
        return movimentacaoBancaria;
    }

    public void setMovimentacaoBancaria(MovimentacaoBancaria movimentacaoBancaria) {
        this.movimentacaoBancaria = movimentacaoBancaria;
    }

    public List<MovimentacaoBancaria> getListaMovimentacoes() {
        return listaMovimentacoes;
    }

    public void setListaMovimentacoes(List<MovimentacaoBancaria> listaMovimentacoes) {
        this.listaMovimentacoes = listaMovimentacoes;
    }

    public String getTipoMov() {
        return tipoMov;
    }

    public void setTipoMov(String tipoMov) {
        this.tipoMov = tipoMov;
    }

    public Date getDtMovimentoInicial() {
        return dtMovimentoInicial;
    }

    public void setDtMovimentoInicial(Date dtMovimentoInicial) {
        this.dtMovimentoInicial = dtMovimentoInicial;
    }

    public Date getDtMovimentoFinal() {
        return dtMovimentoFinal;
    }

    public void setDtMovimentoFinal(Date dtMovimentoFinal) {
        this.dtMovimentoFinal = dtMovimentoFinal;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public ContaCorrente getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(ContaCorrente contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    public BigDecimal getCredito() {
        return credito;
    }

    public void setCredito(BigDecimal credito) {
        this.credito = credito;
    }

    public BigDecimal getDebito() {
        return debito;
    }

    public void setDebito(BigDecimal debito) {
        this.debito = debito;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public Banco getBancoDestino() {
        return bancoDestino;
    }

    public void setBancoDestino(Banco bancoDestino) {
        this.bancoDestino = bancoDestino;
    }

    public ContaCorrente getContaCorrenteDestino() {
        return contaCorrenteDestino;
    }

    public void setContaCorrenteDestino(ContaCorrente contaCorrenteDestino) {
        this.contaCorrenteDestino = contaCorrenteDestino;
    }
    
    

    public void pesquisarMovimentoBancario() {
        if (dtMovimentoInicial == null) {
            dtMovimentoInicial = Bcrutils.addAno(new Date(), -50);
        }
        if (dtMovimentoFinal == null) {
            dtMovimentoFinal = Bcrutils.addAno(new Date(), +50);
        }
        try {
            listaMovimentacoes = new ArrayList<MovimentacaoBancaria>();
            listaMovimentacoes = mbEJB.pesquisaMovimentacao(banco, contaCorrente, dtMovimentoInicial, dtMovimentoFinal);
            credito = mbEJB.retornaCreditos(banco, contaCorrente);
            saldo = mbEJB.retornaSaldo(banco, contaCorrente);
            debito = mbEJB.retornaDebitos(banco, contaCorrente);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void novoLancamentoBancario() {
        movimentacaoBancaria = new MovimentacaoBancaria();
    }

    //Deposito
    public void depositar() {
        try {
            movimentacaoBancaria.setIdBanco(banco);
            movimentacaoBancaria.setIdContaCorrente(contaCorrente);
            movimentacaoBancaria.setTipo("C");
            movimentacaoBancaria.setTipoMovimento("Deposito");
            movimentacaoBancaria.setIdEmpresa(banco.getIdEmpresa());
            mbEJB.Atualizar(movimentacaoBancaria);
            movimentacaoBancaria = new MovimentacaoBancaria();
            MensageFactory.info("Depósito efetuado com sucesso!", null);
            pesquisarMovimentoBancario();
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar depositar!", null);
            System.out.println("O erro ocorrido é: " + e);
            System.out.println(e);
        }
    }

    public void sacar() {
        try {
            movimentacaoBancaria.setIdBanco(banco);
            movimentacaoBancaria.setIdContaCorrente(contaCorrente);
            movimentacaoBancaria.setTipo("D");
            movimentacaoBancaria.setTipoMovimento("Saque");
            movimentacaoBancaria.setIdEmpresa(banco.getIdEmpresa());
            mbEJB.Atualizar(movimentacaoBancaria);
            movimentacaoBancaria = new MovimentacaoBancaria();
            MensageFactory.info("Depósito efetuado com sucesso!", null);
            pesquisarMovimentoBancario();
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar sacar!", null);
            System.out.println("O erro ocorrido é: " + e);
            System.out.println(e);
        }
    }

    //Transferencia banco para caixa!
    public void transferirCaixa() {
        try {
            movimentacaoBancaria.setIdBanco(banco);
            movimentacaoBancaria.setIdContaCorrente(contaCorrente);
            movimentacaoBancaria.setTipo("D");
            movimentacaoBancaria.setTipoMovimento("Transferencia");
            movimentacaoBancaria.setIdEmpresa(banco.getIdEmpresa());
            movimentacaoBancaria.setHistorico("Transferencia do Banco " + banco.getNome() + " da conta " + movimentacaoBancaria.getIdContaCorrente().getNumConta() + " no valor " + movimentacaoBancaria.getValor() + " para o caixa do dia " + Bcrutils.formataData(movimentacaoBancaria.getDtMovimentacaoBancariaItem())+"!");
            mbEJB.Atualizar(movimentacaoBancaria);

            Caixa c = cEJB.retornaCaixaPorData(movimentacaoBancaria.getDtMovimentacaoBancariaItem(), banco.getIdEmpresa());
            if (c.getIdCaixa() == null) {
                c.setDtCaixa(new Date());
                c.setIdEmpresa(banco.getIdEmpresa());
                cEJB.Salvar(c);
                c = cEJB.retornaCaixaPorData(movimentacaoBancaria.getDtMovimentacaoBancariaItem(), banco.getIdEmpresa());
                CaixaItem ci = new CaixaItem();
                ci.setIdCaixa(c);
                ci.setHistorico("Transferencia do Banco " + banco.getNome() + " da conta " + movimentacaoBancaria.getIdContaCorrente().getNumConta() + " no valor " + movimentacaoBancaria.getValor() + " para o caixa!");
                ci.setIdPlanoContas(movimentacaoBancaria.getIdPlanoContas());
                ci.setTipo("C");
                ci.setTipomovimento("Transferencia");
                ci.setValor(movimentacaoBancaria.getValor());
                cEJB.inserirMovimentacao(ci);
            } else {
                CaixaItem ci = new CaixaItem();
                ci.setIdCaixa(c);
                ci.setHistorico("Transferencia do Banco " + banco.getNome() + " da conta " + movimentacaoBancaria.getIdContaCorrente().getNumConta() + " no valor " + movimentacaoBancaria.getValor() + " para o caixa!");
                ci.setIdPlanoContas(movimentacaoBancaria.getIdPlanoContas());
                ci.setTipo("C");
                ci.setTipomovimento("Transferencia");
                ci.setValor(movimentacaoBancaria.getValor());
                cEJB.inserirMovimentacao(ci);
            }
            movimentacaoBancaria = new MovimentacaoBancaria();
            MensageFactory.info("Transferencia efetuada com sucesso!", null);
            pesquisarMovimentoBancario();
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar transferir de banco para caixa!", null);
            System.out.println("O erro ocorrido é: " + e);
            System.out.println(e);
        }
    }
    
    
    public void transferirEntreBancos(){
       HttpServletRequest request = (HttpServletRequest) FacesContext
                .getCurrentInstance().getExternalContext().getRequest();
        Empresa emp = empEJB.SelecionarPorID(Integer.parseInt(request.getParameter("id_empresa")));
        try {
            MovimentacaoBancaria mvbDebito = new MovimentacaoBancaria();
            mvbDebito = movimentacaoBancaria;
            mvbDebito.setIdBanco(banco);
            mvbDebito.setIdContaCorrente(contaCorrente);
            mvbDebito.setHistorico("Transferencia do Banco " + banco.getNome() + " da conta " + movimentacaoBancaria.getIdContaCorrente().getNumConta() + " no valor " + movimentacaoBancaria.getValor() + " para a conta " + movimentacaoBancaria.getIdContaCorrente().getNumConta() + "!");
            mvbDebito.setTipo("D");
            mvbDebito.setIdEmpresa(emp);
            mbEJB.Atualizar(mvbDebito);
            
            movimentacaoBancaria.setTipo("C");
            movimentacaoBancaria.setIdBanco(bancoDestino);
            movimentacaoBancaria.setIdContaCorrente(contaCorrenteDestino);
            movimentacaoBancaria.setTipoMovimento("Transferencia");
            movimentacaoBancaria.setIdEmpresa(bancoDestino.getIdEmpresa());
            movimentacaoBancaria.setHistorico("Transferencia do recebida do Banco " + bancoDestino.getNome() + " da conta " + contaCorrenteDestino.getNumConta() + " no valor " + movimentacaoBancaria.getValor() + " para a conta " + contaCorrenteDestino.getNumConta() + "!");
            mbEJB.Atualizar(movimentacaoBancaria);

            movimentacaoBancaria = new MovimentacaoBancaria();
            MensageFactory.info("Transferencia efetuada com sucesso!", null);
            pesquisarMovimentoBancario();
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar transferir de banco para banco!", null);
            System.out.println("O erro ocorrido é: " + e);
            System.out.println(e);
        }
    }

  
}
