/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.EmpresaEJB;
import com.bcr.controller.RecebimentoEJB;
import com.bcr.model.Centrocustoplanocontas;
import com.bcr.model.ContasReceber;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class RecebimentoMB implements Serializable{

    @EJB
    RecebimentoEJB rEJB;
    private ContasReceber recebimento;
    private List<ContasReceber> recebimentos;
    private List<ContasReceber> recebimentosSelecionados;
    private Centrocustoplanocontas centrocustoplanocontas;
    private List<Centrocustoplanocontas> centrosCusto;
    private String ocorrencia;
    @EJB
    EmpresaEJB eEJB;
    private String status, por;
    private Date dtIni, dtFim;

    /**
     * Creates a new instance of RecebimentoMB
     */
    public RecebimentoMB() {
        novo();
    }

    public void novo() {
        recebimento = new ContasReceber();
        recebimentos = new ArrayList<ContasReceber>();
        recebimentosSelecionados = new ArrayList<ContasReceber>();
        centrocustoplanocontas = new Centrocustoplanocontas();
        centrosCusto = new ArrayList<Centrocustoplanocontas>();
        recebimento.setNumParcela(0);
        recebimento.setStatus(false);
    }

    @PostConstruct
    public void preCarregar() {
        recebimentos = rEJB.listarContasVencendoHoje();
        calcularTotalReceber();
        calcularTotalRecebido();
        dtIni = new Date();
        dtFim = new Date();
    }

    public String calcularTotalReceber() {
        BigDecimal totalPagar = new BigDecimal(BigInteger.ZERO);
        if (recebimentos.isEmpty()) {
            return Bcrutils.formatarMoeda(new BigDecimal(BigInteger.ZERO));
        } else {
            for (ContasReceber cp : recebimentos) {
                if (cp.getStatus() == false) {
                    totalPagar = totalPagar.add(cp.getValorDuplicata());
                }
            }
            return Bcrutils.formatarMoeda(totalPagar);
        }

    }

    public String calcularTotalRecebido() {
        BigDecimal totalPago = new BigDecimal(BigInteger.ZERO);
        if (recebimentos.isEmpty()) {
            return Bcrutils.formatarMoeda(new BigDecimal(BigInteger.ZERO));
        } else {
            for (ContasReceber cp : recebimentos) {
                if (cp.getStatus() == true) {
                    totalPago = totalPago.add(cp.getValorRecebido());
                }
            }
            return Bcrutils.formatarMoeda(totalPago);
        }
    }

    public String calcularTotalVencido() {
        BigDecimal totalVencido = new BigDecimal(BigInteger.ZERO);
        if (recebimentos.isEmpty()) {
            return Bcrutils.formatarMoeda(new BigDecimal(BigInteger.ZERO));
        } else {
            for (ContasReceber cp : recebimentos) {
                if (cp.getStatus() == false && cp.getDtVencimento().before(new Date())) {
                    totalVencido = totalVencido.add(cp.getValorDuplicata());
                }
            }
            return Bcrutils.formatarMoeda(totalVencido);
        }
        
    }

    public void novoLancamento() {
        recebimento = new ContasReceber();
        recebimento.setNumParcela(0);
        recebimento.setStatus(Boolean.FALSE);
    }

    public void excluir() {

        if (recebimento.getIdContasReceber() != null) {
            try {
                rEJB.Excluir(recebimento, recebimento.getIdContasReceber());
                recebimento = new ContasReceber();
                recebimentosSelecionados = new ArrayList<ContasReceber>();
                recebimentos.remove(recebimento);
                pesquisar();
                MensageFactory.info("Excluído com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Não foi possível excluir!", null);
                Bcrutils.escreveLogErro(e);
            }
        }

    }

    public void lancarRecebimento() {
        try {
            HttpServletRequest request = (HttpServletRequest) FacesContext
                    .getCurrentInstance().getExternalContext().getRequest();
            System.out.println("ID Da Empresa "+request.getParameter("empresa"));
            recebimento.setIdEmpresa(eEJB.SelecionarPorID(Integer.parseInt(request.getParameter("empresa"))));
        } catch (Exception e) {
            System.out.println("Nao foi possivel capturar a empresa!");
        }
        if (recebimento.getIdContasReceber() == null) {
            recebimento.setDtLancamento(new Date());
        }
        if (recebimento.getNumParcela().equals(0)) {
            if (recebimento.getStatus() == true) {
                if (recebimento.getRecebidoCom().equals("C")) {
                    try {
                        if (recebimento.getNumParcela().equals(0)) {
                            recebimento.setNumParcela(null);
                        }
                        System.out.println("salvando o recebimento lancando no caixa MB");
                        rEJB.salvarLancandoNoCaixa(recebimento);
//                        recebimento = new ContasReceber();
                        MensageFactory.info("Pagamento efetuado com sucesso! Entrada no caixa!", null);
                        preCarregar();
                    } catch (Exception e) {
                        MensageFactory.error("Não foi possível pagar com o caixa! Verifique os logs!", null);
                        Bcrutils.escreveLogErro(e);
                    }
                } else {
                    try {
                        if (recebimento.getNumParcela().equals(0)) {
                            recebimento.setNumParcela(null);
                        }
                        rEJB.salvarLancandoNoBanco(recebimento);
//                        recebimento = new ContasReceber();
                        MensageFactory.info("Pagamento efetuado com sucesso! Saída com banco!", null);
                        preCarregar();
                    } catch (Exception e) {
                        MensageFactory.error("Não foi possível pagar com o banco! Verifique os logs!", null);
                        Bcrutils.escreveLogErro(e);
                    }
                }
            } else {
                //Não paga
                try {
                    if (recebimento.getNumParcela().equals(0)) {
                        recebimento.setNumParcela(null);
                    }
                    rEJB.salvarAtualizarNormal(recebimento);
//                    recebimento = new ContasReceber();
                    if (recebimento.getIdContasReceber() == null) {
                        MensageFactory.info("Conta lançada com sucesso!", null);
                    } else {
                        MensageFactory.info("Conta atualizado com sucesso!", null);
                    }
                    preCarregar();
                } catch (Exception e) {
                    if (recebimento.getIdContasReceber() == null) {
                        MensageFactory.error("Nao foi possivel lançar a conta! Verifique o log!", null);
                        System.out.println("Nao foi possivel lancar a conta com uma parcela");
                        Bcrutils.escreveLogErro(e);
                    } else {
                        MensageFactory.error("Nao foi possivel atualizar a conta! Verifique o log!", null);
                        System.out.println("Nao foi possivel lancar a conta com uma parcela");
                        Bcrutils.escreveLogErro(e);
                    }
                }
            }
        } else {
            {
                try {
                    int quantidadeMeses = recebimento.getNumParcela();
                    Date dtPrimeiroVencimento = recebimento.getDtVencimento();
                    Date ultimaData = new Date();
                    Long numLote = new Date().getTime();
                    for (int i = 0; i < quantidadeMeses; i++) {

                        recebimento.setNumParcela(i + 1);
                        recebimento.setParcelaSistema(recebimento.getNumParcela().toString());
                        recebimento.setNumLote(numLote.toString());
                        Date novaData = new Date();
                        if (i == 0) {
                            novaData = dtPrimeiroVencimento;
                        } else {
                            if (getOcorrencia().equals("Diariamente")) {
                                novaData = Bcrutils.addDia(ultimaData, 1);
                            } else if (getOcorrencia().equals("Semanalmente")) {
                                novaData = Bcrutils.addDia(ultimaData, 7);
                            } else if (getOcorrencia().equals("Bimestralmente")) {
                                novaData = Bcrutils.addMes(ultimaData, 2);
                            } else if (getOcorrencia().equals("Trimestralmente")) {
                                novaData = Bcrutils.addMes(ultimaData, 3);
                            } else if (getOcorrencia().equals("Semestralmente")) {
                                novaData = Bcrutils.addMes(ultimaData, 4);
                            } else if (getOcorrencia().equals("Anualmente")) {
                                novaData = Bcrutils.addMes(ultimaData, 12);
                            } else {
                                novaData = Bcrutils.addMes(ultimaData, 1);
                            }
                        }
                        Calendar c = Calendar.getInstance();
                        c.setTime(novaData);
//                        if (c.get(Calendar.DAY_OF_WEEK) == 1) { // verifica se é domingo se for adiciona um dia
//                            novaData = Bcrutils.addDia(novaData, 1);
//                        }
//                        if (c.get(Calendar.DAY_OF_WEEK) == 7) { // verifica se é sabado se for adiciona dois dias
//                            novaData = Bcrutils.addDia(novaData, 2);
//                        }
                        recebimento.setDtVencimento(novaData);
                        ultimaData = novaData;
                        rEJB.salvarAtualizarNormal(recebimento);
                    }
//                    preCarregar();
//                    recebimento = new ContasReceber();
                    MensageFactory.info("Contas lançadas com sucesso!", null);
                } catch (Exception e) {
                    MensageFactory.error("Não foi possível lançar as contas!", null);
                    Bcrutils.escreveLogErro(e);
                }
            }
        }

    }
    
    public void atualizar(){
        try {
            rEJB.salvarAtualizarNormal(recebimento);
            MensageFactory.info("Conta atualizada com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Não foi possível atualizar a conta. Verifique o log!", null);
            Bcrutils.escreveLogErro(e);
        }
    }

    public boolean disable() {
        if (recebimentosSelecionados.isEmpty()) {
            if (recebimentosSelecionados.size() == 1) {
                recebimento = recebimentosSelecionados.get(0);
            }
            return true;
        } else {
            if (recebimentosSelecionados.size() == 1) {
                recebimento = recebimentosSelecionados.get(0);
            }
            return false;
        }
    }

    public boolean verificaSePodeExcluirEeditar() {
        if (recebimentosSelecionados.size() == 1 && recebimento.getStatus().equals(false)) {
            return false;
        } else {
            return true;
        }
    }

    public boolean verificaSePodeVer() {
        if (recebimentosSelecionados.size() == 1) {
            return false;
        } else {
            return true;
        }
    }

    public boolean verificaSePodeEstornar() {
        if (recebimentosSelecionados.size() == 1 && recebimento.getStatus().equals(true)) {
            return false;
        } else {
            return true;
        }
    }

    public void verificarStatus() {
        if (recebimento.getStatus().equals(false)) {
            recebimento.setRecebidoCom("");
        }
    }

    public void verificarQuantidadeDeLancamentos() {
        if (recebimento.getNumParcela() > 1) {
            recebimento.setStatus(false);
        }
    }

    public BigDecimal valorRestanteCentroCusto() {
        BigDecimal val = new BigDecimal(BigInteger.ZERO);
        try {
            if (!recebimento.getCentrocustoplanocontasList().isEmpty()) {
                for (Centrocustoplanocontas cpc : recebimento.getCentrocustoplanocontasList()) {
                    val = val.add(cpc.getValor());
                }
            }
            return recebimento.getValorDuplicata().subtract(val);
        } catch (Exception e) {
            return recebimento.getValorDuplicata();
        }
    }

    public void adicionarCentroCusto() {
        centrosCusto.add(centrocustoplanocontas);
        centrocustoplanocontas = new Centrocustoplanocontas();
        recebimento.setCentrocustoplanocontasList(centrosCusto);
    }

    public void processarListaRecebimento() {
        for (ContasReceber cr : recebimentosSelecionados) {
            cr.setDtPagamento(new Date());
            cr.setValorRecebido(cr.getValorDuplicata());
        }
        List<ContasReceber> contasTemp = new ArrayList<ContasReceber>();
        contasTemp.addAll(recebimentosSelecionados);
        int index = 0;
        for (ContasReceber cp : contasTemp) {
            if (cp.getStatus().equals(true)) {
                recebimentosSelecionados.remove(index);
                index = index + 1;
            } else {
                index = index + 1;
            }
        }
    }

    public void receberConta() {
        try {
            for (ContasReceber cr : recebimentosSelecionados) {
                cr.setStatus(Boolean.TRUE);
                rEJB.receber(cr);
            }
            MensageFactory.info("Recebimento efetuados com sucesso!", null);
            preCarregar();
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar efetuar o recebimento. Verifique o log!", null);
            Bcrutils.escreveLogErro(e);
        }
    }

    public String verificarStatusLabel(ContasReceber contasReceber) {
        if (contasReceber.getStatus() == true) {
            return "label label-success";
        } else if (contasReceber.getStatus() == false && contasReceber.getDtVencimento().before(new Date())) {
            return "label label-danger";
        } else if (contasReceber.getStatus() == false && contasReceber.getDtVencimento().after(new Date())) {
            return "label label-primary";
        } else {
            return "";
        }
    }

    public String verificarStatusValue(ContasReceber contasReceber) {
        if (contasReceber.getStatus() == true) {
            return "Recebido";
        } else if (contasReceber.getStatus() == false && contasReceber.getDtVencimento().before(new Date())) {
            return "Vencido";
        } else if (contasReceber.getStatus() == false && contasReceber.getDtVencimento().after(new Date())) {
            return "Aberto";
        } else {
            return "";
        }
    }

    public void limparListaSelecionados() {
        recebimentosSelecionados = new ArrayList<ContasReceber>();
    }

    public void estornar() {
        if (recebimento.getIdContasReceber() != null) {
            try {
                recebimento.setStatus(false);
                recebimento.setValorRecebido(null);
                recebimento.setDtPagamento(null);
                recebimento.setRecebidoCom(null);
                recebimento.setIdBanco(null);
                recebimento.setIdContaCorrente(null);
                rEJB.estornar(recebimento);
                MensageFactory.info("Conta estornada com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Não foi possível estornar a conta!", null);
                Bcrutils.escreveLogErro(e);
            }
            System.out.println("foi no if");
        } else {
            System.out.println("foi no else");
        }
    }

    public void receber(ContasReceber cr) {
        try {
            cr.setValorRecebido(cr.getValorDuplicata());
            cr.setDtPagamento(new Date());
            cr.setStatus(true);
            cr.setRecebidoCom("C");
            rEJB.receber(cr);
            MensageFactory.info("Recebimento efetuado com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar receber a conta!", null);
            System.out.println("O erro ocorrido é:");
            System.out.println(e);
        }
    }

    public void adiar(ContasReceber cr) {
        try {
            cr.setDtVencimento(Bcrutils.addDia(cr.getDtVencimento(), 1));
            rEJB.Atualizar(cr);
            MensageFactory.info("Conta adiada com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar adiar a conta!", null);
            System.out.println("O erro ocorrido é:");
            System.out.println(e);
        }
    }

    public void pesquisar() {
        try {
            recebimentos = rEJB.pesquisar(recebimento, por, status, dtIni, dtFim);
        } catch (Exception e) {
            Bcrutils.escreveLogErro(e);
        }
    }

    public List<ContasReceber> listarContasVencendoHoje() {
        return rEJB.listarContasVencendoHoje();
    }
 
    public int contarContasVencendoHoje() {
        return rEJB.listarContasVencendoHoje().size();
    }

    public Long totalParcela(Integer idCliente, String Dcto){
        return rEJB.contaQuantidadeParcela(idCliente, Dcto);
    }
    
//    ----------------------------------------------------------------------------------
    public ContasReceber getRecebimento() {
        return recebimento;
    }

    public void setRecebimento(ContasReceber recebimento) {
        this.recebimento = recebimento;
    }

    public List<ContasReceber> getRecebimentos() {
        return recebimentos;
    }

    public void setRecebimentos(List<ContasReceber> recebimentos) {
        this.recebimentos = recebimentos;
    }

    public List<ContasReceber> getRecebimentosSelecionados() {
        return recebimentosSelecionados;
    }

    public void setRecebimentosSelecionados(List<ContasReceber> recebimentosSelecionados) {
        this.recebimentosSelecionados = recebimentosSelecionados;
    }

    public String getOcorrencia() {
        return ocorrencia;
    }

    public void setOcorrencia(String ocorrencia) {
        this.ocorrencia = ocorrencia;
    }

    public Centrocustoplanocontas getCentrocustoplanocontas() {
        return centrocustoplanocontas;
    }

    public void setCentrocustoplanocontas(Centrocustoplanocontas centrocustoplanocontas) {
        this.centrocustoplanocontas = centrocustoplanocontas;
    }

    public List<Centrocustoplanocontas> getCentrosCusto() {
        return centrosCusto;
    }

    public void setCentrosCusto(List<Centrocustoplanocontas> centrosCusto) {
        this.centrosCusto = centrosCusto;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPor() {
        return por;
    }

    public void setPor(String por) {
        this.por = por;
    }

    public Date getDtIni() {
        return dtIni;
    }

    public void setDtIni(Date dtIni) {
        this.dtIni = dtIni;
    }

    public Date getDtFim() {
        return dtFim;
    }

    public void setDtFim(Date dtFim) {
        this.dtFim = dtFim;
    }
}
