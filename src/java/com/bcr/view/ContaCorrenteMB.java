package com.bcr.view;

import com.bcr.controller.BancoEJB;
import com.bcr.controller.ContaCorrenteEJB;
import com.bcr.controller.EmpresaEJB;
import com.bcr.model.Banco;
import com.bcr.model.ContaCorrente;
import com.bcr.model.Empresa;
import com.bcr.model.MovimentacaoBancaria;
import com.bcr.util.MensageFactory;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author JOAOPAULO
 */
@ManagedBean
@ViewScoped
public class ContaCorrenteMB implements Serializable {

    /**
     * Creates a new instance of ContaCorrenteMB
     */
    @EJB
    ContaCorrenteEJB ccEJB;
    private ContaCorrente contaCorrente;
    @EJB
    EmpresaEJB eEJB;
    private Empresa empresa;
    @EJB
    BancoEJB bEJB;
    private Banco banco;
    private MovimentacaoBancaria movimentobancario;

    public ContaCorrenteMB() {
        novo();
    }

    public void novo() {
        contaCorrente = new ContaCorrente();
        banco = new Banco();
        empresa = new Empresa();
        movimentobancario = new MovimentacaoBancaria();
    }

    public ContaCorrente getContaCorrente() {
        try {
            if (contaCorrente != null) {
                banco = contaCorrente.getIdBanco();
            }
            movimentobancario = contaCorrente.getMovimentacaoBancariaList().get(0);
           
        } catch (Exception e) {
        }

        return contaCorrente;
    }

    public void setContaCorrente(ContaCorrente contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public MovimentacaoBancaria getMovimentobancario() {
        return movimentobancario;
    }

    public void setMovimentobancario(MovimentacaoBancaria movimentobancario) {
        this.movimentobancario = movimentobancario;
    }

    public void salvar() {
        HttpServletRequest request = (HttpServletRequest) FacesContext
                .getCurrentInstance().getExternalContext().getRequest();
        empresa = eEJB.SelecionarPorID(Integer.parseInt(request.getParameter("id_empresa")));
        contaCorrente.setIdEmpresa(empresa);
        if (banco != null) {
            contaCorrente.setIdBanco(banco);
        }
        if (contaCorrente.getIdContaCorrente() == null) {

            try {
                movimentobancario.setDtMovimentacaoBancariaItem(new Date());
                movimentobancario.setIdContaCorrente(contaCorrente);
                movimentobancario.setIdBanco(banco);
                movimentobancario.setHistorico("Saldo Inicial");
                movimentobancario.setTipo("C");
                movimentobancario.setIdEmpresa(contaCorrente.getIdEmpresa());
                movimentobancario.setDtMovimentacaoBancariaItem(new Date());
                ccEJB.Salvar(contaCorrente);
                MensageFactory.info("Salvo com sucesso!", null);
                movimentobancario.setIdContaCorrente(contaCorrente);
                bEJB.inserirMovimentoBancario(movimentobancario);
                novo();
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar salvar!", null);
                System.out.println("Erro identificado: " + e);
            }
        } else {
            try {
                ccEJB.Atualizar(contaCorrente);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar atualizar!", null);
                System.out.println("Erro identificado: " + e);
            }
        }
    }

    public void excluir() {
        try {
            ccEJB.Excluir(contaCorrente, contaCorrente.getIdContaCorrente());
            MensageFactory.info("Cliente excluído com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.warn("Não foi possível excluir o cliente!", null);
            System.out.println("O erro ocorrido é: " + e);
        }
    }

    public List<ContaCorrente> listarContaCorrente() {
        return ccEJB.ListarTodos();
    }
}
