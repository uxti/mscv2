/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.GrupoEJB;
import com.bcr.controller.SubGrupoEJB;
import com.bcr.model.Grupo;
import com.bcr.model.SubGrupo;
import com.bcr.util.MensageFactory;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author JOAOPAULO
 */
@ManagedBean
@ViewScoped
public class SubGrupoMB implements Serializable {

    @EJB
    SubGrupoEJB sgEJB;
    private SubGrupo subGrupo;
    @EJB
    GrupoEJB gEJB;
    private Grupo grupo;

    public SubGrupoMB() {
        novo();
    }

    public void novo() {
        subGrupo = new SubGrupo();
        grupo = new Grupo();
    }

    public SubGrupo getSubGrupo() {
        try {
            if(subGrupo != null){
            grupo = subGrupo.getIdGrupo();
        }
        } catch (Exception e) {
        }
        
        return subGrupo;
    }

    public void setSubGrupo(SubGrupo subGrupo) {
        this.subGrupo = subGrupo;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public void salvar() {
        if (grupo != null) {
            subGrupo.setIdGrupo(grupo);
        }
        if (subGrupo.getIdSubgrupo() == null) {
            try {
                sgEJB.Salvar(subGrupo);
                novo();
                MensageFactory.info("Salvo com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar salvar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        } else {
            try {
                sgEJB.Atualizar(subGrupo);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar atualizar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        }

    }

    public List<SubGrupo> listarSubGrupos() {
        return sgEJB.ListarTodos();
    }

    public void excluir() {
        try {
            sgEJB.Excluir(subGrupo, subGrupo.getIdSubgrupo());
            MensageFactory.info("Excluido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            System.out.println("Erro ocorrido é: " + e);
        }
    }
}
