/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.CentroCustoEJB;
import com.bcr.controller.CidadeEJB;
import com.bcr.controller.ClienteEJB;
import com.bcr.controller.ClienteResponsavelEJB;
import com.bcr.controller.EmpresaEJB;
import com.bcr.controller.EstadoEJB;
import com.bcr.controller.TelefoneEJB;
import com.bcr.model.CentroCusto;
import com.bcr.model.Cidade;
import com.bcr.model.Cliente;
import com.bcr.model.ClienteResponsavel;
import com.bcr.model.Empresa;
import com.bcr.model.Estado;
import com.bcr.model.Fornecedor;
import com.bcr.model.Telefone;
import com.bcr.util.MensageFactory;
import com.bcr.validator.ValidaCNPJ;
import com.bcr.validator.ValidarCPF;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class ClienteMB implements Serializable {

    @EJB
    ClienteEJB cEJB;
    private Cliente cliente;
    private Cidade cidade;
    private Cidade cidadeCobranca;
    @EJB
    CidadeEJB ciEJB;
    private Telefone telefone;
    private List<Telefone> listaDeTelefones;
    @EJB
    TelefoneEJB tEJB;
    private ClienteResponsavel responsavel;
    private List<ClienteResponsavel> responsaveis;
    @EJB
    ClienteResponsavelEJB crEJB;
    private CentroCusto centroCusto;
    private List<CentroCusto> centrosDeCusto;
    @EJB
    CentroCustoEJB ccEJB;
    @EJB
    EstadoEJB ufEJB;
    private LazyDataModel<Cliente> clientesLazy;
    private List<Cliente> clientes;
    @EJB
    EmpresaEJB empEJB;
    private Empresa empresa;

    /**
     * Creates a new instance of ClienteEJB
     */
    public ClienteMB() {
        novo();
    }

    public void novo() {
        cliente = new Cliente();
        cliente.setDtCadastro(new Date());
        cliente.setTipo("F");
        cliente.setInativo("N");
        cliente.setBloqueado("N");
        cidade = new Cidade();
        cidadeCobranca = new Cidade();
        listaDeTelefones = new ArrayList<Telefone>();
        telefone = new Telefone();
        responsavel = new ClienteResponsavel();
        responsaveis = new ArrayList<ClienteResponsavel>();
        centroCusto = new CentroCusto();
        centrosDeCusto = new ArrayList<CentroCusto>();

        clientes = new ArrayList<Cliente>();
        empresa = new Empresa();
        carregarDatatableCidade();
        carregarDatatableCliente();
    }

    public Cliente getCliente() {
        if (cliente != null) {
            if (cliente.getIdCidade() != null) {
                cidade = cliente.getIdCidade();
            }
            if (cliente.getIdCidadeCobranca() != null) {
                cidadeCobranca = cliente.getIdCidadeCobranca();
            }
            if (cliente.getTelefoneList() != null) {
                listaDeTelefones = cliente.getTelefoneList();
            }
            if (cliente.getClienteResponsavelList() != null) {
                responsaveis = cliente.getClienteResponsavelList();
            }
            if (cliente.getCentroCustoList() != null) {
                centrosDeCusto = cliente.getCentroCustoList();
            }
            try {
                return cliente;
            } catch (Exception e) {
                return new Cliente();
            }
        } else {
            return new Cliente();
        }
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        System.out.println(cidade.getNome());
        this.cidade = cidade;
    }

    public Cidade getCidadeCobranca() {
        return cidadeCobranca;
    }

    public void setCidadeCobranca(Cidade cidadeCobranca) {
        this.cidadeCobranca = cidadeCobranca;
    }

    public Telefone getTelefone() {
        return telefone;
    }

    public void setTelefone(Telefone telefone) {
        this.telefone = telefone;
    }

    public List<Telefone> getListaDeTelefones() {
        return listaDeTelefones;
    }

    public void setListaDeTelefones(List<Telefone> listaDeTelefones) {
        this.listaDeTelefones = listaDeTelefones;
    }

    public ClienteResponsavel getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(ClienteResponsavel responsavel) {
        this.responsavel = responsavel;
    }

    public List<ClienteResponsavel> getResponsaveis() {
        return responsaveis;
    }

    public void setResponsaveis(List<ClienteResponsavel> responsaveis) {
        this.responsaveis = responsaveis;
    }

    public LazyDataModel<Cliente> getClientesLazy() {
        return clientesLazy;
    }

    public void setClientesLazy(LazyDataModel<Cliente> clientesLazy) {
        this.clientesLazy = clientesLazy;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public void salvar() {

        HttpServletRequest request = (HttpServletRequest) FacesContext
                .getCurrentInstance().getExternalContext().getRequest();
        empresa = empEJB.SelecionarPorID(Integer.parseInt(request.getParameter("id_empresa")));
        if (cliente.getIdCliente() == null) {
            try {
                if (cidade.getIdCidade() != null) {
                    cliente.setIdCidade(cidade);
                }
                if (cidadeCobranca.getIdCidade() != null) {
                    cliente.setIdCidadeCobranca(cidadeCobranca);
                }
                if (empresa.getIdEmpresa() != null) {
                    cliente.setIdEmpresa(empresa);
                }
                cEJB.Salvar(cliente, listaDeTelefones, responsaveis, centrosDeCusto);
                novo();
                MensageFactory.info("Salvo com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar salvar!", null);
                System.out.println("Erro identificado: " + e);
            }
        } else {
            try {
                cliente.setDtUltimaAtualizacao(new Date());
                if (cidade.getIdCidade() != null) {
                    cliente.setIdCidade(cidade);
                }
                if (cidadeCobranca.getIdCidade() != null) {
                    cliente.setIdCidadeCobranca(cidadeCobranca);
                }
                if (empresa.getIdEmpresa() != null) {
                    cliente.setIdEmpresa(empresa);
                }
                cEJB.Salvar(cliente, listaDeTelefones, responsaveis, centrosDeCusto);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar atualizar!", null);
                System.out.println("Erro identificado: " + e);
            }
        }
    }

    public CentroCusto getCentroCusto() {
        return centroCusto;
    }

    public void setCentroCusto(CentroCusto centroCusto) {
        this.centroCusto = centroCusto;
    }

    public List<CentroCusto> getCentrosDeCusto() {
        return centrosDeCusto;
    }

    public void setCentrosDeCusto(List<CentroCusto> centrosDeCusto) {
        this.centrosDeCusto = centrosDeCusto;
    }

    public void pesquisarCEP(Integer enderecoTipo) throws MalformedURLException, DocumentException {
        if (enderecoTipo == 1) {
            String cep = cliente.getCep();
            if (cep != null) {
                String cep2 = cep.replace("-", "");
                URL url = new URL("http://viacep.com.br/ws/" + cep2 + "/xml/");
                Document document = getDocumento(url);
                Element root = document.getRootElement();
                cidade = new Cidade();
                String ibge = "";
                String uf = "";
                String nomeCidade = "";
                for (Iterator i = root.elementIterator(); i.hasNext();) {
                    Element element = (Element) i.next();
                    if (element.getQualifiedName().equals("logradouro")) {
                        cliente.setLogradouro(element.getText());
                        System.out.println(element.getText());
                    }
                    if (element.getQualifiedName().equals("bairro")) {
                        cliente.setBairro(element.getText());
                    }
                    if (element.getQualifiedName().equals("ibge")) {
                        ibge = element.getText();
                    }
                    if (element.getQualifiedName().equals("uf")) {
                        uf = element.getText();
                    }
                    if (element.getQualifiedName().equals("localidade")) {
                        nomeCidade = element.getText();
                        cidade = ciEJB.pesquisarCidadePorNome(nomeCidade);
                    }
                }
                if (cidade.getIdCidade() == null) {
                    Cidade c = new Cidade();
                    c.setNome(nomeCidade);
                    c.setCep(cep);
                    c.setCodigoIBGE(ibge);
                    Estado ee = new Estado();
                    System.out.println("sigla pesquisada é: " + uf);
                    ee = ufEJB.pesquisarPorSigla(uf);
                    c.setIdEstado(ee);
                    ciEJB.Salvar(c);
                    cidade = c;
                }
            } else {
                System.out.println("cep normal é nulo");
            }

        } else {
            String cep = cliente.getCEPcobranca();
            if (cep != null) {
                String cep2 = cep.replace("-", "");
                URL url = new URL("http://viacep.com.br/ws/" + cep2 + "/xml/");
                Document document = getDocumento(url);
                Element root = document.getRootElement();
                cidadeCobranca = new Cidade();
                String ibge = "";
                String uf = "";
                String nomeCidade = "";
                for (Iterator i = root.elementIterator(); i.hasNext();) {
                    Element element = (Element) i.next();

                    if (element.getQualifiedName().equals("logradouro")) {
                        cliente.setLogradouroCobranca(element.getText());
                    }
                    if (element.getQualifiedName().equals("bairro")) {
                        cliente.setBairroCobranca(element.getText());
                    }
                    if (element.getQualifiedName().equals("ibge")) {
                        ibge = element.getText();
                    }
                    if (element.getQualifiedName().equals("uf")) {
                        uf = element.getText();
                    }
                    if (element.getQualifiedName().equals("localidade")) {
                        nomeCidade = element.getText();
                        cidadeCobranca = ciEJB.pesquisarCidadePorNome(nomeCidade);
                    }
                }
                if (cidadeCobranca.getIdCidade() == null) {
                    Cidade c = new Cidade();
                    c.setNome(nomeCidade);
                    c.setCep(cep);
                    c.setCodigoIBGE(ibge);
                    Estado ee = new Estado();
                    ee = ufEJB.pesquisarPorSigla(uf);
                    c.setIdEstado(ee);
                    ciEJB.Salvar(c);
                    cidadeCobranca = c;
                }
            } else {
                System.out.println("cep cobrança nulo");
            }

        }
    }

    public static Document getDocumento(URL url) throws DocumentException {
        SAXReader reader = new SAXReader();
        Document document = reader.read(url);
        return document;
    }

    public void adicionarTelefone() {
        System.out.println(telefone.getNumero());
        listaDeTelefones.add(telefone);
        telefone = new Telefone();


    }

    public void excluirTelefone(Telefone t, int index) {
        try {
            if (t.getIdTelefone() != null) {
                tEJB.Excluir(t, t.getIdTelefone());
            }
            listaDeTelefones.remove(index);
            MensageFactory.info("Telefone removido com sucesso!", null);
        } catch (Exception e) {
        }

    }

    public void adicionarResponsavel() {
        System.out.println(responsavel.getNome());
        responsaveis.add(responsavel);
        responsavel = new ClienteResponsavel();
    }

    public void excluirResposavel(ClienteResponsavel cr, int index) {
        try {
            if (cr.getIdClienteResponsavel() != null) {
                crEJB.Excluir(cr, index);
            }
            responsaveis.remove(index);
            MensageFactory.info("Responsavel removido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.fatal("Erro ao tentar excluir o responsavel!", null);
            System.out.println("Erro ocorrido no metodo excluirResposavel: " + e);
        }
    }

    public void adicionarCentroCusto() {
        centrosDeCusto.add(centroCusto);
        centroCusto = new CentroCusto();
    }

    public void excluirCentroCusto(CentroCusto centroCusto, int index) {
        try {
            if (centroCusto.getIdCentroCusto() != null) {
                ccEJB.Excluir(centroCusto, index);
            }
            centrosDeCusto.remove(index);
            MensageFactory.info("Centro de Custo removido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.fatal("Erro ao tentar excluir o centro de custo!", null);
            System.out.println("Erro ocorrido no metodo excluirCentroCusto: " + e);
        }
    }

    public void validarCPFCNPJ() {
        if (cliente.getTipo().equals("F")) {
            ValidarCPF vCPF = new ValidarCPF();
            if (cliente.getCgccpf() != null) {
                if (vCPF.isCPF(cliente.getCgccpf())) {
                    MensageFactory.info("CPF Válido!", null);
                } else {
                    MensageFactory.warn("CPF Inválido!", null);
                }
            }
        } else {
            ValidaCNPJ cCNPJ = new ValidaCNPJ();
            if (cliente.getCgccpf() != null) {
                if (cCNPJ.isCNPJ(cliente.getCgccpf())) {
                    MensageFactory.info("CNPJ Válido!", null);
                } else {
                    MensageFactory.warn("CNPJ Inválido!", null);
                }
            }
        }
    }

    public void validarCPF() {
        ValidarCPF vCPF = new ValidarCPF();
        if (vCPF.isCPF(responsavel.getCpf())) {
            MensageFactory.info("CPF Válido!", null);
        } else {
            MensageFactory.warn("CPF Inválido!", null);
        }
    }

    public void validarCPF(String cpf) {
        ValidarCPF vCPF = new ValidarCPF();
        if (vCPF.isCPF(cpf)) {
            MensageFactory.info("CPF Válido!", null);
        } else {
            MensageFactory.warn("CPF Inválido!", null);
        }
    }

    public void importarDadosDoFornecedorParaCliente() {
        if (cliente.getIdFornecedor() != null) {
            Fornecedor f = new Fornecedor();
            f = cliente.getIdFornecedor();
            cliente.setNome(f.getNome());
            cliente.setTipo(f.getTipoFornecedor());
            cliente.setCgccpf(f.getCgccpf());
            cliente.setEmail(f.getEmail());
            cliente.setCep(f.getCep());
            cliente.setTipoLogradouro(f.getTipoLogradouro());
            cliente.setLogradouro(f.getLogradouro());
            cliente.setNumero(f.getNumero());
            cliente.setBairro(f.getBairro());
            cliente.setIdCidade(f.getIdCidade());
            MensageFactory.info("Dados do fornecedor importados com sucesso!", null);
        } else {
            MensageFactory.warn("Informe o fornecedor para importar os dados!", null);
        }
    }

    public void carregarDatatableCliente() {
        clientesLazy = new LazyDataModel<Cliente>() {
            private static final long serialVersionUID = 1L;

            public List<Cliente> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and c." + filterProperty + " like'%" + filterValue + "%'");
                        } else {
                            sf.append(" where c." + filterProperty + " like'%" + filterValue + "%'");
                        }
                    } else {
                        sf.append(" and c." + filterProperty + " like'%" + filterValue + "%'");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by c." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by c." + sortField + " desc");
                }
                Clausula = sf.toString();

                if (Clausula.contains("_")) {
                    Clausula = Clausula.replace("_", "");
                }
                if (Clausula.contains("() -")) {
                    Clausula = Clausula.replace("() -", "");
                }
                if (Clausula.contains("..")) {
                    Clausula = Clausula.replace("..", "");
                }
                setRowCount(Integer.parseInt(cEJB.contarRegistros(Clausula).toString()));
                clientes = cEJB.listarClientesLazy(first, pageSize, Clausula);
                return clientes;

            }
        };
    }
    private List<Cidade> cidades;
    private LazyDataModel<Cidade> cidadesLazy;

    public List<Cidade> getCidades() {
        return cidades;
    }

    public void setCidades(List<Cidade> cidades) {
        this.cidades = cidades;
    }

    public LazyDataModel<Cidade> getCidadesLazy() {
        return cidadesLazy;
    }

    public void setCidadesLazy(LazyDataModel<Cidade> cidadesLazy) {
        this.cidadesLazy = cidadesLazy;
    }

    public void carregarDatatableCidade() {

        cidadesLazy = new LazyDataModel<Cidade>() {
            private static final long serialVersionUID = 1L;

            public List<Cidade> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and c." + filterProperty + " like'%" + filterValue + "%'");
                        } else {
                            sf.append(" where c." + filterProperty + " like'%" + filterValue + "%'");
                        }
                    } else {
                        sf.append(" and c." + filterProperty + " like'%" + filterValue + "%'");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by c." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by c." + sortField + " desc");
                }
                Clausula = sf.toString();

                if (Clausula.contains("_")) {
                    Clausula = Clausula.replace("_", "");
                }
                if (Clausula.contains("() -")) {
                    Clausula = Clausula.replace("() -", "");
                }
                if (Clausula.contains("..")) {
                    Clausula = Clausula.replace("..", "");
                }
                setRowCount(Integer.parseInt(ciEJB.contarRegistros(Clausula).toString()));
                cidades = ciEJB.listarClientesLazy(first, pageSize, Clausula);
                return cidades;

            }
        };
    }

    public void excluir() {
        try {
            cEJB.Excluir(cliente, cliente.getIdCliente());
            MensageFactory.info("Cliente excluído com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.warn("Não foi possível excluir o cliente!", null);
            System.out.println("O erro ocorrido é: " + e);
        }
    }

    public List<Cliente> listarTodosClientes() {
        return cEJB.ListarTodos();
    }

   
}
