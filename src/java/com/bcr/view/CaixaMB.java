/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.BancoEJB;
import com.bcr.controller.CaixaEJB;
import com.bcr.controller.EmpresaEJB;
import com.bcr.controller.UsuarioEJB;
import com.bcr.model.Caixa;
import com.bcr.model.CaixaItem;
import com.bcr.model.Empresa;
import com.bcr.model.MovimentacaoBancaria;
import com.bcr.model.PlanoContas;
import com.bcr.model.Usuario;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class CaixaMB implements Serializable {

    @EJB
    CaixaEJB cEJB;
    private Caixa caixa;
    private CaixaItem caixaItem;
    private List<CaixaItem> itensCaixa;
    @EJB
    EmpresaEJB empEJB;
    private Empresa empresa;
    @EJB
    BancoEJB bEJB;
    private MovimentacaoBancaria movimentacaoBancaria;
    private PlanoContas planoContas;
    private Usuario usuario;
    @EJB
    UsuarioEJB uEJB;

    /**
     * Creates a new instance of CaixaMB
     */
    public CaixaMB() {
        caixa = new Caixa();
        caixa.setDtCaixa(new Date());
        caixaItem = new CaixaItem();
        itensCaixa = new ArrayList<CaixaItem>();
        empresa = new Empresa();
        novaTransferencia();
        calcularValoresDoCaixaAtual();
        planoContas = new PlanoContas();
    }

    public void novaTransferencia() {
        movimentacaoBancaria = new MovimentacaoBancaria();
        calcularValoresDoCaixaAtual();
    }

    @PostConstruct
    public void pesquisaMovimentoCaixa() {
//        HttpServletRequest request = (HttpServletRequest) FacesContext
//                .getCurrentInstance().getExternalContext().getRequest();
        Empresa e = getUsuario().getIdEmpresa();
        itensCaixa = new ArrayList<CaixaItem>();
        Caixa c = new Caixa();
        if (caixa.getDtCaixa() == null) {
            caixa.setDtCaixa(new Date());
        }
        c = cEJB.retornaCaixaPorData(caixa.getDtCaixa(), e);
        if (c.getIdCaixa() == null) {
            c.setDtCaixa(new Date());
            c.setIdEmpresa(e);
            cEJB.Salvar(c);
            processarTodosCaixas(e);
            c = cEJB.retornaCaixaPorData(caixa.getDtCaixa(), e);
        } else {

            this.caixa = c;
            if (!caixa.getCaixaItemList().isEmpty()) {
                processaItensDoCaixa(c);
            }
            itensCaixa = c.getCaixaItemList();
            if (itensCaixa.isEmpty()) {
                MensageFactory.warn("Este caixa não possui nenhuma movimentação!", null);
            }
        }
        verificaSePodeLancarSangriaSuprimento();
    }

    public void pesquisaMovimentoCaixa(Date dt, Empresa e) {
        itensCaixa = new ArrayList<CaixaItem>();
        Caixa c = new Caixa();
        c = cEJB.retornaCaixaPorData(dt, e);
        if (c.getIdCaixa() == null) {
            c.setDtCaixa(new Date());
            c.setIdEmpresa(e);
            cEJB.Salvar(c);
            processarTodosCaixas(e);
            c = cEJB.retornaCaixaPorData(dt, e);
        } else {
            this.caixa = c;
            if (!caixa.getCaixaItemList().isEmpty()) {
                processaItensDoCaixa(c);
            }
            itensCaixa = c.getCaixaItemList();
            if (itensCaixa.isEmpty()) {
                MensageFactory.warn("Este caixa não possui nenhuma movimentação!", null);
            }
        }
        verificaSePodeLancarSangriaSuprimento();
    }

    public void processarTodosCaixas(Empresa empresa) {
        BigDecimal saldoCaixaAnterior = new BigDecimal(BigInteger.ZERO);
        for (Caixa c : cEJB.listaCaixasParaProcessar(empresa)) {
            BigDecimal credito = new BigDecimal(BigInteger.ZERO);
            BigDecimal debito = new BigDecimal(BigInteger.ZERO);
            BigDecimal saldoAnterior = new BigDecimal(BigInteger.ZERO);
            for (CaixaItem ci : c.getCaixaItemList()) {
                if (ci.getTipo().equals("C")) {
                    credito = credito.add(ci.getValor());
                } else {
                    debito = debito.add(ci.getValor());
                }
            }
            c.setTotalEntradas(credito);
            c.setTotalSaidas(debito);
            c.setSaldoAnterior(saldoAnterior);
            c.setSaldoAtual(credito.subtract(debito));
            c.setSaldoAnterior(saldoCaixaAnterior);
            c.setSaldoAtual(c.getSaldoAnterior().add(credito.subtract(debito)));
            cEJB.Atualizar(c);
            saldoCaixaAnterior = c.getSaldoAtual();
        }
    }

    public void processarTodosCaixas20UltimosDias(Empresa empresa) {
        BigDecimal saldoCaixaAnterior = new BigDecimal(BigInteger.ZERO);
        for (Caixa c : cEJB.listaCaixasParaProcessar20DiasAntes(empresa)) {
            BigDecimal credito = new BigDecimal(BigInteger.ZERO);
            BigDecimal debito = new BigDecimal(BigInteger.ZERO);
            BigDecimal saldoAnterior = new BigDecimal(BigInteger.ZERO);
            for (CaixaItem ci : c.getCaixaItemList()) {
                if (ci.getTipo().equals("C")) {
                    credito = credito.add(ci.getValor());
                } else {
                    debito = debito.add(ci.getValor());
                }
            }
            c.setTotalEntradas(credito);
            c.setTotalSaidas(debito);
            c.setSaldoAnterior(saldoAnterior);
            c.setSaldoAtual(credito.subtract(debito));
            c.setSaldoAnterior(saldoCaixaAnterior);
            c.setSaldoAtual(c.getSaldoAnterior().add(credito.subtract(debito)));
            cEJB.Atualizar(c);
            saldoCaixaAnterior = c.getSaldoAtual();
        }
    }

    public void processaItensDoCaixa(Caixa c) {
        BigDecimal credito = new BigDecimal(BigInteger.ZERO);
        BigDecimal debito = new BigDecimal(BigInteger.ZERO);
        for (CaixaItem ci : c.getCaixaItemList()) {
            if (ci.getTipo().equals("C")) {
                credito = credito.add(ci.getValor());
            } else {
                debito = debito.add(ci.getValor());
            }
        }
        c.setTotalEntradas(credito);
        c.setTotalSaidas(debito);
        try {
            c.setSaldoAtual(c.getSaldoAnterior().add(credito.subtract(debito)));
            System.out.println(credito);
            System.out.println(debito);
        } catch (Exception e) {
            c.setSaldoAtual(new BigDecimal(BigInteger.ZERO));
        }

        cEJB.Atualizar(c);
    }

    public void lancarSangria() {
        try {
            caixaItem.setIdCaixa(caixa);
            caixaItem.setTipo("D");
            caixaItem.setIdPlanoContas(planoContas);
            cEJB.inserirMovimentacao(caixaItem);
            MensageFactory.info("Sangria lançado com sucesso!", null);
            pesquisaMovimentoCaixa(caixaItem.getIdCaixa().getDtCaixa(), caixaItem.getIdCaixa().getIdEmpresa());
        } catch (Exception e) {
            MensageFactory.info("Erro ao lançar a sangria!", null);
            System.out.println("O erro ocorrido é: " + e);
            System.out.println(e);
        }
    }

    public void lancarSuprimento(Caixa caixa) {
        try {
            caixaItem.setIdCaixa(caixa);
//            caixaItem.setHistorico("Suprimento referente à :" + caixaItem.getHistorico());
            caixaItem.setTipo("C");
            caixaItem.setIdPlanoContas(planoContas);
            cEJB.inserirMovimentacao(caixaItem);
            MensageFactory.info("Suprimento lançado com sucesso!", null);
            pesquisaMovimentoCaixa(caixaItem.getIdCaixa().getDtCaixa(), caixaItem.getIdCaixa().getIdEmpresa());
        } catch (Exception e) {
            MensageFactory.info("Erro ao lançar o suprimento!", null);
            System.out.println("O erro ocorrido é: " + e);
            System.out.println(e);
        }
    }

    public Caixa retornaCaixaAnterior(Caixa caixa, Integer dia) {
        Caixa caixaAnterior = cEJB.retornaCaixaPorData(Bcrutils.addDia(caixa.getDtCaixa(), -dia), caixa.getIdEmpresa());
        return caixaAnterior;
    }

    public String defineCor(String tipo) {
        if (tipo.equals("C")) {
            return "color: blue;";
        } else {
            return "color: red;";
        }
    }

    public boolean verificaSePodeLancarSangriaSuprimento() {
        String dataCaixa = Bcrutils.formataData(caixa.getDtCaixa());
        String dataAtual = Bcrutils.formataData(new Date());
        if (caixa.getIdCaixa() != null && dataCaixa.equals(dataAtual)) {
            return false;
        } else {
            return true;
        }
    }

    public void lancarTransferencia() {
        try {
            HttpServletRequest request = (HttpServletRequest) FacesContext
                    .getCurrentInstance().getExternalContext().getRequest();
            Empresa e = empEJB.SelecionarPorID(Integer.parseInt(request.getParameter("id_empresa")));
            movimentacaoBancaria.setIdPlanoContas(planoContas);
            movimentacaoBancaria.setDtMovimentacaoBancariaItem(new Date());
            movimentacaoBancaria.setTipo("C");
            movimentacaoBancaria.setIdEmpresa(e);
            movimentacaoBancaria.setTipoMovimento("Transferencia");
            CaixaItem ci = new CaixaItem();
            ci.setIdCaixa(caixa);
            ci.setValor(movimentacaoBancaria.getValor());
            ci.setHistorico("Transferencia Bancária: " + movimentacaoBancaria.getHistorico());
            ci.setTipo("D");
            ci.setIdPlanoContas(planoContas);
            bEJB.inserirMovimentoBancario(movimentacaoBancaria);
            cEJB.inserirMovimentacao(ci);
            MensageFactory.info("Transferência efetuada com sucesso!", null);
            pesquisaMovimentoCaixa(ci.getIdCaixa().getDtCaixa(), ci.getIdCaixa().getIdEmpresa());
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar transferir!", null);
            System.out.println("O erro ocorrido é: " + e);
            System.out.println(e);
        }
    }

    public void calcularValoresDoCaixaAtual() {
        if (caixa.getIdCaixa() != null) {
            processaItensDoCaixa(caixa);
        }

    }

    public String retornaUsuario() {
        String usuarioLog = "";
        Object usuarioLogado = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (usuarioLogado instanceof UserDetails) {
            return usuarioLog = ((UserDetails) usuarioLogado).getUsername();
        } else {
            return usuarioLog = usuarioLogado.toString().toUpperCase();
        }
    }

    public Usuario getUsuario() {
        try {
            if (usuario.getIdUsuario() == null) {
                return usuario = (Usuario) uEJB.pegaUsuarioLogado();
            } else {
                return usuario;
            }
        } catch (Exception e) {
            return usuario = (Usuario) uEJB.pegaUsuarioLogado();
        }

    }

    public Caixa getCaixa() {
        return caixa;
    }

    public void setCaixa(Caixa caixa) {
        this.caixa = caixa;
    }

    public CaixaItem getCaixaItem() {
        return caixaItem;
    }

    public void setCaixaItem(CaixaItem caixaItem) {
        this.caixaItem = caixaItem;
    }

    public List<CaixaItem> getItensCaixa() {
        return itensCaixa;
    }

    public void setItensCaixa(List<CaixaItem> itensCaixa) {
        this.itensCaixa = itensCaixa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public List<Caixa> listaCaixas(Empresa e) {
        return cEJB.listaCaixas(e);
    }

    public void novoItem() {
        caixaItem = new CaixaItem();
    }

    public MovimentacaoBancaria getMovimentacaoBancaria() {
        return movimentacaoBancaria;
    }

    public void setMovimentacaoBancaria(MovimentacaoBancaria movimentacaoBancaria) {
        this.movimentacaoBancaria = movimentacaoBancaria;
    }

    public PlanoContas getPlanoContas() {
        return planoContas;
    }

    public void setPlanoContas(PlanoContas planoContas) {
        this.planoContas = planoContas;
    }
}
