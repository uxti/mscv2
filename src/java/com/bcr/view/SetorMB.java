
package com.bcr.view;

import com.bcr.controller.EmpresaEJB;
import com.bcr.controller.SetorEJB;
import com.bcr.model.Empresa;
import com.bcr.model.Setor;
import com.bcr.util.MensageFactory;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author JOAOPAULO
 */
@ManagedBean
@ViewScoped

public class SetorMB implements Serializable{

    /**
     * Creates a new instance of SetorMB
     */
    @EJB
    SetorEJB sEJB;
    private Setor setor;
    @EJB
    EmpresaEJB eEJB;
    private Empresa empresa;
    
    public SetorMB() {
        novo();
    }

    public void novo(){
        setor = new Setor();
        empresa = new Empresa();
    }
    public Setor getSetor() {
        return setor;
    }

    public void setSetor(Setor setor) {
        this.setor = setor;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
    
    public void salvar(){
        HttpServletRequest request = (HttpServletRequest) FacesContext
                .getCurrentInstance().getExternalContext().getRequest();
        empresa = eEJB.SelecionarPorID(Integer.parseInt(request.getParameter("id_empresa")));
        setor.setIdEmpresa(empresa);
        if(setor.getIdSetor() == null){
            try {
                sEJB.Salvar(setor);
                novo();
                MensageFactory.info("Salvo efetuado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar salvar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        }else{
            try {
                sEJB.Atualizar(setor);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar atualizar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        }
    }
    
    public void excluir(){
        try {
            sEJB.Excluir(setor, setor.getIdSetor());
            MensageFactory.info("Excluido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            System.out.println("Erro ocorrido é: " + e);
        }
    }
    
    public List<Setor> listarsetores(){
        return sEJB.ListarTodos();
    }
}
