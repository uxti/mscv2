package com.bcr.view;

import com.bcr.controller.*;
import com.bcr.model.*;
import com.bcr.util.MensageFactory;
import com.bcr.util.UsuarioLogado;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class ChequeMB {

    @EJB
    ChequeEJB chequeEJB;
    private Cheque cheque;
    private Cheque chequeExcluir;
    @EJB
    ContaCorrenteEJB contaCorrenteEJB;
    private ContaCorrente contaCorrente;
    @EJB
    FornecedorEJB fornecedorEJB;
    private Fornecedor fornecedor;
    @EJB
    PagamentoEJB pagamentoEJB;
    private ContasReceber contasReceber;
    private Banco banco;
    private Colaborador colaborador;
    private Usuario usuario;
    private UsuarioLogado usuarioLogado;
    private ContasPagar contasPagar;
    private List<Cheque> listaCheque;
    private List<Colaborador> listaColaborador;
    private List<ContaCorrente> listaContaCorrente;
    private List<Fornecedor> listaFornecedor;
    private Double valorTotal;
    private boolean flag;
    private BigDecimal widgetValorPago;
    private BigDecimal widgetValorTotal;
    private BigDecimal widgetValorRestante;

    public ChequeMB() {
        novo();
    }

    public void novo() {
        cheque = new Cheque();
        chequeExcluir = new Cheque();
        colaborador = new Colaborador();
        usuario = new Usuario();
        contaCorrente = new ContaCorrente();
        fornecedor = new Fornecedor();
        usuarioLogado = new UsuarioLogado();
        contasPagar = new ContasPagar();
        contasReceber = new ContasReceber();
        banco = new Banco();
        listaCheque = new ArrayList<Cheque>();
        listaContaCorrente = new ArrayList<ContaCorrente>();
        listaFornecedor = new ArrayList<Fornecedor>();
        valorTotal = 0D;
        flag = false;
    }

    public void Salvar() {
        try {
            if (colaborador.getIdColaborador() != null) {
                cheque.setIdColaborador(colaborador);
            }
            if (fornecedor.getIdFornecedor() != null) {
                cheque.setIdFornecedor(fornecedor);
            }
            if (contaCorrente.getIdContaCorrente() != null) {
                cheque.setConta(contaCorrente.getNumConta());
                cheque.setIdContaCorrente(contaCorrente);
                banco = contaCorrente.getIdBanco();
            }
            if (cheque.getTipo() == null) {
                cheque.setTipo("E");
            }
            cheque.setCompensado("N");
            chequeEJB.Salvar(cheque);
            MensageFactory.addMensagemPadraoSucesso("salvar");
        } catch (Exception e) {
            MensageFactory.addMensagemPadraoErro("salvar");
            System.out.println(e);
        }
    }

    public void SalvarVarios() {
        try {
            for (Cheque chq : listaCheque) {
                chequeEJB.Salvar(chq);
                if (chq.getIdContasPagar() != null) {
                    chq.getIdContasPagar().setDtPagamento(new Date());
                    chq.getIdContasPagar().setIdContaCorrente(chq.getIdContaCorrente());
                    chq.getIdContasPagar().setIdBanco(chq.getIdContaCorrente().getIdBanco());
                    chq.getIdContasPagar().setStatus(Boolean.TRUE);
                    chq.getIdContasPagar().setPagoCom("B");
                    chq.getIdContasPagar().setValorPago(chq.getIdContasPagar().getValorDuplicata());
                }
                if (chq.getIdContasPagar() != null) {
                    pagamentoEJB.pagarCheque(chq.getIdContasPagar(), chq);
                } else {
                    pagamentoEJB.lancarMovimentoBancario(chq.getValor(), chq, contaCorrente, null, banco);
                }
            }
            MensageFactory.info("Cheques lançados.", "Pagamentos efetuados com sucesso!");
        } catch (Exception e) {
            MensageFactory.addMensagemPadraoErro("salvar");
            System.out.println(e);
        }
    }

    public void incluir() {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            String dataChequeFormatada = "";
            String dataAtualFormatada = "";

            if (colaborador.getIdColaborador() != null) {
                cheque.setIdColaborador(colaborador);
            }
            if (fornecedor.getIdFornecedor() != null) {
                cheque.setIdFornecedor(fornecedor);
            }
            if (contaCorrente.getIdContaCorrente() != null) {
                cheque.setConta(contaCorrente.getNumConta());
                System.out.println(contaCorrente.getNumConta());
                cheque.setIdContaCorrente(contaCorrente);
            }
            if (cheque.getTipo() == null) {
                cheque.setTipo("E");
            }
            dataChequeFormatada = dateFormat.format(cheque.getDtCompensacaoVencimento());
            dataAtualFormatada = dateFormat.format(new Date());
            if (dataChequeFormatada == dataAtualFormatada) {
                cheque.setCompensado("S");
            } else {
                cheque.setCompensado("N");
            }
            listaCheque.add(cheque);
            carregarPagamentoAposIncluir();
            if (widgetValorRestante.compareTo(BigDecimal.ZERO) == 0) {
                MensageFactory.info("Saldo pago ok!", "Clique em Salvar e Sair para gravar os pagamentos.");
            } else {
                MensageFactory.info("Cheque adicionado.", "Adicione os demais cheques para completar o pagamento.");
            }
        } catch (Exception e) {
            MensageFactory.addMensagemPadraoErro("salvar");
            System.out.println(e);
        }
    }

    public void carregarPagamento(List listaContasPagar) {
        if (listaContasPagar.size() > 1 || listaContasPagar.isEmpty()) {
            MensageFactory.warn("Atenção!", "Para pagamento com vários cheques, selecione somente um Contas a Pagar.");
        } else {
            cheque = new Cheque();
            listaCheque = new ArrayList<Cheque>();
            cheque.setIdContasPagar((ContasPagar) listaContasPagar.get(0));
            if (cheque.getIdContasPagar().getIdFornecedor() != null) {
                cheque.setIdFornecedor(cheque.getIdContasPagar().getIdFornecedor());
            }
            cheque.setDtCompensacaoVencimento(cheque.getIdContasPagar().getDtVencimento());
            cheque.setDtRecebimentoEmissao(new Date());
            cheque.setIdEmpresa(cheque.getIdContasPagar().getIdEmpresa());
            cheque.setTipo("E");
            cheque.setValor(cheque.getIdContasPagar().getValorDuplicata());
            widgetValorTotal = cheque.getIdContasPagar().getValorDuplicata();
            calcularValoresRestantes();
        }
    }

    public void carregarPagamentoAposIncluir() {
        contasPagar = new ContasPagar();
        contaCorrente = new ContaCorrente();
        contasPagar = cheque.getIdContasPagar();
        cheque = new Cheque();
        cheque.setIdContasPagar(contasPagar);
        if (cheque.getIdContasPagar().getIdFornecedor() != null) {
            cheque.setIdFornecedor(cheque.getIdContasPagar().getIdFornecedor());
        }
        cheque.setDtCompensacaoVencimento(cheque.getIdContasPagar().getDtVencimento());
        cheque.setDtRecebimentoEmissao(new Date());
        cheque.setIdEmpresa(cheque.getIdContasPagar().getIdEmpresa());
        cheque.setTipo("E");
        calcularValoresRestantes();
        cheque.setValor(widgetValorRestante);
    }

    public void calcularValoresRestantes() {
        BigDecimal valorPago = BigDecimal.ZERO;
        BigDecimal valorTotal = BigDecimal.ZERO;
        BigDecimal valorRestante = BigDecimal.ZERO;
        for (Cheque ch : listaCheque) {
            valorPago = valorPago.add(ch.getValor());
        }
        valorRestante = widgetValorTotal.subtract(valorPago);
        widgetValorPago = valorPago;
        widgetValorRestante = valorRestante;
    }

    public void excluirTemp() {
        int indice = 0;
        List<Cheque> listaTemp = new ArrayList<Cheque>();
        listaTemp = listaCheque;
        for (Cheque ch : listaTemp) {
            if (ch == chequeExcluir) {
                listaCheque.remove(indice);
                MensageFactory.info("Ok!", "Cheque excluído.");
                break;
            } else {
                indice++;
            }
        }
        calcularValoresRestantes();
        cheque.setValor(widgetValorRestante);
    }

    public void selecionarChequeAExcluir(Cheque ch) {
        chequeExcluir = ch;
    }

    public void zerarChequeAExcluir() {
        chequeExcluir = new Cheque();
    }

    public List carregarCheques() {
        return chequeEJB.ListarTodos();
    }

    public void excluir(Cheque c) {
        try {
            chequeEJB.Excluir(c, c.getIdCheque());
            MensageFactory.addMensagemPadraoSucesso("excluir");
            novo();
        } catch (Exception e) {
            MensageFactory.addMensagemPadraoErro("excluir");
        }
    }

    public Cheque selecionarPorID(Integer id) {
        cheque = (Cheque) chequeEJB.SelecionarPorID(id);
        if (cheque.getIdFornecedor() != null) {
            fornecedor = cheque.getIdFornecedor();
        }
        if (cheque.getIdColaborador() != null) {
            colaborador = cheque.getIdColaborador();
        }
        if (cheque.getIdContaCorrente() != null) {
            contaCorrente = cheque.getIdContaCorrente();
        }
        if (cheque.getIdContasPagar() != null) {
            contasPagar = cheque.getIdContasPagar();
        }
        if (cheque.getIdContasReceber() != null) {
            contasReceber = cheque.getIdContasReceber();
        }
        return cheque;
    }

    public boolean verificaCompensado(Cheque c) {
        String s = chequeEJB.verificarCompensado(c.getIdCheque());
        if (s == null) {
            return false;
        }
        return s.equals("S");
    }

    public void compensarCheque(Cheque c) {
        try {
            chequeEJB.compensar(c, usuarioLogado.retornaUsuario());
            MensageFactory.info("Informação", "Cheque atualizado com sucesso!");
            novo();
        } catch (Exception e) {
            MensageFactory.error("Erro", "Aconteceu algo inesperado ao tentar excluir!");
            System.out.println(e);
        }
    }

    public void selecionarColaborador(Colaborador c) {
        colaborador = c;
    }

    public List listaFornecedores() {
        return fornecedorEJB.ListarTodos();
    }

    public Cheque getCheque() {
        return cheque;
    }

    public void setCheque(Cheque cheque) {
        this.cheque = cheque;
    }

    public Colaborador getColaborador() {
        return colaborador;
    }

    public void setColaborador(Colaborador colaborador) {
        this.colaborador = colaborador;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public ContaCorrente getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(ContaCorrente contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public UsuarioLogado getUsuarioLogado() {
        return usuarioLogado;
    }

    public void setUsuarioLogado(UsuarioLogado usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }

    public ContasReceber getContasReceber() {
        return contasReceber;
    }

    public void setContasReceber(ContasReceber contasReceber) {
        this.contasReceber = contasReceber;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public ContasPagar getContasPagar() {
        return contasPagar;
    }

    public void setContasPagar(ContasPagar contasPagar) {
        this.contasPagar = contasPagar;
    }

    public List<Cheque> getListaCheque() {
        return listaCheque;
    }

    public void setListaCheque(List<Cheque> listaCheque) {
        this.listaCheque = listaCheque;
    }

    public List<Colaborador> getListaColaborador() {
        return listaColaborador;
    }

    public void setListaColaborador(List<Colaborador> listaColaborador) {
        this.listaColaborador = listaColaborador;
    }

    public List<ContaCorrente> getListaContaCorrente() {
        return listaContaCorrente;
    }

    public void setListaContaCorrente(List<ContaCorrente> listaContaCorrente) {
        this.listaContaCorrente = listaContaCorrente;
    }

    public List<Fornecedor> getListaFornecedor() {
        return listaFornecedor;
    }

    public void setListaFornecedor(List<Fornecedor> listaFornecedor) {
        this.listaFornecedor = listaFornecedor;
    }

    public Cheque getChequeExcluir() {
        return chequeExcluir;
    }

    public void setChequeExcluir(Cheque chequeExcluir) {
        this.chequeExcluir = chequeExcluir;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public BigDecimal getWidgetValorPago() {
        return widgetValorPago;
    }

    public void setWidgetValorPago(BigDecimal widgetValorPago) {
        this.widgetValorPago = widgetValorPago;
    }

    public BigDecimal getWidgetValorTotal() {
        return widgetValorTotal;
    }

    public void setWidgetValorTotal(BigDecimal widgetValorTotal) {
        this.widgetValorTotal = widgetValorTotal;
    }

    public BigDecimal getWidgetValorRestante() {
        return widgetValorRestante;
    }

    public void setWidgetValorRestante(BigDecimal widgetValorRestante) {
        this.widgetValorRestante = widgetValorRestante;
    }

}
