/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.EmpresaEJB;
import com.bcr.controller.NotaObsEJB;
import com.bcr.model.Empresa;
import com.bcr.model.NotaObservacao;
import com.bcr.util.MensageFactory;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author JOAOPAULO
 */
@ManagedBean
@ViewScoped
public class NotaObservacaoMB {

    /**
     * Creates a new instance of NotaObservacaoMB
     */
    @EJB
    NotaObsEJB noEJB;
    private NotaObservacao notaObersavacao;
    @EJB
    EmpresaEJB eEJB;
    private Empresa empresa;
    public NotaObservacaoMB() {
        novo();
    }

    public void novo(){
        notaObersavacao = new NotaObservacao();
        empresa = new Empresa();
    }
    public NotaObservacao getNotaObersavacao() {
        return notaObersavacao;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
    
    public void setNotaObersavacao(NotaObservacao notaObersavacao) {
        this.notaObersavacao = notaObersavacao;
    }
    public void salvar(){
        HttpServletRequest request = (HttpServletRequest) FacesContext
                .getCurrentInstance().getExternalContext().getRequest();
        empresa = eEJB.SelecionarPorID(Integer.parseInt(request.getParameter("id_empresa")));
        notaObersavacao.setIdEmpresa(empresa);
        if(notaObersavacao.getIdNotaObservacao() == null){
            try {
                noEJB.Salvar(notaObersavacao);
                novo();
                MensageFactory.info("Salvo efetuado com sucesso! ", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar salvar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        }else{
            try {
                noEJB.Atualizar(notaObersavacao);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar atualizar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        }
    }
    public void excluir(){
        try {
            noEJB.Excluir(notaObersavacao, notaObersavacao.getIdNotaObservacao());
            MensageFactory.info("Excluido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            System.out.println("Erro ocorrido é: " + e);
        }
    }
    public List<NotaObservacao> listarNotasObservacao(){
        return noEJB.ListarTodos();
    }
}
