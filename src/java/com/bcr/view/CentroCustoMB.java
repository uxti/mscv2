/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.CentroCustoEJB;
import com.bcr.model.Agrupamento;
import com.bcr.model.CentroCusto;
import com.bcr.model.CentroCustoAgrupamento;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class CentroCustoMB {

    @EJB
    CentroCustoEJB ccEJB;
    private CentroCusto centroCusto;
    private Agrupamento agrupamento;
    private CentroCustoAgrupamento centroCustoAgrupamento;

    /**
     * Creates a new instance of CentroCustoMB
     */
    public CentroCustoMB() {
        novo();
    }

    public void novo() {
        centroCusto = new CentroCusto();
        agrupamento = new Agrupamento();
        centroCustoAgrupamento = new CentroCustoAgrupamento();
    }

    public CentroCusto getCentroCusto() {
        return centroCusto;
    }

    public void setCentroCusto(CentroCusto centroCusto) {
        this.centroCusto = centroCusto;
    }

    public Agrupamento getAgrupamento() {
        return agrupamento;
    }

    public void setAgrupamento(Agrupamento agrupamento) {
        this.agrupamento = agrupamento;
    }

    public CentroCustoAgrupamento getCentroCustoAgrupamento() {
        return centroCustoAgrupamento;
    }

    public void setCentroCustoAgrupamento(CentroCustoAgrupamento centroCustoAgrupamento) {
        this.centroCustoAgrupamento = centroCustoAgrupamento;
    }

    public List<CentroCusto> listarCentroCusto() {
        return ccEJB.listarCentrosDeCusto();
    }

    public void adicionarAgrupamento(Agrupamento a) {
        List<CentroCustoAgrupamento> lista = new ArrayList<CentroCustoAgrupamento>();
        centroCustoAgrupamento.setIdAgrupamento(a);
        centroCustoAgrupamento.setIdCentroCusto(centroCusto);
        lista.add(centroCustoAgrupamento);
        centroCusto.getCentroCustoAgrupamentoList().addAll(lista);
        try {
            ccEJB.adicionarCentroAgrupamento(centroCusto);
            MensageFactory.info("Agrupamento adicionado com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Este agrupamento ja se encontra neste centro de custo!", null);
        }
    }

    public void excluirCentroAgrupamento(CentroCustoAgrupamento cc) {
        try {
            ccEJB.excluirCentroAgrupamento(cc);
            MensageFactory.info("Removido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar remover!", null);
            Bcrutils.escreveLogErro(e);
        }
    }
}
