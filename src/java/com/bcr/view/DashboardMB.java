/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;


import com.bcr.controller.PagamentoEJB;
import com.bcr.controller.RecebimentoEJB;
import com.bcr.util.Bcrutils;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LegendPlacement;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class DashboardMB implements Serializable{


    @EJB
    PagamentoEJB pEJB;
    @EJB
    RecebimentoEJB rEJB;
    private BarChartModel animatedModel2;

    /**
     * Creates a new instance of DashboardMB
     */
    public DashboardMB() {
    }

    @PostConstruct
    public void init() {
        createAnimatedModels();
    }

    private void createAnimatedModels() {
        animatedModel2 = initBarModel();
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        animatedModel2.setTitle("Relação de Contas Pagas x Contas Recebidas Mensal (" + year + ")");
        animatedModel2.setAnimate(true);
        animatedModel2.setLegendPosition("ne");
        Axis yAxis = animatedModel2.getAxis(AxisType.Y);

        yAxis.setMin(0);
    }

    private BarChartModel initBarModel() {
        Calendar c = Calendar.getInstance();
        int ano = c.get(Calendar.YEAR);
        BarChartModel model = new BarChartModel();

        ChartSeries recebimentos = new ChartSeries();
        recebimentos.setLabel("Recebimentos");
        ChartSeries pagamentos = new ChartSeries();
        pagamentos.setLabel("Pagamentos");
        for (int i = 0; i < 12; i++) {
            BigDecimal contasPagar = new BigDecimal(0);
            try {
                contasPagar = pEJB.contasPagarPorData(Bcrutils.retornaPrimeiroData(i, ano), Bcrutils.retornaUltimoData(i, ano));
            } catch (Exception e) {
                contasPagar = new BigDecimal(0);
            }
            BigDecimal contasReceber = new BigDecimal(0);
            try {
                contasReceber = rEJB.contasReceberPorData(Bcrutils.retornaPrimeiroData(i, ano), Bcrutils.retornaUltimoData(i, ano));
            } catch (Exception e) {
                contasReceber = new BigDecimal(0);
            }
            if (contasPagar == null) {
                contasPagar = new BigDecimal(0);
            }
            if (contasReceber == null) {
                contasReceber = new BigDecimal(0);
            }
            recebimentos.set(Bcrutils.retornaMes(i), contasReceber);
            pagamentos.set(Bcrutils.retornaMes(i), contasPagar);
        }
        model.addSeries(recebimentos);
        model.addSeries(pagamentos);


        return model;
    }

    public BarChartModel getAnimatedModel2() {
        return animatedModel2;
    }
}
