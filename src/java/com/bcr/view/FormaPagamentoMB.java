/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.FormaPagamentoEJB;
import com.bcr.model.FormaPagamento;
import com.bcr.util.MensageFactory;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class FormaPagamentoMB implements Serializable{

    @EJB
    FormaPagamentoEJB fEJB;
    private FormaPagamento formaPagamento;

    /**
     * Creates a new instance of FormaPagamentoMB
     */
    public FormaPagamentoMB() {
        novo();

    }

    public void novo() {
        formaPagamento = new FormaPagamento();
    }

    public FormaPagamento getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(FormaPagamento formaPagamento) {
        this.formaPagamento = formaPagamento;
    }
    
    public void salvar(){
        if(formaPagamento.getIdFormaPagamento() == null){
            try {
                fEJB.Salvar(formaPagamento);
                novo();
                MensageFactory.info("Salvo efetuado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar salvar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        }else{
            try {
                fEJB.Atualizar(formaPagamento);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar atualizar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        }
    }
    
    public void excluir(){
        try {
            fEJB.Excluir(formaPagamento, formaPagamento.getIdFormaPagamento());
            novo();
            MensageFactory.info("Excluido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            System.out.println("Erro ocorrido é: " + e);
        }
    }
    public List<FormaPagamento> listaFormaDePagamentos() {
        return fEJB.ListarTodos();
    }
}
