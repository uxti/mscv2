/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.ProdutoFornecedorEJB;
import com.bcr.model.ProdutoFornecedor;
import com.bcr.util.MensageFactory;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author JOAOPAULO
 */
@ManagedBean
@ViewScoped
public class ProdutoFornecedorMB implements Serializable {

    @EJB
    ProdutoFornecedorEJB pfEJB;
    private ProdutoFornecedor produtofornecedor;

    public ProdutoFornecedorMB() {
        produtofornecedor = new ProdutoFornecedor();
    }

    public ProdutoFornecedor getProdutofornecedor() {
        return produtofornecedor;
    }

    public void setProdutofornecedor(ProdutoFornecedor produtofornecedor) {
        this.produtofornecedor = produtofornecedor;
    }

    public List<ProdutoFornecedor> produtofornecedores() {
        return pfEJB.ListarTodos();
    }

    public void salvar() {
        try {
            pfEJB.Salvar(produtofornecedor);
            MensageFactory.info("Salvo com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Não foi possivel salvar!", null);
            System.out.println("Erro ocorrido foi é: " + e);
        }
    }

    public void excluir() {
        try {
            pfEJB.Excluir(produtofornecedor, produtofornecedor.getIdProdutoFornecedor());
            MensageFactory.info("Excluido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            System.out.println("Erro ocorrido é: " + e);
        }
    }
}
