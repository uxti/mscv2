/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.CfopEJB;
import com.bcr.controller.FornecedorEJB;
import com.bcr.controller.ModeloNfeEJB;
import com.bcr.controller.NotaEntradaEJB;
import com.bcr.controller.NotaEntradaItemEJB;
import com.bcr.controller.ProdutoEJB;
import com.bcr.controller.UsuarioEJB;
import com.bcr.model.Cfop;
import com.bcr.model.Empresa;
import com.bcr.model.Fornecedor;
import com.bcr.model.LocalTrabalho;
import com.bcr.model.ModeloNfe;
import com.bcr.model.NotaEntrada;
import com.bcr.model.NotaEntradaItem;
import com.bcr.model.Produto;
import com.bcr.model.Usuario;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Charles
 */
@ManagedBean
@ViewScoped
public class NotaEntradaMB {

    /* EJB's */
    @EJB
    NotaEntradaEJB notaEntradaEJB;
    @EJB
    NotaEntradaItemEJB NotaEntradaItemEJB;
    @EJB
    ProdutoEJB produtoEJB;
    @EJB
    UsuarioEJB usuarioEJB;
    @EJB
    FornecedorEJB fornecedorEJB;
    @EJB
    ModeloNfeEJB modeloEJB;
    @EJB
    CfopEJB cfopEJB;

    /* Objetos */
    private NotaEntrada notaEntrada;
    private NotaEntradaItem notaEntradaItem;
    private Produto produto;
    private Usuario usuario;
    private LocalTrabalho localTrabalho;
    private Fornecedor fornecedor;
    private Empresa empresa;
    private ModeloNfe modeloNfe;
    private Cfop cfop;

    /* Listas */
    private List<NotaEntrada> listaNotaEntradas;
    private List<NotaEntrada> listaNotaEntradaSelecionada;
    private List<NotaEntradaItem> listaNotaEntradaItens;
    private List<Produto> listaProdutos;
    private List<LocalTrabalho> listaLocalTrabalho;
    private List<Fornecedor> listaFornecedores;
    private List<NotaEntrada> lista;
    private List<Cfop> listaCFOPs;
    private LazyDataModel<NotaEntrada> model;
    /* Variaveis */
    private BigDecimal quantidade, valorItem, valorTotalItem, valorTotalItensNotaEntrada;
    private Integer quantidadeTotalItensNotaEntrada;
    private String status, data;
    private Date dtIni, dtFim;

    public NotaEntradaMB() {
        novo();
    }

    /* Método - Nova NF */
    public void novo() {
        notaEntrada = new NotaEntrada();
        notaEntradaItem = new NotaEntradaItem();
        produto = new Produto();
        usuario = new Usuario();
        localTrabalho = new LocalTrabalho();
        fornecedor = new Fornecedor();
        empresa = new Empresa();
        modeloNfe = new ModeloNfe();

        listaNotaEntradas = new ArrayList<NotaEntrada>();
        listaNotaEntradaItens = new ArrayList<NotaEntradaItem>();
        listaProdutos = new ArrayList<Produto>();
        listaLocalTrabalho = new ArrayList<LocalTrabalho>();
        listaFornecedores = new ArrayList<Fornecedor>();
        lista = new ArrayList<NotaEntrada>();

        quantidade = new BigDecimal(BigInteger.ONE);
        valorItem = new BigDecimal(BigInteger.ZERO);
        valorTotalItem = new BigDecimal(BigInteger.ZERO);
        quantidadeTotalItensNotaEntrada = 0;
        carregarNotaEntrada();
    }

    /* Método - Nova NF */
    public void novaNF() {
        novo();
        notaEntrada.setDtEmissao(new Date());
        notaEntrada.setStatus("Lançada");
        notaEntrada.setTipoEmissao("N");
        notaEntrada.setValortotalNF(BigDecimal.ZERO);
    }
    
    /* Método - Novo Item NF */
    public void novaItemNF() {
        notaEntradaItem = new NotaEntradaItem();
        valorTotalItem = new BigDecimal(BigInteger.ZERO);
    }

    /* Método - Verifica se o usuário pode editar ou excluir o registro */
    public boolean verificaSePodeEditar() {
        if (notaEntrada.getIdNotaEntrada() != null) {
            if (!notaEntrada.getStatus().equals("Faturada")) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    /* Método - Carrega todas as NF com Lazy */
    public void carregarNotaEntrada() {
        model = new LazyDataModel<NotaEntrada>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<NotaEntrada> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and p." + filterProperty + " like'%" + filterValue + "%'");
                        } else {
                            sf.append(" where p." + filterProperty + " like'%" + filterValue + "%'");
                        }
                    } else {
                        sf.append(" and p." + filterProperty + " like'%" + filterValue + "%'");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " desc");
                }
                Clausula = sf.toString();

                if (Clausula.contains("_")) {
                    Clausula = Clausula.replace("_", "");
                }
                if (Clausula.contains("() -")) {
                    Clausula = Clausula.replace("() -", "");
                }
                if (Clausula.contains("..")) {
                    Clausula = Clausula.replace("..", "");
                }

                System.out.println(Clausula);
                setRowCount(Integer.parseInt(notaEntradaEJB.contarRegistro(Clausula).toString()));
                lista = notaEntradaEJB.listarRequisicaoMaterialLazyModeWhere(first, pageSize, Clausula);
                return lista;
            }
        };
    }

    /* Método - Salvar NF e seus itens */
    public void Salvar() {
        usuario = usuarioEJB.pegaUsuarioLogadoNaSessao();
        notaEntrada.setIdEmpresa(usuario.getIdEmpresa());
        if (notaEntrada.getIdNotaEntrada() == null) {
            try {
                notaEntrada.setStatus("Lançada");
                notaEntradaEJB.SalvarNotaEntrada(notaEntrada, listaNotaEntradaItens);
                MensageFactory.addMensagemPadraoSucesso("salvar");
            } catch (Exception e) {
                MensageFactory.addMensagemPadraoErro("salvar");
                System.out.println(e);
            }
        } else {
            try {
                notaEntradaEJB.SalvarNotaEntrada(notaEntrada, listaNotaEntradaItens);
                MensageFactory.addMensagemPadraoSucesso("editar");
            } catch (Exception e) {
                MensageFactory.addMensagemPadraoErro("editar");
                System.out.println(e);
            }
        }

    }

    /* Método - Exclui a NF */
    public void Excluir() {
        if (notaEntrada.getIdNotaEntrada() != null) {
            try {
                notaEntradaEJB.Excluir(notaEntrada, notaEntrada.getIdNotaEntrada());
                MensageFactory.addMensagemPadraoSucesso("excluir");
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar excluir. Verifique o log!", null);
                Bcrutils.escreveLogErro(e);
            }
        }
    }

    /* Método - Exclui o item referente a NF */
    public void excluirItem() {
        try {
            int indice = 0;
            List<NotaEntradaItem> listaTemporaria = new ArrayList<NotaEntradaItem>();
            listaTemporaria = listaNotaEntradaItens;
            for (NotaEntradaItem nfi : listaTemporaria) {
                if (nfi == notaEntradaItem) {
                    listaNotaEntradaItens.remove(indice);
                    break;
                } else {
                    indice++;
                }
            }
            calcularValoresItensNotaEntrada();
            MensageFactory.info("Item Removido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.info("Não foi possível excluir. Tente novamente.", null);
            System.out.println(e);
        }
    }

    /* Método - Calcula o total dos itens da NF de Entrada */
    public String calcularValoresItensNotaEntrada() {
        valorTotalItensNotaEntrada = new BigDecimal(BigInteger.ZERO);
        quantidadeTotalItensNotaEntrada = 0;
        for (NotaEntradaItem nfi : listaNotaEntradaItens) {
            valorTotalItensNotaEntrada = valorTotalItensNotaEntrada.add(nfi.getValorUnitario().multiply(BigDecimal.valueOf(nfi.getQuantidade().longValue())));
            quantidadeTotalItensNotaEntrada++;
        }
        notaEntrada.setValorTotalProdutos(valorTotalItensNotaEntrada);
        return Bcrutils.formatarMoeda(valorTotalItensNotaEntrada);
    }

    /* Método - Seleciona o NF referenciada para objeto principal */
    public void selecionarNotaEntrada(NotaEntrada nf) {
        notaEntrada = nf;
    }
    
    /* Método - Seleciona o NFItem referenciada para objeto principal */
    public void selecionarNotaEntradaItem(NotaEntradaItem nfi) {
        notaEntradaItem = nfi;
    }
    
    public boolean semSelecionar() {
        if (listaNotaEntradaSelecionada.isEmpty()) {
            if (listaNotaEntradaSelecionada.size() == 1) {
                notaEntrada = listaNotaEntradaSelecionada.get(0);
            }
            return true;
        } else {
            if (listaNotaEntradaSelecionada.size() == 1) {
                notaEntrada = listaNotaEntradaSelecionada.get(0);
            }
            return false;
        }
    }
   
    /* Método - Pesquisar NF de Entrada com paramêtros */
    public void pesquisar() {
        try {
            listaNotaEntradas = notaEntradaEJB.pesquisar(notaEntrada, data, status, dtIni, dtFim);
        } catch (Exception e) {
            Bcrutils.escreveLogErro(e);
        }
    }

    /* Método - Lista todas as NF sem ordenação e sem Lazy */
    public List<NotaEntrada> listarNotasEntrada() {
        return notaEntradaEJB.ListarTodos();
    }

    /* Inicio GET's e SET's*/
    public NotaEntrada getNotaEntrada() {
        return notaEntrada;
    }

    public void setNotaEntrada(NotaEntrada notaEntrada) {
        this.notaEntrada = notaEntrada;
    }

    public NotaEntradaItem getNotaEntradaItem() {
        return notaEntradaItem;
    }

    public void setNotaEntradaItem(NotaEntradaItem notaEntradaItem) {
        this.notaEntradaItem = notaEntradaItem;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public LocalTrabalho getLocalTrabalho() {
        return localTrabalho;
    }

    public void setLocalTrabalho(LocalTrabalho localTrabalho) {
        this.localTrabalho = localTrabalho;
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public List<NotaEntrada> getListaNotaEntradas() {
        return listaNotaEntradas;
    }

    public void setListaNotaEntradas(List<NotaEntrada> listaNotaEntradas) {
        this.listaNotaEntradas = listaNotaEntradas;
    }

    public List<NotaEntradaItem> getListaNotaEntradaItens() {
        return listaNotaEntradaItens;
    }

    public void setListaNotaEntradaItens(List<NotaEntradaItem> listaNotaEntradaItens) {
        this.listaNotaEntradaItens = listaNotaEntradaItens;
    }

    public List<Produto> getListaProdutos() {
        return listaProdutos;
    }

    public void setListaProdutos(List<Produto> listaProdutos) {
        this.listaProdutos = listaProdutos;
    }

    public List<LocalTrabalho> getListaLocalTrabalho() {
        return listaLocalTrabalho;
    }

    public void setListaLocalTrabalho(List<LocalTrabalho> listaLocalTrabalho) {
        this.listaLocalTrabalho = listaLocalTrabalho;
    }

    public List<Fornecedor> getListaFornecedores() {
        return listaFornecedores;
    }

    public void setListaFornecedores(List<Fornecedor> listaFornecedores) {
        this.listaFornecedores = listaFornecedores;
    }

    public List<NotaEntrada> getLista() {
        return lista;
    }

    public void setLista(List<NotaEntrada> lista) {
        this.lista = lista;
    }

    public LazyDataModel<NotaEntrada> getModel() {
        return model;
    }

    public void setModel(LazyDataModel<NotaEntrada> model) {
        this.model = model;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public BigDecimal getValorItem() {
        return valorItem;
    }

    public void setValorItem(BigDecimal valorItem) {
        this.valorItem = valorItem;
    }

    public BigDecimal getValorTotalItem() {
        return valorTotalItem;
    }

    public void setValorTotalItem(BigDecimal valorTotalItem) {
        this.valorTotalItem = valorTotalItem;
    }

    public BigDecimal getValorTotalItensNotaEntrada() {
        return valorTotalItensNotaEntrada;
    }

    public void setValorTotalItensNotaEntrada(BigDecimal valorTotalItensNotaEntrada) {
        this.valorTotalItensNotaEntrada = valorTotalItensNotaEntrada;
    }

    public Integer getQuantidadeTotalItensNotaEntrada() {
        return quantidadeTotalItensNotaEntrada;
    }

    public void setQuantidadeTotalItensNotaEntrada(Integer quantidadeTotalItensNotaEntrada) {
        this.quantidadeTotalItensNotaEntrada = quantidadeTotalItensNotaEntrada;
    }

    public List<NotaEntrada> getListaNotaEntradaSelecionada() {
        return listaNotaEntradaSelecionada;
    }

    public void setListaNotaEntradaSelecionada(List<NotaEntrada> listaNotaEntradaSelecionada) {
        this.listaNotaEntradaSelecionada = listaNotaEntradaSelecionada;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDtIni() {
        return dtIni;
    }

    public void setDtIni(Date dtIni) {
        this.dtIni = dtIni;
    }

    public Date getDtFim() {
        return dtFim;
    }

    public void setDtFim(Date dtFim) {
        this.dtFim = dtFim;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public ModeloNfe getModeloNfe() {
        return modeloNfe;
    }

    public void setModeloNfe(ModeloNfe modeloNfe) {
        this.modeloNfe = modeloNfe;
    }

    public Cfop getCfop() {
        return cfop;
    }

    public void setCfop(Cfop cfop) {
        this.cfop = cfop;
    }

    public List<Cfop> getListaCFOPs() {
        return listaCFOPs;
    }

    public void setListaCFOPs(List<Cfop> listaCFOPs) {
        this.listaCFOPs = listaCFOPs;
    }
}
