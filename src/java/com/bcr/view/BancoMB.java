/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.BancoEJB;
import com.bcr.controller.EmpresaEJB;
import com.bcr.model.Banco;
import com.bcr.model.Empresa;
import com.bcr.util.MensageFactory;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author JOAOPAULO
 */
@ManagedBean
@ViewScoped
public class BancoMB implements Serializable {

    @EJB
    BancoEJB bEJB;
    private Banco banco;
    @EJB
    EmpresaEJB empEJB;
    private Empresa empresa;

    public BancoMB() {
        novo();
    }

    public void novo() {
        banco = new Banco();
        empresa = new Empresa();
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public void salvar() {

        HttpServletRequest request = (HttpServletRequest) FacesContext
                .getCurrentInstance().getExternalContext().getRequest();
        empresa = empEJB.SelecionarPorID(Integer.parseInt(request.getParameter("id_empresa")));
        if (banco.getIdBanco() == null) {
            try {
                if (empresa.getIdEmpresa() != null) {
                    banco.setIdEmpresa(empresa);
                }
                bEJB.Salvar(banco);
                novo();
                MensageFactory.info("Salvo efetuado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar salvar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        } else {
            try {
                bEJB.Atualizar(banco);
                novo();
                MensageFactory.info("Atualizado com sucesso", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar salvar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        }
    }

    public List<Banco> listarBancos() {
        return bEJB.ListarTodos();
    }

    public void excluir() {
        try {
            bEJB.Excluir(banco, banco.getIdBanco());
            novo();
            MensageFactory.info("Excluido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            System.out.println("Erro ocorrido é: " + e);
        }
    }

 
}
