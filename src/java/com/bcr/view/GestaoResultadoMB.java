/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.GestaoResultadosEJB;
import com.bcr.controller.UsuarioEJB;
import com.bcr.model.Cliente;
import com.bcr.model.ContasPagar;
import com.bcr.model.ContasReceber;
import com.bcr.model.Empresa;
import com.bcr.pojo.FluxoCaixa;
import com.bcr.util.Bcrutils;
import com.bcr.util.RelatorioFactory;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanComparator;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class GestaoResultadoMB {

    @EJB
    GestaoResultadosEJB grEJB;
    @EJB
    UsuarioEJB uEJB;
    private Date dtIni;
    private Date dtFim;
    private List<FluxoCaixa> fluxosCaixa;
    private BigDecimal saldoAnterior;
    private BigDecimal saldoAtual;
    private Empresa emop;

    public GestaoResultadoMB() {
        fluxosCaixa = new ArrayList<FluxoCaixa>();
        dtIni = Bcrutils.primeiroDiaDoMes();
        dtFim = Bcrutils.ultimoDiaDoMes();
        saldoAnterior = new BigDecimal(BigInteger.ZERO);
        saldoAtual = new BigDecimal(BigInteger.ZERO);
        emop = new Empresa();
    }

//    @PostConstruct
    public void pesquisarFlux() {
        emop = uEJB.pegaUsuarioLogadoNaSessao().getIdEmpresa();

        saldoAnterior = new BigDecimal(BigInteger.ZERO);

        fluxosCaixa = grEJB.listaFluxoCaixa(getDtIni(), getDtFim());
        List<FluxoCaixa> fluxosTemp = new ArrayList<FluxoCaixa>();


        for (FluxoCaixa fc : fluxosCaixa) {
            if (Bcrutils.betweenDates(fc.getDtMovimento(), getDtIni(), getDtFim())) {
                fluxosTemp.add(fc);
            }
        }
        for (FluxoCaixa fc : fluxosCaixa) {
            if (fc.getDtMovimento().before(dtIni)) {
                saldoAnterior = saldoAnterior.add(fc.getSaldo());
            }
            saldoAtual = saldoAtual.add(fc.getSaldo());
        }
        fluxosCaixa = new ArrayList<FluxoCaixa>();
        FluxoCaixa saldoIniFlux = new FluxoCaixa();
        saldoAnterior = grEJB.calcularSaldoAnterior(getDtIni());
        saldoAnterior = saldoAnterior.add(emop.getCaixaInicialValor());
        saldoIniFlux.setSaldo(saldoAnterior);
        Cliente c = new Cliente();
        c.setNome("Saldo Anterior");
        ContasReceber cr = new ContasReceber();
        cr.setIdCliente(c);
        saldoIniFlux.setSituacao("Baixado");
        saldoIniFlux.setContasReceber(cr);
        saldoIniFlux.setEntrada(c.getNome());
        saldoIniFlux.setDtMovimento(Bcrutils.addDia(dtIni, -1));
        saldoIniFlux.setSaldo(saldoAnterior);

        fluxosCaixa.add(saldoIniFlux);

        fluxosCaixa.addAll(fluxosTemp);

        

//        Collections.sort(fluxosCaixa, new Comparator<FluxoCaixa>() {
//            @Override
//            public int compare(FluxoCaixa o1, FluxoCaixa o2) {
//                if (o1.getDtMovimento().after(o2.getDtMovimento())) {
//                    return 1;
//                } else {
//                    return -1;
//                }
//            }
//        });

        Comparator<FluxoCaixa> comp = new BeanComparator("dtMovimento");
        Collections.sort(fluxosCaixa, comp);

        FluxoCaixa saldoFinaFlux = new FluxoCaixa();
        int max = fluxosCaixa.size();
//        BigDecimal saldoAtu = new BigDecimal(BigInteger.ZERO);
//        try {
//            saldoAtu = fluxosTemp.get(max).getSaldo();
//        } catch (Exception e) {
//            saldoAtu = new BigDecimal(BigInteger.ZERO);
//        }

        
        Cliente c2 = new Cliente();
        c2.setNome("Saldo Atual ");
        ContasReceber cr2 = new ContasReceber();
        cr2.setIdCliente(c2);
     
        saldoFinaFlux.setSaldo(fluxosCaixa.get(max-1).getSaldo().add(fluxosCaixa.get(0).getSaldo()));
        saldoFinaFlux.setSituacao("Baixado");
        saldoFinaFlux.setContasReceber(cr2);
        saldoFinaFlux.setDtMovimento(dtFim);
        fluxosCaixa.add(saldoFinaFlux);
        saldoAtual = saldoFinaFlux.getSaldo();
        
        
        System.out.println("Tamanho da Lista de Fluxo "+fluxosCaixa.size());

        reprocessarValores();
    }

    public void imprimirGestao() {
        RelatorioFactory.gestaoResultado("/WEB-INF/Relatorios/report_Gestao_Resultado.jasper", fluxosCaixa);
    }

    public void reprocessarValores() {
        int position = 0;
        for (FluxoCaixa cccc : fluxosCaixa) {
            if (position != 0) {
                BigDecimal dd = new BigDecimal(BigInteger.ZERO);
                if (cccc.getContasPagar() != null) {
                    if (cccc.getContasPagar().getValorDuplicata() != null) {
                        FluxoCaixa f = fluxosCaixa.get(position - 1);
                        cccc.setSaldo(f.getSaldo().subtract(cccc.getContasPagar().getValorDuplicata()));
                    }

                }
                if (cccc.getContasReceber() != null) {
                    if (cccc.getContasReceber().getValorDuplicata() != null) {
                        FluxoCaixa f = fluxosCaixa.get(position - 1);
                        BigDecimal sss = cccc.getContasReceber().getValorDuplicata().add(f.getSaldo());
                        cccc.setSaldo(sss);
                    }
                }
            }
            position++;
        }
    }

    public static boolean betweenDates(Date date, Date dateStart, Date dateEnd) {
        if (date != null && dateStart != null && dateEnd != null) {
            if (date.after(dateStart) && date.before(dateEnd)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public List<FluxoCaixa> listaGestaoResultados() {
        return grEJB.listaFluxoCaixa(getDtIni(), getDtFim());
    }

    public String formatarData(Date d) {
        return Bcrutils.formataDataDDMMYYYY(d);
    }

    public String verificaSeFoiPagaRecebida(String status) {
        if (status.equals("P") || status.equals("R")) {
            return "color:green;";
        } else {
            return "";
        }
    }

    public String verificaSePagaOuRecebe(ContasPagar cp, ContasReceber cr) {
        if (cp != null) {
            return "background-color: #ffdcdc";
        } else if (cr != null) {
            return "background-color: #d2ffd0";
        } else {
            return "";
        }
    }

    public Date getDtIni() {
        return dtIni;
    }

    public void setDtIni(Date dtIni) {
        this.dtIni = dtIni;
    }

    public Date getDtFim() {
        return dtFim;
    }

    public void setDtFim(Date dtFim) {
        this.dtFim = dtFim;
    }

    public List<FluxoCaixa> getFluxosCaixa() {
        return fluxosCaixa;
    }

    public void setFluxosCaixa(List<FluxoCaixa> fluxosCaixa) {
        this.fluxosCaixa = fluxosCaixa;
    }

    public BigDecimal getSaldoAnterior() {
        return saldoAnterior;
    }

    public void setSaldoAnterior(BigDecimal saldoAnterior) {
        this.saldoAnterior = saldoAnterior;
    }

    public String formataMoeda(Double bd) {
        return Bcrutils.formatarMoeda(bd);
    }

    public BigDecimal getSaldoAtual() {
        return saldoAtual;
    }

    public void setSaldoAtual(BigDecimal saldoAtual) {
        this.saldoAtual = saldoAtual;
    }

    public Empresa getEmop() {
        return emop;
    }

    public void setEmop(Empresa emop) {
        this.emop = emop;
    }

    public String formataData(Date d) {
        if (d != null) {
            return Bcrutils.formataData(d);
        } else {
            return "";
        }
    }

    public String verificaSeTaBaixada(FluxoCaixa fluxoCaixa) {
        if (fluxoCaixa.getSituacao().equals("Baixado")) {
            return "color:green";
        } else if (fluxoCaixa.getSituacao().equals("Aberto")) {
            return "";
        } else {
            return "";
        }
    }

    public Object verificaSePagaOuRecebe(FluxoCaixa fx) {
        if (fx.getContasPagar() != null) {
            return fx.getContasPagar();
        } else {
            return fx.getContasReceber();
        }
    }

    public void exportExcelData(StringBuffer sb) {
        byte[] csvData = sb.toString().getBytes();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) FacesContext
                .getCurrentInstance().getExternalContext().getResponse();
        response.setHeader("Content-disposition",
                "attachment; filename= test.xls");
        response.setContentLength(sb.length());
        response.setContentType("application/vnd.ms-excel");
        try {

            response.getOutputStream().write(csvData);
            response.getOutputStream().flush();
            response.getOutputStream().close();
            context.responseComplete();
        } catch (IOException e) {
// TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
