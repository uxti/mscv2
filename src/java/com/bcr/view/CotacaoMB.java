/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.CotacaoEJB;
import com.bcr.controller.FornecedorEJB;
import com.bcr.controller.ProdutoEJB;
import com.bcr.controller.UsuarioEJB;
import com.bcr.model.Cotacao;
import com.bcr.model.CotacaoItem;
import com.bcr.model.CotacaoItemFornecedor;
import com.bcr.model.Fornecedor;
import com.bcr.model.Produto;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class CotacaoMB {

    @EJB
    CotacaoEJB cEJB;
    private Cotacao cotacao;
    private CotacaoItem cotacaoItem;
    private List<CotacaoItem> listaTemporariaCotacao;
    @EJB
    UsuarioEJB uEJB;
    @EJB
    FornecedorEJB fEJB;
    private Fornecedor fornecedor;
    @EJB
    ProdutoEJB pEJB;

    public CotacaoMB() {
        novo();
    }

    public void novo() {
        cotacao = new Cotacao();
        listaTemporariaCotacao = new ArrayList<CotacaoItem>();
        cotacao.setStatus("Andamento");
        cotacao.setDtCadastro(new Date());
        cotacao.setValortotal(new BigDecimal(BigInteger.ZERO));
        cotacaoItem = new CotacaoItem();
    }

    @PostConstruct
    public void preCarregar() {
        cotacao.setIdUsuario(uEJB.pegaUsuarioLogadoNaSessao());
    }

    public List<Cotacao> listaDeCotacoesAbertas() {
        return cEJB.ListarTodos();
    }

    public void adicionarProdutoCotacao() {
        listaTemporariaCotacao.add(cotacaoItem);
        cotacaoItem = new CotacaoItem();
        cotacao.setCotacaoItemList(listaTemporariaCotacao);
    }

    public void calcularTotalDoItem(CotacaoItemFornecedor cif) {
        cif.setValorTotal(cif.getQuantidade().multiply(cif.getValorCotado()));
    }

    public void excluirProdutoCotacao(CotacaoItem item, int index) {
        if (item.getIdCotacaoItem() != null) {
            cEJB.excluirCotacaoItem(item);
        }
        cotacao.getCotacaoItemList().remove(index);
    }
    
    public void excluirCotacaoFornecedor(CotacaoItemFornecedor cif, int index){
        
    }

    public void adicionarCotacaoFornecedor(CotacaoItem cotacaoItem) {
        CotacaoItemFornecedor cif = new CotacaoItemFornecedor();
        cif.setQuantidade(new BigDecimal(cotacaoItem.getQuantidade()));
        cif.setStatus("N");
        try {
            cotacaoItem.getCotacaoItemFornecedorList().add(cif);
        } catch (Exception e) {
            cotacaoItem.setCotacaoItemFornecedorList(new ArrayList<CotacaoItemFornecedor>());
            cotacaoItem.getCotacaoItemFornecedorList().add(cif);
        }
    }

    public Cotacao getCotacao() {
        return cotacao;
    }

    public void setCotacao(Cotacao cotacao) {
        this.cotacao = cotacao;
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public CotacaoItem getCotacaoItem() {
        return cotacaoItem;
    }

    public void setCotacaoItem(CotacaoItem cotacaoItem) {
        this.cotacaoItem = cotacaoItem;
    }

    public List<CotacaoItem> getListaTemporariaCotacao() {
        return listaTemporariaCotacao;
    }

    public void setListaTemporariaCotacao(List<CotacaoItem> listaTemporariaCotacao) {
        this.listaTemporariaCotacao = listaTemporariaCotacao;
    }
}
