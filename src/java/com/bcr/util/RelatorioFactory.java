/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;

/**
 *
 * @author Renato
 */
public class RelatorioFactory extends Conexao {

    private static Connection getConexao() {
        return Conexao.conexaoJDBC();
    }

    public static void gestaoResultado(String relatorio, List list) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("USUARIO", usuario);
            JRBeanCollectionDataSource fonteDados = new JRBeanCollectionDataSource(list);  
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, fonteDados);
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void RelatorioOrcamento(String relatorio, Integer id) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("SUBREPORT_DIR", context.getExternalContext().getRealPath("/WEB-INF/Relatorios/") + System.getProperty("file.separator"));
            map.put("ID_ORCAMENTO", id);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void ComprovantePagamentoMedico(String relatorio, List<String> lote) {
        FacesContext context = FacesContext.getCurrentInstance();

        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();

            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);

            response.setContentType("application/pdf");
            response.setHeader("Content-disposition", "inline; filename=ComprovanteTransferencia.pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("idsConta", lote);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void ComprovanteTransferencia(String relatorio, List<String> lote) {
        FacesContext context = FacesContext.getCurrentInstance();

        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();

            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);

            response.setContentType("application/pdf");
            response.setHeader("Content-disposition", "inline; filename=ComprovanteTransferencia.pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("lote", lote);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void ComprovanteTransferenciaDownload(String relatorio, List<String> lote) throws IOException, SQLException {

        try {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) facesContext
                    .getExternalContext().getResponse();
            InputStream relatorioStream = facesContext.getExternalContext().getResourceAsStream(relatorio);
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("lote", lote);
            JasperPrint print = JasperFillManager.fillReport(relatorioStream, map, conexaoJDBC());

            JRExporter exportador = new JRPdfExporter();
            exportador.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
            exportador.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exportador.exportReport();

            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "attachment; filename=Comprovante Transferencia.pdf");

        } catch (JRException ex) {
            Logger.getLogger(RelatorioFactory.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    public static void ComprovanteTransferenciaDownload2(String relatorio, List<String> lote) throws IOException, SQLException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("lote", lote);
        try {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            facesContext.responseComplete();
            ServletContext scontext = (ServletContext) facesContext.getExternalContext().getContext();
            JasperPrint jasperPrint = JasperFillManager.fillReport(scontext.getRealPath(relatorio), map, conexaoJDBC());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
            exporter.exportReport();
            byte[] bytes = baos.toByteArray();
            HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
            response.setContentType("application/pdf");
            response.setHeader("Content-disposition", "inline; filename=\"comprovanteTransferencia.pdf\"");
            response.getOutputStream().write(bytes);
            response.getOutputStream().flush();
            response.getOutputStream().close();
            FacesContext.getCurrentInstance().responseComplete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void ListagemTerceiro(String relatorio, String tipoPessoa, String especialidade, String usuario) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("SUBREPORT_DIR", context.getExternalContext().getRealPath("/WEB-INF/Relatorios/") + System.getProperty("file.separator"));
            // map.put("SUBREPORT_DIR", context.getExternalContext().getRealPath("/WEB-INF/Relatorios/report1DetailOrcto.jasper") + System.getProperty("file.separator"));
            map.put("TIPO_PESSOA", tipoPessoa);
            map.put("ESPECIALIDAE", especialidade);
            map.put("USUARIO", usuario);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void ListagemPaciente(String relatorio, String usuario, Date data_inicio_cadastro, Date data_fim_cadastro, Date data_inicio_nasc, Date data_fim_nasc) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("SUBREPORT_DIR", context.getExternalContext().getRealPath("/WEB-INF/Relatorios/") + System.getProperty("file.separator"));
            // map.put("SUBREPORT_DIR", context.getExternalContext().getRealPath("/WEB-INF/Relatorios/report1DetailOrcto.jasper") + System.getProperty("file.separator"));
            map.put("USUARIO", usuario);
            map.put("DATA_FIM_CADASTRO", data_fim_cadastro);
            map.put("DATA_INICIO_CADASTRO", data_inicio_cadastro);
            map.put("DATA_INICIO_NASC", data_inicio_nasc);
            map.put("DATA_FIM_NASC", data_fim_nasc);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void atendimentosPorTerceiro(String relatorio, String id_cirurgia, String id_paciente, Integer id_terceiro,
            String tipo_atendimento, Date data_inicio, Date data_fim, String id_procedimento, String caminho) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("SUBREPORT_DIR", context.getExternalContext().getRealPath("/WEB-INF/Relatorios/") + System.getProperty("file.separator"));
            map.put("ID_CIRURGIA", id_cirurgia);
            map.put("ID_PACIENTE", id_paciente);
            map.put("ID_TERCEIRO", id_terceiro);
            map.put("TIPO_ATENDIMENTO", tipo_atendimento);
            map.put("DATA_INICIO_CIRURGIA", data_inicio);
            map.put("DATA_FIM_CIRURGIA", data_fim);
            map.put("ID_PROCEDIMENTO", id_procedimento);
            map.put("LOGO", caminho);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void extratosPorTerceiro(String relatorio, String id_cirurgia,
            String id_paciente, String id_terceiro, String tipo_atendimento,
            Date data_inicio, Date data_fim, String id_procedimento, String id_servico_item, String status, String situacao, String caminho) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("SUBREPORT_DIR", context.getExternalContext().getRealPath("/WEB-INF/Relatorios/") + System.getProperty("file.separator"));
            map.put("ID_TERCEIRO", id_terceiro);
            map.put("ID_CIRURGIA", id_cirurgia);
            map.put("ID_SERVICO_ITEM", id_servico_item);
            map.put("ID_PACIENTE", id_paciente);
            map.put("ID_PROCEDIMENTO", id_procedimento);
            map.put("STATUS", status);
            map.put("SITUACAO", situacao);
            map.put("DATA_INICIO", data_inicio);
            map.put("DATA_FIM", data_fim);
            map.put("LOGO", caminho);

            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void internacao(String relatorio, Integer id_cirurgia, String caminho) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("SUBREPORT_DIR", context.getExternalContext().getRealPath("/WEB-INF/Relatorios/") + System.getProperty("file.separator"));
            map.put("ID_CIRURGIA", id_cirurgia);
            map.put("LOGO", caminho);

            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void listagemCheques(String relatorio, String usuario, Date DT_INI_EMISSAO, Date DT_FIM_EMISSAO, Date DT_INI_VENC,
            Date DT_FIM_VENC, String id_paciente, String id_banco, String tipo, String compensado, String id_terceiro, String caminho) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("SUBREPORT_DIR", context.getExternalContext().getRealPath("/WEB-INF/Relatorios/") + System.getProperty("file.separator"));
            map.put("USUARIO", usuario);
            map.put("LOGO", caminho);
            map.put("INI_DT_RECEBIMENTO_EMISSAO", DT_INI_EMISSAO);
            map.put("FIM_DT_RECEBIMENTO_EMISSAO", DT_FIM_EMISSAO);
            map.put("INI_DT_COMPENSACAO_VENCIMENTO", DT_INI_VENC);
            map.put("FIM_DT_COMPENSACAO_VENCIMENTO", DT_FIM_VENC);
            map.put("ID_PACIENTE", id_paciente);
            map.put("ID_BANCO", id_banco);
            map.put("TIPO", tipo);
            map.put("COMPENSADO", compensado);
            map.put("ID_TERCEIRO", id_terceiro);

            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }
}
