/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Renato
 */
public class MensageFactory {
    public static void info(String strong, String mensagem) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, strong, mensagem));
    }
     
    public static void warn(String strong, String mensagem) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, strong, mensagem));
    }
     
    public static void error(String strong, String mensagem) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, strong, mensagem));
    }
     
    public static void fatal(String strong, String mensagem) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, strong, mensagem));
    }
    
    public static String addMensagemPadraoSucesso(String tipoMsg) {
        FacesContext fc = FacesContext.getCurrentInstance();
        if (tipoMsg.equalsIgnoreCase("salvar")) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Salvo com sucesso!", null));
        }
        if (tipoMsg.equalsIgnoreCase("editar")) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Editado com sucesso!", null));
        }
        if (tipoMsg.equalsIgnoreCase("excluir")) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Excluído com sucesso!", null));
        }
        return "";
    }

    public static String addMensagemPadraoErro(String tipoMsg) {
        FacesContext fc = FacesContext.getCurrentInstance();
        if (tipoMsg.equalsIgnoreCase("salvar")) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao tentar salvar!", null));
        }
        if (tipoMsg.equalsIgnoreCase("editar")) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao tentar editar!", null));
        }
        if (tipoMsg.equalsIgnoreCase("excluir")) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao tentar excluir!", null));
        }
        return "";
    }
}
