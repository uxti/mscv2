/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.util;

import com.bcr.controller.EmpresaEJB;
import com.bcr.model.Empresa;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author Renato
 */
public class Bcrutils {

    @EJB
    private static EmpresaEJB empEJB;

    public static Date ultimoDiaDoMes() {
        Calendar data = new GregorianCalendar();
        int ultimo_dia_mes = data.getActualMaximum(Calendar.DAY_OF_MONTH);
        data.set(Calendar.DAY_OF_MONTH, ultimo_dia_mes);
        return data.getTime();
    }

    public static Date primeiroDiaDoMes() {
        Calendar data = new GregorianCalendar();
        int ultimo_dia_mes = data.getActualMinimum(Calendar.DAY_OF_MONTH);
        data.set(Calendar.DAY_OF_MONTH, ultimo_dia_mes);
        return data.getTime();
    }

    public static Date primeiroDiaDaSemana() {
        Calendar data = new GregorianCalendar();
        int ultimo_dia_mes = data.getActualMinimum(Calendar.DAY_OF_WEEK);
        data.set(Calendar.DAY_OF_WEEK, ultimo_dia_mes);
        return data.getTime();
    }

    public static Date ultimoDiaDaSemana() {
        Calendar data = new GregorianCalendar();
        int ultimo_dia_mes = data.getActualMaximum(Calendar.DAY_OF_WEEK);
        data.set(Calendar.DAY_OF_WEEK, ultimo_dia_mes);
        return data.getTime();
    }

    public static Date primeiroDiaDoAno() {
        Calendar data = new GregorianCalendar();
        int primeiro_dia_ano = data.getActualMinimum(Calendar.DAY_OF_YEAR);
        data.set(Calendar.DAY_OF_YEAR, primeiro_dia_ano);
        return data.getTime();
    }

    public static Date ultimoDiaDoAno() {
        Calendar data = new GregorianCalendar();
        int ultimo_dia_ano = data.getActualMaximum(Calendar.DAY_OF_YEAR);
        data.set(Calendar.DAY_OF_YEAR, ultimo_dia_ano);
        return data.getTime();
    }

    public static Date addMes(Date data, int qtd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.MONTH, qtd);
        return cal.getTime();
    }

    public static Date addDia(Date data, int qtd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.DAY_OF_MONTH, qtd);
        return cal.getTime();
    }

    public static Date addAno(Date data, int qtd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.YEAR, qtd);
        return cal.getTime();
    }

    public static String formatarMoeda(Double val) {
        return NumberFormat.getCurrencyInstance().format(val);
    }

    public static String formatarMoeda(BigDecimal val) {
        return NumberFormat.getCurrencyInstance().format(val);
    }

    public static String formataData(Date d) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        if (d != null) {
            return df.format(d);
        } else {
            return "";
        }
    }

    public static Empresa retornaEmpresaLogada(String paramName) {
        HttpServletRequest request = (HttpServletRequest) FacesContext
                .getCurrentInstance().getExternalContext().getRequest();
        Empresa e = empEJB.SelecionarPorID(Integer.parseInt(request.getParameter(paramName)));
        return e;
    }

    public static Integer retornaTamanhoString(String s) {
        return s.length();
    }

    public static String formataDataDDMMYYYY(Date date) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        if (date != null) {
            return df.format(date);
        }
        return "";
    }

    public static boolean betweenDates(Date date, Date dateStart, Date dateEnd) {
        if (date != null && dateStart != null && dateEnd != null) {
            if ((date.after(dateStart) || date.equals(dateStart)) && (date.before(dateEnd) || date.equals(dateEnd))) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public static void escreveLogErro(Exception e) {
        System.out.println("O erro ocorrido é:" + e);
        System.out.println(e);
    }

    public static Date retornaPrimeiroData(int mes, int ano) {
        Calendar c = new GregorianCalendar(ano, mes, 1);
        int dia = c.getMinimum(Calendar.DAY_OF_MONTH);
        Calendar ca = new GregorianCalendar(ano, mes, dia);
        return ca.getTime();
    }

    public static Date retornaUltimoData(int mes, int ano) {
        Calendar c = new GregorianCalendar(ano, mes, 1);
        int dia = c.getActualMaximum(Calendar.DAY_OF_MONTH);
        Calendar ca = new GregorianCalendar(ano, mes, dia);
        return ca.getTime();
    }

    public static String retornaMes(int i) {
        if (i == 0) {
            return "Jan";
        } else if (i == 1) {
            return "Fev";
        } else if (i == 2) {
            return "Mar";
        }else if (i == 3) {
            return "Abr";
        }else if (i == 4) {
            return "Mai";
        }else if (i == 5) {
            return "Jun";
        }else if (i == 6) {
            return "Jul";
        }else if (i == 7) {
            return "Ago";
        }else if (i == 8) {
            return "Set";
        }else if (i == 9) {
            return "Out";
        }else if (i == 10) {
            return "Nov";
        }else if (i == 11) {
            return "Dez";
        }else {
            return "INVÁLIDO";
        }
    }
    
     public static void descobreErroDeEJBException(EJBException e) {
        @SuppressWarnings("ThrowableResultIgnored")
        Exception cause = e.getCausedByException();
        if (cause instanceof ConstraintViolationException) {
            @SuppressWarnings("ThrowableResultIgnored")
            ConstraintViolationException cve = (ConstraintViolationException) e.getCausedByException();
            for (Iterator<ConstraintViolation<?>> it = cve.getConstraintViolations().iterator(); it.hasNext();) {
                ConstraintViolation<? extends Object> v = it.next();
                System.err.println(v);
                System.err.println("==>>" + v.getMessage());
            }
        }
    }
}
