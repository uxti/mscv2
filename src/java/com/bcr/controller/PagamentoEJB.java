/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Banco;
import com.bcr.model.Caixa;
import com.bcr.model.CaixaItem;
import com.bcr.model.Centrocustoplanocontas;
import com.bcr.model.Cheque;
import com.bcr.model.ContaCorrente;
import com.bcr.model.ContasPagar;
import com.bcr.model.MovimentacaoBancaria;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import java.math.BigDecimal;
import java.text.DateFormatSymbols;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 *
 * @author Renato
 */
@Stateless
public class PagamentoEJB extends FacadeEJB<ContasPagar> {

    public PagamentoEJB() {
        super(ContasPagar.class);
    }
    @EJB
    CaixaEJB cEJB;

    public void salvarLancandoNoCaixa(ContasPagar contasPagar) {
        try {
            if (contasPagar.getIdContasPagar() != null) {
                em.merge(contasPagar);
            }
            if (!contasPagar.getCentrocustoplanocontasList().isEmpty()) {
                for (Centrocustoplanocontas cpcp : contasPagar.getCentrocustoplanocontasList()) {
                    cpcp.setIdContasPagar(contasPagar);
                    em.merge(cpcp);
                }
            }
            lancarMovimentoCaixa(contasPagar.getValorPago(), contasPagar);
            contasPagar = new ContasPagar();
        } catch (Exception e) {
            Bcrutils.escreveLogErro(e);
        }
    }

    public void salvarAtualizarNormal(ContasPagar contasPagar) {
        em.merge(contasPagar);
        if (!contasPagar.getCentrocustoplanocontasList().isEmpty()) {
            for (Centrocustoplanocontas cpcp : contasPagar.getCentrocustoplanocontasList()) {
                cpcp.setIdContasPagar(contasPagar);
                em.merge(cpcp);
            }
        }
    }

    public Long contarContasVencendoHoje() {
        return (Long) em.createQuery("Select COUNT(c.idContasPagar) From ContasPagar c where c.dtPagamento IS NULL and c.dtVencimento = CURRENT_DATE").getSingleResult();
    }

    public void salvarLancandoNoBanco(ContasPagar contasPagar, ContaCorrente contaCorrente, Banco banco) {
        try {
            em.merge(contasPagar);
            if (!contasPagar.getCentrocustoplanocontasList().isEmpty()) {
                for (Centrocustoplanocontas cpcp : contasPagar.getCentrocustoplanocontasList()) {
                    cpcp.setIdContasPagar(contasPagar);
                    em.merge(cpcp);
                }
            }
            lancarMovimentoBancario(contasPagar.getValorPago(), null, contaCorrente, contasPagar, banco);
            MensageFactory.info("Pagamento efetuado com sucesso! Saída com banco!", null);
        } catch (Exception e) {
            MensageFactory.error("Não foi possível pagar com o banco! Verifique os logs!", null);
            Bcrutils.escreveLogErro(e);
        }
    }

//    public void salvarAtualizar(ContasPagar contasPagar, Banco banco, ContaCorrente contaCorrente) {
//        if (contasPagar.getNumParcela().equals(1)) {
//            if (contasPagar.getStatus() == true) {
//                //Paga
//                if (contasPagar.getPagoCom().equals("C")) {
//                    try {
//                        em.merge(contasPagar);
//                        if (!contasPagar.getCentrocustoplanocontasList().isEmpty()) {
//                            for (Centrocustoplanocontas cpcp : contasPagar.getCentrocustoplanocontasList()) {
//                                cpcp.setIdContasPagar(contasPagar);
//                                em.merge(cpcp);
//                            }
//                        }
//                        lancarMovimentoCaixa(contasPagar.getValorPago(), contasPagar);
//                        MensageFactory.info("Pagamento efetuado com sucesso! Saída com caixa!", null);
//                    } catch (Exception e) {
//                        MensageFactory.error("Não foi possível pagar com o caixa! Verifique os logs!", null);
//                        Bcrutils.escreveLogErro(e);
//                    }
//                } else {
//                    try {
//                        em.merge(contasPagar);
//                        if (!contasPagar.getCentrocustoplanocontasList().isEmpty()) {
//                            for (Centrocustoplanocontas cpcp : contasPagar.getCentrocustoplanocontasList()) {
//                                cpcp.setIdContasPagar(contasPagar);
//                                em.merge(cpcp);
//                            }
//                        }
//                        lancarMovimentoBancario(contasPagar.getValorPago(), contaCorrente, contasPagar, banco);
//                        MensageFactory.info("Pagamento efetuado com sucesso! Saída com banco!", null);
//                    } catch (Exception e) {
//                        MensageFactory.error("Não foi possível pagar com o banco! Verifique os logs!", null);
//                        Bcrutils.escreveLogErro(e);
//                    }
//                }
//            } else {
//                em.merge(contasPagar);
//                if (!contasPagar.getCentrocustoplanocontasList().isEmpty()) {
//                    for (Centrocustoplanocontas cpcp : contasPagar.getCentrocustoplanocontasList()) {
//                        cpcp.setIdContasPagar(contasPagar);
//                        em.merge(cpcp);
//                    }
//                }
//            }
//            System.out.println("1 parcela");
//        } else {
//            em.merge(contasPagar);
//            if (!contasPagar.getCentrocustoplanocontasList().isEmpty()) {
//                for (Centrocustoplanocontas cpcp : contasPagar.getCentrocustoplanocontasList()) {
//                    cpcp.setIdContasPagar(contasPagar);
//                    em.merge(cpcp);
//                }
//            }
//
//        }
//    }
    public List<ContasPagar> pesquisar(ContasPagar contasPagar, String por, String status, Date dtIni, Date dtFim) {
        String fornecedor;
        String planContas;
        String numDocto;
        StringBuilder sb = new StringBuilder();
        if (contasPagar.getIdFornecedor() != null) {
            fornecedor = contasPagar.getIdFornecedor().getIdFornecedor().toString();
            sb.append(" and c.idFornecedor.idFornecedor  =  " + fornecedor);
        }
        if (!contasPagar.getNumDocto().isEmpty()) {
            numDocto = contasPagar.getNumDocto();
            sb.append(" and c.numDocto =  " + numDocto);
        }
        if (contasPagar.getIdPlanoContas() == null) {
            planContas = "%";
        } else {
            planContas = contasPagar.getIdPlanoContas().getIdPlanoContas().toString();
        }
        if (status.equals("A")) {
            sb.append(" and c.status = 0");
        } else if (status.equals("P")) {
            sb.append(" and c.status = 1");
        } else if (status.equals("V")) {
            sb.append(" and c.status =0 and c.dtVencimento < CURRENT_DATE ");
        }
        if (por.equals("L")) {
            sb.append(" and c.dtLancamento BETWEEN :dtIni and :dtFim ");
        } else if (por.equals("V")) {
            sb.append(" and c.dtVencimento BETWEEN :dtIni and :dtFim  ");
        } else if (por.equals("P")) {
            sb.append(" and c.dtPagamento BETWEEN :dtIni and :dtFim  ");
        }

        String sql = "Select c From ContasPagar c where  c.idPlanoContas.idPlanoContas like :idpl " + sb;
        System.out.println(sql);
        Query query = em.createQuery(sql);
        query.setParameter("idpl", planContas + "%");
        if (sb.toString().contains("and c.dtLancamento BETWEEN :dtIni and :dtFim")
                || sb.toString().contains("and c.dtVencimento BETWEEN :dtIni and :dtFim")
                || sb.toString().contains("and c.dtPagamento BETWEEN :dtIni and :dtFim")) {
            query.setParameter("dtIni", dtIni, TemporalType.DATE);
            query.setParameter("dtFim", dtFim, TemporalType.DATE);
        }
        return query.getResultList();
    }

    public void pagar(ContasPagar contasPagar) {
        if (contasPagar.getIdContasPagar() != null) {
            em.merge(contasPagar);
        }
        if (contasPagar.getPagoCom().equals("C")) {
            lancarMovimentoCaixa(contasPagar.getValorPago(), contasPagar);
        } else {
            lancarMovimentoBancario(contasPagar.getValorPago(), null, contasPagar.getIdContaCorrente(), contasPagar, contasPagar.getIdBanco());
        }
    }

    public void pagarCheque(ContasPagar contasPagar, Cheque chq) {
        try {
            if (contasPagar.getIdContasPagar() != null) {
                em.merge(contasPagar);
            }
            lancarMovimentoBancario(chq.getValor(), chq, contasPagar.getIdContaCorrente(), contasPagar, contasPagar.getIdBanco());
        } catch (Exception e) {
            MensageFactory.error("Ops!", "Aconteceu um erro inesperado ao lançar este pagamento.");
            System.out.println(e);
        }
    }

    public void lancarMovimentoCaixa(BigDecimal valorRepassado, ContasPagar contasPagar) {
        Caixa c = new Caixa();
        if (cEJB.verificarExisteMovimentoCaixa(contasPagar.getDtPagamento(), contasPagar.getIdEmpresa()) == 0) {
            cEJB.criaNovoCaixa(contasPagar.getDtPagamento(), contasPagar.getIdEmpresa());
        }
        c = cEJB.retornaCaixaPorData(contasPagar.getDtPagamento(), contasPagar.getIdEmpresa());
        CaixaItem ci = new CaixaItem();
        ci.setHistorico("Pagamento do Fornecedor " + contasPagar.getIdFornecedor().getNome() + " referente ao documento " + contasPagar.getNumDocto() + "  no valor de " + contasPagar.getValorDuplicata() + " pago em " + Bcrutils.formataData(contasPagar.getDtPagamento()) + ".");
        ci.setIdCaixa(c);
        System.out.println("id do contas a pagar " + contasPagar.getIdContasPagar());
        ci.setIdContasPagar(contasPagar);
        ci.setTipomovimento("Pagamento");
        ci.setTipo("D");
        ci.setIdPlanoContas(contasPagar.getIdPlanoContas());
        ci.setValor(valorRepassado);
        cEJB.inserirMovimentacao(ci);
        ci = new CaixaItem();
    }

    public void lancarMovimentoBancario(BigDecimal valorRepassado, Cheque chq, ContaCorrente contaCorrente, ContasPagar contasPagar, Banco banco) {
        MovimentacaoBancaria movimentacaoBancaria = new MovimentacaoBancaria();
        movimentacaoBancaria.setIdBanco(banco);
        movimentacaoBancaria.setIdContaCorrente(contaCorrente);
        movimentacaoBancaria.setIdContasPagar(contasPagar);
        movimentacaoBancaria.setValor(valorRepassado);
        try {
            if (chq.getIdCheque() != null) {
                movimentacaoBancaria.setIdCheque(chq);
            }
        } catch (Exception e) {
        }
        movimentacaoBancaria.setTipoMovimento("Pagamento");
        movimentacaoBancaria.setTipo("D");
        movimentacaoBancaria.setDtMovimentacaoBancariaItem(contasPagar.getDtPagamento());
        movimentacaoBancaria.setIdPlanoContas(contasPagar.getIdPlanoContas());
        movimentacaoBancaria.setIdEmpresa(contasPagar.getIdEmpresa());
//      Gambiarra violenta 
//      movimentacaoBancaria.setHistorico((new StringBuilder()).append("Pagamento - ").append(contasPagar.getIdFornecedor().getNome()).append(" - Ref. ").append(contasPagar.getNumDocto()).append(" - Valor pago: R$ ").append(chq.getValor() == null ? ((Object) (contasPagar.getValorPago())) : ((Object) (chq.getValor()))).append(" - Pago em: ").append(Bcrutils.formataData(contasPagar.getDtPagamento())).append(".").toString());
        movimentacaoBancaria.setHistorico("Pagamento - "+contasPagar.getIdFornecedor().getNome()+" - Ref. "+contasPagar.getNumDocto()+" - Valor Pago: R$ "+contasPagar.getValorPago());
        em.merge(movimentacaoBancaria);
    }

    public void estornar(ContasPagar contasPagar) {
        Query query1 = em.createQuery("DELETE FROM CaixaItem c where c.idContasPagar.idContasPagar = :idp");
        query1.setParameter("idp", contasPagar.getIdContasPagar());
        query1.executeUpdate();
        Query query2 = em.createQuery("DELETE FROM MovimentacaoBancaria m where m.idContasPagar.idContasPagar = :idp");
        query2.setParameter("idp", contasPagar.getIdContasPagar());
        query2.executeUpdate();
        em.merge(contasPagar);
    }

    public List<ContasPagar> listarContasVencendoHoje() {
        Query query = em.createQuery("Select c From ContasPagar c where c.dtVencimento = CURRENT_DATE and c.dtPagamento IS NULL");
        return query.getResultList();
    }

    public Long numeroDeLancamentos() {
        return (Long) em.createQuery("Select COUNT(c.idContasPagar) From ContasPagar c").getSingleResult();
    }

    public BigDecimal valorTotalVencidos() {
        Query query = em.createQuery("Select SUM(c.valorDuplicata) From ContasPagar c where c.dtPagamento IS NULL and c.dtVencimento <= CURRENT_DATE");
        Long total = (Long) query.getSingleResult();
        return new BigDecimal(total);
    }

    public BigDecimal contasPagarPorData(Date ini, Date fim) {
        Query query = em.createQuery("Select SUM(c.valorPago) From ContasPagar c where c.dtPagamento BETWEEN :ini and :fim");
        query.setParameter("ini", ini);
        query.setParameter("fim", fim);
        return (BigDecimal) query.getSingleResult();
    }
    
    public Long contaQuantidadeParcela(Integer idFornecedor, String Dcto){
        Query query = em.createQuery("select count(p.idContasPagar) from ContasPagar p where p.idFornecedor.idFornecedor = :idf and p.numDocto = :ndoc");
        query.setParameter("idf", idFornecedor);
        query.setParameter("ndoc", Dcto);
        return (Long) query.getSingleResult();
    }
}
