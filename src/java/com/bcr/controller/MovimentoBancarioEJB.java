/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Banco;
import com.bcr.model.ContaCorrente;
import com.bcr.model.MovimentacaoBancaria;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 *
 * @author Renato
 */
@Stateless
public class MovimentoBancarioEJB extends FacadeEJB<MovimentacaoBancaria> {

    public MovimentoBancarioEJB() {
        super(MovimentacaoBancaria.class);
    }

    public List<MovimentacaoBancaria> pesquisarMovimentoBancario(ContaCorrente contaCorrente, String tipoMov, Date dtInic, Date dtFim) {
        Query query = em.createQuery("Select m From MovimentacaoBancaria m "
                + "where m.idContaCorrente.idContaCorrente :idc and m.tipo LIKE :tip and m.dtMovimentacaoBancariaItem BETWEEN :dini and :dfim ");
        query.setParameter("idc", contaCorrente.getIdContaCorrente());
        query.setParameter("tipo", tipoMov);
        query.setParameter("dini", dtInic);
        query.setParameter("dfim", dtFim);
        return query.getResultList();
    }

    public List<MovimentacaoBancaria> pesquisaMovimentacao(Banco b, ContaCorrente cc, Date ini, Date fim) {
        Query query = em.createQuery("Select m From MovimentacaoBancaria m where m.idBanco.idBanco = :ban and m.idContaCorrente.idContaCorrente = :ic and m.dtMovimentacaoBancariaItem BETWEEN :ini and :fim ");
        query.setParameter("ban", b.getIdBanco());
        query.setParameter("ic", cc.getIdContaCorrente());
        query.setParameter("ini", ini, TemporalType.DATE);
        query.setParameter("fim", fim, TemporalType.DATE);
        return query.getResultList();
    }

    public BigDecimal retornaSaldo(Banco b, ContaCorrente cc) {
        Query queryCredito = em.createQuery("Select SUM(m.valor)  From MovimentacaoBancaria m where m.idBanco.idBanco= :idban and m.idContaCorrente.idContaCorrente = :idcor and m.tipo = 'C'");
        queryCredito.setParameter("idban", b.getIdBanco());
        queryCredito.setParameter("idcor", cc.getIdContaCorrente());
        BigDecimal creditos = new BigDecimal(BigInteger.ZERO);
        creditos = (BigDecimal) queryCredito.getSingleResult();
        if (creditos == null) {
            creditos = new BigDecimal(BigInteger.ZERO);
        }
        Query queryDebito = em.createQuery("Select SUM(m.valor)  From MovimentacaoBancaria m where m.idBanco.idBanco= :idban and m.idContaCorrente.idContaCorrente = :idcor and m.tipo = 'D'");
        queryDebito.setParameter("idban", b.getIdBanco());
        queryDebito.setParameter("idcor", cc.getIdContaCorrente());
        BigDecimal debitos = new BigDecimal(BigInteger.ZERO);
        debitos = (BigDecimal) queryDebito.getSingleResult();
        if (debitos == null) {
            debitos = new BigDecimal(BigInteger.ZERO);
        }
        return creditos.subtract(debitos);
    }

    public BigDecimal retornaDebitos(Banco b, ContaCorrente cc) {
        Query queryDebito = em.createQuery("Select SUM(m.valor)  From MovimentacaoBancaria m where m.idBanco.idBanco= :idban and m.idContaCorrente.idContaCorrente = :idcor and m.tipo = 'D'");
        queryDebito.setParameter("idban", b.getIdBanco());
        queryDebito.setParameter("idcor", cc.getIdContaCorrente());
        BigDecimal debitos = new BigDecimal(BigInteger.ZERO);
        debitos = (BigDecimal) queryDebito.getSingleResult();
        if (debitos == null) {
            debitos = new BigDecimal(BigInteger.ZERO);
        }
        return debitos;
    }

    public BigDecimal retornaCreditos(Banco b, ContaCorrente cc) {
        Query queryCredito = em.createQuery("Select SUM(m.valor)  From MovimentacaoBancaria m where m.idBanco.idBanco= :idban and m.idContaCorrente.idContaCorrente = :idcor and m.tipo = 'C'");
        queryCredito.setParameter("idban", b.getIdBanco());
        queryCredito.setParameter("idcor", cc.getIdContaCorrente());
        BigDecimal creditos = new BigDecimal(BigInteger.ZERO);
        creditos = (BigDecimal) queryCredito.getSingleResult();
        if (creditos == null) {
            creditos = new BigDecimal(BigInteger.ZERO);
        }
        return creditos;
    }
}
