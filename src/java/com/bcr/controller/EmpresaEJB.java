/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Empresa;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Renato
 */
@Stateless
public class EmpresaEJB extends FacadeEJB<Empresa> {

    public EmpresaEJB() {
        super(Empresa.class);
    }

    public Empresa returnEmpresa() {
        Query query = em.createQuery("Select e From Empresa e");
        query.setMaxResults(1);
        return (Empresa) query.getSingleResult();
    }

    public Empresa retornaUltimaEmpresaInserida() {
        Query query = em.createQuery("Select e From Empresa e where e.idEmpresa in (Select MAX(ee.idEmpresa) From Empresa ee)");
        return (Empresa) query.getSingleResult();
    }

    
}
