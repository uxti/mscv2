/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.RequisicaoMaterial;
import com.bcr.model.RequisicaoMaterialItem;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Charles
 */
@Stateless
public class RequisicaoMaterialEJB extends FacadeEJB<RequisicaoMaterial> {

    public RequisicaoMaterialEJB() {
        super(RequisicaoMaterial.class);
    }
    
    public void atualizarItem(RequisicaoMaterialItem ri){
        em.merge(ri);
    }

    public void SalvarRequisicao(RequisicaoMaterial rm, List<RequisicaoMaterialItem> listaRMItens) {
        em.merge(rm);
        for (RequisicaoMaterialItem rmi : listaRMItens) {
            rmi.setIdRequisicaoMaterial(rm);
            em.merge(rmi);
        }
    }

    public List<RequisicaoMaterial> listarRequisaoMateriais() {
        Query query = em.createQuery("Select c From RequisicaoMaterial c ORDER BY c.dtCadastro");
        return query.getResultList();
    }

    public List<RequisicaoMaterial> listarRequisicoesParaAnalise() {
        Query query = em.createQuery("Select c From RequisicaoMaterial c WHERE c.status= 'Emitido'");
        return query.getResultList();
    }
    public List<RequisicaoMaterial> listarRequisicoesParaAlmoxarifado() {
        Query query = em.createQuery("Select c From RequisicaoMaterial c WHERE c.status= 'Aprovado'");
        return query.getResultList();
    }

    public List<RequisicaoMaterial> listarRequisicaoMaterialLazyModeWhere(int primeiro, int qtd, String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select p From RequisicaoMaterial p " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public Number contarRegistro(String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select COUNT(p.idRequisicaoMaterial) From RequisicaoMaterial p " + clausula);
        return (Number) query.getSingleResult();
    }
}
