/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Agrupamento;
import com.bcr.model.CentroCusto;
import com.bcr.model.CentroCustoAgrupamento;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class CentroCustoEJB extends FacadeEJB<CentroCusto> {

    public CentroCustoEJB() {
        super(CentroCusto.class);
    }
    
    public void adicionarCentroAgrupamento(CentroCusto cc){
        em.merge(cc);
        for(CentroCustoAgrupamento cca : cc.getCentroCustoAgrupamentoList()){
            em.merge(cca);
        }
    }
    
    public List<CentroCusto> listarCentrosDeCusto(){
        return em.createQuery("Select c From CentroCusto c").getResultList();
    }
    

    
    public void excluirCentroAgrupamento(CentroCustoAgrupamento cca){
        cca = em.getReference(CentroCustoAgrupamento.class, cca.getIdCentroCustoAgrupamento());
        em.remove(cca);
    }
    
}
