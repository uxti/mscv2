/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Perfil;
import com.bcr.model.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author Renato
 */
@Stateless
public class UsuarioEJB extends FacadeEJB<Usuario> {

    public UsuarioEJB() {
        super(Usuario.class);
    }

    public Object pegaUsuarioLogado() {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select u FROM Usuario u where u.login = :login");
        System.out.println("Usuario do Spring é " + retornaUsuario());
        query.setParameter("login", retornaUsuario());
        return  query.getResultList().get(0);
    }

    public List<Perfil> retornaPerfil(Usuario u) {
        Query query = em.createQuery("Select u.idPerfil From Usuario u where u.idUsuario = :idu");
        query.setParameter("idu", u.getIdUsuario());
        return query.getResultList();
    }

    public List<Perfil> listaPerfis() {
        return em.createQuery("Select p From Perfil p ").getResultList();
    }

    public String retornaUsuario() {
        String usuarioLog = "";
        Object usuarioLogado = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (usuarioLogado instanceof UserDetails) {
            return usuarioLog = ((UserDetails) usuarioLogado).getUsername();
        } else {
            return usuarioLog = usuarioLogado.toString().toUpperCase();
        }
    }

    public Usuario pegaUsuarioLogadoNaSessao() {
        Query query = em.createQuery("Select u From Usuario u where u.login = :login");
        query.setParameter("login", retornaUsuario());
        query.setMaxResults(1);
        return (Usuario) query.getSingleResult();
    }
}
