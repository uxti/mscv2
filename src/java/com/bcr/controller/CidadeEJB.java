/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Cidade;
import com.bcr.model.Cliente;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class CidadeEJB extends FacadeEJB<Cidade> {

    public CidadeEJB() {
        super(Cidade.class);
    }

    public List<Cidade> listarClientesLazy(int primeiro, int qtd, String clausula) {
        Query query = em.createQuery("Select c From Cidade c " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public Number contarRegistros(String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select COUNT(c.idCidade) From Cidade c " + clausula);
        return (Number) query.getSingleResult();
    }

    public Long verificaIBGECadastrado(String ibge) {
        Query query = em.createQuery("Select count(c.idCidade) From Cidade c where c.codigoIBGE = :ibge");
        query.setParameter("ibge", ibge);
        return (Long) query.getSingleResult();
    }

    public Cidade pesquisarCidadePorNome(String nome) {
        Query query = em.createQuery("Select c From Cidade c where c.nome  = :nome");
        query.setParameter("nome", nome);
        try {
            return (Cidade) query.getSingleResult();
        } catch (Exception e) {
            return new Cidade();
        }

    }

    public List<Cidade> completarCidadePorNome(String nome) {
        Query query = em.createQuery("Select c From Cidade c where c.nome like :nome");
        query.setParameter("nome", "%" + nome + "%");
        return query.getResultList();
    }
}
