/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Telefone;
import javax.ejb.Stateless;

/**
 *
 * @author Renato
 */
@Stateless
public class TelefoneEJB extends FacadeEJB<Telefone> {

    public TelefoneEJB(){
        super(Telefone.class);
    }

}
