/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Cotacao;
import com.bcr.model.CotacaoItem;
import com.bcr.model.CotacaoItemFornecedor;
import com.bcr.model.RequisicaoMaterialItem;
import com.bcr.util.Bcrutils;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class CotacaoEJB extends FacadeEJB<Cotacao> {

    public CotacaoEJB() {
        super(Cotacao.class);
    }

    public void finalizarCotacao(Cotacao cotacao) {
        BigDecimal vlrTotal = new BigDecimal(BigInteger.ZERO);
        try {
            for (CotacaoItem cotacaoItem : cotacao.getCotacaoItemList()) {
                vlrTotal = vlrTotal.add(cotacaoItem.getValorTotal());
            }
        } catch (NullPointerException e) {
            vlrTotal = new BigDecimal(BigInteger.ZERO);
        }

        try {
            cotacao.setValortotal(vlrTotal);
            em.merge(cotacao);
            for (CotacaoItem cotacaoItem : cotacao.getCotacaoItemList()) {
                cotacaoItem.setIdCotacao(cotacao);
                cotacaoItem.setIdEmpresa(cotacao.getIdUsuario().getIdEmpresa());
                em.merge(cotacaoItem);
                for (CotacaoItemFornecedor cif : cotacaoItem.getCotacaoItemFornecedorList()) {
                    cif.setIdCotacao(cotacao);
                    cif.setIdCotacaoItem(cotacaoItem);
                    em.merge(cif);
                }
            }
        } catch (EJBException e) {
            Bcrutils.descobreErroDeEJBException(e);
        }
    }

    public Long TotalProdutoPendentesCotacao() {
        Query query = em.createQuery("Select COUNT(ri.idRequisicaoMaterialItem) From RequisicaoMaterialItem ri where ri.status = 'Cotação'");
        return (Long) query.getSingleResult();
    }

    public List<RequisicaoMaterialItem> itensPendentes() {
        Query query = em.createQuery("Select ri From RequisicaoMaterialItem ri where ri.status = 'Cotação'");
        return query.getResultList();
    }

    public void excluirCotacaoItem(CotacaoItem c) {
        for (CotacaoItemFornecedor cif : c.getCotacaoItemFornecedorList()) {
            cif = em.getReference(CotacaoItemFornecedor.class, cif.getIDCotacaoItemFornecedor());
            em.remove(cif);
        }
        c = em.getReference(CotacaoItem.class, c.getIdCotacaoItem());
        em.remove(c);
    }
}
