/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Cheque;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Charles
 */
@Stateless
public class ChequeEJB extends FacadeEJB<Cheque> {

    public ChequeEJB() {
        super(Cheque.class);
    }

    public String verificarCompensado(Integer id) {
        Query query = em.createQuery("Select c.compensado From Cheque c where c.idCheque = :id");
        query.setParameter("id", id);
        return (String) query.getSingleResult();
    }

    public void compensar(Cheque cheque, String usuario) {
        if (cheque.getCompensado().equals("S")) {
            cheque.setCompensado("N");
            cheque.setDtCompensacaoVencimento(null);
            em.merge(cheque);
        } else {
            cheque.setCompensado("S");
            cheque.setDtCompensacaoVencimento(new Date());
            em.merge(cheque);
        }
    }
}
