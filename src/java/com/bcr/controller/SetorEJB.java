/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Setor;
import javax.ejb.Stateless;

/**
 *
 * @author JOAOPAULO
 */
@Stateless
public class SetorEJB extends FacadeEJB<Setor> {

    public SetorEJB() {
        super(Setor.class);
    }
}
