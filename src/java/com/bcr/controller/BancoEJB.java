/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Banco;
import com.bcr.model.ContasPagar;
import com.bcr.model.ContasReceber;
import com.bcr.model.MovimentacaoBancaria;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author JOAOPAULO
 */
@Stateless
public class BancoEJB extends FacadeEJB<Banco> {

    public BancoEJB() {
        super(Banco.class);
    }
    
    public void inserirMovimentoBancario(MovimentacaoBancaria movimentacaoBancaria){
        em.merge(movimentacaoBancaria);
    }
    
    public int deletarMovimentoBancarioPorPagamento(ContasPagar cp){
        Query query = em.createQuery("Delete from MovimentacaoBancaria m where m.idContasPagar.idContasPagar = :idp");
        query.setParameter("idp", cp.getIdContasPagar());
        int res = query.executeUpdate();
        return res;
    }
    public int deletarMovimentoBancarioPorRecebimento(ContasReceber cr){
        Query query = em.createQuery("Delete from MovimentacaoBancaria m where m.idContasReceber.idContasReceber = :idp");
        query.setParameter("idp", cr.getIdContasReceber());
        int res = query.executeUpdate();
        return res;
    }
    
}
