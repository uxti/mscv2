/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.FormaPagamento;
import javax.ejb.Stateless;

/**
 *
 * @author Renato
 */
@Stateless
public class FormaPagamentoEJB extends FacadeEJB<FormaPagamento> {

    public FormaPagamentoEJB(){
        super(FormaPagamento.class);
    }

}
