/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Fornecedor;
import com.bcr.model.Telefone;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class FornecedorEJB extends FacadeEJB<Fornecedor> {

    public FornecedorEJB() {
        super(Fornecedor.class);
    }

    public void salvar(Fornecedor fornecedor, List<Telefone> phones) {
        em.merge(fornecedor);
        for (Telefone t : phones) {
            t.setIdFornecedor(fornecedor);
            em.merge(t);
        }
    }

    public List<Fornecedor> completeListaFornecedor(String nome) {
        Query query = em.createQuery("Select f From Fornecedor f where f.nome LIKE :nome or f.idFornecedor LIKE :id");
        query.setParameter("nome", nome + "%");
        query.setParameter("id", nome + "%");
        return query.getResultList();
    }
}
