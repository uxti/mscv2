/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.NotaEntrada;
import com.bcr.model.NotaEntradaItem;
import com.bcr.model.RequisicaoMaterial;
import com.bcr.model.RequisicaoMaterialItem;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 *
 * @author Charles
 */
@Stateless
public class NotaEntradaEJB extends FacadeEJB<NotaEntrada> {

    public NotaEntradaEJB() {
        super(NotaEntrada.class);
    }

    public void atualizarItem(NotaEntradaItem notaEntradaItem) {
        em.merge(notaEntradaItem);
    }

    public void SalvarNotaEntrada(NotaEntrada notaEntrada, List<NotaEntradaItem> listaItens) {
        em.merge(notaEntrada);
        for (NotaEntradaItem notaEntradaItem : listaItens) {
            notaEntradaItem.setIdNotaEntrada(notaEntrada);
            em.merge(notaEntradaItem);
        }
    }

    public Number contarRegistro(String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select COUNT(p.idNotaEntrada) From NotaEntrada p " + clausula);
        return (Number) query.getSingleResult();
    }

    public List<NotaEntrada> listarRequisicaoMaterialLazyModeWhere(int primeiro, int qtd, String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select n From NotaEntrada n " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public List<NotaEntrada> pesquisar(NotaEntrada notaEntrada, String por, String status, Date dtIni, Date dtFim) {
        String fornecedor;
        String planContas = "";
        String numDocto;
        StringBuilder sb = new StringBuilder();
        if (notaEntrada.getIdFornecedor() != null) {
            fornecedor = notaEntrada.getIdFornecedor().getIdFornecedor().toString();
            sb.append(" and n.idFornecedor.idFornecedor  =  " + fornecedor);
        }
        if (!notaEntrada.getNumero().isEmpty()) {
            numDocto = notaEntrada.getNumero();
            sb.append(" and n.numDocto =  " + numDocto);
        }
        if (status.equals("Lançada")) {
            sb.append(" and n.status = 'Lançada'");
        } else {
            sb.append(" and n.status = 'Faturada'");
        }

        if (por.equals("E")) {
            sb.append(" and n.dtEmissao BETWEEN :dtIni and :dtFim ");
        } else if (por.equals("N")) {
            sb.append(" and n.dtEntrada BETWEEN :dtIni and :dtFim  ");
        }

        String sql = "Select n From NotaEntrada n where n.TipoEntrada like :tipo " + sb;
        System.out.println(sql);
        Query query = em.createQuery(sql);
        query.setParameter("tipo", planContas + "%");
        if (sb.toString().contains("and n.dtEmissao BETWEEN :dtIni and :dtFim")
                || sb.toString().contains("and c.dtVencimento BETWEEN :dtIni and :dtFim")) {
            query.setParameter("dtIni", dtIni, TemporalType.DATE);
            query.setParameter("dtFim", dtFim, TemporalType.DATE);
        }
        return query.getResultList();
    }
}
