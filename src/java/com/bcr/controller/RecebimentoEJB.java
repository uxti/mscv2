/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Caixa;
import com.bcr.model.CaixaItem;
import com.bcr.model.Centrocustoplanocontas;
import com.bcr.model.ContasPagar;
import com.bcr.model.ContasReceber;
import com.bcr.model.MovimentacaoBancaria;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 *
 * @author Renato
 */
@Stateless
public class RecebimentoEJB extends FacadeEJB<ContasReceber> {

    public RecebimentoEJB() {
        super(ContasReceber.class);
    }
    @EJB
    CaixaEJB cEJB;

    public Long contarContasVencendoHoje() {
        return (Long) em.createQuery("Select COUNT(c.idContasReceber) From ContasReceber c where c.dtVencimento = CURRENT_DATE and c.dtPagamento IS NULL").getSingleResult();
    }

    public List<ContasReceber> listarContasVencendoHoje() {
        return em.createQuery("Select c From ContasReceber c where c.dtVencimento = CURRENT_DATE and c.dtPagamento IS NULL").getResultList();
    }

    public void salvarLancandoNoCaixa(ContasReceber contasReceber) {

        try {
            lancarMovimentoCaixa(contasReceber.getValorRecebido(), contasReceber);
        } catch (Exception e) {
            Bcrutils.escreveLogErro(e);
        }
    }

    public void salvarAtualizarNormal(ContasReceber contasReceber) {
        em.merge(contasReceber);
        if (!contasReceber.getCentrocustoplanocontasList().isEmpty()) {
            for (Centrocustoplanocontas cpcp : contasReceber.getCentrocustoplanocontasList()) {
                cpcp.setIdContasReceber(contasReceber);
                em.merge(cpcp);
            }
        }
    }

    public void salvarLancandoNoBanco(ContasReceber contasReceber) {
        try {
                em.merge(contasReceber);
            if (!contasReceber.getCentrocustoplanocontasList().isEmpty()) {
                for (Centrocustoplanocontas cpcp : contasReceber.getCentrocustoplanocontasList()) {
                    cpcp.setIdContasReceber(contasReceber);
                    em.merge(cpcp);
                }
            }
            lancarMovimentoBancario(contasReceber.getValorRecebido(), contasReceber);
        } catch (Exception e) {
            MensageFactory.error("Não foi possível pagar com o banco! Verifique os logs!", null);
            Bcrutils.escreveLogErro(e);
        }
    }

    public void receber(ContasReceber contasReceber) {
        if (contasReceber.getIdContasReceber() != null) {
            em.merge(contasReceber);
        }
        if (contasReceber.getRecebidoCom().equals("C")) {
            lancarMovimentoCaixa(contasReceber.getValorRecebido(), contasReceber);
        } else {
            lancarMovimentoBancario(contasReceber.getValorRecebido(), contasReceber);
        }
    }

    public void lancarMovimentoCaixa(BigDecimal valorRepassado, ContasReceber contasReceber) {
        Caixa c = new Caixa();
        if (cEJB.verificarExisteMovimentoCaixa(contasReceber.getDtPagamento(), contasReceber.getIdEmpresa()) == 0) {
            cEJB.criaNovoCaixa(contasReceber.getDtPagamento(), contasReceber.getIdEmpresa());
        }
        c = cEJB.retornaCaixaPorData(contasReceber.getDtPagamento(), contasReceber.getIdEmpresa());
        CaixaItem ci = new CaixaItem();
        ci.setHistorico("Recebimento do Cliente " + contasReceber.getIdCliente().getNome() + " referente ao documento " + contasReceber.getNumDocto() + "  no valor de " + contasReceber.getValorDuplicata() + " pago em " + Bcrutils.formataData(contasReceber.getDtPagamento()) + ".");
        ci.setIdCaixa(c);
        ci.setIdContasReceber(contasReceber);
        ci.setTipomovimento("Recebimento");
        ci.setTipo("C");
        ci.setIdPlanoContas(contasReceber.getIdPlanoContas());
        ci.setValor(valorRepassado);
        cEJB.inserirMovimentacao(ci);
        ci = new CaixaItem();
    }

    public void lancarMovimentoBancario(BigDecimal valorRepassado, ContasReceber contasReceber) {
        MovimentacaoBancaria movimentacaoBancaria = new MovimentacaoBancaria();
        movimentacaoBancaria.setIdBanco(contasReceber.getIdBanco());
        movimentacaoBancaria.setIdContaCorrente(contasReceber.getIdContaCorrente());
        movimentacaoBancaria.setIdContasReceber(contasReceber);
        movimentacaoBancaria.setValor(valorRepassado);
        movimentacaoBancaria.setTipoMovimento("Recebimento");
        movimentacaoBancaria.setTipo("C");
        movimentacaoBancaria.setDtMovimentacaoBancariaItem(contasReceber.getDtPagamento());
        movimentacaoBancaria.setIdPlanoContas(contasReceber.getIdPlanoContas());
        movimentacaoBancaria.setIdEmpresa(contasReceber.getIdEmpresa());
        movimentacaoBancaria.setHistorico("Recebimento do Cliente " + contasReceber.getIdCliente().getNome() + " referente ao documento " + contasReceber.getNumDocto() + "  no valor de " + contasReceber.getValorDuplicata() + " recebido em " + Bcrutils.formataData(contasReceber.getDtPagamento()) + ".");
        em.merge(movimentacaoBancaria);
    }

    public void estornar(ContasReceber recebimento) {

        Query query1 = em.createQuery("DELETE FROM CaixaItem c where c.idContasReceber.idContasReceber = :idp");
        query1.setParameter("idp", recebimento.getIdContasReceber());
        query1.executeUpdate();
        Query query2 = em.createQuery("DELETE FROM MovimentacaoBancaria m where m.idContasReceber.idContasReceber = :idp");
        query2.setParameter("idp", recebimento.getIdContasReceber());
        query2.executeUpdate();
        em.merge(recebimento);
    }

    public List<ContasReceber> pesquisar(ContasReceber contasReceber, String por, String status, Date dtIni, Date dtFim) {
        String cliente;
        String planContas;
        String numDocto;
        StringBuilder sb = new StringBuilder();

        if (contasReceber.getIdCliente() == null) {
            cliente = "%";
        } else {
            cliente = contasReceber.getIdCliente().getIdCliente().toString();
        }
        if (contasReceber.getNumDocto() == null) {
            numDocto = "%";
        } else {
            numDocto = contasReceber.getNumDocto();
        }
        if (contasReceber.getIdPlanoContas() == null) {
            planContas = "%";
        } else {
            planContas = contasReceber.getIdPlanoContas().getIdPlanoContas().toString();
        }
        if(status != null){
           if (status.equals("A")) {
            sb.append(" and c.status = 0");
            } else if (status.equals("R")) {
                sb.append(" and c.status = 1");
            } else if (status.equals("V")) {
                sb.append(" and c.status =0 and c.dtVencimento < CURRENT_DATE ");
            }   
        }
        if(por != null){
             if (por.equals("L")) {
            sb.append(" and c.dtLancamento BETWEEN :dtIni and :dtFim ");
        } else if (por.equals("V")) {
            sb.append(" and c.dtVencimento BETWEEN :dtIni and :dtFim  ");
        } else if (por.equals("P")) {
            sb.append(" and c.dtPagamento BETWEEN :dtIni and :dtFim  ");
        }
        }
       
        String sql = "Select c From ContasReceber c where c.numDocto LIKE :numDoc and c.idCliente.idCliente like :idClie and c.idPlanoContas.idPlanoContas like :idpl " + sb;
        System.out.println(sql);
        Query query = em.createQuery(sql);
        query.setParameter("numDoc", numDocto + "%");
        query.setParameter("idClie", cliente + "%");
        query.setParameter("idpl", planContas + "%");
        query.setParameter("dtIni", dtIni, TemporalType.DATE);
        query.setParameter("dtFim", dtFim, TemporalType.DATE);
        return query.getResultList();
    }

    public BigDecimal contasReceberPorData(Date ini, Date fim) {
        Query query = em.createQuery("Select SUM(c.valorRecebido) From ContasReceber c where c.dtPagamento BETWEEN :ini and :fim");
        query.setParameter("ini", ini);
        query.setParameter("fim", fim);
        return (BigDecimal) query.getSingleResult();
    }
    
    public Long contaQuantidadeParcela(Integer idCliente, String Dcto){
        Query query = em.createQuery("select count(p.idContasReceber) from ContasReceber p where p.idCliente.idCliente = :idc and p.numDocto = :ndoc");
        query.setParameter("idc", idCliente);
        query.setParameter("ndoc", Dcto);
        return (Long) query.getSingleResult();
    }
}
