/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Produto;
import com.bcr.model.ProdutoFornecedor;
import com.bcr.model.SubGrupo;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 *
 * @author JOAOPAULO
 */
@Stateless
public class ProdutoEJB extends FacadeEJB<Produto> {

    public ProdutoEJB() {
        super(Produto.class);
    }

    public void salvar(Produto p, List<ProdutoFornecedor> lista) {
        em.merge(p);
        for (ProdutoFornecedor pf : lista) {
            pf.setIdProduto(p);
            em.merge(pf);
        }
    }

    public List<SubGrupo> carregaListadeSubgrupoPorGrupo(Integer idGrupo) {

        Query query = em.createQuery("select sg from SubGrupo sg where sg.idGrupo.idGrupo =:idgru");
        query.setParameter("idgru", idGrupo);
        return query.getResultList();
    }

    public Number contarRegistros(String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select COUNT(c.idProduto) From Produto c " + clausula);
        return (Number) query.getSingleResult();
    }

    public List<Produto> listarProdutosLazy(int primeiro, int qtd, String clausula) {
        Query query = em.createQuery("Select c From Produto c " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public void EntradaEstoque(Produto produto, int quantidade) {
        int estoqueAtual = produto.getQuantidade();
        produto.setQuantidade(estoqueAtual + quantidade);
        Atualizar(produto);
    }

    public void SaidaEstoque(Produto produto, int quantidade) {
        int estoqueAtual = produto.getQuantidade();
        produto.setQuantidade(estoqueAtual - quantidade);
        Atualizar(produto);
    }

    public List<Produto> autoCompleteProduto(String var) {
        Query query = em.createQuery("Select p From Produto p where p.descricao LIKE :desc OR p.idProduto like :id");
        query.setParameter("desc", var + "%");
        query.setParameter("id", var + "%");
        return query.getResultList();

    }
}
