/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Banco;
import com.bcr.model.ContaCorrente;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class ContaCorrenteEJB extends FacadeEJB<ContaCorrente> {

    public ContaCorrenteEJB() {
        super(ContaCorrente.class);
    }

    public List<ContaCorrente> listarContasPorBanco(Banco banco) {
        Query query = em.createQuery("SELECT c From ContaCorrente c where c.idBanco.idBanco = :id");
        query.setParameter("id", banco.getIdBanco());
        return query.getResultList();
    }
}
