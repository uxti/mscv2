/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.RequisicaoMaterialItem;
import javax.ejb.Stateless;

/**
 *
 * @author Charles
 */
@Stateless
public class RequisicaoMaterialItemEJB extends FacadeEJB<RequisicaoMaterialItem> {

    public RequisicaoMaterialItemEJB() {
        super(RequisicaoMaterialItem.class);
    }

}
