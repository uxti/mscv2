/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.ContasPagar;
import com.bcr.model.ContasReceber;
import com.bcr.pojo.FluxoCaixa;
import com.bcr.util.Bcrutils;
import com.bcr.util.Conexao;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 *
 * @author Renato
 */
@Stateless
public class GestaoResultadosEJB extends Conexao {

    public BigDecimal calcularSaldoAnterior(Date dataInicial) {
        Query queryPagar = em.createQuery("Select SUM(cp.valorDuplicata) From ContasPagar cp where cp.dtVencimento < :dini");
        queryPagar.setParameter("dini", dataInicial, TemporalType.DATE);

        BigDecimal debito = (BigDecimal) queryPagar.getSingleResult();
        Query queryReceber = em.createQuery("Select SUM(cp.valorDuplicata) From ContasReceber cp where cp.dtVencimento < :dini");
        queryReceber.setParameter("dini", dataInicial, TemporalType.DATE);
        BigDecimal credito = (BigDecimal) queryReceber.getSingleResult();

        if (debito == null) {
            debito = new BigDecimal(BigInteger.ZERO);
        }
        if (credito == null) {
            credito = new BigDecimal(BigInteger.ZERO);
        }

        return credito.subtract(debito);
    }

    public List<FluxoCaixa> listaFluxoCaixa(Date dtIni, Date dtFim) {
        if (dtIni == null) {
            dtIni = Bcrutils.addDia(new Date(), -1);
        }
        if (dtFim == null) {
            dtFim = Bcrutils.addDia(new Date(), 1);
        }
        //carrega a lista de contas a pagar
        Query cp = em.createQuery("Select cp From ContasPagar cp WHERE cp.dtVencimento BETWEEN :dini and :dfim  ORDER BY cp.dtVencimento, cp.dtPagamento");
        cp.setParameter("dini", dtIni, TemporalType.DATE);
        cp.setParameter("dfim", dtFim, TemporalType.DATE);
        List<ContasPagar> cps = cp.getResultList();
        //carrega a lista de contas a receber
        Query cr = em.createQuery("Select cr From ContasReceber cr WHERE cr.dtVencimento BETWEEN :dini and :dfim  ORDER BY cr.dtVencimento, cr.dtPagamento");
        cr.setParameter("dini", dtIni, TemporalType.DATE);
        cr.setParameter("dfim", dtFim, TemporalType.DATE);
        List<ContasReceber> crs = cr.getResultList();

        FluxoCaixa fc = new FluxoCaixa();
        List<FluxoCaixa> fcs = new ArrayList<FluxoCaixa>();
        //adiciona a lista de contas a pagar em uma lista de fluxo caixa

        //adiciona a lista de contas a receber em uma lista de fluxo caixa
        for (ContasReceber crr : crs) {
            fc = new FluxoCaixa();
            fc.setContasReceber(crr);
            if (crr.getStatus().equals("R")) {
                fc.setDtMovimento(crr.getDtPagamento());
                fc.setSituacao("Baixado");
                fc.setObservacao(crr.getObservacao());
                fc.setEntrada(crr.getIdCliente().getNome());
                fc.setEntradaValor(crr.getValorDuplicata());
                
            } else {
                fc.setDtMovimento(crr.getDtVencimento());
                fc.setSituacao("Aberto");
                fc.setObservacao(crr.getObservacao());
                fc.setEntrada(crr.getIdCliente().getNome());
                fc.setEntradaValor(crr.getValorDuplicata());
            }
            fcs.add(fc);
        }

        for (ContasPagar cpp : cps) {
            fc = new FluxoCaixa();
            fc.setContasPagar(cpp);
            if (cpp.getStatus().equals("P")) {
                fc.setDtMovimento(cpp.getDtPagamento());
                fc.setSituacao("Baixado");
                fc.setObservacao(cpp.getObservacao());
                fc.setSaida(cpp.getIdFornecedor().getNome());
                fc.setSaidaValor(cpp.getValorDuplicata());
            } else {
                fc.setDtMovimento(cpp.getDtVencimento());
                fc.setSituacao("Aberto");
                fc.setObservacao(cpp.getObservacao());
                fc.setSaida(cpp.getIdFornecedor().getNome());
                fc.setSaidaValor(cpp.getValorDuplicata());
            }
            fcs.add(fc);
        }
        //ordena a lista por datas
//        Collections.sort(fcs, new compareDatas());



        //seta saldo anterior da movimentacao
        BigDecimal saldoAnterior = new BigDecimal(BigInteger.ZERO);
        //calcula saldo anterior de cada item
        for (FluxoCaixa fcx : fcs) {
            BigDecimal credito = new BigDecimal(BigInteger.ZERO);
            try {
                credito = fcx.getContasReceber().getValorDuplicata();
                if (credito == null) {
                    credito = new BigDecimal(BigInteger.ZERO);
                }
            } catch (Exception e) {
                credito = new BigDecimal(BigInteger.ZERO);
            }

            BigDecimal debito = new BigDecimal(BigInteger.ZERO);
            try {
                debito = fcx.getContasPagar().getValorDuplicata();
                if (debito == null) {
                    debito = new BigDecimal(BigInteger.ZERO);
                }
            } catch (Exception e) {
                debito = new BigDecimal(BigInteger.ZERO);
            }
            fcx.setSaldo(credito.subtract(debito).add(saldoAnterior));
            saldoAnterior = fcx.getSaldo();
        }

        


        return fcs;
    }

    class compareDatas implements Comparator<FluxoCaixa> {

        public int compare(FluxoCaixa fc1, FluxoCaixa fc2) {
            if (fc1.getDtMovimento().after(fc2.getDtMovimento())) {
                return 1;
            } else {
                return -1;
            }
        }
    }
}
