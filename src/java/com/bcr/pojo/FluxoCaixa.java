/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.pojo;

import com.bcr.model.ContasPagar;
import com.bcr.model.ContasReceber;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Renato
 */
public class FluxoCaixa implements Serializable {

    private ContasPagar contasPagar;
    private ContasReceber contasReceber;
    private String situacao;
    
    private Date dtMovimento;
    private String entrada;
    private BigDecimal entradaValor;
    private String saida;
    private BigDecimal saidaValor;
    private BigDecimal saldo;
    private String observacao;

    public ContasPagar getContasPagar() {
        return contasPagar;
    }

    public void setContasPagar(ContasPagar contasPagar) {
        this.contasPagar = contasPagar;
    }

    public ContasReceber getContasReceber() {
        return contasReceber;
    }

    public void setContasReceber(ContasReceber contasReceber) {
        this.contasReceber = contasReceber;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public Date getDtMovimento() {
        return dtMovimento;
    }

    public void setDtMovimento(Date dtMovimento) {
        this.dtMovimento = dtMovimento;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getEntrada() {
        return entrada;
    }

    public void setEntrada(String entrada) {
        this.entrada = entrada;
    }

    public String getSaida() {
        return saida;
    }

    public void setSaida(String saida) {
        this.saida = saida;
    }

    public BigDecimal getEntradaValor() {
        return entradaValor;
    }

    public void setEntradaValor(BigDecimal entradaValor) {
        this.entradaValor = entradaValor;
    }

    public BigDecimal getSaidaValor() {
        return saidaValor;
    }

    public void setSaidaValor(BigDecimal saidaValor) {
        this.saidaValor = saidaValor;
    }
}
