/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "cfop")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cfop.findAll", query = "SELECT c FROM Cfop c"),
    @NamedQuery(name = "Cfop.findByIdCfop", query = "SELECT c FROM Cfop c WHERE c.idCfop = :idCfop"),
    @NamedQuery(name = "Cfop.findByDescricao", query = "SELECT c FROM Cfop c WHERE c.descricao = :descricao"),
    @NamedQuery(name = "Cfop.findByEstado", query = "SELECT c FROM Cfop c WHERE c.estado = :estado")})
public class Cfop implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_CFOP")
    private Integer idCfop;
    @Column(name = "DESCRICAO")
    private String descricao;
    @Column(name = "CFOP_INTERNO")
    private String cfopInterno;
    @Column(name = "CFOP_EXTERNO")
    private String cfopExterno;
    @Column(name = "CFOP_EXTERIOR")
    private String cfopExterior;
    @Column(name = "ESTADO")
    private String estado;
    @OneToMany(mappedBy = "idCFOP")
    private List<NotaSaidaItem> notaSaidaItemList;
    @OneToMany(mappedBy = "idCFOP")
    private List<NotaEntrada> notaEntradaList;
    @OneToMany(mappedBy = "idCFOP")
    private List<NotaEntradaItem> notaEntradaItemList;
    @OneToMany(mappedBy = "idCFOP")
    private List<NotaSaida> notaSaidaList;

    public Cfop() {
    }

    public Cfop(Integer idCfop) {
        this.idCfop = idCfop;
    }

    public Integer getIdCfop() {
        return idCfop;
    }

    public void setIdCfop(Integer idCfop) {
        this.idCfop = idCfop;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCfopInterno() {
        return cfopInterno;
    }

    public void setCfopInterno(String cfopInterno) {
        this.cfopInterno = cfopInterno;
    }

    public String getCfopExterno() {
        return cfopExterno;
    }

    public void setCfopExterno(String cfopExterno) {
        this.cfopExterno = cfopExterno;
    }

    public String getCfopExterior() {
        return cfopExterior;
    }

    public void setCfopExterior(String cfopExterior) {
        this.cfopExterior = cfopExterior;
    }
    
    @XmlTransient
    public List<NotaSaidaItem> getNotaSaidaItemList() {
        return notaSaidaItemList;
    }

    public void setNotaSaidaItemList(List<NotaSaidaItem> notaSaidaItemList) {
        this.notaSaidaItemList = notaSaidaItemList;
    }

    @XmlTransient
    public List<NotaEntrada> getNotaEntradaList() {
        return notaEntradaList;
    }

    public void setNotaEntradaList(List<NotaEntrada> notaEntradaList) {
        this.notaEntradaList = notaEntradaList;
    }

    @XmlTransient
    public List<NotaEntradaItem> getNotaEntradaItemList() {
        return notaEntradaItemList;
    }

    public void setNotaEntradaItemList(List<NotaEntradaItem> notaEntradaItemList) {
        this.notaEntradaItemList = notaEntradaItemList;
    }

    @XmlTransient
    public List<NotaSaida> getNotaSaidaList() {
        return notaSaidaList;
    }

    public void setNotaSaidaList(List<NotaSaida> notaSaidaList) {
        this.notaSaidaList = notaSaidaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCfop != null ? idCfop.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cfop)) {
            return false;
        }
        Cfop other = (Cfop) object;
        if ((this.idCfop == null && other.idCfop != null) || (this.idCfop != null && !this.idCfop.equals(other.idCfop))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Cfop[ idCfop=" + idCfop + " ]";
    }
    
}
