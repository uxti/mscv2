/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "pedido_compra")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PedidoCompra.findAll", query = "SELECT p FROM PedidoCompra p"),
    @NamedQuery(name = "PedidoCompra.findByIdPedidoCompra", query = "SELECT p FROM PedidoCompra p WHERE p.idPedidoCompra = :idPedidoCompra"),
    @NamedQuery(name = "PedidoCompra.findByNumero", query = "SELECT p FROM PedidoCompra p WHERE p.numero = :numero"),
    @NamedQuery(name = "PedidoCompra.findByStatus", query = "SELECT p FROM PedidoCompra p WHERE p.status = :status"),
    @NamedQuery(name = "PedidoCompra.findByDtEmissao", query = "SELECT p FROM PedidoCompra p WHERE p.dtEmissao = :dtEmissao"),
    @NamedQuery(name = "PedidoCompra.findByDtPrevisao", query = "SELECT p FROM PedidoCompra p WHERE p.dtPrevisao = :dtPrevisao"),
    @NamedQuery(name = "PedidoCompra.findByDtRecebimento", query = "SELECT p FROM PedidoCompra p WHERE p.dtRecebimento = :dtRecebimento"),
    @NamedQuery(name = "PedidoCompra.findByValorTotal", query = "SELECT p FROM PedidoCompra p WHERE p.valorTotal = :valorTotal"),
    @NamedQuery(name = "PedidoCompra.findByObservacao", query = "SELECT p FROM PedidoCompra p WHERE p.observacao = :observacao")})
public class PedidoCompra implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_pedido_compra")
    private Integer idPedidoCompra;
    @Size(max = 20)
    @Column(name = "numero")
    private String numero;
    @Size(max = 100)
    @Column(name = "status")
    private String status;
    @Column(name = "dt_emissao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtEmissao;
    @Column(name = "dt_previsao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtPrevisao;
    @Column(name = "dt_recebimento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtRecebimento;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor_total")
    private BigDecimal valorTotal;
    @Size(max = 255)
    @Column(name = "observacao")
    private String observacao;
    @OneToMany(mappedBy = "idPedidoCompra")
    private List<NotaEntrada> notaEntradaList;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @JoinColumn(name = "id_fornecedor", referencedColumnName = "id_fornecedor")
    @ManyToOne
    private Fornecedor idFornecedor;
    @JoinColumn(name = "id_cotacao_item", referencedColumnName = "id_cotacao_item")
    @ManyToOne
    private CotacaoItem idCotacaoItem;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPedidoCompra")
    private List<PedidoCompraItem> pedidoCompraItemList;

    public PedidoCompra() {
    }

    public PedidoCompra(Integer idPedidoCompra) {
        this.idPedidoCompra = idPedidoCompra;
    }

    public Integer getIdPedidoCompra() {
        return idPedidoCompra;
    }

    public void setIdPedidoCompra(Integer idPedidoCompra) {
        this.idPedidoCompra = idPedidoCompra;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDtEmissao() {
        return dtEmissao;
    }

    public void setDtEmissao(Date dtEmissao) {
        this.dtEmissao = dtEmissao;
    }

    public Date getDtPrevisao() {
        return dtPrevisao;
    }

    public void setDtPrevisao(Date dtPrevisao) {
        this.dtPrevisao = dtPrevisao;
    }

    public Date getDtRecebimento() {
        return dtRecebimento;
    }

    public void setDtRecebimento(Date dtRecebimento) {
        this.dtRecebimento = dtRecebimento;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @XmlTransient
    public List<NotaEntrada> getNotaEntradaList() {
        return notaEntradaList;
    }

    public void setNotaEntradaList(List<NotaEntrada> notaEntradaList) {
        this.notaEntradaList = notaEntradaList;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Fornecedor getIdFornecedor() {
        return idFornecedor;
    }

    public void setIdFornecedor(Fornecedor idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    public CotacaoItem getIdCotacaoItem() {
        return idCotacaoItem;
    }

    public void setIdCotacaoItem(CotacaoItem idCotacaoItem) {
        this.idCotacaoItem = idCotacaoItem;
    }

    @XmlTransient
    public List<PedidoCompraItem> getPedidoCompraItemList() {
        return pedidoCompraItemList;
    }

    public void setPedidoCompraItemList(List<PedidoCompraItem> pedidoCompraItemList) {
        this.pedidoCompraItemList = pedidoCompraItemList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPedidoCompra != null ? idPedidoCompra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PedidoCompra)) {
            return false;
        }
        PedidoCompra other = (PedidoCompra) object;
        if ((this.idPedidoCompra == null && other.idPedidoCompra != null) || (this.idPedidoCompra != null && !this.idPedidoCompra.equals(other.idPedidoCompra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.PedidoCompra[ idPedidoCompra=" + idPedidoCompra + " ]";
    }
    
}
