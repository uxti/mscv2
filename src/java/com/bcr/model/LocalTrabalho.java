/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "local_trabalho")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LocalTrabalho.findAll", query = "SELECT l FROM LocalTrabalho l"),
    @NamedQuery(name = "LocalTrabalho.findByIdLocalTrabalho", query = "SELECT l FROM LocalTrabalho l WHERE l.idLocalTrabalho = :idLocalTrabalho"),
    @NamedQuery(name = "LocalTrabalho.findByDescricao", query = "SELECT l FROM LocalTrabalho l WHERE l.descricao = :descricao")})
public class LocalTrabalho implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_local_trabalho")
    private Integer idLocalTrabalho;
    @Size(max = 100)
    @Column(name = "descricao")
    private String descricao;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLocalTrabalho")
    private List<ComunicadoImplantacao> comunicadoImplantacaoList;
    @JoinColumn(name = "id_centro_custo", referencedColumnName = "id_centro_custo")
    @ManyToOne
    private CentroCusto idCentroCusto;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLocalTrabalho")
    private List<Colaborador> colaboradorList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLocalTrabalho")
    private List<RequisicaoMaterial> requisicaoMaterialList;
    @OneToMany(mappedBy = "idLocalTrabalho")
    private List<ComunicadoEncItem> comunicadoEncItemList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLocalTrabalho")
    private List<ComunicadoEncerramento> comunicadoEncerramentoList;

    public LocalTrabalho() {
    }

    public LocalTrabalho(Integer idLocalTrabalho) {
        this.idLocalTrabalho = idLocalTrabalho;
    }

    public Integer getIdLocalTrabalho() {
        return idLocalTrabalho;
    }

    public void setIdLocalTrabalho(Integer idLocalTrabalho) {
        this.idLocalTrabalho = idLocalTrabalho;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @XmlTransient
    public List<ComunicadoImplantacao> getComunicadoImplantacaoList() {
        return comunicadoImplantacaoList;
    }

    public void setComunicadoImplantacaoList(List<ComunicadoImplantacao> comunicadoImplantacaoList) {
        this.comunicadoImplantacaoList = comunicadoImplantacaoList;
    }

    public CentroCusto getIdCentroCusto() {
        return idCentroCusto;
    }

    public void setIdCentroCusto(CentroCusto idCentroCusto) {
        this.idCentroCusto = idCentroCusto;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @XmlTransient
    public List<Colaborador> getColaboradorList() {
        return colaboradorList;
    }

    public void setColaboradorList(List<Colaborador> colaboradorList) {
        this.colaboradorList = colaboradorList;
    }

    @XmlTransient
    public List<RequisicaoMaterial> getRequisicaoMaterialList() {
        return requisicaoMaterialList;
    }

    public void setRequisicaoMaterialList(List<RequisicaoMaterial> requisicaoMaterialList) {
        this.requisicaoMaterialList = requisicaoMaterialList;
    }

    @XmlTransient
    public List<ComunicadoEncItem> getComunicadoEncItemList() {
        return comunicadoEncItemList;
    }

    public void setComunicadoEncItemList(List<ComunicadoEncItem> comunicadoEncItemList) {
        this.comunicadoEncItemList = comunicadoEncItemList;
    }

    @XmlTransient
    public List<ComunicadoEncerramento> getComunicadoEncerramentoList() {
        return comunicadoEncerramentoList;
    }

    public void setComunicadoEncerramentoList(List<ComunicadoEncerramento> comunicadoEncerramentoList) {
        this.comunicadoEncerramentoList = comunicadoEncerramentoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLocalTrabalho != null ? idLocalTrabalho.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LocalTrabalho)) {
            return false;
        }
        LocalTrabalho other = (LocalTrabalho) object;
        if ((this.idLocalTrabalho == null && other.idLocalTrabalho != null) || (this.idLocalTrabalho != null && !this.idLocalTrabalho.equals(other.idLocalTrabalho))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.LocalTrabalho[ idLocalTrabalho=" + idLocalTrabalho + " ]";
    }
    
}
