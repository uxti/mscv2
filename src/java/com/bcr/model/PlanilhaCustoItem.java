/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "planilha_custo_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PlanilhaCustoItem.findAll", query = "SELECT p FROM PlanilhaCustoItem p"),
    @NamedQuery(name = "PlanilhaCustoItem.findByIdPlanilhaCustosItem", query = "SELECT p FROM PlanilhaCustoItem p WHERE p.idPlanilhaCustosItem = :idPlanilhaCustosItem"),
    @NamedQuery(name = "PlanilhaCustoItem.findByTipo", query = "SELECT p FROM PlanilhaCustoItem p WHERE p.tipo = :tipo"),
    @NamedQuery(name = "PlanilhaCustoItem.findByGrupo", query = "SELECT p FROM PlanilhaCustoItem p WHERE p.grupo = :grupo"),
    @NamedQuery(name = "PlanilhaCustoItem.findByDesricao", query = "SELECT p FROM PlanilhaCustoItem p WHERE p.desricao = :desricao"),
    @NamedQuery(name = "PlanilhaCustoItem.findByPercentual", query = "SELECT p FROM PlanilhaCustoItem p WHERE p.percentual = :percentual"),
    @NamedQuery(name = "PlanilhaCustoItem.findByValor", query = "SELECT p FROM PlanilhaCustoItem p WHERE p.valor = :valor")})
public class PlanilhaCustoItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_planilha_custos_item")
    private Integer idPlanilhaCustosItem;
    @Size(max = 100)
    @Column(name = "tipo")
    private String tipo;
    @Size(max = 100)
    @Column(name = "grupo")
    private String grupo;
    @Size(max = 255)
    @Column(name = "desricao")
    private String desricao;
    @Column(name = "percentual")
    private Integer percentual;
    @Column(name = "valor")
    private Integer valor;
    @JoinColumn(name = "id_Planilha_custo", referencedColumnName = "id_Planilha_custo")
    @ManyToOne(optional = false)
    private PlanilhaCustos idPlanilhacusto;

    public PlanilhaCustoItem() {
    }

    public PlanilhaCustoItem(Integer idPlanilhaCustosItem) {
        this.idPlanilhaCustosItem = idPlanilhaCustosItem;
    }

    public Integer getIdPlanilhaCustosItem() {
        return idPlanilhaCustosItem;
    }

    public void setIdPlanilhaCustosItem(Integer idPlanilhaCustosItem) {
        this.idPlanilhaCustosItem = idPlanilhaCustosItem;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getDesricao() {
        return desricao;
    }

    public void setDesricao(String desricao) {
        this.desricao = desricao;
    }

    public Integer getPercentual() {
        return percentual;
    }

    public void setPercentual(Integer percentual) {
        this.percentual = percentual;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public PlanilhaCustos getIdPlanilhacusto() {
        return idPlanilhacusto;
    }

    public void setIdPlanilhacusto(PlanilhaCustos idPlanilhacusto) {
        this.idPlanilhacusto = idPlanilhacusto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPlanilhaCustosItem != null ? idPlanilhaCustosItem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlanilhaCustoItem)) {
            return false;
        }
        PlanilhaCustoItem other = (PlanilhaCustoItem) object;
        if ((this.idPlanilhaCustosItem == null && other.idPlanilhaCustosItem != null) || (this.idPlanilhaCustosItem != null && !this.idPlanilhaCustosItem.equals(other.idPlanilhaCustosItem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.PlanilhaCustoItem[ idPlanilhaCustosItem=" + idPlanilhaCustosItem + " ]";
    }
    
}
