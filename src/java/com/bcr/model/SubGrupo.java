/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "sub_grupo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SubGrupo.findAll", query = "SELECT s FROM SubGrupo s"),
    @NamedQuery(name = "SubGrupo.findByIdSubgrupo", query = "SELECT s FROM SubGrupo s WHERE s.idSubgrupo = :idSubgrupo"),
    @NamedQuery(name = "SubGrupo.findByDescricao", query = "SELECT s FROM SubGrupo s WHERE s.descricao = :descricao")})
public class SubGrupo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_subgrupo")
    private Integer idSubgrupo;
    @Size(max = 255)
    @Column(name = "descricao")
    private String descricao;
    @JoinColumn(name = "id_grupo", referencedColumnName = "id_grupo")
    @ManyToOne
    private Grupo idGrupo;
    @OneToMany(mappedBy = "idSubgrupo")
    private List<Produto> produtoList;

    public SubGrupo() {
    }

    public SubGrupo(Integer idSubgrupo) {
        this.idSubgrupo = idSubgrupo;
    }

    public Integer getIdSubgrupo() {
        return idSubgrupo;
    }

    public void setIdSubgrupo(Integer idSubgrupo) {
        this.idSubgrupo = idSubgrupo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Grupo getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(Grupo idGrupo) {
        this.idGrupo = idGrupo;
    }

    @XmlTransient
    public List<Produto> getProdutoList() {
        return produtoList;
    }

    public void setProdutoList(List<Produto> produtoList) {
        this.produtoList = produtoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSubgrupo != null ? idSubgrupo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SubGrupo)) {
            return false;
        }
        SubGrupo other = (SubGrupo) object;
        if ((this.idSubgrupo == null && other.idSubgrupo != null) || (this.idSubgrupo != null && !this.idSubgrupo.equals(other.idSubgrupo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.SubGrupo[ idSubgrupo=" + idSubgrupo + " ]";
    }
    
}
