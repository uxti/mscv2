/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "comunicado_enc_financeiro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ComunicadoEncFinanceiro.findAll", query = "SELECT c FROM ComunicadoEncFinanceiro c"),
    @NamedQuery(name = "ComunicadoEncFinanceiro.findByIdcomunicadoencFinanceiro", query = "SELECT c FROM ComunicadoEncFinanceiro c WHERE c.idcomunicadoencFinanceiro = :idcomunicadoencFinanceiro"),
    @NamedQuery(name = "ComunicadoEncFinanceiro.findByDtEnvioNf", query = "SELECT c FROM ComunicadoEncFinanceiro c WHERE c.dtEnvioNf = :dtEnvioNf"),
    @NamedQuery(name = "ComunicadoEncFinanceiro.findByValorRestante", query = "SELECT c FROM ComunicadoEncFinanceiro c WHERE c.valorRestante = :valorRestante"),
    @NamedQuery(name = "ComunicadoEncFinanceiro.findByObservacao", query = "SELECT c FROM ComunicadoEncFinanceiro c WHERE c.observacao = :observacao")})
public class ComunicadoEncFinanceiro implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_comunicado_enc_Financeiro")
    private Integer idcomunicadoencFinanceiro;
    @Column(name = "dt_envio_nf")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtEnvioNf;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor_restante")
    private BigDecimal valorRestante;
    @Size(max = 255)
    @Column(name = "observacao")
    private String observacao;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    @ManyToOne
    private Cliente idCliente;
    @JoinColumn(name = "id_comunicado_encerramento", referencedColumnName = "id_comunicado_encerramento")
    @ManyToOne(optional = false)
    private ComunicadoEncerramento idComunicadoEncerramento;

    public ComunicadoEncFinanceiro() {
    }

    public ComunicadoEncFinanceiro(Integer idcomunicadoencFinanceiro) {
        this.idcomunicadoencFinanceiro = idcomunicadoencFinanceiro;
    }

    public Integer getIdcomunicadoencFinanceiro() {
        return idcomunicadoencFinanceiro;
    }

    public void setIdcomunicadoencFinanceiro(Integer idcomunicadoencFinanceiro) {
        this.idcomunicadoencFinanceiro = idcomunicadoencFinanceiro;
    }

    public Date getDtEnvioNf() {
        return dtEnvioNf;
    }

    public void setDtEnvioNf(Date dtEnvioNf) {
        this.dtEnvioNf = dtEnvioNf;
    }

    public BigDecimal getValorRestante() {
        return valorRestante;
    }

    public void setValorRestante(BigDecimal valorRestante) {
        this.valorRestante = valorRestante;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    public ComunicadoEncerramento getIdComunicadoEncerramento() {
        return idComunicadoEncerramento;
    }

    public void setIdComunicadoEncerramento(ComunicadoEncerramento idComunicadoEncerramento) {
        this.idComunicadoEncerramento = idComunicadoEncerramento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcomunicadoencFinanceiro != null ? idcomunicadoencFinanceiro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComunicadoEncFinanceiro)) {
            return false;
        }
        ComunicadoEncFinanceiro other = (ComunicadoEncFinanceiro) object;
        if ((this.idcomunicadoencFinanceiro == null && other.idcomunicadoencFinanceiro != null) || (this.idcomunicadoencFinanceiro != null && !this.idcomunicadoencFinanceiro.equals(other.idcomunicadoencFinanceiro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.ComunicadoEncFinanceiro[ idcomunicadoencFinanceiro=" + idcomunicadoencFinanceiro + " ]";
    }
    
}
