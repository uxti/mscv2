/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "requisicao_material_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RequisicaoMaterialItem.findAll", query = "SELECT r FROM RequisicaoMaterialItem r"),
    @NamedQuery(name = "RequisicaoMaterialItem.findByIdRequisicaoMaterialItem", query = "SELECT r FROM RequisicaoMaterialItem r WHERE r.idRequisicaoMaterialItem = :idRequisicaoMaterialItem"),
    @NamedQuery(name = "RequisicaoMaterialItem.findByDescricao", query = "SELECT r FROM RequisicaoMaterialItem r WHERE r.descricao = :descricao"),
    @NamedQuery(name = "RequisicaoMaterialItem.findByQuantidade", query = "SELECT r FROM RequisicaoMaterialItem r WHERE r.quantidade = :quantidade"),
    @NamedQuery(name = "RequisicaoMaterialItem.findByQuantidadeAprovada", query = "SELECT r FROM RequisicaoMaterialItem r WHERE r.quantidadeAprovada = :quantidadeAprovada"),
    @NamedQuery(name = "RequisicaoMaterialItem.findByVlrUnitario", query = "SELECT r FROM RequisicaoMaterialItem r WHERE r.vlrUnitario = :vlrUnitario")})
public class RequisicaoMaterialItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_requisicao_material_item")
    private Integer idRequisicaoMaterialItem;
    @Size(max = 100)
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "Status")
    private String status;
    @Column(name = "quantidade")
    private Integer quantidade;
    @Column(name = "quantidade_aprovada")
    private Integer quantidadeAprovada;
    @Column(name = "quantidade_cotada")
    private Integer quantidadeCotada;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "vlr_unitario")
    private BigDecimal vlrUnitario;
    @JoinColumn(name = "id_requisicao_material", referencedColumnName = "id_requisicao_material")
    @ManyToOne(optional = false)
    private RequisicaoMaterial idRequisicaoMaterial;
    @JoinColumn(name = "id_produto", referencedColumnName = "id_produto")
    @ManyToOne(optional = false)
    private Produto idProduto;

    public RequisicaoMaterialItem() {
    }

    public RequisicaoMaterialItem(Integer idRequisicaoMaterialItem) {
        this.idRequisicaoMaterialItem = idRequisicaoMaterialItem;
    }

    public Integer getIdRequisicaoMaterialItem() {
        return idRequisicaoMaterialItem;
    }

    public void setIdRequisicaoMaterialItem(Integer idRequisicaoMaterialItem) {
        this.idRequisicaoMaterialItem = idRequisicaoMaterialItem;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getQuantidadeAprovada() {
        return quantidadeAprovada;
    }

    public void setQuantidadeAprovada(Integer quantidadeAprovada) {
        this.quantidadeAprovada = quantidadeAprovada;
    }

    public BigDecimal getVlrUnitario() {
        return vlrUnitario;
    }

    public void setVlrUnitario(BigDecimal vlrUnitario) {
        this.vlrUnitario = vlrUnitario;
    }

    public RequisicaoMaterial getIdRequisicaoMaterial() {
        return idRequisicaoMaterial;
    }

    public void setIdRequisicaoMaterial(RequisicaoMaterial idRequisicaoMaterial) {
        this.idRequisicaoMaterial = idRequisicaoMaterial;
    }

    public Produto getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Produto idProduto) {
        this.idProduto = idProduto;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRequisicaoMaterialItem != null ? idRequisicaoMaterialItem.hashCode() : 0);
        return hash;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getQuantidadeCotada() {
        return quantidadeCotada;
    }

    public void setQuantidadeCotada(Integer quantidadeCotada) {
        this.quantidadeCotada = quantidadeCotada;
    }
    
    

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RequisicaoMaterialItem)) {
            return false;
        }
        RequisicaoMaterialItem other = (RequisicaoMaterialItem) object;
        if ((this.idRequisicaoMaterialItem == null && other.idRequisicaoMaterialItem != null) || (this.idRequisicaoMaterialItem != null && !this.idRequisicaoMaterialItem.equals(other.idRequisicaoMaterialItem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.RequisicaoMaterialItem[ idRequisicaoMaterialItem=" + idRequisicaoMaterialItem + " ]";
    }
    
}
