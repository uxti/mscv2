/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "cotacao_item_fornecedor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CotacaoItemFornecedor.findAll", query = "SELECT c FROM CotacaoItemFornecedor c"),
    @NamedQuery(name = "CotacaoItemFornecedor.findByIDCotacaoItemFornecedor", query = "SELECT c FROM CotacaoItemFornecedor c WHERE c.iDCotacaoItemFornecedor = :iDCotacaoItemFornecedor"),
    @NamedQuery(name = "CotacaoItemFornecedor.findByValorCotado", query = "SELECT c FROM CotacaoItemFornecedor c WHERE c.valorCotado = :valorCotado"),
    @NamedQuery(name = "CotacaoItemFornecedor.findByValorTotal", query = "SELECT c FROM CotacaoItemFornecedor c WHERE c.valorTotal = :valorTotal"),
    @NamedQuery(name = "CotacaoItemFornecedor.findByQuantidade", query = "SELECT c FROM CotacaoItemFornecedor c WHERE c.quantidade = :quantidade"),
    @NamedQuery(name = "CotacaoItemFornecedor.findByStatus", query = "SELECT c FROM CotacaoItemFornecedor c WHERE c.status = :status")})
public class CotacaoItemFornecedor implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_Cotacao_Item_Fornecedor")
    private Integer iDCotacaoItemFornecedor;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Valor_Cotado")
    private BigDecimal valorCotado;
    @Column(name = "Valor_Total")
    private BigDecimal valorTotal;
    @Column(name = "Quantidade")
    private BigDecimal quantidade;
    @Column(name = "Status")
    private String status;
    @JoinColumn(name = "id_fornecedor", referencedColumnName = "id_fornecedor")
    @ManyToOne
    private Fornecedor idFornecedor;
    @JoinColumn(name = "id_cotacao_item", referencedColumnName = "id_cotacao_item")
    @ManyToOne
    private CotacaoItem idCotacaoItem;
    @JoinColumn(name = "id_cotacao", referencedColumnName = "id_cotacao")
    @ManyToOne(optional = false)
    private Cotacao idCotacao;

    public CotacaoItemFornecedor() {
    }

    public CotacaoItemFornecedor(Integer iDCotacaoItemFornecedor) {
        this.iDCotacaoItemFornecedor = iDCotacaoItemFornecedor;
    }

    public Integer getIDCotacaoItemFornecedor() {
        return iDCotacaoItemFornecedor;
    }

    public void setIDCotacaoItemFornecedor(Integer iDCotacaoItemFornecedor) {
        this.iDCotacaoItemFornecedor = iDCotacaoItemFornecedor;
    }

    public BigDecimal getValorCotado() {
        return valorCotado;
    }

    public void setValorCotado(BigDecimal valorCotado) {
        this.valorCotado = valorCotado;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Fornecedor getIdFornecedor() {
        return idFornecedor;
    }

    public void setIdFornecedor(Fornecedor idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    public CotacaoItem getIdCotacaoItem() {
        return idCotacaoItem;
    }

    public void setIdCotacaoItem(CotacaoItem idCotacaoItem) {
        this.idCotacaoItem = idCotacaoItem;
    }

    public Cotacao getIdCotacao() {
        return idCotacao;
    }

    public void setIdCotacao(Cotacao idCotacao) {
        this.idCotacao = idCotacao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iDCotacaoItemFornecedor != null ? iDCotacaoItemFornecedor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CotacaoItemFornecedor)) {
            return false;
        }
        CotacaoItemFornecedor other = (CotacaoItemFornecedor) object;
        if ((this.iDCotacaoItemFornecedor == null && other.iDCotacaoItemFornecedor != null) || (this.iDCotacaoItemFornecedor != null && !this.iDCotacaoItemFornecedor.equals(other.iDCotacaoItemFornecedor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.CotacaoItemFornecedor[ iDCotacaoItemFornecedor=" + iDCotacaoItemFornecedor + " ]";
    }
    
}
