/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "pedido_compra_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PedidoCompraItem.findAll", query = "SELECT p FROM PedidoCompraItem p"),
    @NamedQuery(name = "PedidoCompraItem.findByIdPedidoCompraItem", query = "SELECT p FROM PedidoCompraItem p WHERE p.idPedidoCompraItem = :idPedidoCompraItem"),
    @NamedQuery(name = "PedidoCompraItem.findByDescricao", query = "SELECT p FROM PedidoCompraItem p WHERE p.descricao = :descricao"),
    @NamedQuery(name = "PedidoCompraItem.findByStatus", query = "SELECT p FROM PedidoCompraItem p WHERE p.status = :status"),
    @NamedQuery(name = "PedidoCompraItem.findByQtdSolicitada", query = "SELECT p FROM PedidoCompraItem p WHERE p.qtdSolicitada = :qtdSolicitada"),
    @NamedQuery(name = "PedidoCompraItem.findByQtdChegada", query = "SELECT p FROM PedidoCompraItem p WHERE p.qtdChegada = :qtdChegada"),
    @NamedQuery(name = "PedidoCompraItem.findByQtdRestante", query = "SELECT p FROM PedidoCompraItem p WHERE p.qtdRestante = :qtdRestante"),
    @NamedQuery(name = "PedidoCompraItem.findByValorUnitario", query = "SELECT p FROM PedidoCompraItem p WHERE p.valorUnitario = :valorUnitario"),
    @NamedQuery(name = "PedidoCompraItem.findByValorTotal", query = "SELECT p FROM PedidoCompraItem p WHERE p.valorTotal = :valorTotal")})
public class PedidoCompraItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_pedido_compra_item")
    private Integer idPedidoCompraItem;
    @Size(max = 255)
    @Column(name = "descricao")
    private String descricao;
    @Size(max = 100)
    @Column(name = "status")
    private String status;
    @Column(name = "qtd_solicitada")
    private Short qtdSolicitada;
    @Column(name = "qtd_chegada")
    private Short qtdChegada;
    @Column(name = "qtd_restante")
    private Short qtdRestante;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor_unitario")
    private BigDecimal valorUnitario;
    @Column(name = "valor_total")
    private BigDecimal valorTotal;
    @JoinColumn(name = "id_pedido_compra", referencedColumnName = "id_pedido_compra")
    @ManyToOne(optional = false)
    private PedidoCompra idPedidoCompra;

    public PedidoCompraItem() {
    }

    public PedidoCompraItem(Integer idPedidoCompraItem) {
        this.idPedidoCompraItem = idPedidoCompraItem;
    }

    public Integer getIdPedidoCompraItem() {
        return idPedidoCompraItem;
    }

    public void setIdPedidoCompraItem(Integer idPedidoCompraItem) {
        this.idPedidoCompraItem = idPedidoCompraItem;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Short getQtdSolicitada() {
        return qtdSolicitada;
    }

    public void setQtdSolicitada(Short qtdSolicitada) {
        this.qtdSolicitada = qtdSolicitada;
    }

    public Short getQtdChegada() {
        return qtdChegada;
    }

    public void setQtdChegada(Short qtdChegada) {
        this.qtdChegada = qtdChegada;
    }

    public Short getQtdRestante() {
        return qtdRestante;
    }

    public void setQtdRestante(Short qtdRestante) {
        this.qtdRestante = qtdRestante;
    }

    public BigDecimal getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(BigDecimal valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public PedidoCompra getIdPedidoCompra() {
        return idPedidoCompra;
    }

    public void setIdPedidoCompra(PedidoCompra idPedidoCompra) {
        this.idPedidoCompra = idPedidoCompra;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPedidoCompraItem != null ? idPedidoCompraItem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PedidoCompraItem)) {
            return false;
        }
        PedidoCompraItem other = (PedidoCompraItem) object;
        if ((this.idPedidoCompraItem == null && other.idPedidoCompraItem != null) || (this.idPedidoCompraItem != null && !this.idPedidoCompraItem.equals(other.idPedidoCompraItem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.PedidoCompraItem[ idPedidoCompraItem=" + idPedidoCompraItem + " ]";
    }
    
}
