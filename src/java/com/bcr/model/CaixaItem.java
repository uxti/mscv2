/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "caixa_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CaixaItem.findAll", query = "SELECT c FROM CaixaItem c"),
    @NamedQuery(name = "CaixaItem.findByIdCaixaItem", query = "SELECT c FROM CaixaItem c WHERE c.idCaixaItem = :idCaixaItem"),
    @NamedQuery(name = "CaixaItem.findByTipo", query = "SELECT c FROM CaixaItem c WHERE c.tipo = :tipo"),
    @NamedQuery(name = "CaixaItem.findByTipomovimento", query = "SELECT c FROM CaixaItem c WHERE c.tipomovimento = :tipomovimento"),
    @NamedQuery(name = "CaixaItem.findByHistorico", query = "SELECT c FROM CaixaItem c WHERE c.historico = :historico"),
    @NamedQuery(name = "CaixaItem.findByValor", query = "SELECT c FROM CaixaItem c WHERE c.valor = :valor"),
    @NamedQuery(name = "CaixaItem.findByObservacao", query = "SELECT c FROM CaixaItem c WHERE c.observacao = :observacao")})
public class CaixaItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_caixa_item")
    private Integer idCaixaItem;
    @Size(max = 100)
    @Column(name = "tipo")
    private String tipo;
    @Size(max = 20)
    @Column(name = "Tipo_movimento")
    private String tipomovimento;
    @Size(max = 255)
    @Column(name = "historico")
    private String historico;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor")
    private BigDecimal valor;
    @Size(max = 255)
    @Column(name = "observacao")
    private String observacao;
    @JoinColumn(name = "id_contas_receber", referencedColumnName = "id_contas_receber")
    @ManyToOne
    private ContasReceber idContasReceber;
    @JoinColumn(name = "id_plano_contas", referencedColumnName = "id_plano_contas")
    @ManyToOne
    private PlanoContas idPlanoContas;
    @JoinColumn(name = "id_cheque", referencedColumnName = "id_cheque")
    @ManyToOne
    private Cheque idCheque;
    @JoinColumn(name = "id_contas_pagar", referencedColumnName = "id_contas_pagar")
    @ManyToOne
    private ContasPagar idContasPagar;
    @JoinColumn(name = "id_caixa", referencedColumnName = "id_caixa")
    @ManyToOne
    private Caixa idCaixa;

    public CaixaItem() {
    }

    public CaixaItem(Integer idCaixaItem) {
        this.idCaixaItem = idCaixaItem;
    }

    public Integer getIdCaixaItem() {
        return idCaixaItem;
    }

    public void setIdCaixaItem(Integer idCaixaItem) {
        this.idCaixaItem = idCaixaItem;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipomovimento() {
        return tipomovimento;
    }

    public void setTipomovimento(String tipomovimento) {
        this.tipomovimento = tipomovimento;
    }

    public String getHistorico() {
        return historico;
    }

    public void setHistorico(String historico) {
        this.historico = historico;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public ContasReceber getIdContasReceber() {
        return idContasReceber;
    }

    public void setIdContasReceber(ContasReceber idContasReceber) {
        this.idContasReceber = idContasReceber;
    }

    public PlanoContas getIdPlanoContas() {
        return idPlanoContas;
    }

    public void setIdPlanoContas(PlanoContas idPlanoContas) {
        this.idPlanoContas = idPlanoContas;
    }

    public Cheque getIdCheque() {
        return idCheque;
    }

    public void setIdCheque(Cheque idCheque) {
        this.idCheque = idCheque;
    }

    public ContasPagar getIdContasPagar() {
        return idContasPagar;
    }

    public void setIdContasPagar(ContasPagar idContasPagar) {
        this.idContasPagar = idContasPagar;
    }

    public Caixa getIdCaixa() {
        return idCaixa;
    }

    public void setIdCaixa(Caixa idCaixa) {
        this.idCaixa = idCaixa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCaixaItem != null ? idCaixaItem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CaixaItem)) {
            return false;
        }
        CaixaItem other = (CaixaItem) object;
        if ((this.idCaixaItem == null && other.idCaixaItem != null) || (this.idCaixaItem != null && !this.idCaixaItem.equals(other.idCaixaItem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.CaixaItem[ idCaixaItem=" + idCaixaItem + " ]";
    }
    
}
