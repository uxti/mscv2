/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "servico_resumido")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ServicoResumido.findAll", query = "SELECT s FROM ServicoResumido s"),
    @NamedQuery(name = "ServicoResumido.findByIdServicoResumido", query = "SELECT s FROM ServicoResumido s WHERE s.idServicoResumido = :idServicoResumido"),
    @NamedQuery(name = "ServicoResumido.findByDescricao", query = "SELECT s FROM ServicoResumido s WHERE s.descricao = :descricao"),
    @NamedQuery(name = "ServicoResumido.findByValor", query = "SELECT s FROM ServicoResumido s WHERE s.valor = :valor")})
public class ServicoResumido implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_servico_resumido")
    private Integer idServicoResumido;
    @Size(max = 255)
    @Column(name = "descricao")
    private String descricao;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor")
    private BigDecimal valor;
    @OneToMany(mappedBy = "idServicoResumido")
    private List<ComunicadoEncItem> comunicadoEncItemList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idServicoResumido")
    private List<ComunicadoImplItem> comunicadoImplItemList;

    public ServicoResumido() {
    }

    public ServicoResumido(Integer idServicoResumido) {
        this.idServicoResumido = idServicoResumido;
    }

    public Integer getIdServicoResumido() {
        return idServicoResumido;
    }

    public void setIdServicoResumido(Integer idServicoResumido) {
        this.idServicoResumido = idServicoResumido;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    @XmlTransient
    public List<ComunicadoEncItem> getComunicadoEncItemList() {
        return comunicadoEncItemList;
    }

    public void setComunicadoEncItemList(List<ComunicadoEncItem> comunicadoEncItemList) {
        this.comunicadoEncItemList = comunicadoEncItemList;
    }

    @XmlTransient
    public List<ComunicadoImplItem> getComunicadoImplItemList() {
        return comunicadoImplItemList;
    }

    public void setComunicadoImplItemList(List<ComunicadoImplItem> comunicadoImplItemList) {
        this.comunicadoImplItemList = comunicadoImplItemList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idServicoResumido != null ? idServicoResumido.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ServicoResumido)) {
            return false;
        }
        ServicoResumido other = (ServicoResumido) object;
        if ((this.idServicoResumido == null && other.idServicoResumido != null) || (this.idServicoResumido != null && !this.idServicoResumido.equals(other.idServicoResumido))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.ServicoResumido[ idServicoResumido=" + idServicoResumido + " ]";
    }
    
}
