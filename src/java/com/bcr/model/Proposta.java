/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "proposta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proposta.findAll", query = "SELECT p FROM Proposta p"),
    @NamedQuery(name = "Proposta.findByIdProposta", query = "SELECT p FROM Proposta p WHERE p.idProposta = :idProposta"),
    @NamedQuery(name = "Proposta.findByNome", query = "SELECT p FROM Proposta p WHERE p.nome = :nome"),
    @NamedQuery(name = "Proposta.findByStatus", query = "SELECT p FROM Proposta p WHERE p.status = :status"),
    @NamedQuery(name = "Proposta.findByDtEmissao", query = "SELECT p FROM Proposta p WHERE p.dtEmissao = :dtEmissao"),
    @NamedQuery(name = "Proposta.findByDtValidade", query = "SELECT p FROM Proposta p WHERE p.dtValidade = :dtValidade"),
    @NamedQuery(name = "Proposta.findByValorDesconto", query = "SELECT p FROM Proposta p WHERE p.valorDesconto = :valorDesconto"),
    @NamedQuery(name = "Proposta.findByValorTotal", query = "SELECT p FROM Proposta p WHERE p.valorTotal = :valorTotal"),
    @NamedQuery(name = "Proposta.findByObservacao", query = "SELECT p FROM Proposta p WHERE p.observacao = :observacao"),
    @NamedQuery(name = "Proposta.findByIntroducao", query = "SELECT p FROM Proposta p WHERE p.introducao = :introducao")})
public class Proposta implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_proposta")
    private Integer idProposta;
    @Size(max = 255)
    @Column(name = "nome")
    private String nome;
    @Size(max = 1)
    @Column(name = "status")
    private String status;
    @Column(name = "dt_emissao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtEmissao;
    @Column(name = "dt_validade")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtValidade;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor_desconto")
    private BigDecimal valorDesconto;
    @Column(name = "valor_total")
    private BigDecimal valorTotal;
    @Size(max = 255)
    @Column(name = "observacao")
    private String observacao;
    @Size(max = 255)
    @Column(name = "introducao")
    private String introducao;
    @OneToMany(mappedBy = "idProposta")
    private List<Contrato> contratoList;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @OneToMany(mappedBy = "idProposta")
    private List<PropostaItem> propostaItemList;

    public Proposta() {
    }

    public Proposta(Integer idProposta) {
        this.idProposta = idProposta;
    }

    public Integer getIdProposta() {
        return idProposta;
    }

    public void setIdProposta(Integer idProposta) {
        this.idProposta = idProposta;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDtEmissao() {
        return dtEmissao;
    }

    public void setDtEmissao(Date dtEmissao) {
        this.dtEmissao = dtEmissao;
    }

    public Date getDtValidade() {
        return dtValidade;
    }

    public void setDtValidade(Date dtValidade) {
        this.dtValidade = dtValidade;
    }

    public BigDecimal getValorDesconto() {
        return valorDesconto;
    }

    public void setValorDesconto(BigDecimal valorDesconto) {
        this.valorDesconto = valorDesconto;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getIntroducao() {
        return introducao;
    }

    public void setIntroducao(String introducao) {
        this.introducao = introducao;
    }

    @XmlTransient
    public List<Contrato> getContratoList() {
        return contratoList;
    }

    public void setContratoList(List<Contrato> contratoList) {
        this.contratoList = contratoList;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @XmlTransient
    public List<PropostaItem> getPropostaItemList() {
        return propostaItemList;
    }

    public void setPropostaItemList(List<PropostaItem> propostaItemList) {
        this.propostaItemList = propostaItemList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProposta != null ? idProposta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proposta)) {
            return false;
        }
        Proposta other = (Proposta) object;
        if ((this.idProposta == null && other.idProposta != null) || (this.idProposta != null && !this.idProposta.equals(other.idProposta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Proposta[ idProposta=" + idProposta + " ]";
    }
    
}
