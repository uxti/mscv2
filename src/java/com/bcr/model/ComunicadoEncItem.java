/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "comunicado_enc_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ComunicadoEncItem.findAll", query = "SELECT c FROM ComunicadoEncItem c"),
    @NamedQuery(name = "ComunicadoEncItem.findByIdComunicadoEncItem", query = "SELECT c FROM ComunicadoEncItem c WHERE c.idComunicadoEncItem = :idComunicadoEncItem"),
    @NamedQuery(name = "ComunicadoEncItem.findByQtdCargos", query = "SELECT c FROM ComunicadoEncItem c WHERE c.qtdCargos = :qtdCargos"),
    @NamedQuery(name = "ComunicadoEncItem.findByDtDispensa", query = "SELECT c FROM ComunicadoEncItem c WHERE c.dtDispensa = :dtDispensa"),
    @NamedQuery(name = "ComunicadoEncItem.findByObservacao", query = "SELECT c FROM ComunicadoEncItem c WHERE c.observacao = :observacao")})
public class ComunicadoEncItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_comunicado_enc_item")
    private Integer idComunicadoEncItem;
    @Column(name = "qtd_cargos")
    private Short qtdCargos;
    @Column(name = "dt_dispensa")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtDispensa;
    @Size(max = 255)
    @Column(name = "observacao")
    private String observacao;
    @JoinColumn(name = "id_local_trabalho", referencedColumnName = "id_local_trabalho")
    @ManyToOne
    private LocalTrabalho idLocalTrabalho;
    @JoinColumn(name = "id_servico_resumido", referencedColumnName = "id_servico_resumido")
    @ManyToOne
    private ServicoResumido idServicoResumido;
    @JoinColumn(name = "id_cargo", referencedColumnName = "id_cargo")
    @ManyToOne(optional = false)
    private Cargo idCargo;
    @JoinColumn(name = "id_comunicado_encerramento", referencedColumnName = "id_comunicado_encerramento")
    @ManyToOne(optional = false)
    private ComunicadoEncerramento idComunicadoEncerramento;
    @JoinColumn(name = "id_setor", referencedColumnName = "id_setor")
    @ManyToOne(optional = false)
    private Setor idSetor;

    public ComunicadoEncItem() {
    }

    public ComunicadoEncItem(Integer idComunicadoEncItem) {
        this.idComunicadoEncItem = idComunicadoEncItem;
    }

    public Integer getIdComunicadoEncItem() {
        return idComunicadoEncItem;
    }

    public void setIdComunicadoEncItem(Integer idComunicadoEncItem) {
        this.idComunicadoEncItem = idComunicadoEncItem;
    }

    public Short getQtdCargos() {
        return qtdCargos;
    }

    public void setQtdCargos(Short qtdCargos) {
        this.qtdCargos = qtdCargos;
    }

    public Date getDtDispensa() {
        return dtDispensa;
    }

    public void setDtDispensa(Date dtDispensa) {
        this.dtDispensa = dtDispensa;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public LocalTrabalho getIdLocalTrabalho() {
        return idLocalTrabalho;
    }

    public void setIdLocalTrabalho(LocalTrabalho idLocalTrabalho) {
        this.idLocalTrabalho = idLocalTrabalho;
    }

    public ServicoResumido getIdServicoResumido() {
        return idServicoResumido;
    }

    public void setIdServicoResumido(ServicoResumido idServicoResumido) {
        this.idServicoResumido = idServicoResumido;
    }

    public Cargo getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Cargo idCargo) {
        this.idCargo = idCargo;
    }

    public ComunicadoEncerramento getIdComunicadoEncerramento() {
        return idComunicadoEncerramento;
    }

    public void setIdComunicadoEncerramento(ComunicadoEncerramento idComunicadoEncerramento) {
        this.idComunicadoEncerramento = idComunicadoEncerramento;
    }

    public Setor getIdSetor() {
        return idSetor;
    }

    public void setIdSetor(Setor idSetor) {
        this.idSetor = idSetor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idComunicadoEncItem != null ? idComunicadoEncItem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComunicadoEncItem)) {
            return false;
        }
        ComunicadoEncItem other = (ComunicadoEncItem) object;
        if ((this.idComunicadoEncItem == null && other.idComunicadoEncItem != null) || (this.idComunicadoEncItem != null && !this.idComunicadoEncItem.equals(other.idComunicadoEncItem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.ComunicadoEncItem[ idComunicadoEncItem=" + idComunicadoEncItem + " ]";
    }
    
}
