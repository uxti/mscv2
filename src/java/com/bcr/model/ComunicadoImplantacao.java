/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "comunicado_implantacao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ComunicadoImplantacao.findAll", query = "SELECT c FROM ComunicadoImplantacao c"),
    @NamedQuery(name = "ComunicadoImplantacao.findByIdComunicadoImplantacao", query = "SELECT c FROM ComunicadoImplantacao c WHERE c.idComunicadoImplantacao = :idComunicadoImplantacao"),
    @NamedQuery(name = "ComunicadoImplantacao.findByDtSolicitacao", query = "SELECT c FROM ComunicadoImplantacao c WHERE c.dtSolicitacao = :dtSolicitacao"),
    @NamedQuery(name = "ComunicadoImplantacao.findByDtAprovacao", query = "SELECT c FROM ComunicadoImplantacao c WHERE c.dtAprovacao = :dtAprovacao"),
    @NamedQuery(name = "ComunicadoImplantacao.findByDtPrevisaoImplantacao", query = "SELECT c FROM ComunicadoImplantacao c WHERE c.dtPrevisaoImplantacao = :dtPrevisaoImplantacao"),
    @NamedQuery(name = "ComunicadoImplantacao.findByDescricao", query = "SELECT c FROM ComunicadoImplantacao c WHERE c.descricao = :descricao"),
    @NamedQuery(name = "ComunicadoImplantacao.findByStatus", query = "SELECT c FROM ComunicadoImplantacao c WHERE c.status = :status"),
    @NamedQuery(name = "ComunicadoImplantacao.findByAreaAproximada", query = "SELECT c FROM ComunicadoImplantacao c WHERE c.areaAproximada = :areaAproximada"),
    @NamedQuery(name = "ComunicadoImplantacao.findByInformacoesDiversas", query = "SELECT c FROM ComunicadoImplantacao c WHERE c.informacoesDiversas = :informacoesDiversas"),
    @NamedQuery(name = "ComunicadoImplantacao.findByExigencias", query = "SELECT c FROM ComunicadoImplantacao c WHERE c.exigencias = :exigencias"),
    @NamedQuery(name = "ComunicadoImplantacao.findByRecomendacoes", query = "SELECT c FROM ComunicadoImplantacao c WHERE c.recomendacoes = :recomendacoes"),
    @NamedQuery(name = "ComunicadoImplantacao.findByObservacao", query = "SELECT c FROM ComunicadoImplantacao c WHERE c.observacao = :observacao")})
public class ComunicadoImplantacao implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_comunicado_implantacao")
    private Integer idComunicadoImplantacao;
    @Column(name = "dt_solicitacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtSolicitacao;
    @Column(name = "dt_aprovacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtAprovacao;
    @Column(name = "dt_previsao_implantacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtPrevisaoImplantacao;
    @Size(max = 255)
    @Column(name = "descricao")
    private String descricao;
    @Size(max = 100)
    @Column(name = "status")
    private String status;
    @Size(max = 100)
    @Column(name = "area_aproximada")
    private String areaAproximada;
    @Size(max = 255)
    @Column(name = "informacoes_diversas")
    private String informacoesDiversas;
    @Size(max = 255)
    @Column(name = "exigencias")
    private String exigencias;
    @Size(max = 255)
    @Column(name = "recomendacoes")
    private String recomendacoes;
    @Size(max = 255)
    @Column(name = "observacao")
    private String observacao;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    @ManyToOne
    private Cliente idCliente;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne
    private Usuario idUsuario;
    @JoinColumn(name = "id_contrato", referencedColumnName = "id_contrato")
    @ManyToOne
    private Contrato idContrato;
    @JoinColumn(name = "id_local_trabalho", referencedColumnName = "id_local_trabalho")
    @ManyToOne(optional = false)
    private LocalTrabalho idLocalTrabalho;
    @OneToMany(mappedBy = "idComunicadoImplantacao")
    private List<ComunicadoImplFinanceiro> comunicadoImplFinanceiroList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idComunicadoImplantacao")
    private List<ComunicadoImplItem> comunicadoImplItemList;

    public ComunicadoImplantacao() {
    }

    public ComunicadoImplantacao(Integer idComunicadoImplantacao) {
        this.idComunicadoImplantacao = idComunicadoImplantacao;
    }

    public Integer getIdComunicadoImplantacao() {
        return idComunicadoImplantacao;
    }

    public void setIdComunicadoImplantacao(Integer idComunicadoImplantacao) {
        this.idComunicadoImplantacao = idComunicadoImplantacao;
    }

    public Date getDtSolicitacao() {
        return dtSolicitacao;
    }

    public void setDtSolicitacao(Date dtSolicitacao) {
        this.dtSolicitacao = dtSolicitacao;
    }

    public Date getDtAprovacao() {
        return dtAprovacao;
    }

    public void setDtAprovacao(Date dtAprovacao) {
        this.dtAprovacao = dtAprovacao;
    }

    public Date getDtPrevisaoImplantacao() {
        return dtPrevisaoImplantacao;
    }

    public void setDtPrevisaoImplantacao(Date dtPrevisaoImplantacao) {
        this.dtPrevisaoImplantacao = dtPrevisaoImplantacao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAreaAproximada() {
        return areaAproximada;
    }

    public void setAreaAproximada(String areaAproximada) {
        this.areaAproximada = areaAproximada;
    }

    public String getInformacoesDiversas() {
        return informacoesDiversas;
    }

    public void setInformacoesDiversas(String informacoesDiversas) {
        this.informacoesDiversas = informacoesDiversas;
    }

    public String getExigencias() {
        return exigencias;
    }

    public void setExigencias(String exigencias) {
        this.exigencias = exigencias;
    }

    public String getRecomendacoes() {
        return recomendacoes;
    }

    public void setRecomendacoes(String recomendacoes) {
        this.recomendacoes = recomendacoes;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Contrato getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(Contrato idContrato) {
        this.idContrato = idContrato;
    }

    public LocalTrabalho getIdLocalTrabalho() {
        return idLocalTrabalho;
    }

    public void setIdLocalTrabalho(LocalTrabalho idLocalTrabalho) {
        this.idLocalTrabalho = idLocalTrabalho;
    }

    @XmlTransient
    public List<ComunicadoImplFinanceiro> getComunicadoImplFinanceiroList() {
        return comunicadoImplFinanceiroList;
    }

    public void setComunicadoImplFinanceiroList(List<ComunicadoImplFinanceiro> comunicadoImplFinanceiroList) {
        this.comunicadoImplFinanceiroList = comunicadoImplFinanceiroList;
    }

    @XmlTransient
    public List<ComunicadoImplItem> getComunicadoImplItemList() {
        return comunicadoImplItemList;
    }

    public void setComunicadoImplItemList(List<ComunicadoImplItem> comunicadoImplItemList) {
        this.comunicadoImplItemList = comunicadoImplItemList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idComunicadoImplantacao != null ? idComunicadoImplantacao.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComunicadoImplantacao)) {
            return false;
        }
        ComunicadoImplantacao other = (ComunicadoImplantacao) object;
        if ((this.idComunicadoImplantacao == null && other.idComunicadoImplantacao != null) || (this.idComunicadoImplantacao != null && !this.idComunicadoImplantacao.equals(other.idComunicadoImplantacao))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.ComunicadoImplantacao[ idComunicadoImplantacao=" + idComunicadoImplantacao + " ]";
    }
    
}
