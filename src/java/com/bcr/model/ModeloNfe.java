/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "modelo_nfe")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ModeloNfe.findAll", query = "SELECT m FROM ModeloNfe m"),
    @NamedQuery(name = "ModeloNfe.findByIdModelo", query = "SELECT m FROM ModeloNfe m WHERE m.idModelo = :idModelo"),
    @NamedQuery(name = "ModeloNfe.findByDescricao", query = "SELECT m FROM ModeloNfe m WHERE m.descricao = :descricao"),
    @NamedQuery(name = "ModeloNfe.findByCodigo", query = "SELECT m FROM ModeloNfe m WHERE m.codigo = :codigo")})
public class ModeloNfe implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_modelo")
    private Integer idModelo;
    @Size(max = 255)
    @Column(name = "descricao")
    private String descricao;
    @Size(max = 2)
    @Column(name = "codigo")
    private String codigo;
    @OneToMany(mappedBy = "idModelo")
    private List<NotaEntrada> notaEntradaList;
    @OneToMany(mappedBy = "idModelo")
    private List<NotaSaida> notaSaidaList;

    public ModeloNfe() {
    }

    public ModeloNfe(Integer idModelo) {
        this.idModelo = idModelo;
    }

    public Integer getIdModelo() {
        return idModelo;
    }

    public void setIdModelo(Integer idModelo) {
        this.idModelo = idModelo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @XmlTransient
    public List<NotaEntrada> getNotaEntradaList() {
        return notaEntradaList;
    }

    public void setNotaEntradaList(List<NotaEntrada> notaEntradaList) {
        this.notaEntradaList = notaEntradaList;
    }

    @XmlTransient
    public List<NotaSaida> getNotaSaidaList() {
        return notaSaidaList;
    }

    public void setNotaSaidaList(List<NotaSaida> notaSaidaList) {
        this.notaSaidaList = notaSaidaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idModelo != null ? idModelo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ModeloNfe)) {
            return false;
        }
        ModeloNfe other = (ModeloNfe) object;
        if ((this.idModelo == null && other.idModelo != null) || (this.idModelo != null && !this.idModelo.equals(other.idModelo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.ModeloNfe[ idModelo=" + idModelo + " ]";
    }
    
}
