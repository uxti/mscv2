/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "fornecedor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fornecedor.findAll", query = "SELECT f FROM Fornecedor f"),
    @NamedQuery(name = "Fornecedor.findByIdFornecedor", query = "SELECT f FROM Fornecedor f WHERE f.idFornecedor = :idFornecedor"),
    @NamedQuery(name = "Fornecedor.findByNome", query = "SELECT f FROM Fornecedor f WHERE f.nome = :nome"),
    @NamedQuery(name = "Fornecedor.findByCgccpf", query = "SELECT f FROM Fornecedor f WHERE f.cgccpf = :cgccpf"),
    @NamedQuery(name = "Fornecedor.findByIe", query = "SELECT f FROM Fornecedor f WHERE f.ie = :ie"),
    @NamedQuery(name = "Fornecedor.findByTipoFornecedor", query = "SELECT f FROM Fornecedor f WHERE f.tipoFornecedor = :tipoFornecedor"),
    @NamedQuery(name = "Fornecedor.findByCep", query = "SELECT f FROM Fornecedor f WHERE f.cep = :cep"),
    @NamedQuery(name = "Fornecedor.findByTipoLogradouro", query = "SELECT f FROM Fornecedor f WHERE f.tipoLogradouro = :tipoLogradouro"),
    @NamedQuery(name = "Fornecedor.findByLogradouro", query = "SELECT f FROM Fornecedor f WHERE f.logradouro = :logradouro"),
    @NamedQuery(name = "Fornecedor.findByNumero", query = "SELECT f FROM Fornecedor f WHERE f.numero = :numero"),
    @NamedQuery(name = "Fornecedor.findByBairro", query = "SELECT f FROM Fornecedor f WHERE f.bairro = :bairro"),
    @NamedQuery(name = "Fornecedor.findByComplemento", query = "SELECT f FROM Fornecedor f WHERE f.complemento = :complemento"),
    @NamedQuery(name = "Fornecedor.findByEmail", query = "SELECT f FROM Fornecedor f WHERE f.email = :email"),
    @NamedQuery(name = "Fornecedor.findByHomePage", query = "SELECT f FROM Fornecedor f WHERE f.homePage = :homePage"),
    @NamedQuery(name = "Fornecedor.findByDtCadastro", query = "SELECT f FROM Fornecedor f WHERE f.dtCadastro = :dtCadastro"),
    @NamedQuery(name = "Fornecedor.findByObservacao", query = "SELECT f FROM Fornecedor f WHERE f.observacao = :observacao"),
    @NamedQuery(name = "Fornecedor.findByInativo", query = "SELECT f FROM Fornecedor f WHERE f.inativo = :inativo"),
    @NamedQuery(name = "Fornecedor.findByCodigoGps", query = "SELECT f FROM Fornecedor f WHERE f.codigoGps = :codigoGps"),
    @NamedQuery(name = "Fornecedor.findByNumJuridico", query = "SELECT f FROM Fornecedor f WHERE f.numJuridico = :numJuridico")})
public class Fornecedor implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_fornecedor")
    private Integer idFornecedor;
    @Size(max = 255)
    @Column(name = "nome")
    private String nome;
    @Size(max = 20)
    @Column(name = "CGCCPF")
    private String cgccpf;
    @Size(max = 50)
    @Column(name = "IE")
    private String ie;
    @Size(max = 1)
    @Column(name = "tipo_fornecedor")
    private String tipoFornecedor;
    @Size(max = 9)
    @Column(name = "CEP")
    private String cep;
    @Size(max = 20)
    @Column(name = "tipo_logradouro")
    private String tipoLogradouro;
    @Size(max = 200)
    @Column(name = "logradouro")
    private String logradouro;
    @Size(max = 5)
    @Column(name = "numero")
    private String numero;
    @Size(max = 255)
    @Column(name = "bairro")
    private String bairro;
    @Size(max = 255)
    @Column(name = "complemento")
    private String complemento;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="E-mail inválido")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 255)
    @Column(name = "email")
    private String email;
    @Size(max = 255)
    @Column(name = "home_page")
    private String homePage;
    @Column(name = "dt_cadastro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtCadastro;
    @Size(max = 255)
    @Column(name = "observacao")
    private String observacao;
    @Size(max = 1)
    @Column(name = "inativo")
    private String inativo;
    @Size(max = 20)
    @Column(name = "codigo_gps")
    private String codigoGps;
    @Size(max = 20)
    @Column(name = "num_juridico")
    private String numJuridico;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idFornecedor")
    private List<ProdutoFornecedor> produtoFornecedorList;
    @OneToMany(mappedBy = "idFornecedor")
    private List<ContasPagar> contasPagarList;
    @OneToMany(mappedBy = "idFornecedor")
    private List<NotaEntrada> notaEntradaList;
    @OneToMany(mappedBy = "idFornecedorSelecionado")
    private List<CotacaoItem> cotacaoItemList;
    @OneToMany(mappedBy = "idFornecedor")
    private List<Cotacao> cotacaoList;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @JoinColumn(name = "id_cidade", referencedColumnName = "id_cidade")
    @ManyToOne
    private Cidade idCidade;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    @ManyToOne
    private Cliente idCliente;
    @OneToMany(mappedBy = "idFornecedor")
    private List<CotacaoItemFornecedor> cotacaoItemFornecedorList;
    @OneToMany(mappedBy = "idFornecedor")
    private List<Cliente> clienteList;
    @OneToMany(mappedBy = "idFornecedor")
    private List<PedidoCompra> pedidoCompraList;
    @OneToMany(mappedBy = "idFornecedor")
    private List<Cheque> chequeList;
    @OneToMany(mappedBy = "idFornecedor")
    private List<NotaSaida> notaSaidaList;
    @OneToMany(mappedBy = "idFornecedor")
    private List<Telefone> telefoneList;

    public Fornecedor() {
    }

    public Fornecedor(Integer idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    public Integer getIdFornecedor() {
        return idFornecedor;
    }

    public void setIdFornecedor(Integer idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCgccpf() {
        return cgccpf;
    }

    public void setCgccpf(String cgccpf) {
        this.cgccpf = cgccpf;
    }

    public String getIe() {
        return ie;
    }

    public void setIe(String ie) {
        this.ie = ie;
    }

    public String getTipoFornecedor() {
        return tipoFornecedor;
    }

    public void setTipoFornecedor(String tipoFornecedor) {
        this.tipoFornecedor = tipoFornecedor;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getTipoLogradouro() {
        return tipoLogradouro;
    }

    public void setTipoLogradouro(String tipoLogradouro) {
        this.tipoLogradouro = tipoLogradouro;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHomePage() {
        return homePage;
    }

    public void setHomePage(String homePage) {
        this.homePage = homePage;
    }

    public Date getDtCadastro() {
        return dtCadastro;
    }

    public void setDtCadastro(Date dtCadastro) {
        this.dtCadastro = dtCadastro;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getInativo() {
        return inativo;
    }

    public void setInativo(String inativo) {
        this.inativo = inativo;
    }

    public String getCodigoGps() {
        return codigoGps;
    }

    public void setCodigoGps(String codigoGps) {
        this.codigoGps = codigoGps;
    }

    public String getNumJuridico() {
        return numJuridico;
    }

    public void setNumJuridico(String numJuridico) {
        this.numJuridico = numJuridico;
    }

    @XmlTransient
    public List<ProdutoFornecedor> getProdutoFornecedorList() {
        return produtoFornecedorList;
    }

    public void setProdutoFornecedorList(List<ProdutoFornecedor> produtoFornecedorList) {
        this.produtoFornecedorList = produtoFornecedorList;
    }

    @XmlTransient
    public List<ContasPagar> getContasPagarList() {
        return contasPagarList;
    }

    public void setContasPagarList(List<ContasPagar> contasPagarList) {
        this.contasPagarList = contasPagarList;
    }

    @XmlTransient
    public List<NotaEntrada> getNotaEntradaList() {
        return notaEntradaList;
    }

    public void setNotaEntradaList(List<NotaEntrada> notaEntradaList) {
        this.notaEntradaList = notaEntradaList;
    }

    @XmlTransient
    public List<CotacaoItem> getCotacaoItemList() {
        return cotacaoItemList;
    }

    public void setCotacaoItemList(List<CotacaoItem> cotacaoItemList) {
        this.cotacaoItemList = cotacaoItemList;
    }

    @XmlTransient
    public List<Cotacao> getCotacaoList() {
        return cotacaoList;
    }

    public void setCotacaoList(List<Cotacao> cotacaoList) {
        this.cotacaoList = cotacaoList;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Cidade getIdCidade() {
        return idCidade;
    }

    public void setIdCidade(Cidade idCidade) {
        this.idCidade = idCidade;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    @XmlTransient
    public List<CotacaoItemFornecedor> getCotacaoItemFornecedorList() {
        return cotacaoItemFornecedorList;
    }

    public void setCotacaoItemFornecedorList(List<CotacaoItemFornecedor> cotacaoItemFornecedorList) {
        this.cotacaoItemFornecedorList = cotacaoItemFornecedorList;
    }

    @XmlTransient
    public List<Cliente> getClienteList() {
        return clienteList;
    }

    public void setClienteList(List<Cliente> clienteList) {
        this.clienteList = clienteList;
    }

    @XmlTransient
    public List<PedidoCompra> getPedidoCompraList() {
        return pedidoCompraList;
    }

    public void setPedidoCompraList(List<PedidoCompra> pedidoCompraList) {
        this.pedidoCompraList = pedidoCompraList;
    }

    @XmlTransient
    public List<Cheque> getChequeList() {
        return chequeList;
    }

    public void setChequeList(List<Cheque> chequeList) {
        this.chequeList = chequeList;
    }

    @XmlTransient
    public List<NotaSaida> getNotaSaidaList() {
        return notaSaidaList;
    }

    public void setNotaSaidaList(List<NotaSaida> notaSaidaList) {
        this.notaSaidaList = notaSaidaList;
    }

    @XmlTransient
    public List<Telefone> getTelefoneList() {
        return telefoneList;
    }

    public void setTelefoneList(List<Telefone> telefoneList) {
        this.telefoneList = telefoneList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFornecedor != null ? idFornecedor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fornecedor)) {
            return false;
        }
        Fornecedor other = (Fornecedor) object;
        if ((this.idFornecedor == null && other.idFornecedor != null) || (this.idFornecedor != null && !this.idFornecedor.equals(other.idFornecedor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Fornecedor[ idFornecedor=" + idFornecedor + " ]";
    }
    
}
