/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "plano_contas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PlanoContas.findAll", query = "SELECT p FROM PlanoContas p"),
    @NamedQuery(name = "PlanoContas.findByIdPlanoContas", query = "SELECT p FROM PlanoContas p WHERE p.idPlanoContas = :idPlanoContas"),
    @NamedQuery(name = "PlanoContas.findByCodigo", query = "SELECT p FROM PlanoContas p WHERE p.codigo = :codigo"),
    @NamedQuery(name = "PlanoContas.findByTipo", query = "SELECT p FROM PlanoContas p WHERE p.tipo = :tipo"),
    @NamedQuery(name = "PlanoContas.findByDescricao", query = "SELECT p FROM PlanoContas p WHERE p.descricao = :descricao"),
    @NamedQuery(name = "PlanoContas.findByCreditodebito", query = "SELECT p FROM PlanoContas p WHERE p.creditodebito = :creditodebito")})
public class PlanoContas implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_plano_contas")
    private Integer idPlanoContas;
    @Size(max = 10)
    @Column(name = "codigo")
    private String codigo;
    @Size(max = 1)
    @Column(name = "tipo")
    private String tipo;
    @Size(max = 255)
    @Column(name = "descricao")
    private String descricao;
    @Size(max = 1)
    @Column(name = "creditodebito")
    private String creditodebito;
    @OneToMany(mappedBy = "idPlanoContas")
    private List<ContasPagar> contasPagarList;
    @OneToMany(mappedBy = "idPlanoContas")
    private List<NotaEntrada> notaEntradaList;
    @OneToMany(mappedBy = "idPlanoContasReferente")
    private List<PlanoContas> planoContasList;
    @JoinColumn(name = "ID_PLANO_CONTAS_REFERENTE", referencedColumnName = "id_plano_contas")
    @ManyToOne
    private PlanoContas idPlanoContasReferente;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @JoinColumn(name = "id_agrupamento", referencedColumnName = "id_agrupamento")
    @ManyToOne(optional = false)
    private Agrupamento idAgrupamento;
    @JoinColumn(name = "id_contaDRE", referencedColumnName = "id_contaDRE")
    @ManyToOne
    private ContasDre idcontaDRE;
    @OneToMany(mappedBy = "idPlanoContas")
    private List<MovimentacaoBancaria> movimentacaoBancariaList;
    @OneToMany(mappedBy = "idPlanoContas")
    private List<NotaSaida> notaSaidaList;
    @OneToMany(mappedBy = "idPlanoContas")
    private List<CaixaItem> caixaItemList;
    @OneToMany(mappedBy = "idPlanoContas")
    private List<Centrocustoplanocontas> centrocustoplanocontasList;
    @OneToMany(mappedBy = "idPlanoDespesa")
    private List<Empresa> empresaList;
    @OneToMany(mappedBy = "idPlanoReceita")
    private List<Empresa> empresaList1;
    @OneToMany(mappedBy = "idPlanoContas")
    private List<ContasReceber> contasReceberList;

    public PlanoContas() {
    }

    public PlanoContas(Integer idPlanoContas) {
        this.idPlanoContas = idPlanoContas;
    }

    public Integer getIdPlanoContas() {
        return idPlanoContas;
    }

    public void setIdPlanoContas(Integer idPlanoContas) {
        this.idPlanoContas = idPlanoContas;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getCreditodebito() {
        return creditodebito;
    }

    public void setCreditodebito(String creditodebito) {
        this.creditodebito = creditodebito;
    }

    @XmlTransient
    public List<ContasPagar> getContasPagarList() {
        return contasPagarList;
    }

    public void setContasPagarList(List<ContasPagar> contasPagarList) {
        this.contasPagarList = contasPagarList;
    }

    @XmlTransient
    public List<NotaEntrada> getNotaEntradaList() {
        return notaEntradaList;
    }

    public void setNotaEntradaList(List<NotaEntrada> notaEntradaList) {
        this.notaEntradaList = notaEntradaList;
    }

    @XmlTransient
    public List<PlanoContas> getPlanoContasList() {
        return planoContasList;
    }

    public void setPlanoContasList(List<PlanoContas> planoContasList) {
        this.planoContasList = planoContasList;
    }

    public PlanoContas getIdPlanoContasReferente() {
        return idPlanoContasReferente;
    }

    public void setIdPlanoContasReferente(PlanoContas idPlanoContasReferente) {
        this.idPlanoContasReferente = idPlanoContasReferente;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Agrupamento getIdAgrupamento() {
        return idAgrupamento;
    }

    public void setIdAgrupamento(Agrupamento idAgrupamento) {
        this.idAgrupamento = idAgrupamento;
    }

    public ContasDre getIdcontaDRE() {
        return idcontaDRE;
    }

    public void setIdcontaDRE(ContasDre idcontaDRE) {
        this.idcontaDRE = idcontaDRE;
    }

    @XmlTransient
    public List<MovimentacaoBancaria> getMovimentacaoBancariaList() {
        return movimentacaoBancariaList;
    }

    public void setMovimentacaoBancariaList(List<MovimentacaoBancaria> movimentacaoBancariaList) {
        this.movimentacaoBancariaList = movimentacaoBancariaList;
    }

    @XmlTransient
    public List<NotaSaida> getNotaSaidaList() {
        return notaSaidaList;
    }

    public void setNotaSaidaList(List<NotaSaida> notaSaidaList) {
        this.notaSaidaList = notaSaidaList;
    }

    @XmlTransient
    public List<CaixaItem> getCaixaItemList() {
        return caixaItemList;
    }

    public void setCaixaItemList(List<CaixaItem> caixaItemList) {
        this.caixaItemList = caixaItemList;
    }

    @XmlTransient
    public List<Centrocustoplanocontas> getCentrocustoplanocontasList() {
        return centrocustoplanocontasList;
    }

    public void setCentrocustoplanocontasList(List<Centrocustoplanocontas> centrocustoplanocontasList) {
        this.centrocustoplanocontasList = centrocustoplanocontasList;
    }

    @XmlTransient
    public List<Empresa> getEmpresaList() {
        return empresaList;
    }

    public void setEmpresaList(List<Empresa> empresaList) {
        this.empresaList = empresaList;
    }

    @XmlTransient
    public List<Empresa> getEmpresaList1() {
        return empresaList1;
    }

    public void setEmpresaList1(List<Empresa> empresaList1) {
        this.empresaList1 = empresaList1;
    }

    @XmlTransient
    public List<ContasReceber> getContasReceberList() {
        return contasReceberList;
    }

    public void setContasReceberList(List<ContasReceber> contasReceberList) {
        this.contasReceberList = contasReceberList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPlanoContas != null ? idPlanoContas.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlanoContas)) {
            return false;
        }
        PlanoContas other = (PlanoContas) object;
        if ((this.idPlanoContas == null && other.idPlanoContas != null) || (this.idPlanoContas != null && !this.idPlanoContas.equals(other.idPlanoContas))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.PlanoContas[ idPlanoContas=" + idPlanoContas + " ]";
    }
    
}
