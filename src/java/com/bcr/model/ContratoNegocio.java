/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "contrato_negocio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContratoNegocio.findAll", query = "SELECT c FROM ContratoNegocio c"),
    @NamedQuery(name = "ContratoNegocio.findByIdContratoNegocio", query = "SELECT c FROM ContratoNegocio c WHERE c.idContratoNegocio = :idContratoNegocio")})
public class ContratoNegocio implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_contrato_negocio")
    private Integer idContratoNegocio;
    @JoinColumn(name = "id_contrato", referencedColumnName = "id_contrato")
    @ManyToOne(optional = false)
    private Contrato idContrato;
    @JoinColumn(name = "id_negocio", referencedColumnName = "id_negocio")
    @ManyToOne(optional = false)
    private Negocio idNegocio;

    public ContratoNegocio() {
    }

    public ContratoNegocio(Integer idContratoNegocio) {
        this.idContratoNegocio = idContratoNegocio;
    }

    public Integer getIdContratoNegocio() {
        return idContratoNegocio;
    }

    public void setIdContratoNegocio(Integer idContratoNegocio) {
        this.idContratoNegocio = idContratoNegocio;
    }

    public Contrato getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(Contrato idContrato) {
        this.idContrato = idContrato;
    }

    public Negocio getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Negocio idNegocio) {
        this.idNegocio = idNegocio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idContratoNegocio != null ? idContratoNegocio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContratoNegocio)) {
            return false;
        }
        ContratoNegocio other = (ContratoNegocio) object;
        if ((this.idContratoNegocio == null && other.idContratoNegocio != null) || (this.idContratoNegocio != null && !this.idContratoNegocio.equals(other.idContratoNegocio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.ContratoNegocio[ idContratoNegocio=" + idContratoNegocio + " ]";
    }
    
}
