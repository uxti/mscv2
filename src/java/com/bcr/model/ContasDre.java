/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "contas_dre")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContasDre.findAll", query = "SELECT c FROM ContasDre c"),
    @NamedQuery(name = "ContasDre.findByIdcontaDRE", query = "SELECT c FROM ContasDre c WHERE c.idcontaDRE = :idcontaDRE"),
    @NamedQuery(name = "ContasDre.findByDescricao", query = "SELECT c FROM ContasDre c WHERE c.descricao = :descricao")})
public class ContasDre implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_contaDRE")
    private Integer idcontaDRE;
    @Size(max = 255)
    @Column(name = "descricao")
    private String descricao;
    @OneToMany(mappedBy = "idcontaDRE")
    private List<PlanoContas> planoContasList;

    public ContasDre() {
    }

    public ContasDre(Integer idcontaDRE) {
        this.idcontaDRE = idcontaDRE;
    }

    public Integer getIdcontaDRE() {
        return idcontaDRE;
    }

    public void setIdcontaDRE(Integer idcontaDRE) {
        this.idcontaDRE = idcontaDRE;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @XmlTransient
    public List<PlanoContas> getPlanoContasList() {
        return planoContasList;
    }

    public void setPlanoContasList(List<PlanoContas> planoContasList) {
        this.planoContasList = planoContasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcontaDRE != null ? idcontaDRE.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContasDre)) {
            return false;
        }
        ContasDre other = (ContasDre) object;
        if ((this.idcontaDRE == null && other.idcontaDRE != null) || (this.idcontaDRE != null && !this.idcontaDRE.equals(other.idcontaDRE))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.ContasDre[ idcontaDRE=" + idcontaDRE + " ]";
    }
    
}
