/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "produto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Produto.findAll", query = "SELECT p FROM Produto p"),
    @NamedQuery(name = "Produto.findByIdProduto", query = "SELECT p FROM Produto p WHERE p.idProduto = :idProduto"),
    @NamedQuery(name = "Produto.findByDescricao", query = "SELECT p FROM Produto p WHERE p.descricao = :descricao"),
    @NamedQuery(name = "Produto.findByDtCadastro", query = "SELECT p FROM Produto p WHERE p.dtCadastro = :dtCadastro"),
    @NamedQuery(name = "Produto.findByDtUltimaAtualizacao", query = "SELECT p FROM Produto p WHERE p.dtUltimaAtualizacao = :dtUltimaAtualizacao"),
    @NamedQuery(name = "Produto.findByInativo", query = "SELECT p FROM Produto p WHERE p.inativo = :inativo"),
    @NamedQuery(name = "Produto.findByReferenciaFabricante", query = "SELECT p FROM Produto p WHERE p.referenciaFabricante = :referenciaFabricante"),
    @NamedQuery(name = "Produto.findByDescricaoReduzida", query = "SELECT p FROM Produto p WHERE p.descricaoReduzida = :descricaoReduzida"),
    @NamedQuery(name = "Produto.findByQuantidade", query = "SELECT p FROM Produto p WHERE p.quantidade = :quantidade"),
    @NamedQuery(name = "Produto.findByEstoqueMin", query = "SELECT p FROM Produto p WHERE p.estoqueMin = :estoqueMin"),
    @NamedQuery(name = "Produto.findByEstoqueMax", query = "SELECT p FROM Produto p WHERE p.estoqueMax = :estoqueMax"),
    @NamedQuery(name = "Produto.findByPrecoCusto", query = "SELECT p FROM Produto p WHERE p.precoCusto = :precoCusto"),
    @NamedQuery(name = "Produto.findByPrecoCustoAnterior", query = "SELECT p FROM Produto p WHERE p.precoCustoAnterior = :precoCustoAnterior"),
    @NamedQuery(name = "Produto.findByPrecoCustoInicial", query = "SELECT p FROM Produto p WHERE p.precoCustoInicial = :precoCustoInicial"),
    @NamedQuery(name = "Produto.findByPrecoCustoPromocional", query = "SELECT p FROM Produto p WHERE p.precoCustoPromocional = :precoCustoPromocional"),
    @NamedQuery(name = "Produto.findByBaseConversao", query = "SELECT p FROM Produto p WHERE p.baseConversao = :baseConversao"),
    @NamedQuery(name = "Produto.findByFatorConversaoEntrada", query = "SELECT p FROM Produto p WHERE p.fatorConversaoEntrada = :fatorConversaoEntrada"),
    @NamedQuery(name = "Produto.findByFatorConversaoSaida", query = "SELECT p FROM Produto p WHERE p.fatorConversaoSaida = :fatorConversaoSaida"),
    @NamedQuery(name = "Produto.findByFracionado", query = "SELECT p FROM Produto p WHERE p.fracionado = :fracionado"),
    @NamedQuery(name = "Produto.findByTipoTributacao", query = "SELECT p FROM Produto p WHERE p.tipoTributacao = :tipoTributacao"),
    @NamedQuery(name = "Produto.findByCst", query = "SELECT p FROM Produto p WHERE p.cst = :cst"),
    @NamedQuery(name = "Produto.findByAliqIcmsEntrada", query = "SELECT p FROM Produto p WHERE p.aliqIcmsEntrada = :aliqIcmsEntrada"),
    @NamedQuery(name = "Produto.findByAliqIcmsSaida", query = "SELECT p FROM Produto p WHERE p.aliqIcmsSaida = :aliqIcmsSaida"),
    @NamedQuery(name = "Produto.findByBaseCalculoIcms", query = "SELECT p FROM Produto p WHERE p.baseCalculoIcms = :baseCalculoIcms"),
    @NamedQuery(name = "Produto.findByAliquotaIpi", query = "SELECT p FROM Produto p WHERE p.aliquotaIpi = :aliquotaIpi"),
    @NamedQuery(name = "Produto.findByEan", query = "SELECT p FROM Produto p WHERE p.ean = :ean"),
    @NamedQuery(name = "Produto.findByAliquotaCofins", query = "SELECT p FROM Produto p WHERE p.aliquotaCofins = :aliquotaCofins"),
    @NamedQuery(name = "Produto.findByAliquotaPis", query = "SELECT p FROM Produto p WHERE p.aliquotaPis = :aliquotaPis"),
    @NamedQuery(name = "Produto.findByCstPisEntrada", query = "SELECT p FROM Produto p WHERE p.cstPisEntrada = :cstPisEntrada"),
    @NamedQuery(name = "Produto.findByCstPisSaida", query = "SELECT p FROM Produto p WHERE p.cstPisSaida = :cstPisSaida"),
    @NamedQuery(name = "Produto.findByCstCofinsEntrada", query = "SELECT p FROM Produto p WHERE p.cstCofinsEntrada = :cstCofinsEntrada"),
    @NamedQuery(name = "Produto.findByCstCofinsSaida", query = "SELECT p FROM Produto p WHERE p.cstCofinsSaida = :cstCofinsSaida"),
    @NamedQuery(name = "Produto.findByAliquotaPisSaida", query = "SELECT p FROM Produto p WHERE p.aliquotaPisSaida = :aliquotaPisSaida"),
    @NamedQuery(name = "Produto.findByAliquotaCofinsSaida", query = "SELECT p FROM Produto p WHERE p.aliquotaCofinsSaida = :aliquotaCofinsSaida"),
    @NamedQuery(name = "Produto.findByCsosn", query = "SELECT p FROM Produto p WHERE p.csosn = :csosn"),
    @NamedQuery(name = "Produto.findByLocalizacaoDescrita", query = "SELECT p FROM Produto p WHERE p.localizacaoDescrita = :localizacaoDescrita")})
public class Produto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_produto")
    private Integer idProduto;
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "dt_cadastro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtCadastro;
    @Basic(optional = false)
    @Column(name = "dt_ultima_atualizacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtUltimaAtualizacao;
    @Column(name = "inativo")
    private String inativo;
    @Column(name = "referencia_fabricante")
    private String referenciaFabricante;
    @Column(name = "descricao_reduzida")
    private String descricaoReduzida;
    @Column(name = "quantidade")
    private Integer quantidade;
    @Column(name = "quantidade_reservada")
    private Integer quantidadeReservada;
    @Column(name = "estoque_min")
    private Integer estoqueMin;
    @Column(name = "estoque_max")
    private Integer estoqueMax;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "preco_custo")
    private BigDecimal precoCusto;
    @Column(name = "preco_custo_anterior")
    private BigDecimal precoCustoAnterior;
    @Column(name = "preco_custo_inicial")
    private BigDecimal precoCustoInicial;
    @Column(name = "preco_custo_promocional")
    private BigDecimal precoCustoPromocional;
    @Column(name = "base_conversao")
    private String baseConversao;
    @Column(name = "fator_conversao_entrada")
    private BigDecimal fatorConversaoEntrada;
    @Column(name = "fator_conversao_saida")
    private BigDecimal fatorConversaoSaida;
    @Column(name = "fracionado")
    private String fracionado;
    @Column(name = "tipo_tributacao")
    private String tipoTributacao;
    @Column(name = "CST")
    private String cst;
    @Column(name = "aliq_icms_entrada")
    private BigDecimal aliqIcmsEntrada;
    @Column(name = "aliq_icms_saida")
    private BigDecimal aliqIcmsSaida;
    @Column(name = "base_calculo_icms")
    private BigDecimal baseCalculoIcms;
    @Column(name = "aliquota_ipi")
    private BigDecimal aliquotaIpi;
    @Column(name = "EAN")
    private String ean;
    @Column(name = "aliquota_cofins")
    private BigDecimal aliquotaCofins;
    @Column(name = "aliquota_pis")
    private BigDecimal aliquotaPis;
    @Column(name = "CST_PIS_ENTRADA")
    private Integer cstPisEntrada;
    @Column(name = "CST_PIS_SAIDA")
    private Integer cstPisSaida;
    @Column(name = "CST_COFINS_ENTRADA")
    private Integer cstCofinsEntrada;
    @Column(name = "CST_COFINS_SAIDA")
    private Integer cstCofinsSaida;
    @Column(name = "aliquota_pis_saida")
    private BigDecimal aliquotaPisSaida;
    @Column(name = "aliquota_cofins_saida")
    private BigDecimal aliquotaCofinsSaida;
    @Column(name = "CSOSN")
    private String csosn;
    @Column(name = "LOCALIZACAO_DESCRITA")
    private String localizacaoDescrita;
    @OneToMany(mappedBy = "idProduto")
    private List<ProdutoFornecedor> produtoFornecedorList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProduto")
    private List<NotaSaidaItem> notaSaidaItemList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProduto")
    private List<CotacaoItem> cotacaoItemList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProduto")
    private List<RequisicaoMaterialItem> requisicaoMaterialItemList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProduto")
    private List<NotaEntradaItem> notaEntradaItemList;
    @JoinColumn(name = "id_unidade_saida", referencedColumnName = "id_unidade")
    @ManyToOne
    private Unidade idUnidadeSaida;
    @JoinColumn(name = "ID_LOCALIZACAO", referencedColumnName = "ID_LOCALIZACAO")
    @ManyToOne
    private Localizacao idLocalizacao;
    @JoinColumn(name = "id_NCM", referencedColumnName = "ID_NCM")
    @ManyToOne
    private Ncm idNCM;
    @JoinColumn(name = "id_subgrupo", referencedColumnName = "id_subgrupo")
    @ManyToOne
    private SubGrupo idSubgrupo;
    @JoinColumn(name = "id_unidade_entrada", referencedColumnName = "id_unidade")
    @ManyToOne
    private Unidade idUnidadeEntrada;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @JoinColumn(name = "id_grupo", referencedColumnName = "id_grupo")
    @ManyToOne
    private Grupo idGrupo;

    public Produto() {
    }

    public Produto(Integer idProduto) {
        this.idProduto = idProduto;
    }

    public Produto(Integer idProduto, Date dtUltimaAtualizacao) {
        this.idProduto = idProduto;
        this.dtUltimaAtualizacao = dtUltimaAtualizacao;
    }

    public Integer getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Integer idProduto) {
        this.idProduto = idProduto;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getDtCadastro() {
        return dtCadastro;
    }

    public void setDtCadastro(Date dtCadastro) {
        this.dtCadastro = dtCadastro;
    }

    public Date getDtUltimaAtualizacao() {
        return dtUltimaAtualizacao;
    }

    public void setDtUltimaAtualizacao(Date dtUltimaAtualizacao) {
        this.dtUltimaAtualizacao = dtUltimaAtualizacao;
    }

    public String getInativo() {
        return inativo;
    }

    public void setInativo(String inativo) {
        this.inativo = inativo;
    }

    public String getReferenciaFabricante() {
        return referenciaFabricante;
    }

    public void setReferenciaFabricante(String referenciaFabricante) {
        this.referenciaFabricante = referenciaFabricante;
    }

    public String getDescricaoReduzida() {
        return descricaoReduzida;
    }

    public void setDescricaoReduzida(String descricaoReduzida) {
        this.descricaoReduzida = descricaoReduzida;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getEstoqueMin() {
        return estoqueMin;
    }

    public void setEstoqueMin(Integer estoqueMin) {
        this.estoqueMin = estoqueMin;
    }

    public Integer getEstoqueMax() {
        return estoqueMax;
    }

    public void setEstoqueMax(Integer estoqueMax) {
        this.estoqueMax = estoqueMax;
    }

    public BigDecimal getPrecoCusto() {
        return precoCusto;
    }

    public void setPrecoCusto(BigDecimal precoCusto) {
        this.precoCusto = precoCusto;
    }

    public BigDecimal getPrecoCustoAnterior() {
        return precoCustoAnterior;
    }

    public void setPrecoCustoAnterior(BigDecimal precoCustoAnterior) {
        this.precoCustoAnterior = precoCustoAnterior;
    }

    public BigDecimal getPrecoCustoInicial() {
        return precoCustoInicial;
    }

    public void setPrecoCustoInicial(BigDecimal precoCustoInicial) {
        this.precoCustoInicial = precoCustoInicial;
    }

    public BigDecimal getPrecoCustoPromocional() {
        return precoCustoPromocional;
    }

    public void setPrecoCustoPromocional(BigDecimal precoCustoPromocional) {
        this.precoCustoPromocional = precoCustoPromocional;
    }

    public String getBaseConversao() {
        return baseConversao;
    }

    public void setBaseConversao(String baseConversao) {
        this.baseConversao = baseConversao;
    }

    public BigDecimal getFatorConversaoEntrada() {
        return fatorConversaoEntrada;
    }

    public void setFatorConversaoEntrada(BigDecimal fatorConversaoEntrada) {
        this.fatorConversaoEntrada = fatorConversaoEntrada;
    }

    public BigDecimal getFatorConversaoSaida() {
        return fatorConversaoSaida;
    }

    public void setFatorConversaoSaida(BigDecimal fatorConversaoSaida) {
        this.fatorConversaoSaida = fatorConversaoSaida;
    }

    public String getFracionado() {
        return fracionado;
    }

    public void setFracionado(String fracionado) {
        this.fracionado = fracionado;
    }

    public String getTipoTributacao() {
        return tipoTributacao;
    }

    public void setTipoTributacao(String tipoTributacao) {
        this.tipoTributacao = tipoTributacao;
    }

    public String getCst() {
        return cst;
    }

    public void setCst(String cst) {
        this.cst = cst;
    }

    public BigDecimal getAliqIcmsEntrada() {
        return aliqIcmsEntrada;
    }

    public void setAliqIcmsEntrada(BigDecimal aliqIcmsEntrada) {
        this.aliqIcmsEntrada = aliqIcmsEntrada;
    }

    public BigDecimal getAliqIcmsSaida() {
        return aliqIcmsSaida;
    }

    public void setAliqIcmsSaida(BigDecimal aliqIcmsSaida) {
        this.aliqIcmsSaida = aliqIcmsSaida;
    }

    public BigDecimal getBaseCalculoIcms() {
        return baseCalculoIcms;
    }

    public void setBaseCalculoIcms(BigDecimal baseCalculoIcms) {
        this.baseCalculoIcms = baseCalculoIcms;
    }

    public BigDecimal getAliquotaIpi() {
        return aliquotaIpi;
    }

    public void setAliquotaIpi(BigDecimal aliquotaIpi) {
        this.aliquotaIpi = aliquotaIpi;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public BigDecimal getAliquotaCofins() {
        return aliquotaCofins;
    }

    public void setAliquotaCofins(BigDecimal aliquotaCofins) {
        this.aliquotaCofins = aliquotaCofins;
    }

    public BigDecimal getAliquotaPis() {
        return aliquotaPis;
    }

    public Integer getQuantidadeReservada() {
        return quantidadeReservada;
    }

    public void setQuantidadeReservada(Integer quantidadeReservada) {
        this.quantidadeReservada = quantidadeReservada;
    }

    
    public void setAliquotaPis(BigDecimal aliquotaPis) {
        this.aliquotaPis = aliquotaPis;
    }

    public Integer getCstPisEntrada() {
        return cstPisEntrada;
    }

    public void setCstPisEntrada(Integer cstPisEntrada) {
        this.cstPisEntrada = cstPisEntrada;
    }

    public Integer getCstPisSaida() {
        return cstPisSaida;
    }

    public void setCstPisSaida(Integer cstPisSaida) {
        this.cstPisSaida = cstPisSaida;
    }

    public Integer getCstCofinsEntrada() {
        return cstCofinsEntrada;
    }

    public void setCstCofinsEntrada(Integer cstCofinsEntrada) {
        this.cstCofinsEntrada = cstCofinsEntrada;
    }

    public Integer getCstCofinsSaida() {
        return cstCofinsSaida;
    }

    public void setCstCofinsSaida(Integer cstCofinsSaida) {
        this.cstCofinsSaida = cstCofinsSaida;
    }

    public BigDecimal getAliquotaPisSaida() {
        return aliquotaPisSaida;
    }

    public void setAliquotaPisSaida(BigDecimal aliquotaPisSaida) {
        this.aliquotaPisSaida = aliquotaPisSaida;
    }

    public BigDecimal getAliquotaCofinsSaida() {
        return aliquotaCofinsSaida;
    }

    public void setAliquotaCofinsSaida(BigDecimal aliquotaCofinsSaida) {
        this.aliquotaCofinsSaida = aliquotaCofinsSaida;
    }

    public String getCsosn() {
        return csosn;
    }

    public void setCsosn(String csosn) {
        this.csosn = csosn;
    }

    public String getLocalizacaoDescrita() {
        return localizacaoDescrita;
    }

    public void setLocalizacaoDescrita(String localizacaoDescrita) {
        this.localizacaoDescrita = localizacaoDescrita;
    }

    @XmlTransient
    public List<ProdutoFornecedor> getProdutoFornecedorList() {
        return produtoFornecedorList;
    }

    public void setProdutoFornecedorList(List<ProdutoFornecedor> produtoFornecedorList) {
        this.produtoFornecedorList = produtoFornecedorList;
    }

    @XmlTransient
    public List<NotaSaidaItem> getNotaSaidaItemList() {
        return notaSaidaItemList;
    }

    public void setNotaSaidaItemList(List<NotaSaidaItem> notaSaidaItemList) {
        this.notaSaidaItemList = notaSaidaItemList;
    }

    @XmlTransient
    public List<CotacaoItem> getCotacaoItemList() {
        return cotacaoItemList;
    }

    public void setCotacaoItemList(List<CotacaoItem> cotacaoItemList) {
        this.cotacaoItemList = cotacaoItemList;
    }

    @XmlTransient
    public List<RequisicaoMaterialItem> getRequisicaoMaterialItemList() {
        return requisicaoMaterialItemList;
    }

    public void setRequisicaoMaterialItemList(List<RequisicaoMaterialItem> requisicaoMaterialItemList) {
        this.requisicaoMaterialItemList = requisicaoMaterialItemList;
    }

    @XmlTransient
    public List<NotaEntradaItem> getNotaEntradaItemList() {
        return notaEntradaItemList;
    }

    public void setNotaEntradaItemList(List<NotaEntradaItem> notaEntradaItemList) {
        this.notaEntradaItemList = notaEntradaItemList;
    }

    public Unidade getIdUnidadeSaida() {
        return idUnidadeSaida;
    }

    public void setIdUnidadeSaida(Unidade idUnidadeSaida) {
        this.idUnidadeSaida = idUnidadeSaida;
    }

    public Localizacao getIdLocalizacao() {
        return idLocalizacao;
    }

    public void setIdLocalizacao(Localizacao idLocalizacao) {
        this.idLocalizacao = idLocalizacao;
    }

    public Ncm getIdNCM() {
        return idNCM;
    }

    public void setIdNCM(Ncm idNCM) {
        this.idNCM = idNCM;
    }

    public SubGrupo getIdSubgrupo() {
        return idSubgrupo;
    }

    public void setIdSubgrupo(SubGrupo idSubgrupo) {
        this.idSubgrupo = idSubgrupo;
    }

    public Unidade getIdUnidadeEntrada() {
        return idUnidadeEntrada;
    }

    public void setIdUnidadeEntrada(Unidade idUnidadeEntrada) {
        this.idUnidadeEntrada = idUnidadeEntrada;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Grupo getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(Grupo idGrupo) {
        this.idGrupo = idGrupo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProduto != null ? idProduto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produto)) {
            return false;
        }
        Produto other = (Produto) object;
        if ((this.idProduto == null && other.idProduto != null) || (this.idProduto != null && !this.idProduto.equals(other.idProduto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Produto[ idProduto=" + idProduto + " ]";
    }
    
}
