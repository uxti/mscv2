/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "comunicado_encerramento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ComunicadoEncerramento.findAll", query = "SELECT c FROM ComunicadoEncerramento c"),
    @NamedQuery(name = "ComunicadoEncerramento.findByIdComunicadoEncerramento", query = "SELECT c FROM ComunicadoEncerramento c WHERE c.idComunicadoEncerramento = :idComunicadoEncerramento"),
    @NamedQuery(name = "ComunicadoEncerramento.findByDtCancelamento", query = "SELECT c FROM ComunicadoEncerramento c WHERE c.dtCancelamento = :dtCancelamento"),
    @NamedQuery(name = "ComunicadoEncerramento.findByDescricao", query = "SELECT c FROM ComunicadoEncerramento c WHERE c.descricao = :descricao"),
    @NamedQuery(name = "ComunicadoEncerramento.findByStatus", query = "SELECT c FROM ComunicadoEncerramento c WHERE c.status = :status"),
    @NamedQuery(name = "ComunicadoEncerramento.findByAreaAproximada", query = "SELECT c FROM ComunicadoEncerramento c WHERE c.areaAproximada = :areaAproximada"),
    @NamedQuery(name = "ComunicadoEncerramento.findByInformacoesDiversas", query = "SELECT c FROM ComunicadoEncerramento c WHERE c.informacoesDiversas = :informacoesDiversas"),
    @NamedQuery(name = "ComunicadoEncerramento.findByRecomendacoes", query = "SELECT c FROM ComunicadoEncerramento c WHERE c.recomendacoes = :recomendacoes"),
    @NamedQuery(name = "ComunicadoEncerramento.findByObservacao", query = "SELECT c FROM ComunicadoEncerramento c WHERE c.observacao = :observacao")})
public class ComunicadoEncerramento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_comunicado_encerramento")
    private Integer idComunicadoEncerramento;
    @Column(name = "dt_cancelamento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtCancelamento;
    @Size(max = 255)
    @Column(name = "descricao")
    private String descricao;
    @Size(max = 100)
    @Column(name = "status")
    private String status;
    @Size(max = 100)
    @Column(name = "area_aproximada")
    private String areaAproximada;
    @Size(max = 255)
    @Column(name = "informacoes_diversas")
    private String informacoesDiversas;
    @Size(max = 255)
    @Column(name = "recomendacoes")
    private String recomendacoes;
    @Size(max = 255)
    @Column(name = "observacao")
    private String observacao;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idComunicadoEncerramento")
    private List<ComunicadoEncItem> comunicadoEncItemList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idComunicadoEncerramento")
    private List<ComunicadoEncFinanceiro> comunicadoEncFinanceiroList;
    @JoinColumn(name = "id_contrato", referencedColumnName = "id_contrato")
    @ManyToOne
    private Contrato idContrato;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne
    private Usuario idUsuario;
    @JoinColumn(name = "id_local_trabalho", referencedColumnName = "id_local_trabalho")
    @ManyToOne(optional = false)
    private LocalTrabalho idLocalTrabalho;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    @ManyToOne
    private Cliente idCliente;

    public ComunicadoEncerramento() {
    }

    public ComunicadoEncerramento(Integer idComunicadoEncerramento) {
        this.idComunicadoEncerramento = idComunicadoEncerramento;
    }

    public Integer getIdComunicadoEncerramento() {
        return idComunicadoEncerramento;
    }

    public void setIdComunicadoEncerramento(Integer idComunicadoEncerramento) {
        this.idComunicadoEncerramento = idComunicadoEncerramento;
    }

    public Date getDtCancelamento() {
        return dtCancelamento;
    }

    public void setDtCancelamento(Date dtCancelamento) {
        this.dtCancelamento = dtCancelamento;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAreaAproximada() {
        return areaAproximada;
    }

    public void setAreaAproximada(String areaAproximada) {
        this.areaAproximada = areaAproximada;
    }

    public String getInformacoesDiversas() {
        return informacoesDiversas;
    }

    public void setInformacoesDiversas(String informacoesDiversas) {
        this.informacoesDiversas = informacoesDiversas;
    }

    public String getRecomendacoes() {
        return recomendacoes;
    }

    public void setRecomendacoes(String recomendacoes) {
        this.recomendacoes = recomendacoes;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @XmlTransient
    public List<ComunicadoEncItem> getComunicadoEncItemList() {
        return comunicadoEncItemList;
    }

    public void setComunicadoEncItemList(List<ComunicadoEncItem> comunicadoEncItemList) {
        this.comunicadoEncItemList = comunicadoEncItemList;
    }

    @XmlTransient
    public List<ComunicadoEncFinanceiro> getComunicadoEncFinanceiroList() {
        return comunicadoEncFinanceiroList;
    }

    public void setComunicadoEncFinanceiroList(List<ComunicadoEncFinanceiro> comunicadoEncFinanceiroList) {
        this.comunicadoEncFinanceiroList = comunicadoEncFinanceiroList;
    }

    public Contrato getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(Contrato idContrato) {
        this.idContrato = idContrato;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public LocalTrabalho getIdLocalTrabalho() {
        return idLocalTrabalho;
    }

    public void setIdLocalTrabalho(LocalTrabalho idLocalTrabalho) {
        this.idLocalTrabalho = idLocalTrabalho;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idComunicadoEncerramento != null ? idComunicadoEncerramento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComunicadoEncerramento)) {
            return false;
        }
        ComunicadoEncerramento other = (ComunicadoEncerramento) object;
        if ((this.idComunicadoEncerramento == null && other.idComunicadoEncerramento != null) || (this.idComunicadoEncerramento != null && !this.idComunicadoEncerramento.equals(other.idComunicadoEncerramento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.ComunicadoEncerramento[ idComunicadoEncerramento=" + idComunicadoEncerramento + " ]";
    }
    
}
