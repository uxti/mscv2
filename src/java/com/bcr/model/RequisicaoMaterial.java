/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "requisicao_material")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RequisicaoMaterial.findAll", query = "SELECT r FROM RequisicaoMaterial r"),
    @NamedQuery(name = "RequisicaoMaterial.findByIdRequisicaoMaterial", query = "SELECT r FROM RequisicaoMaterial r WHERE r.idRequisicaoMaterial = :idRequisicaoMaterial"),
    @NamedQuery(name = "RequisicaoMaterial.findByStatus", query = "SELECT r FROM RequisicaoMaterial r WHERE r.status = :status"),
    @NamedQuery(name = "RequisicaoMaterial.findByDtCadastro", query = "SELECT r FROM RequisicaoMaterial r WHERE r.dtCadastro = :dtCadastro"),
    @NamedQuery(name = "RequisicaoMaterial.findByValor", query = "SELECT r FROM RequisicaoMaterial r WHERE r.valor = :valor"),
    @NamedQuery(name = "RequisicaoMaterial.findByAplicacao", query = "SELECT r FROM RequisicaoMaterial r WHERE r.aplicacao = :aplicacao"),
    @NamedQuery(name = "RequisicaoMaterial.findByDtAtualizacao", query = "SELECT r FROM RequisicaoMaterial r WHERE r.dtAtualizacao = :dtAtualizacao"),
    @NamedQuery(name = "RequisicaoMaterial.findByTipo", query = "SELECT r FROM RequisicaoMaterial r WHERE r.tipo = :tipo"),
    @NamedQuery(name = "RequisicaoMaterial.findByLote", query = "SELECT r FROM RequisicaoMaterial r WHERE r.lote = :lote")})
public class RequisicaoMaterial implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_requisicao_material")
    private Integer idRequisicaoMaterial;
    @Size(max = 100)
    @Column(name = "status")
    private String status;
    @Column(name = "dt_cadastro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtCadastro;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor")
    private BigDecimal valor;
    @Size(max = 255)
    @Column(name = "aplicacao")
    private String aplicacao;
    @Column(name = "dt_atualizacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtAtualizacao;
    @Size(max = 1)
    @Column(name = "Tipo")
    private String tipo;
    @Size(max = 50)
    @Column(name = "lote")
    private String lote;
    @OneToMany(mappedBy = "idRequisicaoMaterial")
    private List<Cotacao> cotacaoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idRequisicaoMaterial")
    private List<RequisicaoMaterialItem> requisicaoMaterialItemList;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    @ManyToOne
    private Cliente idCliente;
    @JoinColumn(name = "id_local_trabalho", referencedColumnName = "id_local_trabalho")
    @ManyToOne(optional = false)
    private LocalTrabalho idLocalTrabalho;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne
    private Usuario idUsuario;
    @OneToMany(mappedBy = "idRequisicaoReferente")
    private List<RequisicaoMaterial> requisicaoMaterialList;
    @JoinColumn(name = "id_requisicao_referente", referencedColumnName = "id_requisicao_material")
    @ManyToOne
    private RequisicaoMaterial idRequisicaoReferente;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @OneToMany(mappedBy = "idRequisicaoMaterial")
    private List<Centrocustoplanocontas> centrocustoplanocontasList;

    public RequisicaoMaterial() {
    }

    public RequisicaoMaterial(Integer idRequisicaoMaterial) {
        this.idRequisicaoMaterial = idRequisicaoMaterial;
    }

    public Integer getIdRequisicaoMaterial() {
        return idRequisicaoMaterial;
    }

    public void setIdRequisicaoMaterial(Integer idRequisicaoMaterial) {
        this.idRequisicaoMaterial = idRequisicaoMaterial;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDtCadastro() {
        return dtCadastro;
    }

    public void setDtCadastro(Date dtCadastro) {
        this.dtCadastro = dtCadastro;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getAplicacao() {
        return aplicacao;
    }

    public void setAplicacao(String aplicacao) {
        this.aplicacao = aplicacao;
    }

    public Date getDtAtualizacao() {
        return dtAtualizacao;
    }

    public void setDtAtualizacao(Date dtAtualizacao) {
        this.dtAtualizacao = dtAtualizacao;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    @XmlTransient
    public List<Cotacao> getCotacaoList() {
        return cotacaoList;
    }

    public void setCotacaoList(List<Cotacao> cotacaoList) {
        this.cotacaoList = cotacaoList;
    }

    @XmlTransient
    public List<RequisicaoMaterialItem> getRequisicaoMaterialItemList() {
        return requisicaoMaterialItemList;
    }

    public void setRequisicaoMaterialItemList(List<RequisicaoMaterialItem> requisicaoMaterialItemList) {
        this.requisicaoMaterialItemList = requisicaoMaterialItemList;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    public LocalTrabalho getIdLocalTrabalho() {
        return idLocalTrabalho;
    }

    public void setIdLocalTrabalho(LocalTrabalho idLocalTrabalho) {
        this.idLocalTrabalho = idLocalTrabalho;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    @XmlTransient
    public List<RequisicaoMaterial> getRequisicaoMaterialList() {
        return requisicaoMaterialList;
    }

    public void setRequisicaoMaterialList(List<RequisicaoMaterial> requisicaoMaterialList) {
        this.requisicaoMaterialList = requisicaoMaterialList;
    }

    public RequisicaoMaterial getIdRequisicaoReferente() {
        return idRequisicaoReferente;
    }

    public void setIdRequisicaoReferente(RequisicaoMaterial idRequisicaoReferente) {
        this.idRequisicaoReferente = idRequisicaoReferente;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @XmlTransient
    public List<Centrocustoplanocontas> getCentrocustoplanocontasList() {
        return centrocustoplanocontasList;
    }

    public void setCentrocustoplanocontasList(List<Centrocustoplanocontas> centrocustoplanocontasList) {
        this.centrocustoplanocontasList = centrocustoplanocontasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRequisicaoMaterial != null ? idRequisicaoMaterial.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RequisicaoMaterial)) {
            return false;
        }
        RequisicaoMaterial other = (RequisicaoMaterial) object;
        if ((this.idRequisicaoMaterial == null && other.idRequisicaoMaterial != null) || (this.idRequisicaoMaterial != null && !this.idRequisicaoMaterial.equals(other.idRequisicaoMaterial))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.RequisicaoMaterial[ idRequisicaoMaterial=" + idRequisicaoMaterial + " ]";
    }
    
}
