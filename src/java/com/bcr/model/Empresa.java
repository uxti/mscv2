/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "empresa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Empresa.findAll", query = "SELECT e FROM Empresa e"),
    @NamedQuery(name = "Empresa.findByIdEmpresa", query = "SELECT e FROM Empresa e WHERE e.idEmpresa = :idEmpresa"),
    @NamedQuery(name = "Empresa.findByCnpj", query = "SELECT e FROM Empresa e WHERE e.cnpj = :cnpj"),
    @NamedQuery(name = "Empresa.findByNomeFantasia", query = "SELECT e FROM Empresa e WHERE e.nomeFantasia = :nomeFantasia"),
    @NamedQuery(name = "Empresa.findByRazaoSocial", query = "SELECT e FROM Empresa e WHERE e.razaoSocial = :razaoSocial"),
    @NamedQuery(name = "Empresa.findByCep", query = "SELECT e FROM Empresa e WHERE e.cep = :cep"),
    @NamedQuery(name = "Empresa.findByTipoLogradouro", query = "SELECT e FROM Empresa e WHERE e.tipoLogradouro = :tipoLogradouro"),
    @NamedQuery(name = "Empresa.findByLogradouro", query = "SELECT e FROM Empresa e WHERE e.logradouro = :logradouro"),
    @NamedQuery(name = "Empresa.findByNumero", query = "SELECT e FROM Empresa e WHERE e.numero = :numero"),
    @NamedQuery(name = "Empresa.findByBairro", query = "SELECT e FROM Empresa e WHERE e.bairro = :bairro"),
    @NamedQuery(name = "Empresa.findByComplemento", query = "SELECT e FROM Empresa e WHERE e.complemento = :complemento"),
    @NamedQuery(name = "Empresa.findByEmail", query = "SELECT e FROM Empresa e WHERE e.email = :email"),
    @NamedQuery(name = "Empresa.findByContato", query = "SELECT e FROM Empresa e WHERE e.contato = :contato"),
    @NamedQuery(name = "Empresa.findByInativo", query = "SELECT e FROM Empresa e WHERE e.inativo = :inativo"),
    @NamedQuery(name = "Empresa.findByIe", query = "SELECT e FROM Empresa e WHERE e.ie = :ie"),
    @NamedQuery(name = "Empresa.findByCnae", query = "SELECT e FROM Empresa e WHERE e.cnae = :cnae"),
    @NamedQuery(name = "Empresa.findByNomeContador", query = "SELECT e FROM Empresa e WHERE e.nomeContador = :nomeContador"),
    @NamedQuery(name = "Empresa.findByCRCcontador", query = "SELECT e FROM Empresa e WHERE e.cRCcontador = :cRCcontador"),
    @NamedQuery(name = "Empresa.findByJuntaComercial", query = "SELECT e FROM Empresa e WHERE e.juntaComercial = :juntaComercial"),
    @NamedQuery(name = "Empresa.findByIm", query = "SELECT e FROM Empresa e WHERE e.im = :im"),
    @NamedQuery(name = "Empresa.findByNire", query = "SELECT e FROM Empresa e WHERE e.nire = :nire"),
    @NamedQuery(name = "Empresa.findByObservacao", query = "SELECT e FROM Empresa e WHERE e.observacao = :observacao"),
    @NamedQuery(name = "Empresa.findByInscricaoSuframa", query = "SELECT e FROM Empresa e WHERE e.inscricaoSuframa = :inscricaoSuframa"),
    @NamedQuery(name = "Empresa.findByNumJuridico", query = "SELECT e FROM Empresa e WHERE e.numJuridico = :numJuridico"),
    @NamedQuery(name = "Empresa.findByCaixaInicialValor", query = "SELECT e FROM Empresa e WHERE e.caixaInicialValor = :caixaInicialValor")})
public class Empresa implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Size(max = 17)
    @Column(name = "CNPJ")
    private String cnpj;
    @Size(max = 255)
    @Column(name = "nome_fantasia")
    private String nomeFantasia;
    @Size(max = 255)
    @Column(name = "razao_social")
    private String razaoSocial;
    @Size(max = 9)
    @Column(name = "CEP")
    private String cep;
    @Size(max = 20)
    @Column(name = "tipo_logradouro")
    private String tipoLogradouro;
    @Size(max = 255)
    @Column(name = "logradouro")
    private String logradouro;
    @Size(max = 5)
    @Column(name = "numero")
    private String numero;
    @Size(max = 255)
    @Column(name = "bairro")
    private String bairro;
    @Size(max = 255)
    @Column(name = "complemento")
    private String complemento;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="E-mail inválido")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 255)
    @Column(name = "email")
    private String email;
    @Size(max = 255)
    @Column(name = "contato")
    private String contato;
    @Size(max = 1)
    @Column(name = "inativo")
    private String inativo;
    @Size(max = 20)
    @Column(name = "IE")
    private String ie;
    @Size(max = 7)
    @Column(name = "CNAE")
    private String cnae;
    @Size(max = 255)
    @Column(name = "nome_contador")
    private String nomeContador;
    @Size(max = 20)
    @Column(name = "CRC_contador")
    private String cRCcontador;
    @Size(max = 20)
    @Column(name = "junta_comercial")
    private String juntaComercial;
    @Size(max = 20)
    @Column(name = "IM")
    private String im;
    @Size(max = 20)
    @Column(name = "NIRE")
    private String nire;
    @Size(max = 255)
    @Column(name = "observacao")
    private String observacao;
    @Column(name = "inscricao_suframa")
    private Integer inscricaoSuframa;
    @Size(max = 20)
    @Column(name = "num_juridico")
    private String numJuridico;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Caixa_Inicial_Valor")
    private BigDecimal caixaInicialValor;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<ContasPagar> contasPagarList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<LocalTrabalho> localTrabalhoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<Negocio> negocioList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<NotaEntrada> notaEntradaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<Colaborador> colaboradorList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<Contrato> contratoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<CotacaoItem> cotacaoItemList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<Fornecedor> fornecedorList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<RequisicaoMaterial> requisicaoMaterialList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<ContaCorrente> contaCorrenteList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<PlanoContas> planoContasList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<MovimentacaoBancaria> movimentacaoBancariaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<Cliente> clienteList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<PedidoCompra> pedidoCompraList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<NotaObservacao> notaObservacaoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<Cheque> chequeList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<Proposta> propostaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<NotaSaida> notaSaidaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<Produto> produtoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<Caixa> caixaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<Usuario> usuarioList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<Transportadora> transportadoraList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<Cargo> cargoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<Categoria> categoriaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<Setor> setorList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<Banco> bancoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<Cartao> cartaoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<ComunicadoEncerramento> comunicadoEncerramentoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<Servico> servicoList;
    @JoinColumn(name = "id_cidade", referencedColumnName = "id_cidade")
    @ManyToOne
    private Cidade idCidade;
    @JoinColumn(name = "ID_PLANO_DESPESA", referencedColumnName = "id_plano_contas")
    @ManyToOne
    private PlanoContas idPlanoDespesa;
    @JoinColumn(name = "ID_PLANO_RECEITA", referencedColumnName = "id_plano_contas")
    @ManyToOne
    private PlanoContas idPlanoReceita;
    @JoinColumn(name = "id_parametrizacao", referencedColumnName = "id_parametrizacao")
    @ManyToOne
    private Parametrizacao idParametrizacao;
    @JoinColumn(name = "id_estado", referencedColumnName = "id_estado")
    @ManyToOne
    private Estado idEstado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<ContasReceber> contasReceberList;

    public Empresa() {
    }

    public Empresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getTipoLogradouro() {
        return tipoLogradouro;
    }

    public void setTipoLogradouro(String tipoLogradouro) {
        this.tipoLogradouro = tipoLogradouro;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    public String getInativo() {
        return inativo;
    }

    public void setInativo(String inativo) {
        this.inativo = inativo;
    }

    public String getIe() {
        return ie;
    }

    public void setIe(String ie) {
        this.ie = ie;
    }

    public String getCnae() {
        return cnae;
    }

    public void setCnae(String cnae) {
        this.cnae = cnae;
    }

    public String getNomeContador() {
        return nomeContador;
    }

    public void setNomeContador(String nomeContador) {
        this.nomeContador = nomeContador;
    }

    public String getCRCcontador() {
        return cRCcontador;
    }

    public void setCRCcontador(String cRCcontador) {
        this.cRCcontador = cRCcontador;
    }

    public String getJuntaComercial() {
        return juntaComercial;
    }

    public void setJuntaComercial(String juntaComercial) {
        this.juntaComercial = juntaComercial;
    }

    public String getIm() {
        return im;
    }

    public void setIm(String im) {
        this.im = im;
    }

    public String getNire() {
        return nire;
    }

    public void setNire(String nire) {
        this.nire = nire;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Integer getInscricaoSuframa() {
        return inscricaoSuframa;
    }

    public void setInscricaoSuframa(Integer inscricaoSuframa) {
        this.inscricaoSuframa = inscricaoSuframa;
    }

    public String getNumJuridico() {
        return numJuridico;
    }

    public void setNumJuridico(String numJuridico) {
        this.numJuridico = numJuridico;
    }

    public BigDecimal getCaixaInicialValor() {
        return caixaInicialValor;
    }

    public void setCaixaInicialValor(BigDecimal caixaInicialValor) {
        this.caixaInicialValor = caixaInicialValor;
    }

    @XmlTransient
    public List<ContasPagar> getContasPagarList() {
        return contasPagarList;
    }

    public void setContasPagarList(List<ContasPagar> contasPagarList) {
        this.contasPagarList = contasPagarList;
    }

    @XmlTransient
    public List<LocalTrabalho> getLocalTrabalhoList() {
        return localTrabalhoList;
    }

    public void setLocalTrabalhoList(List<LocalTrabalho> localTrabalhoList) {
        this.localTrabalhoList = localTrabalhoList;
    }

    @XmlTransient
    public List<Negocio> getNegocioList() {
        return negocioList;
    }

    public void setNegocioList(List<Negocio> negocioList) {
        this.negocioList = negocioList;
    }

    @XmlTransient
    public List<NotaEntrada> getNotaEntradaList() {
        return notaEntradaList;
    }

    public void setNotaEntradaList(List<NotaEntrada> notaEntradaList) {
        this.notaEntradaList = notaEntradaList;
    }

    @XmlTransient
    public List<Colaborador> getColaboradorList() {
        return colaboradorList;
    }

    public void setColaboradorList(List<Colaborador> colaboradorList) {
        this.colaboradorList = colaboradorList;
    }

    @XmlTransient
    public List<Contrato> getContratoList() {
        return contratoList;
    }

    public void setContratoList(List<Contrato> contratoList) {
        this.contratoList = contratoList;
    }

    @XmlTransient
    public List<CotacaoItem> getCotacaoItemList() {
        return cotacaoItemList;
    }

    public void setCotacaoItemList(List<CotacaoItem> cotacaoItemList) {
        this.cotacaoItemList = cotacaoItemList;
    }

    @XmlTransient
    public List<Fornecedor> getFornecedorList() {
        return fornecedorList;
    }

    public void setFornecedorList(List<Fornecedor> fornecedorList) {
        this.fornecedorList = fornecedorList;
    }

    @XmlTransient
    public List<RequisicaoMaterial> getRequisicaoMaterialList() {
        return requisicaoMaterialList;
    }

    public void setRequisicaoMaterialList(List<RequisicaoMaterial> requisicaoMaterialList) {
        this.requisicaoMaterialList = requisicaoMaterialList;
    }

    @XmlTransient
    public List<ContaCorrente> getContaCorrenteList() {
        return contaCorrenteList;
    }

    public void setContaCorrenteList(List<ContaCorrente> contaCorrenteList) {
        this.contaCorrenteList = contaCorrenteList;
    }

    @XmlTransient
    public List<PlanoContas> getPlanoContasList() {
        return planoContasList;
    }

    public void setPlanoContasList(List<PlanoContas> planoContasList) {
        this.planoContasList = planoContasList;
    }

    @XmlTransient
    public List<MovimentacaoBancaria> getMovimentacaoBancariaList() {
        return movimentacaoBancariaList;
    }

    public void setMovimentacaoBancariaList(List<MovimentacaoBancaria> movimentacaoBancariaList) {
        this.movimentacaoBancariaList = movimentacaoBancariaList;
    }

    @XmlTransient
    public List<Cliente> getClienteList() {
        return clienteList;
    }

    public void setClienteList(List<Cliente> clienteList) {
        this.clienteList = clienteList;
    }

    @XmlTransient
    public List<PedidoCompra> getPedidoCompraList() {
        return pedidoCompraList;
    }

    public void setPedidoCompraList(List<PedidoCompra> pedidoCompraList) {
        this.pedidoCompraList = pedidoCompraList;
    }

    @XmlTransient
    public List<NotaObservacao> getNotaObservacaoList() {
        return notaObservacaoList;
    }

    public void setNotaObservacaoList(List<NotaObservacao> notaObservacaoList) {
        this.notaObservacaoList = notaObservacaoList;
    }

    @XmlTransient
    public List<Cheque> getChequeList() {
        return chequeList;
    }

    public void setChequeList(List<Cheque> chequeList) {
        this.chequeList = chequeList;
    }

    @XmlTransient
    public List<Proposta> getPropostaList() {
        return propostaList;
    }

    public void setPropostaList(List<Proposta> propostaList) {
        this.propostaList = propostaList;
    }

    @XmlTransient
    public List<NotaSaida> getNotaSaidaList() {
        return notaSaidaList;
    }

    public void setNotaSaidaList(List<NotaSaida> notaSaidaList) {
        this.notaSaidaList = notaSaidaList;
    }

    @XmlTransient
    public List<Produto> getProdutoList() {
        return produtoList;
    }

    public void setProdutoList(List<Produto> produtoList) {
        this.produtoList = produtoList;
    }

    @XmlTransient
    public List<Caixa> getCaixaList() {
        return caixaList;
    }

    public void setCaixaList(List<Caixa> caixaList) {
        this.caixaList = caixaList;
    }

    @XmlTransient
    public List<Usuario> getUsuarioList() {
        return usuarioList;
    }

    public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

    @XmlTransient
    public List<Transportadora> getTransportadoraList() {
        return transportadoraList;
    }

    public void setTransportadoraList(List<Transportadora> transportadoraList) {
        this.transportadoraList = transportadoraList;
    }

    @XmlTransient
    public List<Cargo> getCargoList() {
        return cargoList;
    }

    public void setCargoList(List<Cargo> cargoList) {
        this.cargoList = cargoList;
    }

    @XmlTransient
    public List<Categoria> getCategoriaList() {
        return categoriaList;
    }

    public void setCategoriaList(List<Categoria> categoriaList) {
        this.categoriaList = categoriaList;
    }

    @XmlTransient
    public List<Setor> getSetorList() {
        return setorList;
    }

    public void setSetorList(List<Setor> setorList) {
        this.setorList = setorList;
    }

    @XmlTransient
    public List<Banco> getBancoList() {
        return bancoList;
    }

    public void setBancoList(List<Banco> bancoList) {
        this.bancoList = bancoList;
    }

    @XmlTransient
    public List<Cartao> getCartaoList() {
        return cartaoList;
    }

    public void setCartaoList(List<Cartao> cartaoList) {
        this.cartaoList = cartaoList;
    }

    @XmlTransient
    public List<ComunicadoEncerramento> getComunicadoEncerramentoList() {
        return comunicadoEncerramentoList;
    }

    public void setComunicadoEncerramentoList(List<ComunicadoEncerramento> comunicadoEncerramentoList) {
        this.comunicadoEncerramentoList = comunicadoEncerramentoList;
    }

    @XmlTransient
    public List<Servico> getServicoList() {
        return servicoList;
    }

    public void setServicoList(List<Servico> servicoList) {
        this.servicoList = servicoList;
    }

    public Cidade getIdCidade() {
        return idCidade;
    }

    public void setIdCidade(Cidade idCidade) {
        this.idCidade = idCidade;
    }

    public PlanoContas getIdPlanoDespesa() {
        return idPlanoDespesa;
    }

    public void setIdPlanoDespesa(PlanoContas idPlanoDespesa) {
        this.idPlanoDespesa = idPlanoDespesa;
    }

    public PlanoContas getIdPlanoReceita() {
        return idPlanoReceita;
    }

    public void setIdPlanoReceita(PlanoContas idPlanoReceita) {
        this.idPlanoReceita = idPlanoReceita;
    }

    public Parametrizacao getIdParametrizacao() {
        return idParametrizacao;
    }

    public void setIdParametrizacao(Parametrizacao idParametrizacao) {
        this.idParametrizacao = idParametrizacao;
    }

    public Estado getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Estado idEstado) {
        this.idEstado = idEstado;
    }

    @XmlTransient
    public List<ContasReceber> getContasReceberList() {
        return contasReceberList;
    }

    public void setContasReceberList(List<ContasReceber> contasReceberList) {
        this.contasReceberList = contasReceberList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEmpresa != null ? idEmpresa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empresa)) {
            return false;
        }
        Empresa other = (Empresa) object;
        if ((this.idEmpresa == null && other.idEmpresa != null) || (this.idEmpresa != null && !this.idEmpresa.equals(other.idEmpresa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Empresa[ idEmpresa=" + idEmpresa + " ]";
    }
    
}
