/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "transportadora")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Transportadora.findAll", query = "SELECT t FROM Transportadora t"),
    @NamedQuery(name = "Transportadora.findByIdTransportadora", query = "SELECT t FROM Transportadora t WHERE t.idTransportadora = :idTransportadora"),
    @NamedQuery(name = "Transportadora.findByNomeFantasia", query = "SELECT t FROM Transportadora t WHERE t.nomeFantasia = :nomeFantasia"),
    @NamedQuery(name = "Transportadora.findByRazaoSocial", query = "SELECT t FROM Transportadora t WHERE t.razaoSocial = :razaoSocial"),
    @NamedQuery(name = "Transportadora.findByCodigoANTT", query = "SELECT t FROM Transportadora t WHERE t.codigoANTT = :codigoANTT"),
    @NamedQuery(name = "Transportadora.findByEndereco", query = "SELECT t FROM Transportadora t WHERE t.endereco = :endereco"),
    @NamedQuery(name = "Transportadora.findByIe", query = "SELECT t FROM Transportadora t WHERE t.ie = :ie")})
public class Transportadora implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_transportadora")
    private Integer idTransportadora;
    @Size(max = 255)
    @Column(name = "nome_fantasia")
    private String nomeFantasia;
    @Size(max = 255)
    @Column(name = "razao_social")
    private String razaoSocial;
    @Size(max = 20)
    @Column(name = "codigo_ANTT")
    private String codigoANTT;
    @Size(max = 255)
    @Column(name = "endereco")
    private String endereco;
    @Size(max = 20)
    @Column(name = "IE")
    private String ie;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTransportadora")
    private List<NotaEntrada> notaEntradaList;
    @JoinColumn(name = "id_cidade", referencedColumnName = "id_cidade")
    @ManyToOne
    private Cidade idCidade;
    @JoinColumn(name = "id_estado", referencedColumnName = "id_estado")
    @ManyToOne
    private Estado idEstado;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;

    public Transportadora() {
    }

    public Transportadora(Integer idTransportadora) {
        this.idTransportadora = idTransportadora;
    }

    public Integer getIdTransportadora() {
        return idTransportadora;
    }

    public void setIdTransportadora(Integer idTransportadora) {
        this.idTransportadora = idTransportadora;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getCodigoANTT() {
        return codigoANTT;
    }

    public void setCodigoANTT(String codigoANTT) {
        this.codigoANTT = codigoANTT;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getIe() {
        return ie;
    }

    public void setIe(String ie) {
        this.ie = ie;
    }

    @XmlTransient
    public List<NotaEntrada> getNotaEntradaList() {
        return notaEntradaList;
    }

    public void setNotaEntradaList(List<NotaEntrada> notaEntradaList) {
        this.notaEntradaList = notaEntradaList;
    }

    public Cidade getIdCidade() {
        return idCidade;
    }

    public void setIdCidade(Cidade idCidade) {
        this.idCidade = idCidade;
    }

    public Estado getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Estado idEstado) {
        this.idEstado = idEstado;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTransportadora != null ? idTransportadora.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transportadora)) {
            return false;
        }
        Transportadora other = (Transportadora) object;
        if ((this.idTransportadora == null && other.idTransportadora != null) || (this.idTransportadora != null && !this.idTransportadora.equals(other.idTransportadora))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Transportadora[ idTransportadora=" + idTransportadora + " ]";
    }
    
}
