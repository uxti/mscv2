/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "cliente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cliente.findAll", query = "SELECT c FROM Cliente c"),
    @NamedQuery(name = "Cliente.findByIdCliente", query = "SELECT c FROM Cliente c WHERE c.idCliente = :idCliente"),
    @NamedQuery(name = "Cliente.findByNome", query = "SELECT c FROM Cliente c WHERE c.nome = :nome"),
    @NamedQuery(name = "Cliente.findByCgccpf", query = "SELECT c FROM Cliente c WHERE c.cgccpf = :cgccpf"),
    @NamedQuery(name = "Cliente.findByRg", query = "SELECT c FROM Cliente c WHERE c.rg = :rg"),
    @NamedQuery(name = "Cliente.findByIe", query = "SELECT c FROM Cliente c WHERE c.ie = :ie"),
    @NamedQuery(name = "Cliente.findByIm", query = "SELECT c FROM Cliente c WHERE c.im = :im"),
    @NamedQuery(name = "Cliente.findByEmail", query = "SELECT c FROM Cliente c WHERE c.email = :email"),
    @NamedQuery(name = "Cliente.findBySexo", query = "SELECT c FROM Cliente c WHERE c.sexo = :sexo"),
    @NamedQuery(name = "Cliente.findByDtNascimento", query = "SELECT c FROM Cliente c WHERE c.dtNascimento = :dtNascimento"),
    @NamedQuery(name = "Cliente.findByCep", query = "SELECT c FROM Cliente c WHERE c.cep = :cep"),
    @NamedQuery(name = "Cliente.findByTipoLogradouro", query = "SELECT c FROM Cliente c WHERE c.tipoLogradouro = :tipoLogradouro"),
    @NamedQuery(name = "Cliente.findByLogradouro", query = "SELECT c FROM Cliente c WHERE c.logradouro = :logradouro"),
    @NamedQuery(name = "Cliente.findByNumero", query = "SELECT c FROM Cliente c WHERE c.numero = :numero"),
    @NamedQuery(name = "Cliente.findByBairro", query = "SELECT c FROM Cliente c WHERE c.bairro = :bairro"),
    @NamedQuery(name = "Cliente.findByComplemento", query = "SELECT c FROM Cliente c WHERE c.complemento = :complemento"),
    @NamedQuery(name = "Cliente.findByCEPcobranca", query = "SELECT c FROM Cliente c WHERE c.cEPcobranca = :cEPcobranca"),
    @NamedQuery(name = "Cliente.findByTipoLogradouroCobranca", query = "SELECT c FROM Cliente c WHERE c.tipoLogradouroCobranca = :tipoLogradouroCobranca"),
    @NamedQuery(name = "Cliente.findByLogradouroCobranca", query = "SELECT c FROM Cliente c WHERE c.logradouroCobranca = :logradouroCobranca"),
    @NamedQuery(name = "Cliente.findByNumeroCobranca", query = "SELECT c FROM Cliente c WHERE c.numeroCobranca = :numeroCobranca"),
    @NamedQuery(name = "Cliente.findByBairroCobranca", query = "SELECT c FROM Cliente c WHERE c.bairroCobranca = :bairroCobranca"),
    @NamedQuery(name = "Cliente.findByComplementoCobranca", query = "SELECT c FROM Cliente c WHERE c.complementoCobranca = :complementoCobranca"),
    @NamedQuery(name = "Cliente.findByTipo", query = "SELECT c FROM Cliente c WHERE c.tipo = :tipo"),
    @NamedQuery(name = "Cliente.findByHomePage", query = "SELECT c FROM Cliente c WHERE c.homePage = :homePage"),
    @NamedQuery(name = "Cliente.findByDtCadastro", query = "SELECT c FROM Cliente c WHERE c.dtCadastro = :dtCadastro"),
    @NamedQuery(name = "Cliente.findByDtUltimaAtualizacao", query = "SELECT c FROM Cliente c WHERE c.dtUltimaAtualizacao = :dtUltimaAtualizacao"),
    @NamedQuery(name = "Cliente.findByDtInicioContrato", query = "SELECT c FROM Cliente c WHERE c.dtInicioContrato = :dtInicioContrato"),
    @NamedQuery(name = "Cliente.findByInativo", query = "SELECT c FROM Cliente c WHERE c.inativo = :inativo"),
    @NamedQuery(name = "Cliente.findByContato", query = "SELECT c FROM Cliente c WHERE c.contato = :contato"),
    @NamedQuery(name = "Cliente.findByObservacoes", query = "SELECT c FROM Cliente c WHERE c.observacoes = :observacoes"),
    @NamedQuery(name = "Cliente.findByBloqueado", query = "SELECT c FROM Cliente c WHERE c.bloqueado = :bloqueado"),
    @NamedQuery(name = "Cliente.findByCodigoGps", query = "SELECT c FROM Cliente c WHERE c.codigoGps = :codigoGps"),
    @NamedQuery(name = "Cliente.findByTributacao", query = "SELECT c FROM Cliente c WHERE c.tributacao = :tributacao")})
public class Cliente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_cliente")
    private Integer idCliente;
    @Size(max = 255)
    @Column(name = "nome")
    private String nome;
    @Size(max = 20)
    @Column(name = "CGCCPF")
    private String cgccpf;
    @Size(max = 20)
    @Column(name = "RG")
    private String rg;
    @Size(max = 20)
    @Column(name = "IE")
    private String ie;
    @Size(max = 20)
    @Column(name = "IM")
    private String im;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="E-mail inválido")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 255)
    @Column(name = "email")
    private String email;
    @Size(max = 1)
    @Column(name = "sexo")
    private String sexo;
    @Column(name = "dt_nascimento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtNascimento;
    @Size(max = 9)
    @Column(name = "CEP")
    private String cep;
    @Size(max = 20)
    @Column(name = "tipo_logradouro")
    private String tipoLogradouro;
    @Size(max = 255)
    @Column(name = "logradouro")
    private String logradouro;
    @Size(max = 5)
    @Column(name = "numero")
    private String numero;
    @Size(max = 255)
    @Column(name = "bairro")
    private String bairro;
    @Size(max = 255)
    @Column(name = "complemento")
    private String complemento;
    @Size(max = 9)
    @Column(name = "CEP_cobranca")
    private String cEPcobranca;
    @Size(max = 20)
    @Column(name = "tipo_logradouro_cobranca")
    private String tipoLogradouroCobranca;
    @Size(max = 255)
    @Column(name = "logradouro_cobranca")
    private String logradouroCobranca;
    @Size(max = 5)
    @Column(name = "numero_cobranca")
    private String numeroCobranca;
    @Size(max = 255)
    @Column(name = "bairro_cobranca")
    private String bairroCobranca;
    @Size(max = 255)
    @Column(name = "complemento_cobranca")
    private String complementoCobranca;
    @Size(max = 1)
    @Column(name = "tipo")
    private String tipo;
    @Size(max = 255)
    @Column(name = "home_page")
    private String homePage;
    @Column(name = "dt_cadastro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtCadastro;
    @Column(name = "dt_ultima_atualizacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtUltimaAtualizacao;
    @Column(name = "dt_inicio_contrato")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtInicioContrato;
    @Size(max = 1)
    @Column(name = "inativo")
    private String inativo;
    @Size(max = 255)
    @Column(name = "contato")
    private String contato;
    @Size(max = 255)
    @Column(name = "observacoes")
    private String observacoes;
    @Size(max = 1)
    @Column(name = "bloqueado")
    private String bloqueado;
    @Size(max = 20)
    @Column(name = "codigo_gps")
    private String codigoGps;
    @Size(max = 200)
    @Column(name = "tributacao")
    private String tributacao;
    @OneToMany(mappedBy = "idCliente")
    private List<ClienteResponsavel> clienteResponsavelList;
    @OneToMany(mappedBy = "idCliente")
    private List<ComunicadoImplantacao> comunicadoImplantacaoList;
    @OneToMany(mappedBy = "idCliente")
    private List<Negocio> negocioList;
    @OneToMany(mappedBy = "idCliente")
    private List<CentroCusto> centroCustoList;
    @OneToMany(mappedBy = "idCliente")
    private List<Contrato> contratoList;
    @OneToMany(mappedBy = "idCliente")
    private List<Fornecedor> fornecedorList;
    @OneToMany(mappedBy = "idCliente")
    private List<RequisicaoMaterial> requisicaoMaterialList;
    @OneToMany(mappedBy = "idCliente")
    private List<ComunicadoImplFinanceiro> comunicadoImplFinanceiroList;
    @JoinColumn(name = "id_fornecedor", referencedColumnName = "id_fornecedor")
    @ManyToOne
    private Fornecedor idFornecedor;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @JoinColumn(name = "id_cidade", referencedColumnName = "id_cidade")
    @ManyToOne
    private Cidade idCidade;
    @JoinColumn(name = "id_cidade_cobranca", referencedColumnName = "id_cidade")
    @ManyToOne
    private Cidade idCidadeCobranca;
    @OneToMany(mappedBy = "idCliente")
    private List<ComunicadoEncFinanceiro> comunicadoEncFinanceiroList;
    @OneToMany(mappedBy = "idCliente")
    private List<ComunicadoEncerramento> comunicadoEncerramentoList;
    @OneToMany(mappedBy = "idCliente")
    private List<Centrocustoplanocontas> centrocustoplanocontasList;
    @OneToMany(mappedBy = "idCliente")
    private List<Telefone> telefoneList;
    @OneToMany(mappedBy = "idCliente")
    private List<ContasReceber> contasReceberList;

    public Cliente() {
    }

    public Cliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCgccpf() {
        return cgccpf;
    }

    public void setCgccpf(String cgccpf) {
        this.cgccpf = cgccpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getIe() {
        return ie;
    }

    public void setIe(String ie) {
        this.ie = ie;
    }

    public String getIm() {
        return im;
    }

    public void setIm(String im) {
        this.im = im;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Date getDtNascimento() {
        return dtNascimento;
    }

    public void setDtNascimento(Date dtNascimento) {
        this.dtNascimento = dtNascimento;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getTipoLogradouro() {
        return tipoLogradouro;
    }

    public void setTipoLogradouro(String tipoLogradouro) {
        this.tipoLogradouro = tipoLogradouro;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getCEPcobranca() {
        return cEPcobranca;
    }

    public void setCEPcobranca(String cEPcobranca) {
        this.cEPcobranca = cEPcobranca;
    }

    public String getTipoLogradouroCobranca() {
        return tipoLogradouroCobranca;
    }

    public void setTipoLogradouroCobranca(String tipoLogradouroCobranca) {
        this.tipoLogradouroCobranca = tipoLogradouroCobranca;
    }

    public String getLogradouroCobranca() {
        return logradouroCobranca;
    }

    public void setLogradouroCobranca(String logradouroCobranca) {
        this.logradouroCobranca = logradouroCobranca;
    }

    public String getNumeroCobranca() {
        return numeroCobranca;
    }

    public void setNumeroCobranca(String numeroCobranca) {
        this.numeroCobranca = numeroCobranca;
    }

    public String getBairroCobranca() {
        return bairroCobranca;
    }

    public void setBairroCobranca(String bairroCobranca) {
        this.bairroCobranca = bairroCobranca;
    }

    public String getComplementoCobranca() {
        return complementoCobranca;
    }

    public void setComplementoCobranca(String complementoCobranca) {
        this.complementoCobranca = complementoCobranca;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getHomePage() {
        return homePage;
    }

    public void setHomePage(String homePage) {
        this.homePage = homePage;
    }

    public Date getDtCadastro() {
        return dtCadastro;
    }

    public void setDtCadastro(Date dtCadastro) {
        this.dtCadastro = dtCadastro;
    }

    public Date getDtUltimaAtualizacao() {
        return dtUltimaAtualizacao;
    }

    public void setDtUltimaAtualizacao(Date dtUltimaAtualizacao) {
        this.dtUltimaAtualizacao = dtUltimaAtualizacao;
    }

    public Date getDtInicioContrato() {
        return dtInicioContrato;
    }

    public void setDtInicioContrato(Date dtInicioContrato) {
        this.dtInicioContrato = dtInicioContrato;
    }

    public String getInativo() {
        return inativo;
    }

    public void setInativo(String inativo) {
        this.inativo = inativo;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public String getBloqueado() {
        return bloqueado;
    }

    public void setBloqueado(String bloqueado) {
        this.bloqueado = bloqueado;
    }

    public String getCodigoGps() {
        return codigoGps;
    }

    public void setCodigoGps(String codigoGps) {
        this.codigoGps = codigoGps;
    }

    public String getTributacao() {
        return tributacao;
    }

    public void setTributacao(String tributacao) {
        this.tributacao = tributacao;
    }

    @XmlTransient
    public List<ClienteResponsavel> getClienteResponsavelList() {
        return clienteResponsavelList;
    }

    public void setClienteResponsavelList(List<ClienteResponsavel> clienteResponsavelList) {
        this.clienteResponsavelList = clienteResponsavelList;
    }

    @XmlTransient
    public List<ComunicadoImplantacao> getComunicadoImplantacaoList() {
        return comunicadoImplantacaoList;
    }

    public void setComunicadoImplantacaoList(List<ComunicadoImplantacao> comunicadoImplantacaoList) {
        this.comunicadoImplantacaoList = comunicadoImplantacaoList;
    }

    @XmlTransient
    public List<Negocio> getNegocioList() {
        return negocioList;
    }

    public void setNegocioList(List<Negocio> negocioList) {
        this.negocioList = negocioList;
    }

    @XmlTransient
    public List<CentroCusto> getCentroCustoList() {
        return centroCustoList;
    }

    public void setCentroCustoList(List<CentroCusto> centroCustoList) {
        this.centroCustoList = centroCustoList;
    }

    @XmlTransient
    public List<Contrato> getContratoList() {
        return contratoList;
    }

    public void setContratoList(List<Contrato> contratoList) {
        this.contratoList = contratoList;
    }

    @XmlTransient
    public List<Fornecedor> getFornecedorList() {
        return fornecedorList;
    }

    public void setFornecedorList(List<Fornecedor> fornecedorList) {
        this.fornecedorList = fornecedorList;
    }

    @XmlTransient
    public List<RequisicaoMaterial> getRequisicaoMaterialList() {
        return requisicaoMaterialList;
    }

    public void setRequisicaoMaterialList(List<RequisicaoMaterial> requisicaoMaterialList) {
        this.requisicaoMaterialList = requisicaoMaterialList;
    }

    @XmlTransient
    public List<ComunicadoImplFinanceiro> getComunicadoImplFinanceiroList() {
        return comunicadoImplFinanceiroList;
    }

    public void setComunicadoImplFinanceiroList(List<ComunicadoImplFinanceiro> comunicadoImplFinanceiroList) {
        this.comunicadoImplFinanceiroList = comunicadoImplFinanceiroList;
    }

    public Fornecedor getIdFornecedor() {
        return idFornecedor;
    }

    public void setIdFornecedor(Fornecedor idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Cidade getIdCidade() {
        return idCidade;
    }

    public void setIdCidade(Cidade idCidade) {
        this.idCidade = idCidade;
    }

    public Cidade getIdCidadeCobranca() {
        return idCidadeCobranca;
    }

    public void setIdCidadeCobranca(Cidade idCidadeCobranca) {
        this.idCidadeCobranca = idCidadeCobranca;
    }

    @XmlTransient
    public List<ComunicadoEncFinanceiro> getComunicadoEncFinanceiroList() {
        return comunicadoEncFinanceiroList;
    }

    public void setComunicadoEncFinanceiroList(List<ComunicadoEncFinanceiro> comunicadoEncFinanceiroList) {
        this.comunicadoEncFinanceiroList = comunicadoEncFinanceiroList;
    }

    @XmlTransient
    public List<ComunicadoEncerramento> getComunicadoEncerramentoList() {
        return comunicadoEncerramentoList;
    }

    public void setComunicadoEncerramentoList(List<ComunicadoEncerramento> comunicadoEncerramentoList) {
        this.comunicadoEncerramentoList = comunicadoEncerramentoList;
    }

    @XmlTransient
    public List<Centrocustoplanocontas> getCentrocustoplanocontasList() {
        return centrocustoplanocontasList;
    }

    public void setCentrocustoplanocontasList(List<Centrocustoplanocontas> centrocustoplanocontasList) {
        this.centrocustoplanocontasList = centrocustoplanocontasList;
    }

    @XmlTransient
    public List<Telefone> getTelefoneList() {
        return telefoneList;
    }

    public void setTelefoneList(List<Telefone> telefoneList) {
        this.telefoneList = telefoneList;
    }

    @XmlTransient
    public List<ContasReceber> getContasReceberList() {
        return contasReceberList;
    }

    public void setContasReceberList(List<ContasReceber> contasReceberList) {
        this.contasReceberList = contasReceberList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCliente != null ? idCliente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cliente)) {
            return false;
        }
        Cliente other = (Cliente) object;
        if ((this.idCliente == null && other.idCliente != null) || (this.idCliente != null && !this.idCliente.equals(other.idCliente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Cliente[ idCliente=" + idCliente + " ]";
    }
    
}
