/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "banco")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Banco.findAll", query = "SELECT b FROM Banco b"),
    @NamedQuery(name = "Banco.findByIdBanco", query = "SELECT b FROM Banco b WHERE b.idBanco = :idBanco"),
    @NamedQuery(name = "Banco.findByNome", query = "SELECT b FROM Banco b WHERE b.nome = :nome"),
    @NamedQuery(name = "Banco.findByNumeroBanco", query = "SELECT b FROM Banco b WHERE b.numeroBanco = :numeroBanco")})
public class Banco implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_banco")
    private Integer idBanco;
    @Size(max = 255)
    @Column(name = "nome")
    private String nome;
    @Size(max = 10)
    @Column(name = "numero_banco")
    private String numeroBanco;
    @Size(max = 10)
    @Column(name = "numero_banco_exp")
    private String numeroBancoExp;
    @OneToMany(mappedBy = "idBanco")
    private List<ContasPagar> contasPagarList;
    @OneToMany(mappedBy = "idBanco")
    private List<ContaCorrente> contaCorrenteList;
    @OneToMany(mappedBy = "idBanco")
    private List<MovimentacaoBancaria> movimentacaoBancariaList;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @OneToMany(mappedBy = "idBanco")
    private List<Cartao> cartaoList;
    @OneToMany(mappedBy = "idBanco")
    private List<ContasReceber> contasReceberList;

    public Banco() {
    }

    public Banco(Integer idBanco) {
        this.idBanco = idBanco;
    }

    public Integer getIdBanco() {
        return idBanco;
    }

    public void setIdBanco(Integer idBanco) {
        this.idBanco = idBanco;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNumeroBanco() {
        return numeroBanco;
    }

    public void setNumeroBanco(String numeroBanco) {
        this.numeroBanco = numeroBanco;
    }

    @XmlTransient
    public List<ContasPagar> getContasPagarList() {
        return contasPagarList;
    }

    public void setContasPagarList(List<ContasPagar> contasPagarList) {
        this.contasPagarList = contasPagarList;
    }

    @XmlTransient
    public List<ContaCorrente> getContaCorrenteList() {
        return contaCorrenteList;
    }

    public void setContaCorrenteList(List<ContaCorrente> contaCorrenteList) {
        this.contaCorrenteList = contaCorrenteList;
    }

    @XmlTransient
    public List<MovimentacaoBancaria> getMovimentacaoBancariaList() {
        return movimentacaoBancariaList;
    }

    public void setMovimentacaoBancariaList(List<MovimentacaoBancaria> movimentacaoBancariaList) {
        this.movimentacaoBancariaList = movimentacaoBancariaList;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getNumeroBancoExp() {
        return numeroBancoExp;
    }

    public void setNumeroBancoExp(String numeroBancoExp) {
        this.numeroBancoExp = numeroBancoExp;
    }
    
    @XmlTransient
    public List<Cartao> getCartaoList() {
        return cartaoList;
    }

    public void setCartaoList(List<Cartao> cartaoList) {
        this.cartaoList = cartaoList;
    }

    @XmlTransient
    public List<ContasReceber> getContasReceberList() {
        return contasReceberList;
    }

    public void setContasReceberList(List<ContasReceber> contasReceberList) {
        this.contasReceberList = contasReceberList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idBanco != null ? idBanco.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Banco)) {
            return false;
        }
        Banco other = (Banco) object;
        if ((this.idBanco == null && other.idBanco != null) || (this.idBanco != null && !this.idBanco.equals(other.idBanco))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Banco[ idBanco=" + idBanco + " ]";
    }
    
}
