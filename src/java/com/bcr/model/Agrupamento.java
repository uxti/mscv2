/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "agrupamento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Agrupamento.findAll", query = "SELECT a FROM Agrupamento a"),
    @NamedQuery(name = "Agrupamento.findByIdAgrupamento", query = "SELECT a FROM Agrupamento a WHERE a.idAgrupamento = :idAgrupamento"),
    @NamedQuery(name = "Agrupamento.findByDescricao", query = "SELECT a FROM Agrupamento a WHERE a.descricao = :descricao"),
    @NamedQuery(name = "Agrupamento.findByTipo", query = "SELECT a FROM Agrupamento a WHERE a.tipo = :tipo")})
public class Agrupamento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_agrupamento")
    private Integer idAgrupamento;
    @Size(max = 255)
    @Column(name = "descricao")
    private String descricao;
    @Size(max = 1)
    @Column(name = "tipo")
    private String tipo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAgrupamento")
    private List<CentroCustoAgrupamento> centroCustoAgrupamentoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAgrupamento")
    private List<PlanoContas> planoContasList;

    public Agrupamento() {
    }

    public Agrupamento(Integer idAgrupamento) {
        this.idAgrupamento = idAgrupamento;
    }

    public Integer getIdAgrupamento() {
        return idAgrupamento;
    }

    public void setIdAgrupamento(Integer idAgrupamento) {
        this.idAgrupamento = idAgrupamento;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @XmlTransient
    public List<CentroCustoAgrupamento> getCentroCustoAgrupamentoList() {
        return centroCustoAgrupamentoList;
    }

    public void setCentroCustoAgrupamentoList(List<CentroCustoAgrupamento> centroCustoAgrupamentoList) {
        this.centroCustoAgrupamentoList = centroCustoAgrupamentoList;
    }

    @XmlTransient
    public List<PlanoContas> getPlanoContasList() {
        return planoContasList;
    }

    public void setPlanoContasList(List<PlanoContas> planoContasList) {
        this.planoContasList = planoContasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAgrupamento != null ? idAgrupamento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Agrupamento)) {
            return false;
        }
        Agrupamento other = (Agrupamento) object;
        if ((this.idAgrupamento == null && other.idAgrupamento != null) || (this.idAgrupamento != null && !this.idAgrupamento.equals(other.idAgrupamento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Agrupamento[ idAgrupamento=" + idAgrupamento + " ]";
    }
    
}
