/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "comunicado_impl_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ComunicadoImplItem.findAll", query = "SELECT c FROM ComunicadoImplItem c"),
    @NamedQuery(name = "ComunicadoImplItem.findByIdComunicadoImplantacaoItem", query = "SELECT c FROM ComunicadoImplItem c WHERE c.idComunicadoImplantacaoItem = :idComunicadoImplantacaoItem"),
    @NamedQuery(name = "ComunicadoImplItem.findByQtdCargos", query = "SELECT c FROM ComunicadoImplItem c WHERE c.qtdCargos = :qtdCargos"),
    @NamedQuery(name = "ComunicadoImplItem.findByCargaHorariaSemanal", query = "SELECT c FROM ComunicadoImplItem c WHERE c.cargaHorariaSemanal = :cargaHorariaSemanal"),
    @NamedQuery(name = "ComunicadoImplItem.findBySalario", query = "SELECT c FROM ComunicadoImplItem c WHERE c.salario = :salario"),
    @NamedQuery(name = "ComunicadoImplItem.findByPcmso", query = "SELECT c FROM ComunicadoImplItem c WHERE c.pcmso = :pcmso"),
    @NamedQuery(name = "ComunicadoImplItem.findByAdicionalInsalubridade", query = "SELECT c FROM ComunicadoImplItem c WHERE c.adicionalInsalubridade = :adicionalInsalubridade"),
    @NamedQuery(name = "ComunicadoImplItem.findByAdicionalPericulosidade", query = "SELECT c FROM ComunicadoImplItem c WHERE c.adicionalPericulosidade = :adicionalPericulosidade"),
    @NamedQuery(name = "ComunicadoImplItem.findByHorasExtras", query = "SELECT c FROM ComunicadoImplItem c WHERE c.horasExtras = :horasExtras"),
    @NamedQuery(name = "ComunicadoImplItem.findByAdicionalNoturno", query = "SELECT c FROM ComunicadoImplItem c WHERE c.adicionalNoturno = :adicionalNoturno"),
    @NamedQuery(name = "ComunicadoImplItem.findByGratificacao", query = "SELECT c FROM ComunicadoImplItem c WHERE c.gratificacao = :gratificacao"),
    @NamedQuery(name = "ComunicadoImplItem.findByAlimentacao", query = "SELECT c FROM ComunicadoImplItem c WHERE c.alimentacao = :alimentacao"),
    @NamedQuery(name = "ComunicadoImplItem.findByAcumuloFuncao", query = "SELECT c FROM ComunicadoImplItem c WHERE c.acumuloFuncao = :acumuloFuncao"),
    @NamedQuery(name = "ComunicadoImplItem.findByValeTransporte", query = "SELECT c FROM ComunicadoImplItem c WHERE c.valeTransporte = :valeTransporte"),
    @NamedQuery(name = "ComunicadoImplItem.findBySeguroVida", query = "SELECT c FROM ComunicadoImplItem c WHERE c.seguroVida = :seguroVida")})
public class ComunicadoImplItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_comunicado_implantacao_item")
    private Integer idComunicadoImplantacaoItem;
    @Column(name = "qtd_cargos")
    private Integer qtdCargos;
    @Size(max = 255)
    @Column(name = "carga_horaria_semanal")
    private String cargaHorariaSemanal;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "salario")
    private BigDecimal salario;
    @Column(name = "PCMSO")
    private BigDecimal pcmso;
    @Size(max = 1)
    @Column(name = "adicional_insalubridade")
    private String adicionalInsalubridade;
    @Size(max = 1)
    @Column(name = "adicional_periculosidade")
    private String adicionalPericulosidade;
    @Size(max = 1)
    @Column(name = "horas_extras")
    private String horasExtras;
    @Size(max = 1)
    @Column(name = "adicional_noturno")
    private String adicionalNoturno;
    @Size(max = 1)
    @Column(name = "gratificacao")
    private String gratificacao;
    @Column(name = "alimentacao")
    private BigDecimal alimentacao;
    @Size(max = 1)
    @Column(name = "acumulo_funcao")
    private String acumuloFuncao;
    @Size(max = 1)
    @Column(name = "vale_transporte")
    private String valeTransporte;
    @Column(name = "seguro_vida")
    private BigDecimal seguroVida;
    @JoinColumn(name = "id_comunicado_implantacao", referencedColumnName = "id_comunicado_implantacao")
    @ManyToOne(optional = false)
    private ComunicadoImplantacao idComunicadoImplantacao;
    @JoinColumn(name = "id_cargo", referencedColumnName = "id_cargo")
    @ManyToOne
    private Cargo idCargo;
    @JoinColumn(name = "id_setor", referencedColumnName = "id_setor")
    @ManyToOne(optional = false)
    private Setor idSetor;
    @JoinColumn(name = "id_servico_resumido", referencedColumnName = "id_servico_resumido")
    @ManyToOne(optional = false)
    private ServicoResumido idServicoResumido;

    public ComunicadoImplItem() {
    }

    public ComunicadoImplItem(Integer idComunicadoImplantacaoItem) {
        this.idComunicadoImplantacaoItem = idComunicadoImplantacaoItem;
    }

    public Integer getIdComunicadoImplantacaoItem() {
        return idComunicadoImplantacaoItem;
    }

    public void setIdComunicadoImplantacaoItem(Integer idComunicadoImplantacaoItem) {
        this.idComunicadoImplantacaoItem = idComunicadoImplantacaoItem;
    }

    public Integer getQtdCargos() {
        return qtdCargos;
    }

    public void setQtdCargos(Integer qtdCargos) {
        this.qtdCargos = qtdCargos;
    }

    public String getCargaHorariaSemanal() {
        return cargaHorariaSemanal;
    }

    public void setCargaHorariaSemanal(String cargaHorariaSemanal) {
        this.cargaHorariaSemanal = cargaHorariaSemanal;
    }

    public BigDecimal getSalario() {
        return salario;
    }

    public void setSalario(BigDecimal salario) {
        this.salario = salario;
    }

    public BigDecimal getPcmso() {
        return pcmso;
    }

    public void setPcmso(BigDecimal pcmso) {
        this.pcmso = pcmso;
    }

    public String getAdicionalInsalubridade() {
        return adicionalInsalubridade;
    }

    public void setAdicionalInsalubridade(String adicionalInsalubridade) {
        this.adicionalInsalubridade = adicionalInsalubridade;
    }

    public String getAdicionalPericulosidade() {
        return adicionalPericulosidade;
    }

    public void setAdicionalPericulosidade(String adicionalPericulosidade) {
        this.adicionalPericulosidade = adicionalPericulosidade;
    }

    public String getHorasExtras() {
        return horasExtras;
    }

    public void setHorasExtras(String horasExtras) {
        this.horasExtras = horasExtras;
    }

    public String getAdicionalNoturno() {
        return adicionalNoturno;
    }

    public void setAdicionalNoturno(String adicionalNoturno) {
        this.adicionalNoturno = adicionalNoturno;
    }

    public String getGratificacao() {
        return gratificacao;
    }

    public void setGratificacao(String gratificacao) {
        this.gratificacao = gratificacao;
    }

    public BigDecimal getAlimentacao() {
        return alimentacao;
    }

    public void setAlimentacao(BigDecimal alimentacao) {
        this.alimentacao = alimentacao;
    }

    public String getAcumuloFuncao() {
        return acumuloFuncao;
    }

    public void setAcumuloFuncao(String acumuloFuncao) {
        this.acumuloFuncao = acumuloFuncao;
    }

    public String getValeTransporte() {
        return valeTransporte;
    }

    public void setValeTransporte(String valeTransporte) {
        this.valeTransporte = valeTransporte;
    }

    public BigDecimal getSeguroVida() {
        return seguroVida;
    }

    public void setSeguroVida(BigDecimal seguroVida) {
        this.seguroVida = seguroVida;
    }

    public ComunicadoImplantacao getIdComunicadoImplantacao() {
        return idComunicadoImplantacao;
    }

    public void setIdComunicadoImplantacao(ComunicadoImplantacao idComunicadoImplantacao) {
        this.idComunicadoImplantacao = idComunicadoImplantacao;
    }

    public Cargo getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Cargo idCargo) {
        this.idCargo = idCargo;
    }

    public Setor getIdSetor() {
        return idSetor;
    }

    public void setIdSetor(Setor idSetor) {
        this.idSetor = idSetor;
    }

    public ServicoResumido getIdServicoResumido() {
        return idServicoResumido;
    }

    public void setIdServicoResumido(ServicoResumido idServicoResumido) {
        this.idServicoResumido = idServicoResumido;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idComunicadoImplantacaoItem != null ? idComunicadoImplantacaoItem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComunicadoImplItem)) {
            return false;
        }
        ComunicadoImplItem other = (ComunicadoImplItem) object;
        if ((this.idComunicadoImplantacaoItem == null && other.idComunicadoImplantacaoItem != null) || (this.idComunicadoImplantacaoItem != null && !this.idComunicadoImplantacaoItem.equals(other.idComunicadoImplantacaoItem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.ComunicadoImplItem[ idComunicadoImplantacaoItem=" + idComunicadoImplantacaoItem + " ]";
    }
    
}
