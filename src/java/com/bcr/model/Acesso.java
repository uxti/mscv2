/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "acesso")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Acesso.findAll", query = "SELECT a FROM Acesso a"),
    @NamedQuery(name = "Acesso.findByIdAcesso", query = "SELECT a FROM Acesso a WHERE a.idAcesso = :idAcesso"),
    @NamedQuery(name = "Acesso.findByModuloFinanceiro", query = "SELECT a FROM Acesso a WHERE a.moduloFinanceiro = :moduloFinanceiro"),
    @NamedQuery(name = "Acesso.findByFinanceiroContasPagar", query = "SELECT a FROM Acesso a WHERE a.financeiroContasPagar = :financeiroContasPagar"),
    @NamedQuery(name = "Acesso.findByFinanceiroContasReceber", query = "SELECT a FROM Acesso a WHERE a.financeiroContasReceber = :financeiroContasReceber"),
    @NamedQuery(name = "Acesso.findByFinanceiroCaixa", query = "SELECT a FROM Acesso a WHERE a.financeiroCaixa = :financeiroCaixa"),
    @NamedQuery(name = "Acesso.findByFinanceiroBanco", query = "SELECT a FROM Acesso a WHERE a.financeiroBanco = :financeiroBanco"),
    @NamedQuery(name = "Acesso.findByContasPagarPesquisar", query = "SELECT a FROM Acesso a WHERE a.contasPagarPesquisar = :contasPagarPesquisar"),
    @NamedQuery(name = "Acesso.findByContasPagarInserir", query = "SELECT a FROM Acesso a WHERE a.contasPagarInserir = :contasPagarInserir"),
    @NamedQuery(name = "Acesso.findByContasPagarPagar", query = "SELECT a FROM Acesso a WHERE a.contasPagarPagar = :contasPagarPagar"),
    @NamedQuery(name = "Acesso.findByContasPagarExcluir", query = "SELECT a FROM Acesso a WHERE a.contasPagarExcluir = :contasPagarExcluir"),
    @NamedQuery(name = "Acesso.findByContasPagarEstornar", query = "SELECT a FROM Acesso a WHERE a.contasPagarEstornar = :contasPagarEstornar"),
    @NamedQuery(name = "Acesso.findByContasPagarVer", query = "SELECT a FROM Acesso a WHERE a.contasPagarVer = :contasPagarVer"),
    @NamedQuery(name = "Acesso.findByContasReceberPesquisar", query = "SELECT a FROM Acesso a WHERE a.contasReceberPesquisar = :contasReceberPesquisar"),
    @NamedQuery(name = "Acesso.findByContasReceberInserir", query = "SELECT a FROM Acesso a WHERE a.contasReceberInserir = :contasReceberInserir"),
    @NamedQuery(name = "Acesso.findByContasReceberReceber", query = "SELECT a FROM Acesso a WHERE a.contasReceberReceber = :contasReceberReceber"),
    @NamedQuery(name = "Acesso.findByContasReceberExcluir", query = "SELECT a FROM Acesso a WHERE a.contasReceberExcluir = :contasReceberExcluir"),
    @NamedQuery(name = "Acesso.findByContasReceberEstornar", query = "SELECT a FROM Acesso a WHERE a.contasReceberEstornar = :contasReceberEstornar"),
    @NamedQuery(name = "Acesso.findByContasReceberVer", query = "SELECT a FROM Acesso a WHERE a.contasReceberVer = :contasReceberVer"),
    @NamedQuery(name = "Acesso.findByCaixaPesquisar", query = "SELECT a FROM Acesso a WHERE a.caixaPesquisar = :caixaPesquisar"),
    @NamedQuery(name = "Acesso.findByCaixaSangria", query = "SELECT a FROM Acesso a WHERE a.caixaSangria = :caixaSangria"),
    @NamedQuery(name = "Acesso.findByCaixaSuprimento", query = "SELECT a FROM Acesso a WHERE a.caixaSuprimento = :caixaSuprimento"),
    @NamedQuery(name = "Acesso.findByCaixaTransferencia", query = "SELECT a FROM Acesso a WHERE a.caixaTransferencia = :caixaTransferencia"),
    @NamedQuery(name = "Acesso.findByBancoPesquisar", query = "SELECT a FROM Acesso a WHERE a.bancoPesquisar = :bancoPesquisar"),
    @NamedQuery(name = "Acesso.findByBancoSaque", query = "SELECT a FROM Acesso a WHERE a.bancoSaque = :bancoSaque"),
    @NamedQuery(name = "Acesso.findByBancoDeposito", query = "SELECT a FROM Acesso a WHERE a.bancoDeposito = :bancoDeposito"),
    @NamedQuery(name = "Acesso.findByBancoTransferencia", query = "SELECT a FROM Acesso a WHERE a.bancoTransferencia = :bancoTransferencia"),
    @NamedQuery(name = "Acesso.findByGestaoResultado", query = "SELECT a FROM Acesso a WHERE a.gestaoResultado = :gestaoResultado"),
    @NamedQuery(name = "Acesso.findByFinanceiroAcessoTotal", query = "SELECT a FROM Acesso a WHERE a.financeiroAcessoTotal = :financeiroAcessoTotal")})
public class Acesso implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ACESSO")
    private Integer idAcesso;
    @Column(name = "MODULO_FINANCEIRO")
    private Boolean moduloFinanceiro;
    @Column(name = "FINANCEIRO_CONTAS_PAGAR")
    private Boolean financeiroContasPagar;
    @Column(name = "FINANCEIRO_CONTAS_RECEBER")
    private Boolean financeiroContasReceber;
    @Column(name = "FINANCEIRO_CAIXA")
    private Boolean financeiroCaixa;
    @Column(name = "FINANCEIRO_BANCO")
    private Boolean financeiroBanco;
    @Column(name = "CONTAS_PAGAR_PESQUISAR")
    private Boolean contasPagarPesquisar;
    @Column(name = "CONTAS_PAGAR_INSERIR")
    private Boolean contasPagarInserir;
    @Column(name = "CONTAS_PAGAR_PAGAR")
    private Boolean contasPagarPagar;
    @Column(name = "CONTAS_PAGAR_EXCLUIR")
    private Boolean contasPagarExcluir;
    @Column(name = "CONTAS_PAGAR_ESTORNAR")
    private Boolean contasPagarEstornar;
    @Column(name = "CONTAS_PAGAR_VER")
    private Boolean contasPagarVer;
    @Column(name = "CONTAS_RECEBER_PESQUISAR")
    private Boolean contasReceberPesquisar;
    @Column(name = "CONTAS_RECEBER_INSERIR")
    private Boolean contasReceberInserir;
    @Column(name = "CONTAS_RECEBER_RECEBER")
    private Boolean contasReceberReceber;
    @Column(name = "CONTAS_RECEBER_EXCLUIR")
    private Boolean contasReceberExcluir;
    @Column(name = "CONTAS_RECEBER_ESTORNAR")
    private Boolean contasReceberEstornar;
    @Column(name = "CONTAS_RECEBER_VER")
    private Boolean contasReceberVer;
    @Column(name = "CAIXA_PESQUISAR")
    private Boolean caixaPesquisar;
    @Column(name = "CAIXA_SANGRIA")
    private Boolean caixaSangria;
    @Column(name = "CAIXA_SUPRIMENTO")
    private Boolean caixaSuprimento;
    @Column(name = "CAIXA_TRANSFERENCIA")
    private Boolean caixaTransferencia;
    @Column(name = "BANCO_PESQUISAR")
    private Boolean bancoPesquisar;
    @Column(name = "BANCO_SAQUE")
    private Boolean bancoSaque;
    @Column(name = "BANCO_DEPOSITO")
    private Boolean bancoDeposito;
    @Column(name = "BANCO_TRANSFERENCIA")
    private Boolean bancoTransferencia;
    @Column(name = "GESTAO_RESULTADO")
    private Boolean gestaoResultado;
    @Column(name = "FINANCEIRO_ACESSO_TOTAL")
    private Boolean financeiroAcessoTotal;
    @OneToMany(mappedBy = "idAcesso")
    private List<Perfil> perfilList;

    public Acesso() {
    }

    public Acesso(Integer idAcesso) {
        this.idAcesso = idAcesso;
    }

    public Integer getIdAcesso() {
        return idAcesso;
    }

    public void setIdAcesso(Integer idAcesso) {
        this.idAcesso = idAcesso;
    }

    public Boolean getModuloFinanceiro() {
        return moduloFinanceiro;
    }

    public void setModuloFinanceiro(Boolean moduloFinanceiro) {
        this.moduloFinanceiro = moduloFinanceiro;
    }

    public Boolean getFinanceiroContasPagar() {
        return financeiroContasPagar;
    }

    public void setFinanceiroContasPagar(Boolean financeiroContasPagar) {
        this.financeiroContasPagar = financeiroContasPagar;
    }

    public Boolean getFinanceiroContasReceber() {
        return financeiroContasReceber;
    }

    public void setFinanceiroContasReceber(Boolean financeiroContasReceber) {
        this.financeiroContasReceber = financeiroContasReceber;
    }

    public Boolean getFinanceiroCaixa() {
        return financeiroCaixa;
    }

    public void setFinanceiroCaixa(Boolean financeiroCaixa) {
        this.financeiroCaixa = financeiroCaixa;
    }

    public Boolean getFinanceiroBanco() {
        return financeiroBanco;
    }

    public void setFinanceiroBanco(Boolean financeiroBanco) {
        this.financeiroBanco = financeiroBanco;
    }

    public Boolean getContasPagarPesquisar() {
        return contasPagarPesquisar;
    }

    public void setContasPagarPesquisar(Boolean contasPagarPesquisar) {
        this.contasPagarPesquisar = contasPagarPesquisar;
    }

    public Boolean getContasPagarInserir() {
        return contasPagarInserir;
    }

    public void setContasPagarInserir(Boolean contasPagarInserir) {
        this.contasPagarInserir = contasPagarInserir;
    }

    public Boolean getContasPagarPagar() {
        return contasPagarPagar;
    }

    public void setContasPagarPagar(Boolean contasPagarPagar) {
        this.contasPagarPagar = contasPagarPagar;
    }

    public Boolean getContasPagarExcluir() {
        return contasPagarExcluir;
    }

    public void setContasPagarExcluir(Boolean contasPagarExcluir) {
        this.contasPagarExcluir = contasPagarExcluir;
    }

    public Boolean getContasPagarEstornar() {
        return contasPagarEstornar;
    }

    public void setContasPagarEstornar(Boolean contasPagarEstornar) {
        this.contasPagarEstornar = contasPagarEstornar;
    }

    public Boolean getContasPagarVer() {
        return contasPagarVer;
    }

    public void setContasPagarVer(Boolean contasPagarVer) {
        this.contasPagarVer = contasPagarVer;
    }

    public Boolean getContasReceberPesquisar() {
        return contasReceberPesquisar;
    }

    public void setContasReceberPesquisar(Boolean contasReceberPesquisar) {
        this.contasReceberPesquisar = contasReceberPesquisar;
    }

    public Boolean getContasReceberInserir() {
        return contasReceberInserir;
    }

    public void setContasReceberInserir(Boolean contasReceberInserir) {
        this.contasReceberInserir = contasReceberInserir;
    }

    public Boolean getContasReceberReceber() {
        return contasReceberReceber;
    }

    public void setContasReceberReceber(Boolean contasReceberReceber) {
        this.contasReceberReceber = contasReceberReceber;
    }

    public Boolean getContasReceberExcluir() {
        return contasReceberExcluir;
    }

    public void setContasReceberExcluir(Boolean contasReceberExcluir) {
        this.contasReceberExcluir = contasReceberExcluir;
    }

    public Boolean getContasReceberEstornar() {
        return contasReceberEstornar;
    }

    public void setContasReceberEstornar(Boolean contasReceberEstornar) {
        this.contasReceberEstornar = contasReceberEstornar;
    }

    public Boolean getContasReceberVer() {
        return contasReceberVer;
    }

    public void setContasReceberVer(Boolean contasReceberVer) {
        this.contasReceberVer = contasReceberVer;
    }

    public Boolean getCaixaPesquisar() {
        return caixaPesquisar;
    }

    public void setCaixaPesquisar(Boolean caixaPesquisar) {
        this.caixaPesquisar = caixaPesquisar;
    }

    public Boolean getCaixaSangria() {
        return caixaSangria;
    }

    public void setCaixaSangria(Boolean caixaSangria) {
        this.caixaSangria = caixaSangria;
    }

    public Boolean getCaixaSuprimento() {
        return caixaSuprimento;
    }

    public void setCaixaSuprimento(Boolean caixaSuprimento) {
        this.caixaSuprimento = caixaSuprimento;
    }

    public Boolean getCaixaTransferencia() {
        return caixaTransferencia;
    }

    public void setCaixaTransferencia(Boolean caixaTransferencia) {
        this.caixaTransferencia = caixaTransferencia;
    }

    public Boolean getBancoPesquisar() {
        return bancoPesquisar;
    }

    public void setBancoPesquisar(Boolean bancoPesquisar) {
        this.bancoPesquisar = bancoPesquisar;
    }

    public Boolean getBancoSaque() {
        return bancoSaque;
    }

    public void setBancoSaque(Boolean bancoSaque) {
        this.bancoSaque = bancoSaque;
    }

    public Boolean getBancoDeposito() {
        return bancoDeposito;
    }

    public void setBancoDeposito(Boolean bancoDeposito) {
        this.bancoDeposito = bancoDeposito;
    }

    public Boolean getBancoTransferencia() {
        return bancoTransferencia;
    }

    public void setBancoTransferencia(Boolean bancoTransferencia) {
        this.bancoTransferencia = bancoTransferencia;
    }

    public Boolean getGestaoResultado() {
        return gestaoResultado;
    }

    public void setGestaoResultado(Boolean gestaoResultado) {
        this.gestaoResultado = gestaoResultado;
    }

    public Boolean getFinanceiroAcessoTotal() {
        return financeiroAcessoTotal;
    }

    public void setFinanceiroAcessoTotal(Boolean financeiroAcessoTotal) {
        this.financeiroAcessoTotal = financeiroAcessoTotal;
    }

    @XmlTransient
    public List<Perfil> getPerfilList() {
        return perfilList;
    }

    public void setPerfilList(List<Perfil> perfilList) {
        this.perfilList = perfilList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAcesso != null ? idAcesso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Acesso)) {
            return false;
        }
        Acesso other = (Acesso) object;
        if ((this.idAcesso == null && other.idAcesso != null) || (this.idAcesso != null && !this.idAcesso.equals(other.idAcesso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Acesso[ idAcesso=" + idAcesso + " ]";
    }
    
}
