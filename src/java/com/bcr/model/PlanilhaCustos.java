/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "planilha_custos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PlanilhaCustos.findAll", query = "SELECT p FROM PlanilhaCustos p"),
    @NamedQuery(name = "PlanilhaCustos.findByIdPlanilhacusto", query = "SELECT p FROM PlanilhaCustos p WHERE p.idPlanilhacusto = :idPlanilhacusto"),
    @NamedQuery(name = "PlanilhaCustos.findByDescricao", query = "SELECT p FROM PlanilhaCustos p WHERE p.descricao = :descricao"),
    @NamedQuery(name = "PlanilhaCustos.findByTipo", query = "SELECT p FROM PlanilhaCustos p WHERE p.tipo = :tipo")})
public class PlanilhaCustos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_Planilha_custo")
    private Integer idPlanilhacusto;
    @Size(max = 255)
    @Column(name = "descricao")
    private String descricao;
    @Size(max = 100)
    @Column(name = "tipo")
    private String tipo;
    @OneToMany(mappedBy = "idPlanilhacusto")
    private List<PropostaPlanilha> propostaPlanilhaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPlanilhacusto")
    private List<PlanilhaCustoItem> planilhaCustoItemList;

    public PlanilhaCustos() {
    }

    public PlanilhaCustos(Integer idPlanilhacusto) {
        this.idPlanilhacusto = idPlanilhacusto;
    }

    public Integer getIdPlanilhacusto() {
        return idPlanilhacusto;
    }

    public void setIdPlanilhacusto(Integer idPlanilhacusto) {
        this.idPlanilhacusto = idPlanilhacusto;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @XmlTransient
    public List<PropostaPlanilha> getPropostaPlanilhaList() {
        return propostaPlanilhaList;
    }

    public void setPropostaPlanilhaList(List<PropostaPlanilha> propostaPlanilhaList) {
        this.propostaPlanilhaList = propostaPlanilhaList;
    }

    @XmlTransient
    public List<PlanilhaCustoItem> getPlanilhaCustoItemList() {
        return planilhaCustoItemList;
    }

    public void setPlanilhaCustoItemList(List<PlanilhaCustoItem> planilhaCustoItemList) {
        this.planilhaCustoItemList = planilhaCustoItemList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPlanilhacusto != null ? idPlanilhacusto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlanilhaCustos)) {
            return false;
        }
        PlanilhaCustos other = (PlanilhaCustos) object;
        if ((this.idPlanilhacusto == null && other.idPlanilhacusto != null) || (this.idPlanilhacusto != null && !this.idPlanilhacusto.equals(other.idPlanilhacusto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.PlanilhaCustos[ idPlanilhacusto=" + idPlanilhacusto + " ]";
    }
    
}
