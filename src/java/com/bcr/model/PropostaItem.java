/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "proposta_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PropostaItem.findAll", query = "SELECT p FROM PropostaItem p"),
    @NamedQuery(name = "PropostaItem.findByIdPropostaItem", query = "SELECT p FROM PropostaItem p WHERE p.idPropostaItem = :idPropostaItem"),
    @NamedQuery(name = "PropostaItem.findByTipo", query = "SELECT p FROM PropostaItem p WHERE p.tipo = :tipo"),
    @NamedQuery(name = "PropostaItem.findByDescricao", query = "SELECT p FROM PropostaItem p WHERE p.descricao = :descricao"),
    @NamedQuery(name = "PropostaItem.findByQuantidade", query = "SELECT p FROM PropostaItem p WHERE p.quantidade = :quantidade"),
    @NamedQuery(name = "PropostaItem.findBySubTotal", query = "SELECT p FROM PropostaItem p WHERE p.subTotal = :subTotal"),
    @NamedQuery(name = "PropostaItem.findByTotal", query = "SELECT p FROM PropostaItem p WHERE p.total = :total")})
public class PropostaItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_proposta_item")
    private Integer idPropostaItem;
    @Size(max = 100)
    @Column(name = "tipo")
    private String tipo;
    @Size(max = 255)
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "quantidade")
    private Integer quantidade;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "sub_total")
    private BigDecimal subTotal;
    @Column(name = "total")
    private BigDecimal total;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPropostaItem")
    private List<PropostaPlanilha> propostaPlanilhaList;
    @JoinColumn(name = "id_proposta", referencedColumnName = "id_proposta")
    @ManyToOne
    private Proposta idProposta;

    public PropostaItem() {
    }

    public PropostaItem(Integer idPropostaItem) {
        this.idPropostaItem = idPropostaItem;
    }

    public Integer getIdPropostaItem() {
        return idPropostaItem;
    }

    public void setIdPropostaItem(Integer idPropostaItem) {
        this.idPropostaItem = idPropostaItem;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    @XmlTransient
    public List<PropostaPlanilha> getPropostaPlanilhaList() {
        return propostaPlanilhaList;
    }

    public void setPropostaPlanilhaList(List<PropostaPlanilha> propostaPlanilhaList) {
        this.propostaPlanilhaList = propostaPlanilhaList;
    }

    public Proposta getIdProposta() {
        return idProposta;
    }

    public void setIdProposta(Proposta idProposta) {
        this.idProposta = idProposta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPropostaItem != null ? idPropostaItem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PropostaItem)) {
            return false;
        }
        PropostaItem other = (PropostaItem) object;
        if ((this.idPropostaItem == null && other.idPropostaItem != null) || (this.idPropostaItem != null && !this.idPropostaItem.equals(other.idPropostaItem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.PropostaItem[ idPropostaItem=" + idPropostaItem + " ]";
    }
    
}
