/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "nota_entrada_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotaEntradaItem.findAll", query = "SELECT n FROM NotaEntradaItem n"),
    @NamedQuery(name = "NotaEntradaItem.findByIdNotaEntradaItem", query = "SELECT n FROM NotaEntradaItem n WHERE n.idNotaEntradaItem = :idNotaEntradaItem"),
    @NamedQuery(name = "NotaEntradaItem.findByCst", query = "SELECT n FROM NotaEntradaItem n WHERE n.cst = :cst"),
    @NamedQuery(name = "NotaEntradaItem.findByQuantidade", query = "SELECT n FROM NotaEntradaItem n WHERE n.quantidade = :quantidade"),
    @NamedQuery(name = "NotaEntradaItem.findByValorUnitario", query = "SELECT n FROM NotaEntradaItem n WHERE n.valorUnitario = :valorUnitario"),
    @NamedQuery(name = "NotaEntradaItem.findByBasecalculoICMS", query = "SELECT n FROM NotaEntradaItem n WHERE n.basecalculoICMS = :basecalculoICMS"),
    @NamedQuery(name = "NotaEntradaItem.findByValorICMS", query = "SELECT n FROM NotaEntradaItem n WHERE n.valorICMS = :valorICMS"),
    @NamedQuery(name = "NotaEntradaItem.findByValorIPI", query = "SELECT n FROM NotaEntradaItem n WHERE n.valorIPI = :valorIPI"),
    @NamedQuery(name = "NotaEntradaItem.findByAliquotaICMS", query = "SELECT n FROM NotaEntradaItem n WHERE n.aliquotaICMS = :aliquotaICMS"),
    @NamedQuery(name = "NotaEntradaItem.findByAliquotaIPI", query = "SELECT n FROM NotaEntradaItem n WHERE n.aliquotaIPI = :aliquotaIPI"),
    @NamedQuery(name = "NotaEntradaItem.findByNumeracao", query = "SELECT n FROM NotaEntradaItem n WHERE n.numeracao = :numeracao"),
    @NamedQuery(name = "NotaEntradaItem.findByValorFrete", query = "SELECT n FROM NotaEntradaItem n WHERE n.valorFrete = :valorFrete"),
    @NamedQuery(name = "NotaEntradaItem.findByValorSeguro", query = "SELECT n FROM NotaEntradaItem n WHERE n.valorSeguro = :valorSeguro"),
    @NamedQuery(name = "NotaEntradaItem.findByValorDesconto", query = "SELECT n FROM NotaEntradaItem n WHERE n.valorDesconto = :valorDesconto")})
public class NotaEntradaItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_nota_entrada_item")
    private Integer idNotaEntradaItem;
    @Size(max = 3)
    @Column(name = "CST")
    private String cst;
    @Column(name = "quantidade")
    private Integer quantidade;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor_unitario")
    private BigDecimal valorUnitario;
    @Column(name = "base_calculo_ICMS")
    private BigDecimal basecalculoICMS;
    @Column(name = "valor_ICMS")
    private BigDecimal valorICMS;
    @Column(name = "valor_IPI")
    private BigDecimal valorIPI;
    @Column(name = "aliquota_ICMS")
    private BigDecimal aliquotaICMS;
    @Column(name = "aliquota_IPI")
    private BigDecimal aliquotaIPI;
    @Column(name = "numeracao")
    private Short numeracao;
    @Column(name = "valor_frete")
    private BigDecimal valorFrete;
    @Column(name = "valor_seguro")
    private BigDecimal valorSeguro;
    @Column(name = "valor_desconto")
    private BigDecimal valorDesconto;
    @JoinColumn(name = "id_produto", referencedColumnName = "id_produto")
    @ManyToOne(optional = false)
    private Produto idProduto;
    @JoinColumn(name = "id_nota_entrada", referencedColumnName = "id_nota_entrada")
    @ManyToOne(optional = false)
    private NotaEntrada idNotaEntrada;
    @JoinColumn(name = "id_CFOP", referencedColumnName = "ID_CFOP")
    @ManyToOne
    private Cfop idCFOP;

    public NotaEntradaItem() {
    }

    public NotaEntradaItem(Integer idNotaEntradaItem) {
        this.idNotaEntradaItem = idNotaEntradaItem;
    }

    public Integer getIdNotaEntradaItem() {
        return idNotaEntradaItem;
    }

    public void setIdNotaEntradaItem(Integer idNotaEntradaItem) {
        this.idNotaEntradaItem = idNotaEntradaItem;
    }

    public String getCst() {
        return cst;
    }

    public void setCst(String cst) {
        this.cst = cst;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public BigDecimal getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(BigDecimal valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public BigDecimal getBasecalculoICMS() {
        return basecalculoICMS;
    }

    public void setBasecalculoICMS(BigDecimal basecalculoICMS) {
        this.basecalculoICMS = basecalculoICMS;
    }

    public BigDecimal getValorICMS() {
        return valorICMS;
    }

    public void setValorICMS(BigDecimal valorICMS) {
        this.valorICMS = valorICMS;
    }

    public BigDecimal getValorIPI() {
        return valorIPI;
    }

    public void setValorIPI(BigDecimal valorIPI) {
        this.valorIPI = valorIPI;
    }

    public BigDecimal getAliquotaICMS() {
        return aliquotaICMS;
    }

    public void setAliquotaICMS(BigDecimal aliquotaICMS) {
        this.aliquotaICMS = aliquotaICMS;
    }

    public BigDecimal getAliquotaIPI() {
        return aliquotaIPI;
    }

    public void setAliquotaIPI(BigDecimal aliquotaIPI) {
        this.aliquotaIPI = aliquotaIPI;
    }

    public Short getNumeracao() {
        return numeracao;
    }

    public void setNumeracao(Short numeracao) {
        this.numeracao = numeracao;
    }

    public BigDecimal getValorFrete() {
        return valorFrete;
    }

    public void setValorFrete(BigDecimal valorFrete) {
        this.valorFrete = valorFrete;
    }

    public BigDecimal getValorSeguro() {
        return valorSeguro;
    }

    public void setValorSeguro(BigDecimal valorSeguro) {
        this.valorSeguro = valorSeguro;
    }

    public BigDecimal getValorDesconto() {
        return valorDesconto;
    }

    public void setValorDesconto(BigDecimal valorDesconto) {
        this.valorDesconto = valorDesconto;
    }

    public Produto getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Produto idProduto) {
        this.idProduto = idProduto;
    }

    public NotaEntrada getIdNotaEntrada() {
        return idNotaEntrada;
    }

    public void setIdNotaEntrada(NotaEntrada idNotaEntrada) {
        this.idNotaEntrada = idNotaEntrada;
    }

    public Cfop getIdCFOP() {
        return idCFOP;
    }

    public void setIdCFOP(Cfop idCFOP) {
        this.idCFOP = idCFOP;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNotaEntradaItem != null ? idNotaEntradaItem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotaEntradaItem)) {
            return false;
        }
        NotaEntradaItem other = (NotaEntradaItem) object;
        if ((this.idNotaEntradaItem == null && other.idNotaEntradaItem != null) || (this.idNotaEntradaItem != null && !this.idNotaEntradaItem.equals(other.idNotaEntradaItem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.NotaEntradaItem[ idNotaEntradaItem=" + idNotaEntradaItem + " ]";
    }
    
}
