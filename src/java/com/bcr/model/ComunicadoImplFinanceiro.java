/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "comunicado_impl_financeiro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ComunicadoImplFinanceiro.findAll", query = "SELECT c FROM ComunicadoImplFinanceiro c"),
    @NamedQuery(name = "ComunicadoImplFinanceiro.findByIdComunicadoImpFinanceiro", query = "SELECT c FROM ComunicadoImplFinanceiro c WHERE c.idComunicadoImpFinanceiro = :idComunicadoImpFinanceiro"),
    @NamedQuery(name = "ComunicadoImplFinanceiro.findByModalidade", query = "SELECT c FROM ComunicadoImplFinanceiro c WHERE c.modalidade = :modalidade"),
    @NamedQuery(name = "ComunicadoImplFinanceiro.findByResponsavelNfe", query = "SELECT c FROM ComunicadoImplFinanceiro c WHERE c.responsavelNfe = :responsavelNfe"),
    @NamedQuery(name = "ComunicadoImplFinanceiro.findByContato", query = "SELECT c FROM ComunicadoImplFinanceiro c WHERE c.contato = :contato"),
    @NamedQuery(name = "ComunicadoImplFinanceiro.findByEmailEnvio", query = "SELECT c FROM ComunicadoImplFinanceiro c WHERE c.emailEnvio = :emailEnvio"),
    @NamedQuery(name = "ComunicadoImplFinanceiro.findByDocumentoNf", query = "SELECT c FROM ComunicadoImplFinanceiro c WHERE c.documentoNf = :documentoNf"),
    @NamedQuery(name = "ComunicadoImplFinanceiro.findByDtPrimeiraNf", query = "SELECT c FROM ComunicadoImplFinanceiro c WHERE c.dtPrimeiraNf = :dtPrimeiraNf"),
    @NamedQuery(name = "ComunicadoImplFinanceiro.findByValorPrimeiraNf", query = "SELECT c FROM ComunicadoImplFinanceiro c WHERE c.valorPrimeiraNf = :valorPrimeiraNf"),
    @NamedQuery(name = "ComunicadoImplFinanceiro.findByValorMensal", query = "SELECT c FROM ComunicadoImplFinanceiro c WHERE c.valorMensal = :valorMensal"),
    @NamedQuery(name = "ComunicadoImplFinanceiro.findByDiaPreferencialNf", query = "SELECT c FROM ComunicadoImplFinanceiro c WHERE c.diaPreferencialNf = :diaPreferencialNf"),
    @NamedQuery(name = "ComunicadoImplFinanceiro.findByIss", query = "SELECT c FROM ComunicadoImplFinanceiro c WHERE c.iss = :iss"),
    @NamedQuery(name = "ComunicadoImplFinanceiro.findByObservacao", query = "SELECT c FROM ComunicadoImplFinanceiro c WHERE c.observacao = :observacao")})
public class ComunicadoImplFinanceiro implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_comunicado_imp_financeiro")
    private Integer idComunicadoImpFinanceiro;
    @Size(max = 100)
    @Column(name = "modalidade")
    private String modalidade;
    @Size(max = 99)
    @Column(name = "responsavel_nfe")
    private String responsavelNfe;
    @Size(max = 99)
    @Column(name = "contato")
    private String contato;
    @Size(max = 100)
    @Column(name = "email_envio")
    private String emailEnvio;
    @Size(max = 100)
    @Column(name = "documento_nf")
    private String documentoNf;
    @Column(name = "dt_primeira_nf")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtPrimeiraNf;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor_primeira_nf")
    private BigDecimal valorPrimeiraNf;
    @Column(name = "valor_mensal")
    private BigDecimal valorMensal;
    @Column(name = "dia_preferencial_nf")
    @Temporal(TemporalType.TIMESTAMP)
    private Date diaPreferencialNf;
    @Column(name = "ISS")
    private Short iss;
    @Size(max = 255)
    @Column(name = "observacao")
    private String observacao;
    @JoinColumn(name = "id_contrato", referencedColumnName = "id_contrato")
    @ManyToOne(optional = false)
    private Contrato idContrato;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    @ManyToOne
    private Cliente idCliente;
    @JoinColumn(name = "id_comunicado_implantacao", referencedColumnName = "id_comunicado_implantacao")
    @ManyToOne
    private ComunicadoImplantacao idComunicadoImplantacao;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne
    private Usuario idUsuario;

    public ComunicadoImplFinanceiro() {
    }

    public ComunicadoImplFinanceiro(Integer idComunicadoImpFinanceiro) {
        this.idComunicadoImpFinanceiro = idComunicadoImpFinanceiro;
    }

    public Integer getIdComunicadoImpFinanceiro() {
        return idComunicadoImpFinanceiro;
    }

    public void setIdComunicadoImpFinanceiro(Integer idComunicadoImpFinanceiro) {
        this.idComunicadoImpFinanceiro = idComunicadoImpFinanceiro;
    }

    public String getModalidade() {
        return modalidade;
    }

    public void setModalidade(String modalidade) {
        this.modalidade = modalidade;
    }

    public String getResponsavelNfe() {
        return responsavelNfe;
    }

    public void setResponsavelNfe(String responsavelNfe) {
        this.responsavelNfe = responsavelNfe;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    public String getEmailEnvio() {
        return emailEnvio;
    }

    public void setEmailEnvio(String emailEnvio) {
        this.emailEnvio = emailEnvio;
    }

    public String getDocumentoNf() {
        return documentoNf;
    }

    public void setDocumentoNf(String documentoNf) {
        this.documentoNf = documentoNf;
    }

    public Date getDtPrimeiraNf() {
        return dtPrimeiraNf;
    }

    public void setDtPrimeiraNf(Date dtPrimeiraNf) {
        this.dtPrimeiraNf = dtPrimeiraNf;
    }

    public BigDecimal getValorPrimeiraNf() {
        return valorPrimeiraNf;
    }

    public void setValorPrimeiraNf(BigDecimal valorPrimeiraNf) {
        this.valorPrimeiraNf = valorPrimeiraNf;
    }

    public BigDecimal getValorMensal() {
        return valorMensal;
    }

    public void setValorMensal(BigDecimal valorMensal) {
        this.valorMensal = valorMensal;
    }

    public Date getDiaPreferencialNf() {
        return diaPreferencialNf;
    }

    public void setDiaPreferencialNf(Date diaPreferencialNf) {
        this.diaPreferencialNf = diaPreferencialNf;
    }

    public Short getIss() {
        return iss;
    }

    public void setIss(Short iss) {
        this.iss = iss;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Contrato getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(Contrato idContrato) {
        this.idContrato = idContrato;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    public ComunicadoImplantacao getIdComunicadoImplantacao() {
        return idComunicadoImplantacao;
    }

    public void setIdComunicadoImplantacao(ComunicadoImplantacao idComunicadoImplantacao) {
        this.idComunicadoImplantacao = idComunicadoImplantacao;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idComunicadoImpFinanceiro != null ? idComunicadoImpFinanceiro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComunicadoImplFinanceiro)) {
            return false;
        }
        ComunicadoImplFinanceiro other = (ComunicadoImplFinanceiro) object;
        if ((this.idComunicadoImpFinanceiro == null && other.idComunicadoImpFinanceiro != null) || (this.idComunicadoImpFinanceiro != null && !this.idComunicadoImpFinanceiro.equals(other.idComunicadoImpFinanceiro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.ComunicadoImplFinanceiro[ idComunicadoImpFinanceiro=" + idComunicadoImpFinanceiro + " ]";
    }
    
}
