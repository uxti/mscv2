/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "proposta_planilha")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PropostaPlanilha.findAll", query = "SELECT p FROM PropostaPlanilha p"),
    @NamedQuery(name = "PropostaPlanilha.findByIdPropostaPlanilha", query = "SELECT p FROM PropostaPlanilha p WHERE p.idPropostaPlanilha = :idPropostaPlanilha")})
public class PropostaPlanilha implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_proposta_planilha")
    private Integer idPropostaPlanilha;
    @JoinColumn(name = "id_proposta_item", referencedColumnName = "id_proposta_item")
    @ManyToOne(optional = false)
    private PropostaItem idPropostaItem;
    @JoinColumn(name = "id_Planilha_custo", referencedColumnName = "id_Planilha_custo")
    @ManyToOne
    private PlanilhaCustos idPlanilhacusto;

    public PropostaPlanilha() {
    }

    public PropostaPlanilha(Integer idPropostaPlanilha) {
        this.idPropostaPlanilha = idPropostaPlanilha;
    }

    public Integer getIdPropostaPlanilha() {
        return idPropostaPlanilha;
    }

    public void setIdPropostaPlanilha(Integer idPropostaPlanilha) {
        this.idPropostaPlanilha = idPropostaPlanilha;
    }

    public PropostaItem getIdPropostaItem() {
        return idPropostaItem;
    }

    public void setIdPropostaItem(PropostaItem idPropostaItem) {
        this.idPropostaItem = idPropostaItem;
    }

    public PlanilhaCustos getIdPlanilhacusto() {
        return idPlanilhacusto;
    }

    public void setIdPlanilhacusto(PlanilhaCustos idPlanilhacusto) {
        this.idPlanilhacusto = idPlanilhacusto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPropostaPlanilha != null ? idPropostaPlanilha.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PropostaPlanilha)) {
            return false;
        }
        PropostaPlanilha other = (PropostaPlanilha) object;
        if ((this.idPropostaPlanilha == null && other.idPropostaPlanilha != null) || (this.idPropostaPlanilha != null && !this.idPropostaPlanilha.equals(other.idPropostaPlanilha))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.PropostaPlanilha[ idPropostaPlanilha=" + idPropostaPlanilha + " ]";
    }
    
}
