/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "negocio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Negocio.findAll", query = "SELECT n FROM Negocio n"),
    @NamedQuery(name = "Negocio.findByIdNegocio", query = "SELECT n FROM Negocio n WHERE n.idNegocio = :idNegocio"),
    @NamedQuery(name = "Negocio.findByDtAbertura", query = "SELECT n FROM Negocio n WHERE n.dtAbertura = :dtAbertura"),
    @NamedQuery(name = "Negocio.findByDtFechamento", query = "SELECT n FROM Negocio n WHERE n.dtFechamento = :dtFechamento"),
    @NamedQuery(name = "Negocio.findByTitulo", query = "SELECT n FROM Negocio n WHERE n.titulo = :titulo"),
    @NamedQuery(name = "Negocio.findByDescricao", query = "SELECT n FROM Negocio n WHERE n.descricao = :descricao"),
    @NamedQuery(name = "Negocio.findByValor", query = "SELECT n FROM Negocio n WHERE n.valor = :valor"),
    @NamedQuery(name = "Negocio.findByPrioridade", query = "SELECT n FROM Negocio n WHERE n.prioridade = :prioridade"),
    @NamedQuery(name = "Negocio.findByFase", query = "SELECT n FROM Negocio n WHERE n.fase = :fase"),
    @NamedQuery(name = "Negocio.findBySituacao", query = "SELECT n FROM Negocio n WHERE n.situacao = :situacao"),
    @NamedQuery(name = "Negocio.findByHorarioFuncionamento1", query = "SELECT n FROM Negocio n WHERE n.horarioFuncionamento1 = :horarioFuncionamento1"),
    @NamedQuery(name = "Negocio.findByHorarioFuncionamento2", query = "SELECT n FROM Negocio n WHERE n.horarioFuncionamento2 = :horarioFuncionamento2"),
    @NamedQuery(name = "Negocio.findByHorarioFuncionamento3", query = "SELECT n FROM Negocio n WHERE n.horarioFuncionamento3 = :horarioFuncionamento3")})
public class Negocio implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_negocio")
    private Integer idNegocio;
    @Column(name = "dt_abertura")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtAbertura;
    @Column(name = "dt_fechamento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtFechamento;
    @Size(max = 255)
    @Column(name = "titulo")
    private String titulo;
    @Size(max = 255)
    @Column(name = "descricao")
    private String descricao;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor")
    private BigDecimal valor;
    @Size(max = 255)
    @Column(name = "prioridade")
    private String prioridade;
    @Size(max = 255)
    @Column(name = "fase")
    private String fase;
    @Size(max = 255)
    @Column(name = "situacao")
    private String situacao;
    @Size(max = 255)
    @Column(name = "horario_funcionamento1")
    private String horarioFuncionamento1;
    @Size(max = 255)
    @Column(name = "horario_funcionamento2")
    private String horarioFuncionamento2;
    @Size(max = 255)
    @Column(name = "horario_funcionamento3")
    private String horarioFuncionamento3;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne
    private Usuario idUsuario;
    @JoinColumn(name = "id_contrato", referencedColumnName = "id_contrato")
    @ManyToOne
    private Contrato idContrato;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    @ManyToOne
    private Cliente idCliente;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idNegocio")
    private List<ContratoNegocio> contratoNegocioList;

    public Negocio() {
    }

    public Negocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public Date getDtAbertura() {
        return dtAbertura;
    }

    public void setDtAbertura(Date dtAbertura) {
        this.dtAbertura = dtAbertura;
    }

    public Date getDtFechamento() {
        return dtFechamento;
    }

    public void setDtFechamento(Date dtFechamento) {
        this.dtFechamento = dtFechamento;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getPrioridade() {
        return prioridade;
    }

    public void setPrioridade(String prioridade) {
        this.prioridade = prioridade;
    }

    public String getFase() {
        return fase;
    }

    public void setFase(String fase) {
        this.fase = fase;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getHorarioFuncionamento1() {
        return horarioFuncionamento1;
    }

    public void setHorarioFuncionamento1(String horarioFuncionamento1) {
        this.horarioFuncionamento1 = horarioFuncionamento1;
    }

    public String getHorarioFuncionamento2() {
        return horarioFuncionamento2;
    }

    public void setHorarioFuncionamento2(String horarioFuncionamento2) {
        this.horarioFuncionamento2 = horarioFuncionamento2;
    }

    public String getHorarioFuncionamento3() {
        return horarioFuncionamento3;
    }

    public void setHorarioFuncionamento3(String horarioFuncionamento3) {
        this.horarioFuncionamento3 = horarioFuncionamento3;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Contrato getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(Contrato idContrato) {
        this.idContrato = idContrato;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    @XmlTransient
    public List<ContratoNegocio> getContratoNegocioList() {
        return contratoNegocioList;
    }

    public void setContratoNegocioList(List<ContratoNegocio> contratoNegocioList) {
        this.contratoNegocioList = contratoNegocioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNegocio != null ? idNegocio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Negocio)) {
            return false;
        }
        Negocio other = (Negocio) object;
        if ((this.idNegocio == null && other.idNegocio != null) || (this.idNegocio != null && !this.idNegocio.equals(other.idNegocio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Negocio[ idNegocio=" + idNegocio + " ]";
    }
    
}
