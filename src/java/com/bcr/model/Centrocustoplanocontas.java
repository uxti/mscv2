/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "centrocustoplanocontas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Centrocustoplanocontas.findAll", query = "SELECT c FROM Centrocustoplanocontas c"),
    @NamedQuery(name = "Centrocustoplanocontas.findByIdCentrocustoplanocontas", query = "SELECT c FROM Centrocustoplanocontas c WHERE c.idCentrocustoplanocontas = :idCentrocustoplanocontas"),
    @NamedQuery(name = "Centrocustoplanocontas.findByPercentual", query = "SELECT c FROM Centrocustoplanocontas c WHERE c.percentual = :percentual"),
    @NamedQuery(name = "Centrocustoplanocontas.findByValor", query = "SELECT c FROM Centrocustoplanocontas c WHERE c.valor = :valor"),
    @NamedQuery(name = "Centrocustoplanocontas.findByTipo", query = "SELECT c FROM Centrocustoplanocontas c WHERE c.tipo = :tipo"),
    @NamedQuery(name = "Centrocustoplanocontas.findByDataMovimento", query = "SELECT c FROM Centrocustoplanocontas c WHERE c.dataMovimento = :dataMovimento")})
public class Centrocustoplanocontas implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_CENTROCUSTOPLANOCONTAS")
    private Integer idCentrocustoplanocontas;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PERCENTUAL")
    private BigDecimal percentual;
    @Column(name = "VALOR")
    private BigDecimal valor;
    @Size(max = 1)
    @Column(name = "TIPO")
    private String tipo;
    @Column(name = "DATA_MOVIMENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataMovimento;
    @JoinColumn(name = "id_requisicao_material", referencedColumnName = "id_requisicao_material")
    @ManyToOne
    private RequisicaoMaterial idRequisicaoMaterial;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    @ManyToOne
    private Cliente idCliente;
    @JoinColumn(name = "id_centro_custo", referencedColumnName = "id_centro_custo")
    @ManyToOne
    private CentroCusto idCentroCusto;
    @JoinColumn(name = "id_contas_receber", referencedColumnName = "id_contas_receber")
    @ManyToOne
    private ContasReceber idContasReceber;
    @JoinColumn(name = "id_plano_contas", referencedColumnName = "id_plano_contas")
    @ManyToOne
    private PlanoContas idPlanoContas;
    @JoinColumn(name = "id_contas_pagar", referencedColumnName = "id_contas_pagar")
    @ManyToOne
    private ContasPagar idContasPagar;

    public Centrocustoplanocontas() {
    }

    public Centrocustoplanocontas(Integer idCentrocustoplanocontas) {
        this.idCentrocustoplanocontas = idCentrocustoplanocontas;
    }

    public Integer getIdCentrocustoplanocontas() {
        return idCentrocustoplanocontas;
    }

    public void setIdCentrocustoplanocontas(Integer idCentrocustoplanocontas) {
        this.idCentrocustoplanocontas = idCentrocustoplanocontas;
    }

    public BigDecimal getPercentual() {
        return percentual;
    }

    public void setPercentual(BigDecimal percentual) {
        this.percentual = percentual;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getDataMovimento() {
        return dataMovimento;
    }

    public void setDataMovimento(Date dataMovimento) {
        this.dataMovimento = dataMovimento;
    }

    public RequisicaoMaterial getIdRequisicaoMaterial() {
        return idRequisicaoMaterial;
    }

    public void setIdRequisicaoMaterial(RequisicaoMaterial idRequisicaoMaterial) {
        this.idRequisicaoMaterial = idRequisicaoMaterial;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    public CentroCusto getIdCentroCusto() {
        return idCentroCusto;
    }

    public void setIdCentroCusto(CentroCusto idCentroCusto) {
        this.idCentroCusto = idCentroCusto;
    }

    public ContasReceber getIdContasReceber() {
        return idContasReceber;
    }

    public void setIdContasReceber(ContasReceber idContasReceber) {
        this.idContasReceber = idContasReceber;
    }

    public PlanoContas getIdPlanoContas() {
        return idPlanoContas;
    }

    public void setIdPlanoContas(PlanoContas idPlanoContas) {
        this.idPlanoContas = idPlanoContas;
    }

    public ContasPagar getIdContasPagar() {
        return idContasPagar;
    }

    public void setIdContasPagar(ContasPagar idContasPagar) {
        this.idContasPagar = idContasPagar;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCentrocustoplanocontas != null ? idCentrocustoplanocontas.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Centrocustoplanocontas)) {
            return false;
        }
        Centrocustoplanocontas other = (Centrocustoplanocontas) object;
        if ((this.idCentrocustoplanocontas == null && other.idCentrocustoplanocontas != null) || (this.idCentrocustoplanocontas != null && !this.idCentrocustoplanocontas.equals(other.idCentrocustoplanocontas))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Centrocustoplanocontas[ idCentrocustoplanocontas=" + idCentrocustoplanocontas + " ]";
    }
    
}
