
CREATE TABLE Acesso (
  ID_ACESSO                 number(10) NOT NULL, 
  id_perfil                 number(10), 
  MODULO_FINANCEIRO         number(1), 
  FINANCEIRO_CONTAS_PAGAR   number(1), 
  FINANCEIRO_CONTAS_RECEBER number(1), 
  FINANCEIRO_CAIXA          number(1), 
  FINANCEIRO_BANCO          number(1), 
  CONTAS_PAGAR_PESQUISAR    number(1), 
  CONTAS_PAGAR_INSERIR      number(1), 
  CONTAS_PAGAR_PAGAR        number(1), 
  CONTAS_PAGAR_EXCLUIR      number(1), 
  CONTAS_PAGAR_ESTORNAR     number(1), 
  CONTAS_PAGAR_VER          number(1), 
  CONTAS_RECEBER_PESQUISAR  number(1), 
  CONTAS_RECEBER_INSERIR    number(1), 
  CONTAS_RECEBER_RECEBER    number(1), 
  CONTAS_RECEBER_EXCLUIR    number(1), 
  CONTAS_RECEBER_ESTORNAR   number(1), 
  CONTAS_RECEBER_VER        number(1), 
  CAIXA_PESQUISAR           number(1), 
  CAIXA_SANGRIA             number(1), 
  CAIXA_SUPRIMENTO          number(1), 
  CAIXA_TRANSFERENCIA       number(1), 
  BANCO_PESQUISAR           number(1), 
  BANCO_SAQUE               number(1), 
  BANCO_DEPOSITO            number(1), 
  BANCO_TRANSFERENCIA       number(1), 
  PRIMARY KEY (ID_ACESSO));
CREATE TABLE Localizacao (
  ID_LOCALIZACAO number(10) NOT NULL, 
  DESCRICAO      varchar2(200), 
  PRIMARY KEY (ID_LOCALIZACAO));
CREATE TABLE Centro_Custo_Variavel (
  id_centro_custo_variavel number(10) NOT NULL, 
  tipo                     varchar2(1), 
  data_movimento           date, 
  valor                    number(12, 2), 
  percentual               number(5), 
  id_centro_custo          number(10), 
  id_contas_pagar          number(10), 
  id_plano_contas          number(10), 
  id_contas_receber        number(10), 
  id_nota_saida            number(10), 
  id_nota_entrada          number(10), 
  id_caixa_item            number(10), 
  id_cliente               number(10), 
  PRIMARY KEY (id_centro_custo_variavel));
CREATE TABLE contas_DRE (
  id_contaDRE number(10) NOT NULL, 
  descricao   varchar2(255), 
  PRIMARY KEY (id_contaDRE));
CREATE TABLE Agrupamento (
  id_agrupamento number(10) NOT NULL, 
  descricao      varchar2(255), 
  PRIMARY KEY (id_agrupamento));
CREATE TABLE Mensagem (
  ID_MENSAGEM         number(10) NOT NULL, 
  MENSAGEM            varchar2(255), 
  VISTA               char(1), 
  id_usuario_sender   number(10), 
  id_usuario_receiver number(10), 
  PRIMARY KEY (ID_MENSAGEM));
CREATE TABLE Contrato_Negocio (
  id_contrato_negocio number(10) NOT NULL, 
  id_contrato         number(10) NOT NULL, 
  id_negocio          number(10) NOT NULL, 
  PRIMARY KEY (id_contrato_negocio), 
  CONSTRAINT uniq_contrato_negocio 
    UNIQUE (id_contrato, id_negocio));
CREATE TABLE Setor (
  id_setor   number(10) NOT NULL, 
  descricao  varchar2(100), 
  id_empresa number(10) NOT NULL, 
  PRIMARY KEY (id_setor));
CREATE TABLE Cotacao_Item (
  id_cotacao_item number(10) NOT NULL, 
  id_cotacao      number(10), 
  valor_cotado    number(12, 2), 
  id_fornecedor   number(10), 
  id_produto      number(10) NOT NULL, 
  qde             number(10), 
  id_empresa      number(10) NOT NULL, 
  PRIMARY KEY (id_cotacao_item));
CREATE TABLE Produto_Fornecedor (
  id_produto_fornecedor number(10) NOT NULL, 
  id_produto            number(10), 
  id_fornecedor         number(10) NOT NULL, 
  PRIMARY KEY (id_produto_fornecedor), 
  CONSTRAINT uniq_produto_fornecedor 
    UNIQUE (id_produto, id_fornecedor));
CREATE TABLE Cotacao_item_historico (
  id_cotacao_item_historico number(10) NOT NULL, 
  id_fornecedor             number(10) NOT NULL, 
  id_fornecedor_selecionado number(10), 
  id_cotacao                number(10) NOT NULL, 
  id_produto                number(10) NOT NULL, 
  valor_anterior            number(12, 2), 
  valor_cotado              number(12, 2), 
  PRIMARY KEY (id_cotacao_item_historico));
CREATE TABLE Ordem_Compra_Item (
  id_ordem_compra_item        number(10) NOT NULL, 
  id_ordem_compra             number(10) NOT NULL, 
  id_requisicao_material_item number(10), 
  id_produto                  number(10) NOT NULL, 
  descricao                   varchar2(100), 
  quantidade                  number(11), 
  vlr_unitario                number(12, 2), 
  PRIMARY KEY (id_ordem_compra_item));
CREATE TABLE Ordem_Compra (
  id_ordem_compra        number(10) NOT NULL, 
  id_requisicao_material number(10), 
  id_usuario             number(10), 
  status                 varchar2(100), 
  dt_cadastro            date, 
  dt_autorizacao         date, 
  dt_atualizacao         date, 
  id_empresa             number(10) NOT NULL, 
  id_fornecedor          number(10), 
  PRIMARY KEY (id_ordem_compra));
CREATE TABLE Cotacao (
  id_cotacao      number(10) NOT NULL, 
  dt_cadastro     date, 
  dt_autorizacao  date, 
  status          varchar2(100), 
  id_usuario      number(10), 
  id_ordem_compra number(10) NOT NULL, 
  id_fornecedor   number(10), 
  PRIMARY KEY (id_cotacao));
CREATE TABLE Requisicao_Material_item (
  id_requisicao_material_item number(10) NOT NULL, 
  id_requisicao_material      number(10) NOT NULL, 
  id_produto                  number(10) NOT NULL, 
  descricao                   varchar2(100), 
  quantidade                  number(10), 
  vlr_unitario                number(12, 2), 
  PRIMARY KEY (id_requisicao_material_item));
CREATE TABLE Local_trabalho (
  id_local_trabalho number(10) NOT NULL, 
  descricao         varchar2(100), 
  id_empresa        number(10) NOT NULL, 
  PRIMARY KEY (id_local_trabalho));
CREATE TABLE Requisicao_Material (
  id_requisicao_material number(10) NOT NULL, 
  id_local_trabalho      number(10) NOT NULL, 
  id_usuario             number(10), 
  id_cliente             number(10), 
  id_custo               number(10), 
  status                 varchar2(100), 
  dt_cadastro            date, 
  valor                  number(12, 2), 
  aplicacao              varchar2(255), 
  dt_atualizacao         date, 
  id_empresa             number(10) NOT NULL, 
  PRIMARY KEY (id_requisicao_material));
CREATE TABLE Colaborador (
  id_colaborador    number(10) NOT NULL, 
  id_usuario        number(10), 
  id_cargo          number(10), 
  id_local_trabalho number(10) NOT NULL, 
  nome              varchar2(255), 
  CPF               varchar2(14), 
  RG                varchar2(20), 
  email             varchar2(255), 
  dt_nascimento     date, 
  dt_cadastro       date, 
  logradouro        varchar2(255), 
  numero            varchar2(5), 
  bloqueado         number(1), 
  bairro            varchar2(255), 
  complemento       varchar2(255), 
  sexo              varchar2(1), 
  CEP               varchar2(9), 
  tipo_logradouro   varchar2(20), 
  contrato_atual    number(10), 
  id_setor          number(10) NOT NULL, 
  id_empresa        number(10) NOT NULL, 
  PRIMARY KEY (id_colaborador));
CREATE TABLE Servico_Resumido (
  id_servico_resumido number(10) NOT NULL, 
  descricao           varchar2(255), 
  valor               number(12, 2), 
  PRIMARY KEY (id_servico_resumido));
CREATE TABLE Arquivo (
  id_arquivo  number(10) NOT NULL, 
  id_contrato number(10), 
  arquivo     blob, 
  PRIMARY KEY (id_arquivo));
CREATE TABLE Categoria (
  id_categoria number(10) NOT NULL, 
  descricao    varchar2(255), 
  id_empresa   number(10) NOT NULL, 
  PRIMARY KEY (id_categoria));
CREATE TABLE Comunicado_Enc_Financeiro (
  id_comunicado_enc_Financeiro number(10) NOT NULL, 
  id_comunicado_encerramento   number(10) NOT NULL, 
  id_cliente                   number(10), 
  dt_envio_nf                  date, 
  valor_restante               number(12, 2), 
  observacao                   varchar2(255), 
  PRIMARY KEY (id_comunicado_enc_Financeiro));
CREATE TABLE Comunicado_Enc_Item (
  id_comunicado_enc_item     number(10) NOT NULL, 
  id_comunicado_encerramento number(10) NOT NULL, 
  id_cargo                   number(10) NOT NULL, 
  id_servico_resumido        number(10), 
  id_setor                   number(10) NOT NULL, 
  id_local_trabalho          number(10), 
  qtd_cargos                 number(3), 
  dt_dispensa                date, 
  observacao                 varchar2(255), 
  PRIMARY KEY (id_comunicado_enc_item));
CREATE TABLE Comunicado_Encerramento (
  id_comunicado_encerramento number(10) NOT NULL, 
  id_usuario                 number(10), 
  id_contrato                number(10), 
  id_cliente                 number(10), 
  id_local_trabalho          number(10) NOT NULL, 
  dt_cancelamento            date, 
  descricao                  varchar2(255), 
  status                     varchar2(100), 
  area_aproximada            varchar2(100), 
  informacoes_diversas       varchar2(255), 
  recomendacoes              varchar2(255), 
  observacao                 varchar2(255), 
  id_empresa                 number(10) NOT NULL, 
  PRIMARY KEY (id_comunicado_encerramento));
CREATE TABLE Comunicado_Impl_Financeiro (
  id_comunicado_imp_financeiro number(10) NOT NULL, 
  id_contrato                  number(10) NOT NULL, 
  id_comunicado_implantacao    number(10), 
  id_cliente                   number(10), 
  modalidade                   varchar2(100), 
  responsavel_nfe              varchar2(99), 
  contato                      varchar2(99), 
  email_envio                  varchar2(100), 
  documento_nf                 varchar2(100), 
  dt_primeira_nf               date, 
  valor_primeira_nf            number(12, 2), 
  valor_mensal                 number(12, 2), 
  dia_preferencial_nf          date, 
  ISS                          number(2), 
  observacao                   varchar2(255), 
  id_usuario                   number(10), 
  PRIMARY KEY (id_comunicado_imp_financeiro));
CREATE TABLE Comunicado_Impl_Item (
  id_comunicado_implantacao_item number(10) NOT NULL, 
  id_comunicado_implantacao      number(10) NOT NULL, 
  id_cargo                       number(10), 
  id_servico_resumido            number(10) NOT NULL, 
  id_setor                       number(10) NOT NULL, 
  qtd_cargos                     number(10), 
  carga_horaria_semanal          varchar2(255), 
  salario                        number(12, 2), 
  PCMSO                          number(12, 2), 
  adicional_insalubridade        varchar2(1), 
  adicional_periculosidade       varchar2(1), 
  horas_extras                   varchar2(1), 
  adicional_noturno              varchar2(1), 
  gratificacao                   varchar2(1), 
  alimentacao                    number(12, 2), 
  acumulo_funcao                 varchar2(1), 
  vale_transporte                varchar2(1), 
  seguro_vida                    number(12, 2), 
  PRIMARY KEY (id_comunicado_implantacao_item));
CREATE TABLE Comunicado_Implantacao (
  id_comunicado_implantacao number(10) NOT NULL, 
  id_cliente                number(10), 
  id_contrato               number(10), 
  id_usuario                number(10), 
  id_local_trabalho         number(10) NOT NULL, 
  dt_solicitacao            date, 
  dt_aprovacao              date, 
  dt_previsao_implantacao   date, 
  descricao                 varchar2(255), 
  status                    varchar2(100), 
  area_aproximada           varchar2(100), 
  informacoes_diversas      varchar2(255), 
  exigencias                varchar2(255), 
  recomendacoes             varchar2(255), 
  observacao                varchar2(255), 
  PRIMARY KEY (id_comunicado_implantacao));
CREATE TABLE Negocio (
  id_negocio             number(10) NOT NULL, 
  id_contrato            number(10), 
  id_cliente             number(10), 
  id_usuario             number(10), 
  dt_abertura            date, 
  dt_fechamento          date, 
  titulo                 varchar2(255), 
  descricao              varchar2(255), 
  valor                  number(12, 2), 
  prioridade             varchar2(255), 
  fase                   varchar2(255), 
  situacao               varchar2(255), 
  horario_funcionamento1 varchar2(255), 
  horario_funcionamento2 varchar2(255), 
  horario_funcionamento3 varchar2(255), 
  id_empresa             number(10) NOT NULL, 
  PRIMARY KEY (id_negocio));
CREATE TABLE Contrato (
  id_contrato         number(10) NOT NULL, 
  id_proposta         number(10), 
  id_categoria        number(10), 
  id_arquivo          number(10), 
  id_cliente          number(10), 
  id_usuario          number(10), 
  id_forma_pagamento  number(10), 
  descricao           varchar2(255), 
  dt_inicio           date, 
  dt_fim              date, 
  status              varchar2(100), 
  valor               number(12, 2), 
  observação          varchar2(255), 
  id_empresa          number(10) NOT NULL, 
  ID_Contrato_Aditivo number(10) NOT NULL, 
  PRIMARY KEY (id_contrato));
CREATE TABLE aliquota_servico (
  id_aliquota_servico number(10) NOT NULL, 
  id_cidade           number(10), 
  id_servico          number(10), 
  descricao           varchar2(100), 
  aliquota            number(12, 2), 
  PRIMARY KEY (id_aliquota_servico));
CREATE TABLE Nota_observacao (
  id_nota_observacao number(10) NOT NULL, 
  descricao          varchar2(100), 
  mensagem           varchar2(255), 
  id_empresa         number(10) NOT NULL, 
  PRIMARY KEY (id_nota_observacao));
CREATE TABLE Centro_Custo (
  id_centro_custo number(10) NOT NULL, 
  id_cliente      number(10), 
  descricao       varchar2(255), 
  implantacao     date, 
  tipo            varchar2(30), 
  complemento     varchar2(255), 
  ativo           varchar2(1), 
  id_fornecedor   number(10), 
  PRIMARY KEY (id_centro_custo));
CREATE TABLE Nota_Saida (
  id_nota_saida              number(10) NOT NULL, 
  id_modelo                  number(10), 
  id_CFOP                    number(10), 
  id_forma_pagamento         number(10), 
  id_nota_observacao         number(10), 
  id_servico                 number(10), 
  id_plano_contas            number(10), 
  id_fornecedor              number(10), 
  numero                     varchar2(20), 
  serie                      varchar2(3), 
  status                     number(10), 
  tipo_saida                 varchar2(100), 
  chave_acesso               number(10), 
  protocolo_autorizacao      varchar2(99), 
  protocolo_recebimento      varchar2(99), 
  dt_emissao                 timestamp, 
  dt_saida                   timestamp, 
  hora_saida                 timestamp, 
  base_calculo_ICMS          number(12, 2), 
  valor_ICMS                 number(12, 2), 
  base_calculo_ICMS_ST       number(12, 2), 
  valor_ICMS_ST              number(12, 2), 
  valor_frete                number(12, 2), 
  valor_seguro               number(12, 2), 
  valor_desconto             number(12, 2), 
  outras_despesas            number(12, 2), 
  valor_IPI                  number(12, 2), 
  valor_total_NF             number(12, 2), 
  valor_total_produtos       number(12, 2), 
  valor_total_servicos       number(12, 2), 
  base_calculo_ISSQN         number(12, 2), 
  valor_ISSQN                number(12, 2), 
  tipo_frete                 varchar2(100), 
  placa_frete                varchar2(8), 
  placa_UF_frete             varchar2(2), 
  quantidade_frete           number(3), 
  modalidade_frete           varchar2(1), 
  marca_frete                varchar2(60), 
  numero_frete               number(10), 
  peso_bruto                 number(15, 3), 
  peso_liquido               number(15, 3), 
  informacoes_complementares varchar2(255), 
  observacoes                varchar2(255), 
  codigo_numerico            number(8), 
  codigo_municipio_gerador   varchar2(7), 
  impressao_danf             varchar2(1), 
  id_empresa                 number(10) NOT NULL, 
  PRIMARY KEY (id_nota_saida));
CREATE TABLE Modelo_NFE (
  id_modelo number(10) NOT NULL, 
  descricao varchar2(255), 
  codigo    varchar2(2), 
  PRIMARY KEY (id_modelo));
CREATE TABLE Parametrizacao (
  id_parametrizacao number(10) NOT NULL, 
  id_empresa        number(10) NOT NULL, 
  enquadramento     varchar2(1), 
  msg_introducao    varchar2(255), 
  tabela_preco      varchar2(1), 
  versao_NFE        varchar2(4), 
  ambiente_NFE      varchar2(1), 
  assinatura_XML    blob, 
  CPFCNPJ_Duplicado number(1), 
  PRIMARY KEY (id_parametrizacao));
CREATE TABLE Proposta_Planilha (
  id_proposta_planilha number(10) NOT NULL, 
  id_proposta_item     number(10) NOT NULL, 
  id_Planilha_custo    number(10), 
  PRIMARY KEY (id_proposta_planilha));
CREATE TABLE Plano_Contas (
  id_plano_contas number(10) NOT NULL, 
  id_empresa      number(10) NOT NULL, 
  id_contaDRE     number(10), 
  id_agrupamento  number(10), 
  codigo          varchar2(10) UNIQUE, 
  tipo            varchar2(1) UNIQUE, 
  descricao       varchar2(255), 
  PRIMARY KEY (id_plano_contas));
CREATE TABLE Conta_Corrente (
  id_conta_corrente number(10) NOT NULL, 
  num_agencia       varchar2(10), 
  gestor            varchar2(100), 
  num_conta         varchar2(10), 
  id_banco          number(10), 
  id_empresa        number(10) NOT NULL, 
  PRIMARY KEY (id_conta_corrente));
CREATE TABLE Cheque (
  id_cheque                 number(10) NOT NULL, 
  tipo                      varchar2(100), 
  conta                     varchar2(99), 
  numero_cheque             number(10), 
  valor                     number(12, 2), 
  dt_recebimento_emissao    date, 
  dt_compensacao_vencimento date, 
  observacao                varchar2(255), 
  compensado                varchar2(1), 
  id_conta_corrente         number(10), 
  id_empresa                number(10) NOT NULL, 
  PRIMARY KEY (id_cheque));
CREATE TABLE Cartao (
  id_cartao    number(10) NOT NULL, 
  descricao    varchar2(255), 
  tipo         varchar2(100), 
  inativo      varchar2(1), 
  taxa_desagio number(12, 2), 
  id_banco     number(10), 
  id_empresa   number(10) NOT NULL, 
  PRIMARY KEY (id_cartao));
CREATE TABLE Forma_Pagamento (
  id_forma_pagamento number(10) NOT NULL, 
  tipo               varchar2(1), 
  descricao          varchar2(255), 
  padrao             varchar2(1), 
  PRIMARY KEY (id_forma_pagamento));
CREATE TABLE Contas_Receber (
  id_contas_receber        number(10) NOT NULL, 
  id_nota_saida            number(10), 
  id_cheque                number(10), 
  dt_lancamento            date, 
  dt_pagamento             date, 
  dt_vencimento            date, 
  dt_estorno               date, 
  num_parcela              number(11), 
  num_docto                varchar2(60), 
  valor_docto              number(12, 2), 
  valor_descto             number(12, 2), 
  valor_acrescimo          number(12, 2), 
  valor_juros              number(12, 2), 
  status                   varchar2(1), 
  observacao               varchar2(255), 
  num_duplicata            varchar2(60), 
  valor_duplicata          number(12, 2), 
  dt_duplicata             date, 
  id_empresa               number(10) NOT NULL, 
  id_plano_contas          number(10), 
  id_forma_pagamento       number(10), 
  id_cliente               number(10), 
  id_centro_custo_variavel number(10), 
  recebido_com             varchar2(10), 
  PRIMARY KEY (id_contas_receber));
CREATE TABLE Contas_Pagar (
  id_contas_pagar    number(10) NOT NULL, 
  id_nota_entrada    number(10), 
  id_cheque          number(10), 
  dt_lancamento      date, 
  dt_pagamento       date, 
  dt_vencimento      date, 
  dt_estorno         date, 
  num_parcela        number(11), 
  num_docto          varchar2(60), 
  valor              number(12, 2), 
  valor_descto       number(12, 2), 
  valor_acrescimo    number(12, 2), 
  valor_juros        number(12, 2), 
  status             varchar2(1), 
  observacao         varchar2(255), 
  num_duplicata      varchar2(60), 
  valor_duplicata    number(12, 2), 
  dt_duplicata       date, 
  id_empresa         number(10) NOT NULL, 
  id_plano_contas    number(10), 
  id_forma_pagamento number(10), 
  id_fornecedor      number(10), 
  pago_com           varchar2(10), 
  PRIMARY KEY (id_contas_pagar));
CREATE TABLE Caixa_Item (
  id_caixa_item     number(10) NOT NULL, 
  tipo              varchar2(100), 
  Tipo_movimento    varchar2(20), 
  historico         varchar2(255), 
  valor             number(12, 2), 
  observacao        varchar2(255), 
  id_contas_pagar   number(10), 
  id_contas_receber number(10) NOT NULL, 
  id_cheque         number(10), 
  id_caixa          number(10), 
  PRIMARY KEY (id_caixa_item));
CREATE TABLE Caixa (
  id_caixa       number(10) NOT NULL, 
  dt_caixa       date, 
  saldo_anterior number(12, 2), 
  total_entradas number(12, 2), 
  total_saidas   number(12, 2), 
  saldo_atual    number(12, 2), 
  id_empresa     number(10) NOT NULL, 
  PRIMARY KEY (id_caixa), 
  CONSTRAINT UQ_Caixa 
    UNIQUE (dt_caixa, id_empresa));
CREATE TABLE Banco (
  id_banco     number(10) NOT NULL, 
  nome         varchar2(255), 
  numero_banco varchar2(10), 
  id_empresa   number(10) NOT NULL, 
  PRIMARY KEY (id_banco));
CREATE TABLE Movimentacao_Bancaria (
  id_movimentacao_bancaria_Item number(10) NOT NULL, 
  id_contas_receber             number(10), 
  id_contas_pagar               number(10), 
  dt_movimentacao_bancaria_item date, 
  historico                     varchar2(255), 
  tipo                          varchar2(100), 
  tipo_movimento                varchar2(100), 
  valor                         number(12, 2), 
  observacao                    varchar2(255), 
  id_cheque                     number(10), 
  id_banco                      number(10), 
  id_empresa                    number(10) NOT NULL, 
  id_conta_corrente             number(10), 
  PRIMARY KEY (id_movimentacao_bancaria_Item));
CREATE TABLE Pedido_Compra_Item (
  id_pedido_compra_item number(10) NOT NULL, 
  id_pedido_compra      number(10) NOT NULL, 
  descricao             varchar2(255), 
  status                varchar2(100), 
  qtd_solicitada        number(5), 
  qtd_chegada           number(5), 
  qtd_restante          number(5), 
  valor_unitario        number(12, 2), 
  valor_total           number(12, 2), 
  PRIMARY KEY (id_pedido_compra_item));
CREATE TABLE Pedido_Compra (
  id_pedido_compra number(10) NOT NULL, 
  numero           varchar2(20), 
  status           varchar2(100), 
  dt_emissao       date, 
  dt_previsao      date, 
  dt_recebimento   date, 
  valor_total      number(12, 2), 
  observacao       varchar2(255), 
  id_cotacao_item  number(10), 
  id_empresa       number(10) NOT NULL, 
  id_fornecedor    number(10), 
  PRIMARY KEY (id_pedido_compra));
CREATE TABLE Transportadora (
  id_transportadora number(10) NOT NULL, 
  id_cidade         number(10), 
  id_estado         number(10), 
  nome_fantasia     varchar2(255), 
  razao_social      varchar2(255), 
  codigo_ANTT       varchar2(20), 
  endereco          varchar2(255), 
  IE                varchar2(20), 
  id_empresa        number(10) NOT NULL, 
  PRIMARY KEY (id_transportadora));
CREATE TABLE Nota_Saida_Item (
  id_nota_saida_item number(10) NOT NULL, 
  id_nota_saida      number(10) NOT NULL, 
  id_CFOP            number(10), 
  id_produto         number(10) NOT NULL, 
  CST                varchar2(3), 
  quantidade         number(10), 
  valor_unitario     number(12, 2), 
  valor_total        number(12, 2), 
  base_calculo_ICMS  number(12, 2), 
  valor_ICMS         number(12, 2), 
  valor_IPI          number(12, 2), 
  aliquota_ICMS      number(12, 2), 
  aliquota_IPI       number(12, 2), 
  numeracao          number(3), 
  valor_frete        number(12, 2), 
  valor_seguro       number(12, 2), 
  valor_desconto     number(12, 2), 
  PRIMARY KEY (id_nota_saida_item));
CREATE TABLE Nota_entrada_Item (
  id_nota_entrada_item number(10) NOT NULL, 
  id_nota_entrada      number(10) NOT NULL, 
  id_produto           number(10) NOT NULL, 
  id_CFOP              number(10), 
  CST                  varchar2(3), 
  quantidade           number(10), 
  valor_unitario       number(12, 2), 
  base_calculo_ICMS    number(12, 2), 
  valor_ICMS           number(12, 2), 
  valor_IPI            number(12, 2), 
  aliquota_ICMS        number(12, 2), 
  aliquota_IPI         number(12, 2), 
  numeracao            number(3), 
  valor_frete          number(12, 2), 
  valor_seguro         number(12, 2), 
  valor_desconto       number(12, 2), 
  PRIMARY KEY (id_nota_entrada_item));
CREATE TABLE Nota_entrada (
  id_nota_entrada            number(10) NOT NULL, 
  id_CFOP                    number(10), 
  id_forma_pagamento         number(10), 
  id_modelo                  number(10), 
  id_nota_observacao         number(10), 
  id_plano_contas            number(10), 
  id_transportadora          number(10) NOT NULL, 
  numero                     varchar2(20), 
  serie                      varchar2(3), 
  status                     number(10), 
  tipo_entrada               varchar2(100), 
  tipo_emissao               varchar2(100), 
  chave_acesso               number(10), 
  protocolo_autorizacao      varchar2(99), 
  dt_emissao                 timestamp, 
  dt_entrada                 timestamp, 
  hora_saida                 timestamp, 
  base_calculo_ICMS          number(12, 2), 
  valor_ICMS                 number(12, 2), 
  base_calculo_ICMS_ST       number(12, 2), 
  valor_ICMS_ST              number(12, 2), 
  valor_frete                number(12, 2), 
  valor_seguro               number(12, 2), 
  valor_desconto             number(12, 2), 
  outras_despesas            number(12, 2), 
  valor_IPI                  number(12, 2), 
  valor_total_NF             number(12, 2), 
  valor_total_produtos       number(12, 2), 
  valor_total_servicos       number(12, 2), 
  base_calculo_ISSQN         number(12, 2), 
  valor_ISSQN                number(12, 2), 
  tipo_frete                 varchar2(100), 
  placa_frete                varchar2(8), 
  placa_UF_frete             varchar2(2), 
  quantidade_frete           number(3), 
  modalidade_frete           varchar2(1), 
  marca_frete                varchar2(60), 
  numero_frete               number(10), 
  peso_bruto                 number(15, 3), 
  peso_liquido               number(15, 3), 
  informacoes_complementares varchar2(255), 
  observacoes                varchar2(255), 
  codigo_numerico            number(8), 
  cod_municipio_gerador      varchar2(7), 
  impressao_danf             varchar2(1), 
  id_fornecedor              number(10), 
  id_pedido_compra           number(10), 
  id_empresa                 number(10) NOT NULL, 
  PRIMARY KEY (id_nota_entrada));
CREATE TABLE Planilha_Custo_Item (
  id_planilha_custos_item number(10) NOT NULL, 
  id_Planilha_custo       number(10) NOT NULL, 
  tipo                    varchar2(100), 
  grupo                   varchar2(100), 
  desricao                varchar2(255), 
  percentual              number(10), 
  valor                   number(10), 
  PRIMARY KEY (id_planilha_custos_item));
CREATE TABLE Planilha_Custos (
  id_Planilha_custo number(10) NOT NULL, 
  descricao         varchar2(255), 
  tipo              varchar2(100), 
  PRIMARY KEY (id_Planilha_custo));
CREATE TABLE Proposta_Item (
  id_proposta_item number(10) NOT NULL, 
  id_proposta      number(10), 
  tipo             varchar2(100), 
  descricao        varchar2(255), 
  quantidade       number(10), 
  sub_total        number(12, 2), 
  total            number(12, 2), 
  PRIMARY KEY (id_proposta_item));
CREATE TABLE Proposta (
  id_proposta    number(10) NOT NULL, 
  nome           varchar2(255), 
  status         varchar2(1), 
  dt_emissao     date, 
  dt_validade    date, 
  valor_desconto number(12, 2), 
  valor_total    number(12, 2), 
  observacao     varchar2(255), 
  introducao     varchar2(255), 
  id_empresa     number(10) NOT NULL, 
  PRIMARY KEY (id_proposta));
CREATE TABLE Perfil (
  id_perfil number(10) NOT NULL, 
  role      varchar2(20), 
  descricao varchar2(255), 
  PRIMARY KEY (id_perfil));
CREATE TABLE Usuario (
  id_usuario            number(10) NOT NULL, 
  id_perfil             number(10), 
  id_empresa            number(10) NOT NULL, 
  nome                  varchar2(255), 
  email                 varchar2(100), 
  login                 varchar2(20), 
  senha                 varchar2(100), 
  dt_cadastro           date, 
  dt_ultima_atualizacao date, 
  dt_ultimo_acesso      timestamp, 
  inativo               varchar2(1), 
  PRIMARY KEY (id_usuario));
CREATE TABLE CFOP (
  ID_CFOP   number(10) NOT NULL, 
  DESCRICAO varchar2(255), 
  ESTADO    varchar2(2), 
  CONSTRAINT SYS_C008176 
    PRIMARY KEY (ID_CFOP));
CREATE TABLE NCM (
  ID_NCM          number(10) NOT NULL, 
  DESCRICAO       varchar2(255), 
  NOMECLATURA_NCM varchar2(11), 
  CONSTRAINT SYS_C008178 
    PRIMARY KEY (ID_NCM));
CREATE TABLE Unidade (
  id_unidade         number(10) NOT NULL, 
  descricao          varchar2(255), 
  Descricao_Resumida varchar2(5), 
  PRIMARY KEY (id_unidade));
CREATE TABLE Servico (
  id_servico number(10) NOT NULL, 
  descricao  varchar2(255), 
  codigo     varchar2(10), 
  id_empresa number(10) NOT NULL, 
  PRIMARY KEY (id_servico));
CREATE TABLE Sub_Grupo (
  id_subgrupo number(10) NOT NULL, 
  descricao   varchar2(255), 
  id_grupo    number(10), 
  PRIMARY KEY (id_subgrupo));
CREATE TABLE Grupo (
  id_grupo  number(10) NOT NULL, 
  descricao varchar2(255), 
  PRIMARY KEY (id_grupo));
CREATE TABLE Produto (
  id_produto              number(10) NOT NULL, 
  id_NCM                  number(10), 
  id_unidade_entrada      number(10), 
  id_grupo                number(10), 
  descricao               varchar2(255), 
  dt_cadastro             date, 
  dt_ultima_atualizacao   timestamp, 
  inativo                 varchar2(1), 
  referencia_fabricante   varchar2(18), 
  descricao_reduzida      varchar2(100), 
  quantidade              number(10), 
  estoque_min             number(10), 
  estoque_max             number(10), 
  preco_custo             number(12, 2), 
  preco_custo_anterior    number(12, 2), 
  preco_custo_inicial     number(12, 2), 
  preco_custo_promocional number(12, 2), 
  base_conversao          varchar2(1), 
  fator_conversao_entrada number(9, 2), 
  fator_conversao_saida   number(5, 2), 
  fracionado              varchar2(1), 
  tipo_tributacao         varchar2(1), 
  CST                     varchar2(3), 
  aliq_icms_entrada       number(4, 2), 
  aliq_icms_saida         number(4, 2), 
  base_calculo_icms       number(12, 2), 
  aliquota_ipi            number(4, 2), 
  EAN                     varchar2(14), 
  id_subgrupo             number(10), 
  id_empresa              number(10) NOT NULL, 
  id_unidade_saida        number(10), 
  aliquota_cofins         number(4, 2), 
  aliquota_pis            number(4, 2), 
  CST_PIS_ENTRADA         number(10), 
  CST_PIS_SAIDA           number(10), 
  CST_COFINS_ENTRADA      number(10), 
  CST_COFINS_SAIDA        number(10), 
  aliquota_pis_saida      number(4, 2), 
  aliquota_cofins_saida   number(4, 2), 
  CSOSN                   varchar2(3), 
  ID_LOCALIZACAO          number(10), 
  LOCALIZACAO_DESCRITA    varchar2(200), 
  PRIMARY KEY (id_produto));
CREATE TABLE Cargo (
  id_cargo                 number(10) NOT NULL, 
  descricao                varchar2(255), 
  CBO                      varchar2(20), 
  salario_base             number(12, 2), 
  adicional_insalubridade  number(12, 2), 
  adicional_periculosidade number(12, 2), 
  alimentacao              number(12, 2), 
  acumulo_funcao           number(12, 2), 
  vale_transporte          number(12, 2), 
  seguro_vida              number(12, 2), 
  id_empresa               number(10) NOT NULL, 
  PRIMARY KEY (id_cargo));
CREATE TABLE Cliente_Responsavel (
  id_cliente_responsavel number(10) NOT NULL, 
  id_cliente             number(10), 
  tipo                   varchar2(20), 
  nome                   varchar2(255), 
  CPF                    varchar2(14), 
  email                  varchar2(255), 
  CEP                    varchar2(9), 
  tipo_logradouro        varchar2(20), 
  logradouro             varchar2(255), 
  numero                 varchar2(5), 
  complemento            varchar2(255), 
  estado_civil           varchar2(15), 
  observacoes            varchar2(255), 
  PRIMARY KEY (id_cliente_responsavel));
CREATE TABLE Cliente (
  id_cliente               number(10) NOT NULL, 
  id_cidade                number(10), 
  nome                     varchar2(255), 
  CGCCPF                   varchar2(20), 
  RG                       varchar2(20), 
  IE                       varchar2(20), 
  IM                       varchar2(20), 
  email                    varchar2(255), 
  sexo                     varchar2(1), 
  dt_nascimento            date, 
  CEP                      varchar2(9), 
  tipo_logradouro          varchar2(20), 
  logradouro               varchar2(255), 
  numero                   varchar2(5), 
  bairro                   varchar2(255), 
  complemento              varchar2(255), 
  CEP_cobranca             varchar2(9), 
  tipo_logradouro_cobranca varchar2(20), 
  logradouro_cobranca      varchar2(255), 
  numero_cobranca          varchar2(5), 
  bairro_cobranca          varchar2(255), 
  complemento_cobranca     varchar2(255), 
  tipo                     varchar2(1), 
  home_page                varchar2(255), 
  dt_cadastro              date, 
  dt_ultima_atualizacao    date, 
  dt_inicio_contrato       date, 
  inativo                  varchar2(1), 
  contato                  varchar2(255), 
  observacoes              varchar2(255), 
  bloqueado                varchar2(1), 
  codigo_gps               varchar2(20), 
  tributacao               varchar2(200), 
  id_cidade_cobranca       number(10), 
  id_empresa               number(10) NOT NULL, 
  PRIMARY KEY (id_cliente));
CREATE TABLE Fornecedor (
  id_fornecedor   number(10) NOT NULL, 
  nome            varchar2(255), 
  CGCCPF          varchar2(20), 
  tipo_fornecedor varchar2(1), 
  CEP             varchar2(9), 
  tipo_logradouro varchar2(20), 
  logradouro      varchar2(200), 
  numero          varchar2(5), 
  bairro          varchar2(255), 
  complemento     varchar2(255), 
  email           varchar2(255), 
  home_page       varchar2(255), 
  dt_cadastro     date, 
  observacao      varchar2(255), 
  inativo         varchar2(1), 
  codigo_gps      varchar2(20), 
  num_juridico    varchar2(20), 
  id_empresa      number(10) NOT NULL, 
  id_cidade       number(10), 
  PRIMARY KEY (id_fornecedor));
CREATE TABLE Estado (
  id_estado  number(10) NOT NULL, 
  nome       varchar2(255), 
  sigla      varchar2(2) UNIQUE, 
  regiao     varchar2(20), 
  sub_regiao varchar2(100), 
  PRIMARY KEY (id_estado));
CREATE TABLE Cidade (
  id_cidade   number(10) NOT NULL, 
  id_estado   number(10), 
  nome        varchar2(255), 
  codigo_IBGE varchar2(8), 
  CEP         varchar2(20), 
  PRIMARY KEY (id_cidade));
CREATE TABLE Telefone (
  id_telefone   number(10) NOT NULL, 
  numero        varchar2(15), 
  operadora     varchar2(100), 
  id_cliente    number(10), 
  id_fornecedor number(10), 
  PRIMARY KEY (id_telefone));
CREATE TABLE Empresa (
  id_empresa          number(10) NOT NULL, 
  id_cidade           number(10), 
  id_estado           number(10), 
  CNPJ                varchar2(17), 
  nome_fantasia       varchar2(255), 
  razao_social        varchar2(255), 
  CEP                 varchar2(9), 
  tipo_logradouro     varchar2(20), 
  logradouro          varchar2(255), 
  numero              varchar2(5), 
  bairro              varchar2(255), 
  complemento         varchar2(255), 
  email               varchar2(255), 
  contato             varchar2(255), 
  inativo             varchar2(1), 
  IE                  varchar2(20), 
  CNAE                varchar2(7), 
  nome_contador       varchar2(255), 
  CRC_contador        varchar2(20), 
  junta_comercial     varchar2(20), 
  IM                  varchar2(20), 
  NIRE                varchar2(20), 
  observacao          varchar2(255), 
  inscricao_suframa   number(10), 
  num_juridico        varchar2(20), 
  Caixa_Inicial_Valor number(10, 2), 
  PRIMARY KEY (id_empresa));
ALTER TABLE Movimentacao_Bancaria ADD CONSTRAINT FKMovimentac743588 FOREIGN KEY (id_conta_corrente) REFERENCES Conta_Corrente (id_conta_corrente);
ALTER TABLE Acesso ADD CONSTRAINT FKAcesso500032 FOREIGN KEY (id_perfil) REFERENCES Perfil (id_perfil);
ALTER TABLE Contrato ADD CONSTRAINT FKContrato112395 FOREIGN KEY (ID_Contrato_Aditivo) REFERENCES Contrato (id_contrato);
ALTER TABLE Centro_Custo_Variavel ADD CONSTRAINT FKCentro_Cus572819 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente);
ALTER TABLE Centro_Custo ADD CONSTRAINT FKCentro_Cus664334 FOREIGN KEY (id_fornecedor) REFERENCES Fornecedor (id_fornecedor);
ALTER TABLE Produto ADD CONSTRAINT FKProduto730682 FOREIGN KEY (ID_LOCALIZACAO) REFERENCES Localizacao (ID_LOCALIZACAO);
ALTER TABLE Movimentacao_Bancaria ADD CONSTRAINT FKMovimentac743589 FOREIGN KEY (id_conta_corrente) REFERENCES Conta_Corrente (id_conta_corrente);
ALTER TABLE Ordem_Compra_Item ADD CONSTRAINT FKOrdem_Comp811482 FOREIGN KEY (id_produto) REFERENCES Produto (id_produto);
ALTER TABLE Ordem_Compra_Item ADD CONSTRAINT FKOrdem_Comp783266 FOREIGN KEY (id_requisicao_material_item) REFERENCES Requisicao_Material_item (id_requisicao_material_item);
ALTER TABLE Movimentacao_Bancaria ADD CONSTRAINT FKMovimentac394382 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Movimentacao_Bancaria ADD CONSTRAINT FKMovimentac564322 FOREIGN KEY (id_banco) REFERENCES Banco (id_banco);
ALTER TABLE Centro_Custo_Variavel ADD CONSTRAINT FKCentro_Cus851461 FOREIGN KEY (id_plano_contas) REFERENCES Plano_Contas (id_plano_contas);
ALTER TABLE Centro_Custo_Variavel ADD CONSTRAINT FKCentro_Cus676942 FOREIGN KEY (id_caixa_item) REFERENCES Caixa_Item (id_caixa_item);
ALTER TABLE Centro_Custo_Variavel ADD CONSTRAINT FKCentro_Cus697570 FOREIGN KEY (id_nota_entrada) REFERENCES Nota_entrada (id_nota_entrada);
ALTER TABLE Centro_Custo_Variavel ADD CONSTRAINT FKCentro_Cus87269 FOREIGN KEY (id_nota_saida) REFERENCES Nota_Saida (id_nota_saida);
ALTER TABLE Centro_Custo_Variavel ADD CONSTRAINT FKCentro_Cus284872 FOREIGN KEY (id_contas_receber) REFERENCES Contas_Receber (id_contas_receber);
ALTER TABLE Centro_Custo_Variavel ADD CONSTRAINT FKCentro_Cus947639 FOREIGN KEY (id_contas_pagar) REFERENCES Contas_Pagar (id_contas_pagar);
ALTER TABLE Contas_Receber ADD CONSTRAINT FKContas_Rec200801 FOREIGN KEY (id_centro_custo_variavel) REFERENCES Centro_Custo_Variavel (id_centro_custo_variavel);
ALTER TABLE Centro_Custo_Variavel ADD CONSTRAINT FKCentro_Cus666916 FOREIGN KEY (id_centro_custo) REFERENCES Centro_Custo (id_centro_custo);
ALTER TABLE Plano_Contas ADD CONSTRAINT FKPlano_Cont591988 FOREIGN KEY (id_agrupamento) REFERENCES Agrupamento (id_agrupamento);
ALTER TABLE Plano_Contas ADD CONSTRAINT FKPlano_Cont388635 FOREIGN KEY (id_contaDRE) REFERENCES contas_DRE (id_contaDRE);
ALTER TABLE Mensagem ADD CONSTRAINT FKMensagem443202 FOREIGN KEY (id_usuario_receiver) REFERENCES Usuario (id_usuario);
ALTER TABLE Mensagem ADD CONSTRAINT FKMensagem127800 FOREIGN KEY (id_usuario_sender) REFERENCES Usuario (id_usuario);
ALTER TABLE Contas_Receber ADD CONSTRAINT FKContas_Rec867318 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente);
ALTER TABLE Contas_Pagar ADD CONSTRAINT FKContas_Pag459482 FOREIGN KEY (id_fornecedor) REFERENCES Fornecedor (id_fornecedor);
ALTER TABLE Contas_Receber ADD CONSTRAINT FKContas_Rec735197 FOREIGN KEY (id_forma_pagamento) REFERENCES Forma_Pagamento (id_forma_pagamento);
ALTER TABLE Contas_Pagar ADD CONSTRAINT FKContas_Pag475941 FOREIGN KEY (id_forma_pagamento) REFERENCES Forma_Pagamento (id_forma_pagamento);
ALTER TABLE Pedido_Compra ADD CONSTRAINT FKPedido_Com721962 FOREIGN KEY (id_fornecedor) REFERENCES Fornecedor (id_fornecedor);
ALTER TABLE Cotacao ADD CONSTRAINT FKCotacao854533 FOREIGN KEY (id_fornecedor) REFERENCES Fornecedor (id_fornecedor);
ALTER TABLE Ordem_Compra ADD CONSTRAINT FKOrdem_Comp778082 FOREIGN KEY (id_fornecedor) REFERENCES Fornecedor (id_fornecedor);
ALTER TABLE Contas_Receber ADD CONSTRAINT FKContas_Rec411323 FOREIGN KEY (id_plano_contas) REFERENCES Plano_Contas (id_plano_contas);
ALTER TABLE Contas_Pagar ADD CONSTRAINT FKContas_Pag799815 FOREIGN KEY (id_plano_contas) REFERENCES Plano_Contas (id_plano_contas);
ALTER TABLE Fornecedor ADD CONSTRAINT FKFornecedor571917 FOREIGN KEY (id_cidade) REFERENCES Cidade (id_cidade);
ALTER TABLE Produto ADD CONSTRAINT FKProduto948521 FOREIGN KEY (id_unidade_saida) REFERENCES Unidade (id_unidade);
ALTER TABLE Pedido_Compra ADD CONSTRAINT FKPedido_Com786785 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Cotacao_Item ADD CONSTRAINT FKCotacao_It117113 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Ordem_Compra ADD CONSTRAINT FKOrdem_Comp258331 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Transportadora ADD CONSTRAINT FKTransporta234325 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Requisicao_Material ADD CONSTRAINT FKRequisicao176983 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Contas_Receber ADD CONSTRAINT FKContas_Rec605798 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Contas_Pagar ADD CONSTRAINT FKContas_Pag394659 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Cartao ADD CONSTRAINT FKCartao877858 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Conta_Corrente ADD CONSTRAINT FKConta_Corr754890 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Cheque ADD CONSTRAINT FKCheque952955 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Banco ADD CONSTRAINT FKBanco418507 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Caixa ADD CONSTRAINT FKCaixa499153 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Cargo ADD CONSTRAINT FKCargo491017 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Local_trabalho ADD CONSTRAINT FKLocal_trab250657 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Proposta ADD CONSTRAINT FKProposta402772 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Negocio ADD CONSTRAINT FKNegocio516523 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Nota_entrada ADD CONSTRAINT FKNota_entra467910 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Servico ADD CONSTRAINT FKServico440633 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Nota_Saida ADD CONSTRAINT FKNota_Saida961027 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Nota_observacao ADD CONSTRAINT FKNota_obser478591 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Plano_Contas ADD CONSTRAINT FKPlano_Cont606995 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Produto ADD CONSTRAINT FKProduto188482 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Comunicado_Encerramento ADD CONSTRAINT FKComunicado256461 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Categoria ADD CONSTRAINT FKCategoria445726 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Contrato ADD CONSTRAINT FKContrato298598 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Colaborador ADD CONSTRAINT FKColaborado260212 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Setor ADD CONSTRAINT FKSetor593329 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Usuario ADD CONSTRAINT FKUsuario559516 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Cliente ADD CONSTRAINT FKCliente873744 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Fornecedor ADD CONSTRAINT FKFornecedor728838 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Cliente ADD CONSTRAINT FKCliente448042 FOREIGN KEY (id_cidade_cobranca) REFERENCES Cidade (id_cidade);
ALTER TABLE Telefone ADD CONSTRAINT FKTelefone271741 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente) ON DELETE Cascade;
ALTER TABLE Telefone ADD CONSTRAINT FKTelefone809681 FOREIGN KEY (id_fornecedor) REFERENCES Fornecedor (id_fornecedor) ON DELETE Cascade;
ALTER TABLE Colaborador ADD CONSTRAINT FKColaborado277140 FOREIGN KEY (id_setor) REFERENCES Setor (id_setor);
ALTER TABLE Usuario ADD CONSTRAINT FKUsuario833184 FOREIGN KEY (id_perfil) REFERENCES Perfil (id_perfil);
ALTER TABLE Produto ADD CONSTRAINT FKProduto532613 FOREIGN KEY (id_subgrupo) REFERENCES Sub_Grupo (id_subgrupo);
ALTER TABLE Produto ADD CONSTRAINT FKProduto163784 FOREIGN KEY (id_grupo) REFERENCES Grupo (id_grupo);
ALTER TABLE Sub_Grupo ADD CONSTRAINT FKSub_Grupo364878 FOREIGN KEY (id_grupo) REFERENCES Grupo (id_grupo);
ALTER TABLE Caixa_Item ADD CONSTRAINT FKCaixa_Item223632 FOREIGN KEY (id_caixa) REFERENCES Caixa (id_caixa);
ALTER TABLE Cotacao_Item ADD CONSTRAINT FKCotacao_It805320 FOREIGN KEY (id_produto) REFERENCES Produto (id_produto);
ALTER TABLE Nota_entrada ADD CONSTRAINT FKNota_entra209806 FOREIGN KEY (id_pedido_compra) REFERENCES Pedido_Compra (id_pedido_compra);
ALTER TABLE Pedido_Compra ADD CONSTRAINT FKPedido_Com428724 FOREIGN KEY (id_cotacao_item) REFERENCES Cotacao_Item (id_cotacao_item);
ALTER TABLE Cotacao ADD CONSTRAINT FKCotacao57563 FOREIGN KEY (id_ordem_compra) REFERENCES Ordem_Compra (id_ordem_compra);
ALTER TABLE Contrato_Negocio ADD CONSTRAINT FKContrato_N42795 FOREIGN KEY (id_negocio) REFERENCES Negocio (id_negocio);
ALTER TABLE Contrato_Negocio ADD CONSTRAINT FKContrato_N864326 FOREIGN KEY (id_contrato) REFERENCES Contrato (id_contrato);
ALTER TABLE Comunicado_Enc_Item ADD CONSTRAINT FKComunicado932900 FOREIGN KEY (id_local_trabalho) REFERENCES Local_trabalho (id_local_trabalho);
ALTER TABLE Colaborador ADD CONSTRAINT FKColaborado641515 FOREIGN KEY (id_local_trabalho) REFERENCES Local_trabalho (id_local_trabalho);
ALTER TABLE Comunicado_Enc_Item ADD CONSTRAINT FKComunicado120034 FOREIGN KEY (id_setor) REFERENCES Setor (id_setor);
ALTER TABLE Comunicado_Enc_Item ADD CONSTRAINT FKComunicado7553 FOREIGN KEY (id_servico_resumido) REFERENCES Servico_Resumido (id_servico_resumido);
ALTER TABLE Comunicado_Enc_Item ADD CONSTRAINT FKComunicado426970 FOREIGN KEY (id_cargo) REFERENCES Cargo (id_cargo);
ALTER TABLE Comunicado_Encerramento ADD CONSTRAINT FKComunicado362235 FOREIGN KEY (id_local_trabalho) REFERENCES Local_trabalho (id_local_trabalho);
ALTER TABLE Comunicado_Impl_Item ADD CONSTRAINT FKComunicado302763 FOREIGN KEY (id_setor) REFERENCES Setor (id_setor);
ALTER TABLE Comunicado_Enc_Financeiro ADD CONSTRAINT FKComunicado316727 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente);
ALTER TABLE Comunicado_Encerramento ADD CONSTRAINT FKComunicado216656 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente);
ALTER TABLE Comunicado_Impl_Financeiro ADD CONSTRAINT FKComunicado599435 FOREIGN KEY (id_usuario) REFERENCES Usuario (id_usuario);
ALTER TABLE Comunicado_Impl_Financeiro ADD CONSTRAINT FKComunicado72371 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente);
ALTER TABLE Arquivo ADD CONSTRAINT FKArquivo952118 FOREIGN KEY (id_contrato) REFERENCES Contrato (id_contrato);
ALTER TABLE Cotacao_item_historico ADD CONSTRAINT FKCotacao_it432784 FOREIGN KEY (id_produto) REFERENCES Produto (id_produto);
ALTER TABLE Cotacao_item_historico ADD CONSTRAINT FKCotacao_it599875 FOREIGN KEY (id_fornecedor_selecionado) REFERENCES Fornecedor (id_fornecedor);
ALTER TABLE Cotacao_Item ADD CONSTRAINT FKCotacao_It842332 FOREIGN KEY (id_cotacao) REFERENCES Cotacao (id_cotacao);
ALTER TABLE Produto_Fornecedor ADD CONSTRAINT FKProduto_Fo272158 FOREIGN KEY (id_fornecedor) REFERENCES Fornecedor (id_fornecedor);
ALTER TABLE Produto_Fornecedor ADD CONSTRAINT FKProduto_Fo519127 FOREIGN KEY (id_produto) REFERENCES Produto (id_produto) ON DELETE Cascade;
ALTER TABLE Cotacao_item_historico ADD CONSTRAINT FKCotacao_it320246 FOREIGN KEY (id_fornecedor) REFERENCES Fornecedor (id_fornecedor);
ALTER TABLE Cotacao_item_historico ADD CONSTRAINT FKCotacao_it469796 FOREIGN KEY (id_cotacao) REFERENCES Cotacao (id_cotacao);
ALTER TABLE Nota_entrada ADD CONSTRAINT FKNota_entra403087 FOREIGN KEY (id_fornecedor) REFERENCES Fornecedor (id_fornecedor);
ALTER TABLE Nota_Saida ADD CONSTRAINT FKNota_Saida25851 FOREIGN KEY (id_fornecedor) REFERENCES Fornecedor (id_fornecedor);
ALTER TABLE Cotacao ADD CONSTRAINT FKCotacao988399 FOREIGN KEY (id_usuario) REFERENCES Usuario (id_usuario);
ALTER TABLE Ordem_Compra_Item ADD CONSTRAINT FKOrdem_Comp347583 FOREIGN KEY (id_ordem_compra) REFERENCES Ordem_Compra (id_ordem_compra);
ALTER TABLE Ordem_Compra ADD CONSTRAINT FKOrdem_Comp59642 FOREIGN KEY (id_usuario) REFERENCES Usuario (id_usuario);
ALTER TABLE Ordem_Compra ADD CONSTRAINT FKOrdem_Comp847763 FOREIGN KEY (id_requisicao_material) REFERENCES Requisicao_Material (id_requisicao_material);
ALTER TABLE Pedido_Compra_Item ADD CONSTRAINT FKPedido_Com510361 FOREIGN KEY (id_pedido_compra) REFERENCES Pedido_Compra (id_pedido_compra);
ALTER TABLE Nota_entrada ADD CONSTRAINT FKNota_entra933979 FOREIGN KEY (id_transportadora) REFERENCES Transportadora (id_transportadora);
ALTER TABLE Requisicao_Material_item ADD CONSTRAINT FKRequisicao460104 FOREIGN KEY (id_produto) REFERENCES Produto (id_produto);
ALTER TABLE Requisicao_Material ADD CONSTRAINT FKRequisicao703865 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente);
ALTER TABLE Requisicao_Material_item ADD CONSTRAINT FKRequisicao558879 FOREIGN KEY (id_requisicao_material) REFERENCES Requisicao_Material (id_requisicao_material);
ALTER TABLE Requisicao_Material ADD CONSTRAINT FKRequisicao375672 FOREIGN KEY (id_usuario) REFERENCES Usuario (id_usuario);
ALTER TABLE Requisicao_Material ADD CONSTRAINT FKRequisicao441713 FOREIGN KEY (id_local_trabalho) REFERENCES Local_trabalho (id_local_trabalho);
ALTER TABLE Comunicado_Implantacao ADD CONSTRAINT FKComunicado142486 FOREIGN KEY (id_local_trabalho) REFERENCES Local_trabalho (id_local_trabalho);
ALTER TABLE Comunicado_Implantacao ADD CONSTRAINT FKComunicado24307 FOREIGN KEY (id_contrato) REFERENCES Contrato (id_contrato);
ALTER TABLE Nota_entrada ADD CONSTRAINT FKNota_entra309205 FOREIGN KEY (id_plano_contas) REFERENCES Plano_Contas (id_plano_contas);
ALTER TABLE Nota_Saida ADD CONSTRAINT FKNota_Saida233447 FOREIGN KEY (id_plano_contas) REFERENCES Plano_Contas (id_plano_contas);
ALTER TABLE Movimentacao_Bancaria ADD CONSTRAINT FKMovimentac642968 FOREIGN KEY (id_cheque) REFERENCES Cheque (id_cheque);
ALTER TABLE Caixa_Item ADD CONSTRAINT FKCaixa_Item544216 FOREIGN KEY (id_cheque) REFERENCES Cheque (id_cheque);
ALTER TABLE Caixa_Item ADD CONSTRAINT FKCaixa_Item852352 FOREIGN KEY (id_contas_receber) REFERENCES Contas_Receber (id_contas_receber);
ALTER TABLE Caixa_Item ADD CONSTRAINT FKCaixa_Item515120 FOREIGN KEY (id_contas_pagar) REFERENCES Contas_Pagar (id_contas_pagar);
ALTER TABLE Movimentacao_Bancaria ADD CONSTRAINT FKMovimentac416368 FOREIGN KEY (id_contas_pagar) REFERENCES Contas_Pagar (id_contas_pagar);
ALTER TABLE Movimentacao_Bancaria ADD CONSTRAINT FKMovimentac753600 FOREIGN KEY (id_contas_receber) REFERENCES Contas_Receber (id_contas_receber);
ALTER TABLE Conta_Corrente ADD CONSTRAINT FKConta_Corr257995 FOREIGN KEY (id_banco) REFERENCES Banco (id_banco);
ALTER TABLE Cheque ADD CONSTRAINT FKCheque90927 FOREIGN KEY (id_conta_corrente) REFERENCES Conta_Corrente (id_conta_corrente);
ALTER TABLE Contas_Receber ADD CONSTRAINT FKContas_Rec328441 FOREIGN KEY (id_cheque) REFERENCES Cheque (id_cheque);
ALTER TABLE Contas_Pagar ADD CONSTRAINT FKContas_Pag432010 FOREIGN KEY (id_cheque) REFERENCES Cheque (id_cheque);
ALTER TABLE Contas_Receber ADD CONSTRAINT FKContas_Rec444183 FOREIGN KEY (id_nota_saida) REFERENCES Nota_Saida (id_nota_saida);
ALTER TABLE Contas_Pagar ADD CONSTRAINT FKContas_Pag622742 FOREIGN KEY (id_nota_entrada) REFERENCES Nota_entrada (id_nota_entrada);
ALTER TABLE Cartao ADD CONSTRAINT FKCartao836563 FOREIGN KEY (id_banco) REFERENCES Banco (id_banco);
ALTER TABLE Comunicado_Impl_Item ADD CONSTRAINT FKComunicado190282 FOREIGN KEY (id_servico_resumido) REFERENCES Servico_Resumido (id_servico_resumido);
ALTER TABLE Cliente ADD CONSTRAINT FKCliente544579 FOREIGN KEY (id_cidade) REFERENCES Cidade (id_cidade);
ALTER TABLE Cliente_Responsavel ADD CONSTRAINT FKCliente_Re195604 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente);
ALTER TABLE Parametrizacao ADD CONSTRAINT FKParametriz282702 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Comunicado_Implantacao ADD CONSTRAINT FKComunicado674899 FOREIGN KEY (id_usuario) REFERENCES Usuario (id_usuario);
ALTER TABLE Comunicado_Encerramento ADD CONSTRAINT FKComunicado455150 FOREIGN KEY (id_usuario) REFERENCES Usuario (id_usuario);
ALTER TABLE Negocio ADD CONSTRAINT FKNegocio715212 FOREIGN KEY (id_usuario) REFERENCES Usuario (id_usuario);
ALTER TABLE Contrato ADD CONSTRAINT FKContrato474303 FOREIGN KEY (id_usuario) REFERENCES Usuario (id_usuario);
ALTER TABLE Colaborador ADD CONSTRAINT FKColaborado458901 FOREIGN KEY (id_usuario) REFERENCES Usuario (id_usuario);
ALTER TABLE Colaborador ADD CONSTRAINT FKColaborado970203 FOREIGN KEY (id_cargo) REFERENCES Cargo (id_cargo);
ALTER TABLE Comunicado_Impl_Financeiro ADD CONSTRAINT FKComunicado948842 FOREIGN KEY (id_contrato) REFERENCES Contrato (id_contrato);
ALTER TABLE Comunicado_Encerramento ADD CONSTRAINT FKComunicado804557 FOREIGN KEY (id_contrato) REFERENCES Contrato (id_contrato);
ALTER TABLE Comunicado_Enc_Financeiro ADD CONSTRAINT FKComunicado168261 FOREIGN KEY (id_comunicado_encerramento) REFERENCES Comunicado_Encerramento (id_comunicado_encerramento);
ALTER TABLE Comunicado_Impl_Financeiro ADD CONSTRAINT FKComunicado656126 FOREIGN KEY (id_comunicado_implantacao) REFERENCES Comunicado_Implantacao (id_comunicado_implantacao);
ALTER TABLE Comunicado_Impl_Item ADD CONSTRAINT FKComunicado609699 FOREIGN KEY (id_cargo) REFERENCES Cargo (id_cargo);
ALTER TABLE Contrato ADD CONSTRAINT FKContrato951391 FOREIGN KEY (id_categoria) REFERENCES Categoria (id_categoria);
ALTER TABLE Comunicado_Enc_Item ADD CONSTRAINT FKComunicado302333 FOREIGN KEY (id_comunicado_encerramento) REFERENCES Comunicado_Encerramento (id_comunicado_encerramento);
ALTER TABLE Comunicado_Impl_Item ADD CONSTRAINT FKComunicado783242 FOREIGN KEY (id_comunicado_implantacao) REFERENCES Comunicado_Implantacao (id_comunicado_implantacao);
ALTER TABLE Comunicado_Implantacao ADD CONSTRAINT FKComunicado996906 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente);
ALTER TABLE Negocio ADD CONSTRAINT FKNegocio43406 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente);
ALTER TABLE Negocio ADD CONSTRAINT FKNegocio64620 FOREIGN KEY (id_contrato) REFERENCES Contrato (id_contrato);
ALTER TABLE Contrato ADD CONSTRAINT FKContrato427997 FOREIGN KEY (id_forma_pagamento) REFERENCES Forma_Pagamento (id_forma_pagamento);
ALTER TABLE Contrato ADD CONSTRAINT FKContrato825480 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente);
ALTER TABLE Contrato ADD CONSTRAINT FKContrato159217 FOREIGN KEY (id_proposta) REFERENCES Proposta (id_proposta);
ALTER TABLE Nota_Saida ADD CONSTRAINT FKNota_Saida783014 FOREIGN KEY (id_servico) REFERENCES Servico (id_servico);
ALTER TABLE aliquota_servico ADD CONSTRAINT FKaliquota_s567779 FOREIGN KEY (id_servico) REFERENCES Servico (id_servico);
ALTER TABLE aliquota_servico ADD CONSTRAINT FKaliquota_s11066 FOREIGN KEY (id_cidade) REFERENCES Cidade (id_cidade);
ALTER TABLE Nota_entrada ADD CONSTRAINT FKNota_entra471119 FOREIGN KEY (id_nota_observacao) REFERENCES Nota_observacao (id_nota_observacao);
ALTER TABLE Nota_Saida ADD CONSTRAINT FKNota_Saida957818 FOREIGN KEY (id_nota_observacao) REFERENCES Nota_observacao (id_nota_observacao);
ALTER TABLE Centro_Custo ADD CONSTRAINT FKCentro_Cus126394 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente);
ALTER TABLE Nota_Saida_Item ADD CONSTRAINT FKNota_Saida284139 FOREIGN KEY (id_produto) REFERENCES Produto (id_produto);
ALTER TABLE Nota_Saida_Item ADD CONSTRAINT FKNota_Saida160859 FOREIGN KEY (id_nota_saida) REFERENCES Nota_Saida (id_nota_saida);
ALTER TABLE Nota_Saida ADD CONSTRAINT FKNota_Saida909572 FOREIGN KEY (id_forma_pagamento) REFERENCES Forma_Pagamento (id_forma_pagamento);
ALTER TABLE Nota_Saida ADD CONSTRAINT FKNota_Saida592076 FOREIGN KEY (id_CFOP) REFERENCES CFOP (ID_CFOP);
ALTER TABLE Nota_Saida ADD CONSTRAINT FKNota_Saida215686 FOREIGN KEY (id_modelo) REFERENCES Modelo_NFE (id_modelo);
ALTER TABLE Nota_Saida_Item ADD CONSTRAINT FKNota_Saida368195 FOREIGN KEY (id_CFOP) REFERENCES CFOP (ID_CFOP);
ALTER TABLE Nota_entrada_Item ADD CONSTRAINT FKNota_entra20649 FOREIGN KEY (id_CFOP) REFERENCES CFOP (ID_CFOP);
ALTER TABLE Produto ADD CONSTRAINT FKProduto198756 FOREIGN KEY (id_unidade_entrada) REFERENCES Unidade (id_unidade);
ALTER TABLE Produto ADD CONSTRAINT FKProduto724800 FOREIGN KEY (id_NCM) REFERENCES NCM (ID_NCM);
ALTER TABLE Nota_entrada_Item ADD CONSTRAINT FKNota_entra631685 FOREIGN KEY (id_produto) REFERENCES Produto (id_produto);
ALTER TABLE Nota_entrada_Item ADD CONSTRAINT FKNota_entra423614 FOREIGN KEY (id_nota_entrada) REFERENCES Nota_entrada (id_nota_entrada);
ALTER TABLE Cidade ADD CONSTRAINT FKCidade412353 FOREIGN KEY (id_estado) REFERENCES Estado (id_estado);
ALTER TABLE Empresa ADD CONSTRAINT FKEmpresa34707 FOREIGN KEY (id_estado) REFERENCES Estado (id_estado);
ALTER TABLE Empresa ADD CONSTRAINT FKEmpresa945441 FOREIGN KEY (id_cidade) REFERENCES Cidade (id_cidade);
ALTER TABLE Transportadora ADD CONSTRAINT FKTransporta347244 FOREIGN KEY (id_estado) REFERENCES Estado (id_estado);
ALTER TABLE Transportadora ADD CONSTRAINT FKTransporta436509 FOREIGN KEY (id_cidade) REFERENCES Cidade (id_cidade);
ALTER TABLE Nota_entrada ADD CONSTRAINT FKNota_entra326966 FOREIGN KEY (id_modelo) REFERENCES Modelo_NFE (id_modelo);
ALTER TABLE Nota_entrada ADD CONSTRAINT FKNota_entra633079 FOREIGN KEY (id_forma_pagamento) REFERENCES Forma_Pagamento (id_forma_pagamento);
ALTER TABLE Nota_entrada ADD CONSTRAINT FKNota_entra836861 FOREIGN KEY (id_CFOP) REFERENCES CFOP (ID_CFOP);
ALTER TABLE Planilha_Custo_Item ADD CONSTRAINT FKPlanilha_C570266 FOREIGN KEY (id_Planilha_custo) REFERENCES Planilha_Custos (id_Planilha_custo);
ALTER TABLE Proposta_Planilha ADD CONSTRAINT FKProposta_P343805 FOREIGN KEY (id_Planilha_custo) REFERENCES Planilha_Custos (id_Planilha_custo);
ALTER TABLE Proposta_Planilha ADD CONSTRAINT FKProposta_P743137 FOREIGN KEY (id_proposta_item) REFERENCES Proposta_Item (id_proposta_item);
ALTER TABLE Proposta_Item ADD CONSTRAINT FKProposta_I811884 FOREIGN KEY (id_proposta) REFERENCES Proposta (id_proposta);
CREATE SEQUENCE seq_Acesso;
CREATE SEQUENCE seq_Localizacao;
CREATE SEQUENCE seq_Centro_Custo_Variavel;
CREATE SEQUENCE seq_contas_DRE;
CREATE SEQUENCE seq_Agrupamento;
CREATE SEQUENCE seq_Mensagem;
CREATE SEQUENCE seq_Contrato_Negocio;
CREATE SEQUENCE seq_Setor;
CREATE SEQUENCE seq_Cotacao_Item;
CREATE SEQUENCE seq_Produto_Fornecedor;
CREATE SEQUENCE seq_Cotacao_item_historico;
CREATE SEQUENCE seq_Ordem_Compra_Item;
CREATE SEQUENCE seq_Ordem_Compra;
CREATE SEQUENCE seq_Cotacao;
CREATE SEQUENCE seq_Requisicao_Material_item;
CREATE SEQUENCE seq_Local_trabalho;
CREATE SEQUENCE seq_Requisicao_Material;
CREATE SEQUENCE seq_Colaborador;
CREATE SEQUENCE seq_Servico_Resumido;
CREATE SEQUENCE seq_Arquivo;
CREATE SEQUENCE seq_Categoria;
CREATE SEQUENCE seq_Comunicado_Enc_Financeiro;
CREATE SEQUENCE seq_Comunicado_Enc_Item;
CREATE SEQUENCE seq_Comunicado_Encerramento;
CREATE SEQUENCE seq_Comunicado_Impl_Financeiro;
CREATE SEQUENCE seq_Comunicado_Impl_Item;
CREATE SEQUENCE seq_Comunicado_Implantacao;
CREATE SEQUENCE seq_Negocio;
CREATE SEQUENCE seq_Contrato;
CREATE SEQUENCE seq_aliquota_servico;
CREATE SEQUENCE seq_Nota_observacao;
CREATE SEQUENCE seq_Centro_Custo;
CREATE SEQUENCE seq_Nota_Saida;
CREATE SEQUENCE seq_Modelo_NFE;
CREATE SEQUENCE seq_Parametrizacao;
CREATE SEQUENCE seq_Proposta_Planilha;
CREATE SEQUENCE seq_Plano_Contas;
CREATE SEQUENCE seq_Conta_Corrente;
CREATE SEQUENCE seq_Cheque;
CREATE SEQUENCE seq_Cartao;
CREATE SEQUENCE seq_Forma_Pagamento;
CREATE SEQUENCE seq_Contas_Receber;
CREATE SEQUENCE seq_Contas_Pagar;
CREATE SEQUENCE seq_Caixa_Item;
CREATE SEQUENCE seq_Caixa;
CREATE SEQUENCE seq_Banco;
CREATE SEQUENCE seq_Movimentacao_Bancaria;
CREATE SEQUENCE seq_Pedido_Compra_Item;
CREATE SEQUENCE seq_Pedido_Compra;
CREATE SEQUENCE seq_Transportadora;
CREATE SEQUENCE seq_Nota_Saida_Item;
CREATE SEQUENCE seq_Nota_entrada_Item;
CREATE SEQUENCE seq_Nota_entrada;
CREATE SEQUENCE seq_Planilha_Custo_Item;
CREATE SEQUENCE seq_Planilha_Custos;
CREATE SEQUENCE seq_Proposta_Item;
CREATE SEQUENCE seq_Proposta;
CREATE SEQUENCE seq_Perfil;
CREATE SEQUENCE seq_Usuario;
CREATE SEQUENCE seq_CFOP;
CREATE SEQUENCE seq_NCM;
CREATE SEQUENCE seq_Unidade;
CREATE SEQUENCE seq_Servico;
CREATE SEQUENCE seq_Sub_Grupo;
CREATE SEQUENCE seq_Grupo;
CREATE SEQUENCE seq_Produto;
CREATE SEQUENCE seq_Cargo;
CREATE SEQUENCE seq_Cliente_Responsavel;
CREATE SEQUENCE seq_Cliente;
CREATE SEQUENCE seq_Fornecedor;
CREATE SEQUENCE seq_Estado;
CREATE SEQUENCE seq_Cidade;
CREATE SEQUENCE seq_Telefone;
CREATE SEQUENCE seq_Empresa;

INSERT INTO EMPRESA (ID_EMPRESA, NOME_FANTASIA, RAZAO_SOCIAL) VALUES ('1', 'Empresa Demonstrativa', 'ED LTDA');
INSERT INTO PERFIL (ID_PERFIL, ROLE, DESCRICAO) VALUES ('1', 'ROLE_ADM', 'ADMINISTRADOR');
INSERT INTO USUARIO(ID_USUARIO, ID_PERFIL, ID_EMPRESA, NOME, LOGIN, SENHA, INATIVO) VALUES ('1', '1', '1', 'Renato Gonçalves', 'renato', '123', '1');
